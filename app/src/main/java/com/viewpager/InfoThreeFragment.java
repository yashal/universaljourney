package com.viewpager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.queppelin.universal.NewDashboardActivity;
import com.queppelin.universal.R;


/**
 * Created by amit on 14/1/16.
 */
public class InfoThreeFragment extends Fragment {
    private static final String KEY_CONTENT = "TestFragment:Content";

    Typeface font;
    View infolayout;
    public static InfoThreeFragment newInstance(String content) {
        InfoThreeFragment fragment = new InfoThreeFragment();



        return fragment;
    }

    private String mContent = "???";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        font = Typeface.createFromAsset(getActivity().getAssets(), "Nunito_Light.ttf");
        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        String title="THE REJUVENATOR";
        String dec="A short retreat is always a good idea to recharge & relax your senses. Wander solo to reach niravana!";

        layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        infolayout  = ((LayoutInflater)getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.last_info_layout, null, false);

        ((LinearLayout)infolayout.findViewById(R.id.lall)).setBackgroundResource(R.drawable.intro3);
        ((TextView)infolayout.findViewById(R.id.tv_title)).setText(title);
        ((TextView)infolayout.findViewById(R.id.tv_dec)).setText(dec);
        ((TextView)infolayout.findViewById(R.id.tv_title)).setTypeface(font);
        ((TextView)infolayout.findViewById(R.id.tv_dec)).setTypeface(font);
        ((TextView)infolayout.findViewById(R.id.tv_next)).setTypeface(font);
        ((TextView)infolayout.findViewById(R.id.tv_next)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(getActivity(),NewDashboardActivity.class);
                intent.putExtra("position",0);
                startActivity(intent);
                getActivity().finish();

            }
        });

        layout.addView(infolayout);

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }
}
