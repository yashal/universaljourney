package com.viewpager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.image.loader.ImageLoader;
import com.image.loader.ImageLoaderBanner;
import com.queppelin.universal.LoginActivity;
import com.queppelin.universal.R;
import com.queppelin.universal.RestClient;
import com.queppelin.universal.SignUpActivity;
import com.tool.HttpclasswithoutThrade;
import com.tool.StaticMethod;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by lenovo on 24-03-2016.
 */
public class BannerFragmenttwo extends Fragment {

    private static final String KEY_CONTENT = "TestFragment:Content";
    ImageLoaderBanner imageLoader;

    View infolayout;
    static int postion;
    private SharedPreferences mPrefs;

    Typeface font;

    public static BannerFragmenttwo newInstance(String content,int postionview) {
        BannerFragmenttwo fragment = new BannerFragmenttwo();
        postion=postionview;

        return fragment;
    }

    private String mContent = "???";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPrefs = getActivity().getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);
        imageLoader=new ImageLoaderBanner(getActivity());




        font = Typeface.createFromAsset(getActivity().getAssets(), "Nunito_Light.ttf");
        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        String title="ROMANCE BLOSSOMS";
        String dec="Craving some lone time with your beloved? Enjoy a romantic trip to fall in love all over again.";

        layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
        infolayout  = ((LayoutInflater)getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.banner_layout, null, false);


        try
        {


            if(StaticMethod.setting_flag.equalsIgnoreCase("N"))
            {
                new GetsettingOrignal(getActivity(),infolayout).execute();
            }


            String jarray= mPrefs.getString("AppSlideshow","");
            final JSONArray jArray=new JSONArray(jarray);
            imageLoader.DisplayImage(jArray.getJSONObject(postion).getString("image"),((ImageView)infolayout.findViewById(R.id.imgbanner)));

            String tag=jArray.getJSONObject(postion).getString("action");

            if(tag.equalsIgnoreCase("REGISTER"))
            {
                ((TextView)infolayout.findViewById(R.id.tvbutton)).setText("REGISTER");
            }else if(tag.equalsIgnoreCase("LOGIN"))
            {
                if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
                {

                    ((TextView)infolayout.findViewById(R.id.tvbutton)).setVisibility(View.GONE);
                }else
                {
                    ((TextView)infolayout.findViewById(R.id.tvbutton)).setText("LOGIN");
                }




            }else
            {
                ((TextView)infolayout.findViewById(R.id.tvbutton)).setVisibility(View.GONE);
            }


            ((TextView)infolayout.findViewById(R.id.tvbutton)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                    try
                    {


                            if(jArray.getJSONObject(postion).getString("action").equalsIgnoreCase("REGISTER"))
                            {
                                Intent intent=new Intent(getActivity(),SignUpActivity.class);
                                startActivity(intent);




                            }else if(jArray.getJSONObject(postion).getString("action").equalsIgnoreCase("LOGIN"))
                            {

                                Intent intent;
                                intent=new Intent(getActivity(), LoginActivity.class);
                                intent.putExtra("action_flag","N");
                                intent.putExtra("position",0);
                                startActivity(intent);


                            }

                    }catch (Exception e)
                    {

                    }


                }
            });



            ((TextView)infolayout.findViewById(R.id.tvbutton)).setTypeface(font);



        }catch (Exception e)
        {

        }

       // ((LinearLayout)infolayout.findViewById(R.id.lall)).setBackgroundResource(R.drawable.intro1);
       // ((TextView)infolayout.findViewById(R.id.tv_title)).setText(title);
       // ((TextView)infolayout.findViewById(R.id.tv_dec)).setText(dec);

        layout.addView(infolayout);

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }






    private class GetsettingOrignal extends AsyncTask<Void, Void, String[]>
    {

        String mgetThingsResponse;


        Context context;
        View infolayout;
        JSONArray jsonarray;

        public GetsettingOrignal(Context context,View infolayout) {
            super();
            this.context=context;
this.infolayout=infolayout;

        }
        @Override
        protected String[] doInBackground(Void... params) {

            String [] mStrings=null;

            try {

                String[] pro_key = new String[]{};
                String[] pro_value = new String[]{};
                mgetThingsResponse =(new HttpclasswithoutThrade(context,"getSettings", RestClient.RequestMethod.GET,pro_key,pro_value
                )).getResponsedetails();


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return mStrings;
        }
        @Override
        protected void onPostExecute(String[] result) {

            try
            {
                System.out.println("getSettings_firsttime:"+mgetThingsResponse);

                if(!mgetThingsResponse.equalsIgnoreCase(""))
                {
                    JSONObject object=new JSONObject(mgetThingsResponse);

                    String email=object.getString("admin_email");
                    String mobile=object.getString("admin_telephone");//admin_mobile
                    String mobile2=object.getString("admin_telephone_2");

                    SharedPreferences.Editor prefsEditor = mPrefs
                            .edit();
                    prefsEditor
                            .putString("admin_email",email);
                    prefsEditor.putString("admin_number", mobile);
                    prefsEditor.putString("admin_number2", mobile2);

                    prefsEditor.putString("experience_title", object.getString("experience_title"));
                    prefsEditor.putString("experience_description", object.getString("experience_description"));
                    prefsEditor.putString("ten_thing_title", object.getString("ten_thing_title"));
                    prefsEditor.putString("ten_thing_desc", object.getString("ten_thing_desc"));

                    prefsEditor.putString("bannerArray", object.getJSONArray("banner").toString());
                    prefsEditor.putString("AppSlideshow", object.getJSONArray("AppSlideshow").toString());

                    prefsEditor.commit();


                    StaticMethod.setting_flag="Y";


                    try
                    {
                        String jarray= mPrefs.getString("AppSlideshow","");
                        final JSONArray jArray=new JSONArray(jarray);
                        imageLoader.DisplayImage(jArray.getJSONObject(postion).getString("image"),((ImageView)infolayout.findViewById(R.id.imgbanner)));

                        String tag=jArray.getJSONObject(postion).getString("action");

                        if(tag.equalsIgnoreCase("REGISTER"))
                        {
                            ((TextView)infolayout.findViewById(R.id.tvbutton)).setText("REGISTER");
                        }else if(tag.equalsIgnoreCase("LOGIN"))
                        {
                            if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
                            {

                                ((TextView)infolayout.findViewById(R.id.tvbutton)).setVisibility(View.GONE);
                            }else
                            {
                                ((TextView)infolayout.findViewById(R.id.tvbutton)).setText("LOGIN");
                            }




                        }else
                        {
                            ((TextView)infolayout.findViewById(R.id.tvbutton)).setVisibility(View.GONE);
                        }


                        ((TextView)infolayout.findViewById(R.id.tvbutton)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {



                                try
                                {


                                    if(jArray.getJSONObject(postion).getString("action").equalsIgnoreCase("REGISTER"))
                                    {
                                        Intent intent=new Intent(getActivity(),SignUpActivity.class);
                                        startActivity(intent);




                                    }else if(jArray.getJSONObject(postion).getString("action").equalsIgnoreCase("LOGIN"))
                                    {

                                        Intent intent;
                                        intent=new Intent(getActivity(), LoginActivity.class);
                                        intent.putExtra("action_flag","N");
                                        intent.putExtra("position",0);
                                        startActivity(intent);


                                    }

                                }catch (Exception e)
                                {

                                }


                            }
                        });



                        ((TextView)infolayout.findViewById(R.id.tvbutton)).setTypeface(font);



                    }catch (Exception e)
                    {

                    }


                }




            }catch (Exception e)
            {


                StaticMethod.setting_flag="N";
                System.out.println("error"+e);

            }



            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
        }
    }



}
