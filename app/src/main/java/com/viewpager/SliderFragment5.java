package com.viewpager;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.image.loader.ImageLoader;
import com.queppelin.universal.R;

/**
 * Created by lenovo on 03-02-2016.
 */
public class SliderFragment5  extends Fragment {
    private static final String KEY_CONTENT = "TestFragment:Content";
    private SharedPreferences mPrefs;
    View infolayout;
    public static String position_vale;
    public static String bID;
    Typeface font;
    public static SliderFragment5 newInstance(String content,String position) {
        bID=content;
        position_vale=position;
        SliderFragment5 fragment = new SliderFragment5();



        return fragment;
    }

    private String mContent = "???";

    ImageLoader imageLoader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        font = Typeface.createFromAsset(getActivity().getAssets(), "Nunito_Light.ttf");
        mPrefs = getActivity().getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);
        imageLoader=new ImageLoader(getActivity());


        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);


        layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
        infolayout  = ((LayoutInflater)getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.image_slider_layout, null, false);

        ((TextView)infolayout.findViewById(R.id.tv_image_count)).setText(position_vale);
        ((TextView)infolayout.findViewById(R.id.tv_image_count)).setTypeface(font);
        imageLoader.DisplayImage(bID,((ImageView)infolayout.findViewById(R.id.img_banner)));



        layout.addView(infolayout);

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }










}

