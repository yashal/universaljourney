package com.viewpager;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.queppelin.universal.R;
import com.image.loader.ImageLoader;
import com.queppelin.universal.RestClient;
import com.tool.HttpClass;
import com.tool.HttpclasswithoutThrade;
import com.tool.StaticMethod;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by lenovo on 30-01-2016.
 */
public class Things2Fragment extends Fragment {
    private static final String KEY_CONTENT = "TestFragment:Content";

    View infolayout;
    private SharedPreferences mPrefs;
    public static String bID;

    public static int postion;
    public static ViewPager mPager;
    Typeface font;
    public static Things2Fragment newInstance(String content,int p,ViewPager mPagerv) {

        bID=content;
        mPager=mPagerv;
        postion=p;
        Things2Fragment fragment = new Things2Fragment();



        return fragment;
    }

    private String mContent = "???";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        font = Typeface.createFromAsset(getActivity().getAssets(), "Nunito_Light.ttf");
        mPrefs = getActivity().getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);


        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);


        layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
        infolayout  = ((LayoutInflater)getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.activity_things_details, null, false);



        ((ImageView)infolayout.findViewById(R.id.imgbackarrow)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mPager.setCurrentItem(postion-1);


              //  Toast.makeText(getActivity(), "back", Toast.LENGTH_LONG).show();


            }
        });

        ((ImageView)infolayout.findViewById(R.id.imgforword)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

          //      Toast.makeText(getActivity(),"forword",Toast.LENGTH_LONG).show();

                mPager.setCurrentItem(postion+1);



            }
        });






        new GetThings(getActivity(),bID,infolayout).execute();


        layout.addView(infolayout);

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }




    private class GetThings extends AsyncTask<Void, Void, String[]>
    {

        String mgetThingsResponse;
        Context context;

        JSONArray jsonarray;

        String blog_id;
        View view;

        public GetThings(Context context,String blog_id,View view) {
            super();
            this.context=context;
            this.blog_id=blog_id;
            this.view=view;


        }
        @Override
        protected String[] doInBackground(Void... params) {

            String [] mStrings=null;

            try {

                String[] pro_key = new String[]{"blog_id"};
                String[] pro_value = new String[]{blog_id};
                mgetThingsResponse =(new HttpclasswithoutThrade(context,"getBlog", RestClient.RequestMethod.POST,pro_key,pro_value
                )).getResponsedetails();



            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return mStrings;
        }
        @Override
        protected void onPostExecute(String[] result) {

            try
            {
                System.out.println("things:"+mgetThingsResponse);

                if(mgetThingsResponse.equalsIgnoreCase("")||mgetThingsResponse.equalsIgnoreCase("null"))
                {

                }else
                {
                    JSONObject object=new JSONObject(mgetThingsResponse);
                    ((LinearLayout)view.findViewById(R.id.all)).setVisibility(View.VISIBLE);
                    setLayout(view,object) ;
                    setSupport(view);


                }



            }catch (Exception e)
            {

            }



            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
        }
    }

    public void setLayout(View viewLayout,JSONObject jsonObject)
    {
        ImageLoader imageLoader=new ImageLoader(getActivity());


        try
        {

            ((TextView)viewLayout.findViewById(R.id.tv_thing_title)).setText(Html.fromHtml(jsonObject.getString("title")));
            ((TextView)viewLayout.findViewById(R.id.tv_thing_title)).setTypeface(font);
            ((TextView)viewLayout.findViewById(R.id.tv_dec)).setText(Html.fromHtml(jsonObject.getString("description")));
            ((TextView)viewLayout.findViewById(R.id.tv_dec)).setTypeface(font);
            imageLoader.DisplayImage(jsonObject.getString("image"),((ImageView)viewLayout.findViewById(R.id.img_banner)));



        }catch (Exception e)
        {

        }


    }

    public void setSupport(final View mainView)
    {

        ((EditText)mainView.findViewById(R.id.etbphone)).setTypeface(font);
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        final   String admin_number=mPrefs.getString("admin_number","").toString();
        ((TextView)mainView.findViewById(R.id.tvcallat)).setText("Call us at-"+admin_number);
        ((TextView)mainView.findViewById(R.id.tvcallat)).setTypeface(font);

        ((TextView)mainView.findViewById(R.id.tvcallat)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                Intent intent1 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + admin_number));
                startActivity(intent1);

            }
        });


        try {

            if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
            {

                String userObjet = mPrefs.getString("userObject", "");
                JSONObject userJson = new JSONObject(userObjet);

                JSONObject userinfo = null;

                if (userJson.has("user")) {

                    userinfo = userJson.getJSONObject("user");
                } else if (userJson.has("result")) {
                    userinfo = userJson.getJSONObject("result");
                }


                ((EditText) mainView.findViewById(R.id.etbphone)).setText(userinfo.getString("telephone"));


                if (userinfo.getString("telephone").length() >= 8 && userinfo.getString("telephone").length() <= 15) {

                    ((Button) mainView.findViewById(R.id.tvcallme)).setEnabled(true);
//                ((Button) mainView.findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_active));

                    ((Button)mainView.findViewById(R.id.tvcallme)).setTypeface(font);

                } else {

                    ((Button) mainView.findViewById(R.id.tvcallme)).setEnabled(false);
                    //((Button) mainView.findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_hide));
                    ((Button)mainView.findViewById(R.id.tvcallme)).setTypeface(font);
                }

            }else
            {
                ((EditText)mainView.findViewById(R.id.etbphone)).setText("");

                ((Button) mainView.findViewById(R.id.tvcallme)).setEnabled(false);
                //((Button) mainView.findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_hide));
                ((Button)mainView.findViewById(R.id.tvcallme)).setTypeface(font);




            }
        }catch (Exception e)
        {

        }

        ((EditText)mainView.findViewById(R.id.etbphone)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {


                if (editable.length() >= 8&&editable.length()<=15) {


                    //((Button)mainView.findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_active));
                    ((Button) mainView.findViewById(R.id.tvcallme)).setEnabled(true);
                    ((Button)mainView.findViewById(R.id.tvcallme)).setTypeface(font);
                } else{

                    ((Button) mainView.findViewById(R.id.tvcallme)).setEnabled(false);
                    // ((Button)mainView.findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_hide));
                    ((Button)mainView.findViewById(R.id.tvcallme)).setTypeface(font);
                }



            }
        });





        ((Button)mainView.findViewById(R.id.tvcallme)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try
                {

                    String getMobileNumber=((EditText)mainView.findViewById(R.id.etbphone)).getText().toString();
                    if (!getMobileNumber.equalsIgnoreCase("")) {

                        if (getMobileNumber.length() >= 8 && getMobileNumber.length() <= 15) {


                            getCallMe(getMobileNumber);
                            View view1 = getActivity().getCurrentFocus();
                            if (view1 != null) {
                                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view1.getWindowToken(), 0);
                            }

                        } else {


                        }

                    } else {

                    }                }catch (Exception e)
                {

                }



            }
        });


    }



    public void getCallMe(String number)
    {

        String[] pro_key = new String[]{"mobile","entity","entity_id","deviceType"};
        String[] pro_value = new String[]{number,"blog",bID,"ANDROID"};//user_id
        new HttpClass(getActivity(),"registerCall", RestClient.RequestMethod.POST,pro_key,pro_value
        ).start();
        StaticMethod.returnProgressBar(
                getActivity()).setOnDismissListener(
                new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        if (HttpClass.ResponseVector != null && !HttpClass.ResponseVector.equalsIgnoreCase("")) {

                            // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                            // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                            //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                            try {

                                System.out.println("registerCall"+HttpClass.ResponseVector);

                                JSONObject jsonObject=new JSONObject(HttpClass.ResponseVector);


                                String message="";

                                if(jsonObject.has("success"))
                                {
                                    message=jsonObject.getString("message");

                                }else if(jsonObject.has("error"))
                                {

                                    message=jsonObject.getString("Message");

                                }
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                        getActivity());
                                alertDialogBuilder
                                        .setMessage(message)
                                        .setCancelable(false)
                                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {



                                            }
                                        });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                                Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "Nunito_Light.ttf");
                                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                msgTxt.setTypeface(font);

                            } catch (Exception e) {
                                System.err.println(e);
                            }

                        }
                    }
                });



    }



}

