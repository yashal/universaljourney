package com.viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;


import com.queppelin.universal.R;

import org.json.JSONArray;

/**
 * Created by lenovo on 30-01-2016.
 */
public class ThingsFragmentAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
    protected static  String[] CONTENT;
    protected static final int[] ICONS = new int[] {
            R.drawable.ic_arrow_back_white_24dp,
            R.drawable.ic_arrow_back_white_24dp,
            R.drawable.ic_arrow_back_white_24dp,
            R.drawable.ic_arrow_back_white_24dp,
            R.drawable.ic_arrow_back_white_24dp,
            R.drawable.ic_arrow_back_white_24dp,
            R.drawable.ic_arrow_back_white_24dp,
            R.drawable.ic_arrow_back_white_24dp,
            R.drawable.ic_arrow_back_white_24dp,
            R.drawable.ic_arrow_back_white_24dp



    };

    private int mCount;

    String mJArray;
    ViewPager mPager;

    public ThingsFragmentAdapter(FragmentManager fm,String mJArray,ViewPager mPager)
    {
        super(fm);
       this.mJArray=mJArray;
       this.mPager=mPager;
        try
        {
            JSONArray ja=new JSONArray(mJArray);

            if(ja.length()>10){
                CONTENT=new String[10];
            }else
            {
                CONTENT=new String[ja.length()];
            }



            mCount=CONTENT.length;
            for(int i=0;i<ja.length();i++)
            {

                CONTENT[i]=ja.getJSONObject(i).getString("simple_blog_article_id");
            }



        }catch (Exception e)
        {

        }




    }

    @Override
    public Fragment getItem(int position) {

        if(position==0)
        {
            return Things1Fragment.newInstance(CONTENT[position],position,mPager);

        }else if(position==1)
        {
            return Things2Fragment.newInstance(CONTENT[position],position,mPager);

        }else if(position==2)
        {
            return Things3Fragment.newInstance(CONTENT[position],position,mPager);

        }else if(position==3)
        {
            return Things4Fragment.newInstance(CONTENT[position],position,mPager);
        }else if(position==4)
        {
            return Things5Fragment .newInstance(CONTENT[position],position,mPager);

        }else if(position==5)
        {
            return Things6Fragment .newInstance(CONTENT[position],position,mPager);

        }else if(position==6)
        {
            return Things7Fragment .newInstance(CONTENT[position],position,mPager);

        }else if(position==7)
        {

            return Things8Fragment .newInstance(CONTENT[position],position,mPager);
        }else if(position==8)
        {

            return Things9Fragment .newInstance(CONTENT[position],position,mPager);
        }else if(position==9)
        {

            return Things10Fragment .newInstance(CONTENT[position],position,mPager);
        }else
        {

            return InfoOneFragment .newInstance(CONTENT[position]);
        }


    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TestFragmentAdapter.CONTENT[position % CONTENT.length];
    }

    @Override
    public int getIconResId(int index) {
        return ICONS[index % ICONS.length];
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}