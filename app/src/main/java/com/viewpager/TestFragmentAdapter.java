package com.viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.queppelin.universal.R;


public class TestFragmentAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
    protected static final String[] CONTENT = new String[] { "", "", "" };
    protected static final int[] ICONS = new int[] {
            R.drawable.ic_arrow_back_white_24dp,
            R.drawable.ic_arrow_back_white_24dp,
            R.drawable.ic_arrow_back_white_24dp

    };

    private int mCount = CONTENT.length;

    public TestFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        if(position==0)
        {
            return InfoOneFragment.newInstance(CONTENT[position % CONTENT.length]);

        }else if(position==1)
        {
            return InfoTwoFragment.newInstance(CONTENT[position % CONTENT.length]);

        }else if(position==2)
        {
            return InfoThreeFragment.newInstance(CONTENT[position % CONTENT.length]);

        }else
        {
            return InfoOneFragment .newInstance(CONTENT[position % CONTENT.length]);
        }


    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return TestFragmentAdapter.CONTENT[position % CONTENT.length];
    }

    @Override
    public int getIconResId(int index) {
      return ICONS[index % ICONS.length];
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}