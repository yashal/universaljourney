package com.viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.queppelin.universal.R;

/**
 * Created by lenovo on 24-03-2016.
 */
public class BannerFragmentAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
    protected static final String[] CONTENT = new String[] { "", "" };
    protected static final int[] ICONS = new int[] {
            R.drawable.ic_arrow_back_white_24dp,
            R.drawable.ic_arrow_back_white_24dp


    };

    private int mCount = CONTENT.length;

    public BannerFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        if(position==0)
        {
            return BannerFragmentone.newInstance(CONTENT[position % CONTENT.length],position);

        }else if(position==1)
        {
            return BannerFragmenttwo.newInstance(CONTENT[position % CONTENT.length],position);

        }else
        {
            return BannerFragmentone .newInstance(CONTENT[position % CONTENT.length],position);
        }


    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TestFragmentAdapter.CONTENT[position % CONTENT.length];
    }

    @Override
    public int getIconResId(int index) {
        return ICONS[index % ICONS.length];
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}
