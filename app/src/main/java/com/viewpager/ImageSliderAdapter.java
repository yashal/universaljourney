package com.viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.queppelin.universal.R;

import org.json.JSONArray;

/**
 * Created by lenovo on 03-02-2016.
 */
public class ImageSliderAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
    protected static  String[] CONTENT;
    protected static  int[] ICONS;

    private int mCount;

    String mJArray;
    JSONArray ja;
    public ImageSliderAdapter(FragmentManager fm,String mJArray)
    {
        super(fm);
        this.mJArray=mJArray;

        try
        {
             ja=new JSONArray(mJArray);

            if(ja.length()>10)
            {
                CONTENT=new String[10];
                ICONS=new int[10];

            }else{

                CONTENT=new String[ja.length()];
                ICONS=new int[ja.length()];

            }



            mCount=CONTENT.length;
            for(int i=0;i<ja.length();i++)
            {

                CONTENT[i]=ja.getString(i);
                ICONS[i]= R.drawable.ic_termsandconditions;
            }



        }catch (Exception e)
        {

        }




    }

    @Override
    public Fragment getItem(int position) {

        String slide_number=(position+1)+"/"+ ja.length();
        if(position==0)
        {
            return SliderFragment.newInstance(CONTENT[position],slide_number);

        }else if(position==1)
        {
            return SliderFragment1.newInstance(CONTENT[position],slide_number);

        }else if(position==2)
        {
            return SliderFragment2.newInstance(CONTENT[position],slide_number);
        }else if(position==3)
        {
            return SliderFragment3.newInstance(CONTENT[position],slide_number);
        }else if(position==4)
        {
            return SliderFragment4.newInstance(CONTENT[position],slide_number);
        }else if(position==5)
        {
            return SliderFragment5.newInstance(CONTENT[position],slide_number);
        }else if(position==6)
        {
            return SliderFragment6.newInstance(CONTENT[position],slide_number);
        }else if(position==7)
        {
            return SliderFragment7.newInstance(CONTENT[position],slide_number);
        }else if(position==8)
        {
            return SliderFragment8.newInstance(CONTENT[position],slide_number);
        }else if(position==9)
        {
            return SliderFragment9.newInstance(CONTENT[position],slide_number);
        }else {
            return SliderFragment.newInstance(CONTENT[position],slide_number);
        }

    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TestFragmentAdapter.CONTENT[position % CONTENT.length];
    }

    @Override
    public int getIconResId(int index) {
        return ICONS[index % ICONS.length];
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}
