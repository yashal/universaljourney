package com.tool;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.queppelin.universal.ContactUsActivity;

import com.queppelin.universal.FeedBackActivity;
import com.queppelin.universal.LoginActivity;
import com.queppelin.universal.MyTripsActivity;
import com.queppelin.universal.NotificationListActivity;
import com.queppelin.universal.PrivacyPolicyActivity;
import com.queppelin.universal.R;
import com.queppelin.universal.TermAndCondition;


/**
 * Created by SYS128 on 10/19/2015.
 */
public class LeftMenuClickListener implements View.OnClickListener {


    Context context;
    DrawerLayout drawerLayout;
    private SharedPreferences mPrefs;

    public LeftMenuClickListener(Context context,DrawerLayout drawerLayout )
    {
        this.context=context;
        this.drawerLayout=drawerLayout;
        mPrefs = context.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);
    }

    @Override
    public void onClick(View v) {

        Intent intent;


        if((v.getId()) == 2)
        {
            drawerLayout.closeDrawers();

            intent=new Intent(context,NotificationListActivity.class);

            intent.putExtra("action_flag","N");
            context.startActivity(intent);



/*
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    context);
            alertDialogBuilder
                    .setMessage("There is no message available.")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {



                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            Typeface font = Typeface.createFromAsset(context.getAssets(), "Nunito_Light.ttf");
            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTypeface(font);

            */




        }
        if((v.getId()) == 3)
        {
            drawerLayout.closeDrawers();


            intent=new Intent(context, ContactUsActivity.class);
            intent.putExtra("position",0);
            context.startActivity(intent);



        }



        if((v.getId()) == 4)
        {
            drawerLayout.closeDrawers();



            if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
            {
                intent=new Intent(context, FeedBackActivity.class);
                intent.putExtra("position",0);
                context.startActivity(intent);


            }else
            {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set title


                // set dialog message
                alertDialogBuilder
                        .setMessage("Please login to share your feedback.")
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                Intent intent=new Intent(context,LoginActivity.class);
                                intent.putExtra("action_flag","N");
                                context.startActivity(intent);

                            }
                        })
                        .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();



                Typeface font = Typeface.createFromAsset(context.getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);



            }










        }

/*
        if((v.getId()) == 3)
        {

            drawerLayout.closeDrawers();
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    context);

            // set title


            // set dialog message
            alertDialogBuilder
                    .setMessage("Website will be opened in External Browser?")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity
                            //MainActivity.this.finish();
                           // drawerLayout.closeDrawers();




                            Intent intent = new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://www.umustakeholidays.com/user-stories"));
                            context.startActivity(intent);



                        }
                    })
                    .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }
*/
        if((v.getId()) == 6)
        {
            drawerLayout.closeDrawers();

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    context);

            // set title


            // set dialog message
            alertDialogBuilder
                    .setMessage("Website will be opened in External Browser?")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity
                            //MainActivity.this.finish();



                            Intent intent = new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://www.universaljourneys.com/accreditations"));
                            context.startActivity(intent);

                        }
                    })
                    .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();


            Typeface font = Typeface.createFromAsset(context.getAssets(), "Nunito_Light.ttf");
            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTypeface(font);
        }



        if((v.getId()) == 8)
        {
            drawerLayout.closeDrawers();
            intent=new Intent(context, PrivacyPolicyActivity.class);
            intent.putExtra("position",0);
            context.startActivity(intent);
        }


        if((v.getId()) == 7)
        {
            drawerLayout.closeDrawers();
            intent=new Intent(context, TermAndCondition.class);
            intent.putExtra("position",0);
            context.startActivity(intent);
        }

        if((v.getId())==0)
        {

            drawerLayout.closeDrawers();

            if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
            {


                intent=new Intent(context, MyTripsActivity.class);
                intent.putExtra("customer_group_id","");
                intent.putExtra("action_flag","N");

                intent.putExtra("position",0);
                context.startActivity(intent);

            }else
            {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set title


                // set dialog message
                alertDialogBuilder
                        .setMessage("Please login to visit your trips section.")
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, close
                                // current activity
                                //MainActivity.this.finish();
                                // drawerLayout.closeDrawers();

                                Intent intent=new Intent(context,LoginActivity.class);
                                intent.putExtra("action_flag","N");
                                context.startActivity(intent);

                            }
                        })
                        .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();



                Typeface font = Typeface.createFromAsset(context.getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);

            }






        }


        if((v.getId()) == 1)
        {
            drawerLayout.closeDrawers();


            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    context);

            // set title
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();

            View parent_view=inflater.inflate(R.layout.contact_us_option, null);

            // set dialog message
            alertDialogBuilder
                    .setTitle("Please contact us below options-")
                    .setCancelable(false)

                    .setView(parent_view)
                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // if this buttoPleasen is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });



            // create alert dialog
           final AlertDialog alertDialog = alertDialogBuilder.create();






            ((RelativeLayout)parent_view.findViewById(R.id.rmailus)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    alertDialog.dismiss();

                    String adminEmail=mPrefs.getString("admin_email","").toString();

                    String[] TO = {adminEmail};

                    Intent emailIntent = new Intent(Intent.ACTION_SEND);

                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.setType("text/plain");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);

                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Support");
                    //emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

                    try {
                        context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));

                    }
                    catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(context, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                    }

                }
            });


            ((RelativeLayout)parent_view.findViewById(R.id.rcallus)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String admin_number=mPrefs.getString("admin_number","").toString();

                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + admin_number));
                    context.startActivity(intent);
                    alertDialog.dismiss();

                }
            });

            // show it
            alertDialog.show();




            Typeface font = Typeface.createFromAsset(context.getAssets(), "Nunito_Light.ttf");
            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTypeface(font);





        }
        if((v.getId()) == 5)
        {

            drawerLayout.closeDrawers();
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    context);

            // set title


            // set dialog message
            alertDialogBuilder
                    .setTitle("Rate this app")
                    .setMessage("We hope that you are satisfied with our services. Would you like to review this app in the App Store now? ")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity
                            //MainActivity.this.finish();
                            // drawerLayout.closeDrawers();


                            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.queppelin.universal")));





                        }
                    })
                    .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();



            Typeface font = Typeface.createFromAsset(context.getAssets(), "Nunito_Light.ttf");
            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTypeface(font);


        }



/*
        if ((v.getId()) == 3) {

            intent=new Intent(context, PracticAssessmentListActivity.class);
            context.startActivity(intent);

        }
        if ((v.getId()) == 4) {


            intent=new Intent(context, CourseVideoActivity.class);
            context.startActivity(intent);

        }
        if ((v.getId()) == 5) {


            intent=new Intent(context, EventListingActivity.class);
            context.startActivity(intent);



        }


        if((v.getId()) == 6)
        {
            intent=new Intent(context, PaymentActivity.class);
            context.startActivity(intent);


        }
        if((v.getId()) == 7)
        {

            intent=new Intent(context, QuickQuery.class);
            context.startActivity(intent);

        }
        if ((v.getId()) == 8) {

            intent=new Intent(context, MainTabActivity.class);
            intent.putExtra("position",2);
            context.startActivity(intent);

        }if((v.getId()) == 9)
        {
            ShareApp();
        }

        if((v.getId()) == 10)
        {


            context.startActivity(new Intent(context, SampleChatActivity.class));
            // Sample breadcrumb
            ZopimChat.trackEvent("Chat Initiated from Mobile App");

        }*/




    }




    public void ShareApp()
    {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Check out Multisoft Systems App! Download this amazing mobile app on your phone today! https://goo.gl/LYtw6V";
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Join me on Multisoft Systems App!");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Share App via"));

    }













}
