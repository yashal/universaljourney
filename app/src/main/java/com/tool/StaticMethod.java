package com.tool;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.github.mrengineer13.snackbar.SnackBar;
import com.queppelin.universal.R;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StaticMethod {

    public static ArrayList<Activity> state=new ArrayList<>();

    public static ArrayList<Activity> stateactivity=new ArrayList<>();




	public static Context SlideMenuContext;
	public static Context c;
	public static Activity mActivity;
	public static String cardType;
    public static String setting_flag="N";
	public static boolean logoutflag = false;
	public static boolean lession_flag = true;
	public static boolean compose_flag=false;
	public static String Samsungdevice="Samsung";

	public static String deviceId = "000000";
	private static final String LOG_TAG = "Piron-: " + StaticMethod.class;
	public static EditText mTextWatcherEditTextEmail;
	public static EditText mTextWatcherEditTextPassword;
	public static EditText mTextWatcherEditTextConfirmPassword;
	public static ProgressDialog mCustomProgressDialog = null;
	public static ProgressDialog mProgressDialog;
	public static EditText mTextWatcherEditTextCreadit;
	public static ArrayList<String> sArrListImagePath = new ArrayList<String>();
	public static ArrayList<String> sArrListImageDesc = new ArrayList<String>();



         //   public static String[] leftMenuTitle={"Experiences","10 things to do","My trips","Stories" ,"Support","Feedback","Accreditations","Rate us","Privacy policy","Terms and conditions"};


    public static String[] leftMenuTitle={"My trips","Support","Notifications","Contact us","Feedback","Rate us","Accreditations","Terms and conditions","Privacy policy"};

    public static TextView notifiaction_text;
    public static RelativeLayout notifiaction_r;

         public static String[] leftMenuTitleafterlogin={"Experiences","10 things to do","My trips","Stories" ,"Support","Feedback","Accreditations","Rate us","Privacy policy","Terms and conditions"};



    //public static int[] leftMenuicon={R.drawable.ic_experiences, R.drawable.ic_tenthingstodo,R.drawable.ic_mytrips
    //,R.drawable.ic_stories,R.drawable.ic_support,R.drawable.ic_feedback,R.drawable.ic_accreditations,R.drawable.ic_rateus,R.drawable.ic_privacypolicy,R.drawable.ic_termsandconditions};



    public static int[] leftMenuicon={R.drawable.ic_mytrip_new
            ,R.drawable.ic_support,R.drawable.ic_menu_notifications,R.drawable.ic_menu_contact_us,R.drawable.ic_feedback_new,R.drawable.ic_rateus,R.drawable.ic_accreditations,R.drawable.ic_termsandconditions,R.drawable.ic_privacypolicy};


    public static String mLogin_flag="N";

    public StaticMethod(Context c) {
		this.c=c;
	}
	public synchronized static ProgressDialog returnProgressBar(
			final Context context) {

		mCustomProgressDialog = CustomProgressDialog.createDialog(context ,"",
				"");
		mCustomProgressDialog.show();
        mCustomProgressDialog.setCancelable(false);
		mCustomProgressDialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {

				((Activity) context).finish();
			}
		});
		return mCustomProgressDialog;
	}


//	public static void setLayoutFont(Context context, TextView... params) {
//		Typeface tf = null;
//		tf = Typeface.createFromAsset(context.getAssets(), "arial.ttf");
//
//		for (TextView tv : params) {
//			tv.setTypeface(tf);
//
//		}
//	}




    public static void showSnackBar(Activity acti, String message) {
        new SnackBar.Builder(acti)
                .withOnClickListener(new SnackBar.OnMessageClickListener() {
                    @Override
                    public void onMessageClick(Parcelable token) {
                    }
                })
                .withMessage(message)
                .withActionMessage("OK")
                .withStyle(SnackBar.Style.CONFIRM)
                .withBackgroundColorId(R.color.sb__snack_bkgnd)
                .withDuration(SnackBar.LONG_SNACK)
                .show();
    }



	public static boolean isEditTextBlank(EditText argEditText) {
		try {
			if (argEditText.getText().toString().trim().length() < 1) {
				return true;
				
			} else {
				return false;
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}

	
	
	
	
	public static boolean isInternetAvailable(final Context context) {
		boolean returnTemp = true;

		ConnectivityManager conManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo i = conManager.getActiveNetworkInfo();
		if ((i == null) || (!i.isConnected()) || (!i.isAvailable())) {
			Builder dialog = new Builder(context);
			dialog.setTitle("Attention.");
			dialog.setMessage("There is no network connection right now. Please try again later.");
			Log.v(LOG_TAG, "No Internet Connection!");
			dialog.setCancelable(false);
			dialog.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK);
						    intent.putExtra("only_access_points",true);
						    intent.putExtra("extra_prefs_show_button_bar",true);
						    intent.putExtra("wifi_enable_next_on_connect",true);
						    //context.startActivity( intent);
						    ((Activity) context).startActivityForResult(intent, 1);
						}
					});
			dialog.show();
			return false;
		}
		return true;
	}




    public static boolean isInternetAvailableRest(final Context context) {
        boolean returnTemp = true;

        ConnectivityManager conManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conManager.getActiveNetworkInfo();
        if ((i == null) || (!i.isConnected()) || (!i.isAvailable())) {

            return false;
        }
        return true;
    }




    /**
	 * This is the method is use for checking the edittext if editext contain
	 * email then return true otherwise return false.
	 * */
	public static boolean isEditTextContainEmail(EditText argEditText) {

		try {
			Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
			Matcher matcher = pattern.matcher(argEditText.getText());
			return matcher.matches();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}





	/**
	 * This method is use for set the hint in edittext we pass three argument in
	 * it first is edittext it is the edittext in witch we want to set hint
	 * second is the message we want to set in edittest third ont is for if the
	 * edittext is require fiel then its add red* in the last of hint
	 * */
	public static EditText setHintEditText(EditText argEditText,
			String argHintMessage, boolean argIsRequire) {
		try {
			if (argIsRequire) {
				argHintMessage = "   " + argHintMessage;
				String text = "<font color=#8c8c8c>" + argHintMessage
						+ "</font> <font color=#cc0029>*</font>";
				// String text =
				// "<font color=#8c8c8c>"+argHintMessage+"</font>";
				argEditText.setHint(Html.fromHtml(text));
			} else {
				argEditText.setHint(argHintMessage);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return argEditText;
	}

	public static int getWidth(Context mContext){
	    int width=0;
	    WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    if(Build.VERSION.SDK_INT>12){                   
	        Point size = new Point();
	        display.getSize(size);
	        width = size.x;
	    }
	    else{
	        width = display.getWidth();  // deprecated
	    }
	    return width;
	}
	public static int getHeight(Context mContext){
	    int height=0;
	    WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    if(Build.VERSION.SDK_INT>12){               
	        Point size = new Point();
	        display.getSize(size);
	        height = size.y;
	    }else{          
	        height = display.getHeight();  // deprecated
	    }
	    return height;      
	}
	
	public static Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			
			Toast.makeText(c, "Server Response problem. Please try again", Toast.LENGTH_LONG).show();
			
			      }
		};
		public static String getDeviceName() {
			  String manufacturer = Build.MANUFACTURER;
			  String model = Build.MODEL;
			  if (model.startsWith(manufacturer)) {
			    return capitalize(model);
			  } else {
			    return capitalize(manufacturer);
			  }
			}





			private static String capitalize(String s) {
			  if (s == null || s.length() == 0) {
			    return "";
			  }
			  char first = s.charAt(0);
			  if (Character.isUpperCase(first)) {
			    return s;
			  } else {
			    return Character.toUpperCase(first) + s.substring(1);
			  }
			} 
			
			
			public static String convertToUTF8(String s) {
		        String out = null;
		        try {
		            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
		        } catch (java.io.UnsupportedEncodingException e) {
		            return null;
		        }
		        return out;
		    }
	
			public static void watchYoutubeVideo(Context context,String id){
		        try{
		        	//for samasung mobile play not show intent action direct open
		        	//Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
		        	//startActivity(intent);
		             
		             Intent intent=new Intent(Intent.ACTION_VIEW, 
		                     Uri.parse("http://www.youtube.com/watch?v="+id));
		             context.startActivity(intent);

		             }catch (ActivityNotFoundException ex){
		                 Intent intent=new Intent(Intent.ACTION_VIEW, 
		                 Uri.parse("http://www.youtube.com/watch?v="+id));
		                 context.startActivity(intent);
		             }
		    }
			
			
			public static void showhelp(Context context,int resourseLayout)
			{
				final Dialog mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar); 
				LayoutInflater inflater1 = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View parent = (View) inflater1.inflate(
						resourseLayout, null);
				parent.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						mDialog.dismiss();
					}
				});
				mDialog.setContentView(parent);
				mDialog.setCancelable(true);
				mDialog.show();
			}

    public static int getUnreadMessageCount(JSONArray jsonArray)
    {
        int count = 0;
        try {

            for(int i=0;i<jsonArray.length();i++)
            {
                if(jsonArray.getJSONObject(i).getString("is_read").equalsIgnoreCase("0"))
                {
                    count++;
                }

            }

            return count;
        }catch (Exception e)
        {
            return count;
        }
    }


    public static String getUniqueId(Context context)
    {
        String unique_id="";

        String m_szDevIDShort = "35" + //we make this look like a valid IMEI
                Build.BOARD.length()%10+ Build.BRAND.length()%10 +
                Build.CPU_ABI.length()%10 + Build.DEVICE.length()%10 +
                Build.DISPLAY.length()%10 + Build.HOST.length()%10 +
                Build.ID.length()%10 + Build.MANUFACTURER.length()%10 +
                Build.MODEL.length()%10 + Build.PRODUCT.length()%10 +
                Build.TAGS.length()%10 + Build.TYPE.length()%10 +
                Build.USER.length()%10 ; //13 digits

        String m_szAndroidID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);


        unique_id=m_szDevIDShort+m_szAndroidID;

        return unique_id;
    }











}
