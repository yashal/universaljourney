/**
 * 
 */
package com.tool;

import android.content.Context;

import com.queppelin.universal.RestClient;

import org.apache.http.conn.ConnectTimeoutException;

/**
 * @author ${Amit kumar <a.kumar@pironcorp.com>}
 * 
 */
final public class HttpClass extends Thread {
	private static final String TAG ="HttpClass";
	public static String ResponseVector;
    String METHOD_NAME;
    RestClient.RequestMethod method;
    String Property_Key[];
    String Property_Value[];
	// Handler handler;
	Context context;

	public HttpClass(Context context,String METHOD_NAME,RestClient.RequestMethod method,String Property_Key[]
            ,String Property_Value[]) {
		super();
		// this.handler = handler;
		this.context = context;
        this.METHOD_NAME=METHOD_NAME;
        this.method=method;

        this.Property_Key=Property_Key;
        this.Property_Value=Property_Value;

		this.ResponseVector = null;
		if (StaticMethod.isInternetAvailable(this.context)) {
			try{
			if (StaticMethod.mCustomProgressDialog.isShowing()) {
				
				StaticMethod.mCustomProgressDialog.dismiss();

			}
			}catch(Exception e)
			{
				e.getMessage();
			}
		}
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();

		try {

            RestClient client = new RestClient(AppConfig.APP_URL+METHOD_NAME,context);
            client.AddHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            client.AddHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);
            for(int i=0;i<Property_Key.length;i++)
            {
                client.AddParam(Property_Key[i], Property_Value[i]);
               System.out.println("input::"+ Property_Key[i]+"-"+ Property_Value[i]);
            }
            client.Execute(method);
           ResponseVector = client.getResponse();

		 System.out.println("output::" + ResponseVector);
			

		} catch (ConnectTimeoutException e) {
            if (StaticMethod.mCustomProgressDialog.isShowing()) {
                StaticMethod.mCustomProgressDialog.dismiss();
            }

            System.out.println("Time out");

            new StaticMethod(context).handler.sendEmptyMessage(0);
            System.out.println("pull");
            ResponseVector = null;
//			 Toast.makeText(context, "Server Problem",
//			 Toast.LENGTH_LONG).show();

            e.printStackTrace();
            // TODO: handle exception
            // System.out.println("HttpClass Run()"+e);
            // handler.sendEmptyMessage(1);

        }catch (Exception e)
        {

        }
		if (StaticMethod.mCustomProgressDialog.isShowing()) {
			StaticMethod.mCustomProgressDialog.dismiss();
		}

	}

	/*
	* String Property1, String Property1Value, String Property2, String
    * Property2Value, String Property3, String Property3Value, String
    * Property4, String Property4Value,String Property5, String Property5Value,
    * String PropertyName
    */

	

	
	
}
