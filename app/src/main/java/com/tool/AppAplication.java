package com.tool;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.view.View;

import com.queppelin.universal.RestClient;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by lenovo on 30-01-2016.
 */
public class AppAplication extends Application {

    private SharedPreferences mPrefs;
    @Override
    public void onCreate() {
        super.onCreate();

        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        if(StaticMethod.isInternetAvailableRest(this)) {
            new GetsettingOrignal(this).execute();
            new Getsetting(this).execute();
        }else
        {
            return;
        }
    }

    private class Getsetting extends AsyncTask<Void, Void, String[]>
    {

        String mgetThingsResponse;

        String mgetThingsResponseVersion="";
        Context context;

        JSONArray jsonarray;

        public Getsetting(Context context) {
            super();
            this.context=context;


        }
        @Override
        protected String[] doInBackground(Void... params) {

            String [] mStrings=null;

            try {



if(!mPrefs.getString("deviceId", "").equalsIgnoreCase("")) {

    if (mPrefs.getString("isLogin", "").equalsIgnoreCase("Y")) {


        String userValue = mPrefs.getString("userObject", "").toString();

        String user_id;
        JSONObject object = new JSONObject(userValue);
        JSONObject userObject = object.getJSONObject("result");
        user_id = userObject.getString("customer_id");//customer_id


        String[] pro_keyVersion = new String[]{"userId", "deviceType", "isLoggedIn", "deviceId"};
        String[] pro_valueVersion = new String[]{user_id, "ANDROID", "1", mPrefs.getString("deviceId", "")};

        mgetThingsResponseVersion = (new HttpclasswithoutThreadVersion(context, "getAllMessages", RestClient.RequestMethod.POST, pro_keyVersion, pro_valueVersion
        )).getResponsedetails();


    } else {
        String[] pro_keyVersion = new String[]{"userId", "deviceType", "isLoggedIn", "deviceId"};
        String[] pro_valueVersion = new String[]{"0", "ANDROID", "0", mPrefs.getString("deviceId", "")};
        mgetThingsResponseVersion = (new HttpclasswithoutThreadVersion(context, "getAllMessages", RestClient.RequestMethod.POST, pro_keyVersion, pro_valueVersion
        )).getResponsedetails();

    }


}



            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return mStrings;
        }
        @Override
        protected void onPostExecute(String[] result) {

            try
            {


                System.out.println("getReadMessage"+mgetThingsResponseVersion);


                if(!mgetThingsResponseVersion.equalsIgnoreCase(""))
                {
                    JSONObject jsonObject=new JSONObject(mgetThingsResponseVersion);

                    if(jsonObject.has("result")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("result");
                        SharedPreferences.Editor prefsEditor = mPrefs
                                .edit();
                        prefsEditor
                                .putString("getMessageArray", jsonArray.toString());
                        prefsEditor.commit();


                        int count = StaticMethod.getUnreadMessageCount(jsonArray);

                        if (count > 0) {

                            StaticMethod.notifiaction_r.setVisibility(View.VISIBLE);
                            StaticMethod.notifiaction_text.setText(count + "");
                        } else {
                            StaticMethod.notifiaction_r.setVisibility(View.GONE);
                            StaticMethod.notifiaction_text.setText(count + "");
                        }
                    }
                }




            }catch (Exception e)
            {

                System.out.println("error"+e);

            }



            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
        }
    }



    private class GetsettingOrignal extends AsyncTask<Void, Void, String[]>
    {

        String mgetThingsResponse;


        Context context;

        JSONArray jsonarray;

        public GetsettingOrignal(Context context) {
            super();
            this.context=context;


        }
        @Override
        protected String[] doInBackground(Void... params) {

            String [] mStrings=null;

            try {

                String[] pro_key = new String[]{};
                String[] pro_value = new String[]{};
                mgetThingsResponse =(new HttpclasswithoutThrade(context,"getSettings", RestClient.RequestMethod.GET,pro_key,pro_value
                )).getResponsedetails();






            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return mStrings;
        }
        @Override
        protected void onPostExecute(String[] result) {

            try
            {
                System.out.println("getSettings:"+mgetThingsResponse);

                if(!mgetThingsResponse.equalsIgnoreCase(""))
                {
                    JSONObject object=new JSONObject(mgetThingsResponse);

                    String email=object.getString("admin_email");
                    String mobile=object.getString("admin_telephone");//admin_mobile
                    String mobile2=object.getString("admin_telephone_2");

                    SharedPreferences.Editor prefsEditor = mPrefs
                            .edit();
                    prefsEditor
                            .putString("admin_email",email);
                    prefsEditor.putString("admin_number", mobile);
                    prefsEditor.putString("admin_number2", mobile2);

                    prefsEditor.putString("experience_title", object.getString("experience_title"));
                    prefsEditor.putString("experience_description", object.getString("experience_description"));
                    prefsEditor.putString("ten_thing_title", object.getString("ten_thing_title"));
                    prefsEditor.putString("ten_thing_desc", object.getString("ten_thing_desc"));

                    prefsEditor.putString("bannerArray", object.getJSONArray("banner").toString());
                    prefsEditor.putString("AppSlideshow", object.getJSONArray("AppSlideshow").toString());

                    prefsEditor.commit();


                    StaticMethod.setting_flag="Y";
                }




            }catch (Exception e)
            {


                StaticMethod.setting_flag="N";
                System.out.println("error"+e);

            }



            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
        }
    }



}
