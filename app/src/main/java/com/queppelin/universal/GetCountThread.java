package com.queppelin.universal;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.view.View;

import com.tool.HttpclasswithoutThrade;
import com.tool.HttpclasswithoutThreadVersion;
import com.tool.StaticMethod;

import org.json.JSONArray;
import org.json.JSONObject;
/**
 * Created by lenovo on 18-04-2016.
 */
public class GetCountThread extends AsyncTask<Void, Void, String[]>
{

    String mgetThingsResponse;
    String mgetThingsResponseVersion;
    Context context;
    JSONArray jsonarray;
    private SharedPreferences mPrefs;

    public GetCountThread(Context context) {
        super();
        this.context=context;

        mPrefs = context.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);



    }
    @Override
    protected String[] doInBackground(Void... params) {

        String [] mStrings=null;

        try {




            if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
            {

                String userValue=mPrefs.getString("userObject","").toString();
                String user_id;
                JSONObject object=new JSONObject(userValue);
                JSONObject userObject=object.getJSONObject("result");
                user_id=userObject.getString("customer_id");//customer_id

                String[] pro_keyVersion = new String[]{"userId","deviceType","isLoggedIn","deviceId"};
                String[] pro_valueVersion = new String[]{user_id,"ANDROID","1",mPrefs.getString("deviceId","")};

                mgetThingsResponseVersion =(new HttpclasswithoutThreadVersion(context,"getAllMessages", RestClient.RequestMethod.POST,pro_keyVersion,pro_valueVersion
                )).getResponsedetails();


            }else
            {
                String[] pro_keyVersion = new String[]{"userId","deviceType","isLoggedIn","deviceId"};
                String[] pro_valueVersion = new String[]{"0","ANDROID","0",mPrefs.getString("deviceId","")};
                mgetThingsResponseVersion =(new HttpclasswithoutThreadVersion(context,"getAllMessages", RestClient.RequestMethod.POST,pro_keyVersion,pro_valueVersion
                )).getResponsedetails();

            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mStrings;
    }
    @Override
    protected void onPostExecute(String[] result) {

        try
        {
         //   System.out.println("getSettings:"+mgetThingsResponse);

            System.out.println("getCount"+mgetThingsResponseVersion);


            if(!mgetThingsResponseVersion.equalsIgnoreCase(""))
            {
                JSONObject jsonObject=new JSONObject(mgetThingsResponseVersion);

                JSONArray jsonArray=jsonObject.getJSONArray("result");


                SharedPreferences.Editor prefsEditor = mPrefs
                        .edit();
                prefsEditor
                        .putString("getMessageArray",jsonArray.toString());
                prefsEditor.commit();


                int count= StaticMethod.getUnreadMessageCount(jsonArray);

                if(count>0)
                {

                    StaticMethod.notifiaction_r.setVisibility(View.VISIBLE);
                    StaticMethod.notifiaction_text.setText(count+"");
                }else
                {
                    StaticMethod.notifiaction_r.setVisibility(View.GONE);
                    StaticMethod.notifiaction_text.setText(count+"");
                }

            }

        }catch (Exception e)
        {
            System.out.println("outside try"+e);
            try {

                StaticMethod.notifiaction_r.setVisibility(View.GONE);
                StaticMethod.notifiaction_text.setText("" + "");
            }catch (Exception e1)
            {

                System.out.println("inside try"+e1);
            }
        }



        super.onPostExecute(result);
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub

        super.onPreExecute();
    }
}
