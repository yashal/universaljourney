package com.queppelin.universal;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.tool.HttpClass;
import com.tool.StaticMethod;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class FilterActivity extends AppCompatActivity {



    ArrayList<JSONObject> jDuration;
    ArrayList<JSONObject> jInclusion;


    public static  ArrayList<JSONObject> selectDuration;
    public static  ArrayList<JSONObject> selectInclusion;

    Typeface font;
    Boolean clear_flag=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);


        font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");

        ((TextView)findViewById(R.id.tv_duration)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_travel)).setTypeface(font);
        ((TextView)findViewById(R.id.tvclear)).setTypeface(font);
        ((TextView)findViewById(R.id.tvapplyfi)).setTypeface(font);




        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        String[] pro_key = new String[]{};
        String[] pro_value = new String[]{};//user_id


        ((RelativeLayout)findViewById(R.id.rapplyfilter)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(selectInclusion.size()>0||selectDuration.size()>0)
                {

                    Intent intent = new Intent();
                    intent.putExtra("flag_selection","S");
                    setResult(RESULT_OK, intent);
                    finish();



                }else
                {

                    Intent intent = new Intent();
                    intent.putExtra("flag_selection","F");
                    setResult(RESULT_OK, intent);
                    finish();


                    // Toast.makeText(getApplicationContext(),"Please select atleast one filter",Toast.LENGTH_LONG).show();
                }


            }
        });

        ((RelativeLayout)findViewById(R.id.rclearfilter)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // ((ListView)findViewById(R.id.lvduration)).notify();

                if(selectDuration.size()>0)
                {
                    selectDuration.remove(0);
                    ListinfoDetailsActivity.selection_day_flag.remove(0);
                    ListinfoDetailsActivity.selection_day_flag.clear();
                    ListinfoDetailsActivity.selection_day_flag=new ArrayList<>();
                    ListinfoDetailsActivity.selection_day_flag.removeAll(ListinfoDetailsActivity.selection_day_flag);
                    selectDuration.clear();
                }
                setDurationLayout("C");

                selectInclusion.clear();
                ListinfoDetailsActivity.selection_flag.clear();

                setFilter();
                clear_flag=true;

            }
        });



        if(StaticMethod.isInternetAvailable(this)) {
            new HttpClass(FilterActivity.this, "getFilters", RestClient.RequestMethod.GET, pro_key, pro_value
            ).start();
            StaticMethod.returnProgressBar(
                    FilterActivity.this).setOnDismissListener(
                    new DialogInterface.OnDismissListener() {
                        public void onDismiss(DialogInterface dialog) {
                            if (HttpClass.ResponseVector != null && !HttpClass.ResponseVector.equalsIgnoreCase("")) {

                                // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                                // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                                //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                                try {


                                    System.out.println("getFilters" + HttpClass.ResponseVector);

                                    JSONObject jsonObject = new JSONObject(HttpClass.ResponseVector);


                                    if (jsonObject.has("success")) {
                                        jDuration = new ArrayList<JSONObject>();
                                        jInclusion = new ArrayList<JSONObject>();


                                        selectDuration = new ArrayList<JSONObject>();
                                        selectInclusion = new ArrayList<JSONObject>();


                                        JSONArray json_filter = jsonObject.getJSONArray("result");

                                        for (int i = 0; i < json_filter.length(); i++) {

                                            if (json_filter.getJSONObject(i).getString("option_id").equalsIgnoreCase("1")) {
                                                jDuration.add(json_filter.getJSONObject(i));

                                            } else if (json_filter.getJSONObject(i).getString("option_id").equalsIgnoreCase("7")) {
                                                jInclusion.add(json_filter.getJSONObject(i));
                                            }


                                        }
                                        setDurationLayout("N");
                                        setFilter();


                                        ((RelativeLayout) findViewById(R.id.rduration)).setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                ((ScrollView) findViewById(R.id.sinclusion)).setVisibility(View.GONE);
                                                ((LinearLayout) findViewById(R.id.lduration)).setVisibility(View.VISIBLE);


                                                ((ImageView) findViewById(R.id.imgduration)).setVisibility(View.VISIBLE);
                                                ((ImageView) findViewById(R.id.imgincluion)).setVisibility(View.GONE);


                                            }
                                        });


                                        ((RelativeLayout) findViewById(R.id.rtravale)).setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                ((ScrollView) findViewById(R.id.sinclusion)).setVisibility(View.VISIBLE);
                                                ((LinearLayout) findViewById(R.id.lduration)).setVisibility(View.GONE);


                                                ((ImageView) findViewById(R.id.imgduration)).setVisibility(View.GONE);
                                                ((ImageView) findViewById(R.id.imgincluion)).setVisibility(View.VISIBLE);

                                            }
                                        });

                                    } else if (jsonObject.has("error")) {

                                        Toast.makeText(getApplicationContext(), jsonObject.getString("Message"), Toast.LENGTH_LONG).show();

                                    }


                                } catch (Exception e) {
                                    System.err.println(e);
                                }

                            }
                        }
                    });


        }else
        {
            return;
        }


    }

        public void setFilter()
        {

            try
            {

                //LinearLayout durationLayout=null;
                LinearLayout InclusionLayout=null;

              //  durationLayout=(LinearLayout)findViewById(R.id.lduration);
                InclusionLayout=(LinearLayout)findViewById(R.id.linclusion);
                //durationLayout.removeAllViews();
                InclusionLayout.removeAllViews();



                 JSONObject jinclusion=jInclusion.get(0);
                 final JSONArray jsonArrayinclusion=jinclusion.getJSONArray("values");



                for(int i=0;i<jsonArrayinclusion.length();i++)
                {
                    final View parentinclusion=getLayoutInflater().inflate(R.layout.filtter_row, null);
                    final CheckedTextView msubCat=((CheckedTextView)parentinclusion.findViewById(R.id.tgl_status));
                    parentinclusion.setId(i);
                    ((TextView)parentinclusion.findViewById(R.id.subcatname)).setText(jsonArrayinclusion.getJSONObject(i).getString("name"));
                    ((TextView)parentinclusion.findViewById(R.id.subcatname)).setTypeface(font);

                    String imageNmae=jsonArrayinclusion.getJSONObject(i).getString("text");



                   // jsonArrayinclusion.getJSONObject(i).getString("option_value_id").

                    if(selectInclusion.contains(jsonArrayinclusion.getJSONObject(i))) {
                        msubCat.setChecked(true);

                    }else
                    {
                        msubCat.setChecked(false);
                    }


                    if(ListinfoDetailsActivity.selection_flag.contains(jsonArrayinclusion.getJSONObject(i).getString("name"))) {
                        msubCat.setChecked(true);
                        selectInclusion.add(jsonArrayinclusion.getJSONObject(i));

                    }else
                    {
                        msubCat.setChecked(false);
                    }





                    try
                    {
                        Resources res = getResources();
                        String mDrawableName = imageNmae.replace("-","_");
                        int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
                        Drawable drawable = res.getDrawable(resID );

                        ColorFilter filter = new LightingColorFilter( Color.WHITE, Color.WHITE );
                         drawable.setColorFilter(filter);

                        ((ImageView)parentinclusion.findViewById(R.id.imginclusion)).setImageDrawable(drawable );


                    }catch (Exception e)
                    {

                        System.out.println("Error" + imageNmae);
                    }



                    parentinclusion.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            if (msubCat.isChecked()) {
                                try {
                                    msubCat.setChecked(false);


                                    selectInclusion.remove(jsonArrayinclusion.getJSONObject(v.getId()));
                                   // Toast.makeText(getApplicationContext(),"add",Toast.LENGTH_LONG).show();

                                } catch (Exception e) {
                                    e.getMessage();
                                }
                            }
                            else {
                                try {

                                    msubCat.setChecked(true);
                                    selectInclusion.add(jsonArrayinclusion.getJSONObject(v.getId()));

                                  //  Toast.makeText(getApplicationContext(),"Remove",Toast.LENGTH_LONG).show();
                                } catch (Exception e) {
                                    e.getMessage();
                                }

                                //Toast.makeText(getApplicationContext(), "on uncheck"+parent1.getId(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });




                    //((ImageView)parentinclusion.findViewById(R.id.imginclusion)).setBackgroundResource(R.drawable.ico_plain);




                    InclusionLayout.addView(parentinclusion);

                }
            }catch (Exception e)
            {

            }

        }
    ArrayAdapter<String> a;

    public void setDurationLayout(String flag)
    {


    try
    {

    ListView lduration=(ListView)findViewById(R.id.lvduration);
    JSONObject jduration=jDuration.get(0);
    final JSONArray jsonArrayduration=sortJsonArray(jduration.getJSONArray("values"));

        System.out.println("Array:"+jsonArrayduration);

    String duration[]=new String[jsonArrayduration.length()];

    for(int i=0;i<jsonArrayduration.length();i++)
    {
        duration[i]="\u0020"+jsonArrayduration.getJSONObject(i).getString("name")+"\u0020Nights";
    }

     a=new ArrayAdapter<String>(FilterActivity.this,
            android.R.layout.simple_list_item_single_choice,
            android.R.id.text1, duration){
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            TextView text = (TextView) view.findViewById(android.R.id.text1);
            text.setTypeface(font);
            text.setTextColor(Color.WHITE);
            return view;
        }
    };
    ColorDrawable sage = new ColorDrawable(this.getResources().getColor(R.color.transparent));
    lduration.setDivider(sage);
    lduration.setDividerHeight(1);
    lduration.setAdapter(a);


        System.out.println("Select postion L" + ListinfoDetailsActivity.selection_day_flag.size());


     if(flag.equalsIgnoreCase("C"))
     {

     }else {

         if (ListinfoDetailsActivity.selection_day_flag.size() > 0) {

             System.out.println("Select postion" + ListinfoDetailsActivity.selection_day_flag);
             lduration.setItemChecked((int) ListinfoDetailsActivity.selection_day_flag.get(ListinfoDetailsActivity.selection_day_flag.size() - 1), true);


         }
     }
        lduration.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                try
                {
                        if(selectDuration.size()>0)
                        {

                            selectDuration.remove(0);
                            ListinfoDetailsActivity.selection_day_flag.remove(0);
                            selectDuration.add(jsonArrayduration.getJSONObject(i));
                            ListinfoDetailsActivity.selection_day_flag.add(i);
                        }else
                        {
                            selectDuration.add(jsonArrayduration.getJSONObject(i));
                            ListinfoDetailsActivity.selection_day_flag.add(i);
                        }





                }catch (Exception e)
                {
                System.out.println("Error adding");

                }




            }
        });

    lduration.setChoiceMode(ListView.CHOICE_MODE_SINGLE);




/*

    LinearLayout durationLayout=null;
    durationLayout=(LinearLayout)findViewById(R.id.lduration);
    durationLayout.removeAllViews();
    JSONObject jduration=jInclusion.get(0);
    JSONArray jsonArrayduration=jduration.getJSONArray("values");









    for(int i=0;i<jsonArrayduration.length();i++)
    {
        final View parentduration=getLayoutInflater().inflate(R.layout.filtter_row, null);



        durationLayout.addView(parentduration);

    }
*/


}catch (Exception e)
{

}





    }




    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_filter, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==android.R.id.home)
        {


            if(clear_flag)
            {

                Intent intent = new Intent();
                intent.putExtra("flag_selection","F");
                setResult(RESULT_OK, intent);
                finish();


            }else
            {
                finish();

            }


        }

        return super.onOptionsItemSelected(item);
    }



    public static JSONArray sortJsonArray( JSONArray array) throws Exception{


            List<JSONObject> jsons = new ArrayList<JSONObject>();
            for (int i = 0; i < array.length(); i++) {
                jsons.add(array.getJSONObject(i));
            }
            Collections.sort(jsons, new Comparator<JSONObject>() {
                @Override
                public int compare(JSONObject lhs, JSONObject rhs) {

                    try
                    {

                        // Here you could parse string id to integer and then compare.
                        return lhs.getInt("name") > rhs.getInt("name") ? 1 : (lhs
                                .getInt("name") < rhs.getInt("name") ? -1 : 0);


                    }catch (Exception e)
                    {
                        return 0;
                    }


                }
            });




            return new JSONArray(jsons);



    }


    @Override
    public void onBackPressed() {

        if(clear_flag)
        {

            Intent intent = new Intent();
            intent.putExtra("flag_selection","F");
            setResult(RESULT_OK, intent);
            finish();


        }else
        {
            super.onBackPressed();
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {

            startActivity(getIntent());
            finish();
        }
    }

}
