package com.queppelin.universal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveFolder;
import com.tool.AppConfig;
import com.tool.StaticMethod;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class PhotoGraphActivity extends AppCompatActivity {

    private static final String TAG = "drive";
    private static final int REQUEST_CODE_SELECT = 102;
    private static final int REQUEST_CODE_RESOLUTION = 103;
    private GoogleApiClient googleApiClient;

    private static final int RESULT_LOAD_IMAGE_PHOTOGRAPH = 2;
    private static final int PICK_FROM_CAMERA = 4;
    private static final int CROP_FROM_CAMERA = 5;
    private static final int PICK_FROM_FILE = 6;

    String travel_id;
    String traveleOject;

    private Uri mImageCaptureUri;

    Bitmap bitmapphoto=null;

    Uri bitmapphotouri=null;


    public static Activity next;


    JSONObject  object;
    String photo;
    Typeface font;


    String imageType="";
    boolean max_size_flag=false;


    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_graph);

//       buildGoogleApiClient();
        font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");

        StaticMethod.stateactivity.add(PhotoGraphActivity.this);
        imageType="";

        ((TextView)findViewById(R.id.tvheadertitle)).setTypeface(font);
        ((TextView)findViewById(R.id.tvphoto)).setTypeface(font);
        ((TextView)findViewById(R.id.tvphotoscan)).setTypeface(font);
        ((TextView)findViewById(R.id.tvphotobrowse)).setTypeface(font);
        ((TextView)findViewById(R.id.tvnote)).setTypeface(font);
        ((TextView)findViewById(R.id.tvskip)).setTypeface(font);
        ((TextView)findViewById(R.id.tvsubmit)).setTypeface(font);
        ((TextView)findViewById(R.id.tvback)).setTypeface(font);


        next=PhotoGraphActivity.this;
        ((RelativeLayout)findViewById(R.id.rback)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });


        getSupportActionBar().hide();

        traveleOject=getIntent().getExtras().getString("object");


        try
        {

            object=new JSONObject(traveleOject);


            //passport = object.getString("pass_copy");
            photo = object.getString("photograph");
           // support = object.getString("supp_doc");

            //passport_back = object.getString("pass_copy_back");
            //support1 = object.getString("supp_doc_2");
            //support2 = object.getString("supp_doc_3");

            travel_id=object.getString("id");

        }catch (Exception e)
        {

        }



        if(photo.equalsIgnoreCase("")||photo.equalsIgnoreCase("null"))
        {

            ((TextView)findViewById(R.id.tvscan_photo_flag)).setVisibility(View.INVISIBLE);
            ((TextView)findViewById(R.id.tvscan_photo_flag)).setBackgroundResource(R.drawable.icon_closemark);
        }else
        {
            ((TextView)findViewById(R.id.tvscan_photo_flag)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_photo_flag)).setBackgroundResource(R.drawable.icon_checkmark);
        }




        ((TextView)findViewById(R.id.tvphotoscan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                imageType="S";
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/temp");
                myDir.mkdirs();
                String fname = "crap_temp.jpg";
                File file = new File (myDir, fname);
                if (file.exists ()) file.delete ();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, PICK_FROM_CAMERA);
                }


/*






                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                mImageCaptureUri = Uri.fromFile(new File(Environment
                        .getExternalStorageDirectory(), "tmp_avatar_"
                        + String.valueOf(System.currentTimeMillis())
                        + ".jpg"));

                try {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            mImageCaptureUri);
                    intent.putExtra("return-data", true);
                    startActivityForResult(intent, PICK_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
*/

            }
        });



        ((TextView)findViewById(R.id.tvphotobrowse)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageType="B";



                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE_PHOTOGRAPH);

/*

                CharSequence colors[] = new CharSequence[] {"Image from sdcard", "PDF file from Drive"};

                AlertDialog.Builder builder = new AlertDialog.Builder(PhotoGraphActivity.this);
                builder.setTitle("Choose file from...");
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // the user clicked on colors[which]


                        if(which==0)
                        {
                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, RESULT_LOAD_IMAGE_PHOTOGRAPH);


                        }else if(which==1)
                        {



                            IntentSender intentSender = Drive.DriveApi
                                    .newOpenFileActivityBuilder()
//                these mimetypes enable these folders/files types to be selected
                                    .setMimeType(new String[] { DriveFolder.MIME_TYPE, "application/pdf"})
                                    .build(googleApiClient);
                            try {
                                startIntentSenderForResult(
                                        intentSender, REQUEST_CODE_SELECT, null, 0, 0, 0);
                            } catch (IntentSender.SendIntentException e) {
                                Log.i(TAG, "Unable to send intent", e);
                            }

                        }

                    }
                });
                builder.show();
*/


            }
        });



        ((TextView)findViewById(R.id.tvskip)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {





                Intent intent=new Intent(PhotoGraphActivity.this,PassportActivity.class);
                intent.putExtra("v_surname", getIntent().getExtras().getString("v_surname"));
                intent.putExtra("v_firstname", getIntent().getExtras().getString("v_firstname"));
                intent.putExtra("v_sex", getIntent().getExtras().getString("v_sex"));
                intent.putExtra("v_passport", getIntent().getExtras().getString("v_passport"));
                intent.putExtra("v_dob", getIntent().getExtras().getString("v_dob"));
                intent.putExtra("v_doi", getIntent().getExtras().getString("v_doi"));
                intent.putExtra("v_doe", getIntent().getExtras().getString("v_doe"));
                intent.putExtra("v_poissuance", getIntent().getExtras().getString("v_poissuance"));
                intent.putExtra("v_mealplan", getIntent().getExtras().getString("v_mealplan"));
                intent.putExtra("v_hub", getIntent().getExtras().getString("v_hub"));
                intent.putExtra("v_special", getIntent().getExtras().getString("v_special"));
                intent.putExtra("object",object.toString());
                startActivity(intent);



            }
        });


        ((TextView)findViewById(R.id.tvsubmit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(StaticMethod.isInternetAvailable(PhotoGraphActivity.this)) {
                    if (bitmapphotouri != null) {//bitmapphoto

                        if (StaticMethod.isInternetAvailable(PhotoGraphActivity.this)) {

                            new GetThings(PhotoGraphActivity.this, traveleOject).execute();

                        } else {

                            return;
                        }

                    } else {


                        if (photo.equalsIgnoreCase("") || photo.equalsIgnoreCase("null")) {

                            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                                    PhotoGraphActivity.this);
                            alertDialogBuilder
                                    .setMessage("Please select photo graph")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                        }
                                    });
                            android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                            Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                            msgTxt.setTypeface(font);


                        } else {

                            Intent intent = new Intent(PhotoGraphActivity.this, PassportActivity.class);
                            intent.putExtra("v_surname", getIntent().getExtras().getString("v_surname"));
                            intent.putExtra("v_firstname", getIntent().getExtras().getString("v_firstname"));
                            intent.putExtra("v_sex", getIntent().getExtras().getString("v_sex"));
                            intent.putExtra("v_passport", getIntent().getExtras().getString("v_passport"));
                            intent.putExtra("v_dob", getIntent().getExtras().getString("v_dob"));
                            intent.putExtra("v_doi", getIntent().getExtras().getString("v_doi"));
                            intent.putExtra("v_doe", getIntent().getExtras().getString("v_doe"));
                            intent.putExtra("v_poissuance", getIntent().getExtras().getString("v_poissuance"));
                            intent.putExtra("v_mealplan", getIntent().getExtras().getString("v_mealplan"));
                            intent.putExtra("v_hub", getIntent().getExtras().getString("v_hub"));
                            intent.putExtra("v_special", getIntent().getExtras().getString("v_special"));
                            intent.putExtra("object", object.toString());
                            startActivity(intent);


                        }


                    }


                }else
                {
                    return;
                }

            }
        });




    }

    Boolean max_size=false;
    private class GetThings extends AsyncTask<Void, Void, String[]>
    {

        String mgetThingsResponse="";
        Context context;
        String jsonObject;
        JSONArray jsonarray;
        ProgressDialog pd;
        Boolean flag=true;
        String send_passport_result="";
        String send_backpassport_result="";
        String send_photo_result="";
        String send_support_result="";
        String send_support_result2="";
        String send_support_result3="";


        public GetThings(Context context,String jsonObject) {
            super();
            this.context=context;
            this.jsonObject=jsonObject;
            max_size=false;
            pd= new ProgressDialog(context).show(context, "", "Uploading document..");
            pd.show();
        }
        @Override
        protected String[] doInBackground(Void... params) {

            String [] mStrings=null;

            try {



                if(bitmapphotouri!=null)
                {
                    send_photo_result= sendPhotouri(bitmapphotouri, travel_id);
                }

               /* if(bitmapphoto!=null)
                {
                   send_photo_result=   sendPhoto(bitmapphoto,travel_id);
                }*/

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

                flag=false;
            }
            return mStrings;
        }
        @Override
        protected void onPostExecute(String[] result) {

            try {
                if (pd.isShowing()) {
                    pd.dismiss();
                }


                if (true) {


                    JSONObject savefilephoto;

                    if (!send_photo_result.equalsIgnoreCase("")) {
                        savefilephoto = new JSONObject(send_photo_result);

                        if (savefilephoto.has("success")) {
                            flag = true;
                        } else {
                            flag = false;
                        }

                    }


                    if (flag) {

                        JSONObject temp = null;
                        try {
                            temp = new JSONObject(jsonObject);
                            temp.put("photograph", "y");

                        } catch (Exception e) {

                        }


                        if(bitmapphoto!=null)
                        {
                            bitmapphoto.recycle();
                        }

                        Intent intent = new Intent(PhotoGraphActivity.this, PassportActivity.class);
                        intent.putExtra("v_surname", getIntent().getExtras().getString("v_surname"));
                        intent.putExtra("v_firstname", getIntent().getExtras().getString("v_firstname"));
                        intent.putExtra("v_sex", getIntent().getExtras().getString("v_sex"));
                        intent.putExtra("v_passport", getIntent().getExtras().getString("v_passport"));
                        intent.putExtra("v_dob", getIntent().getExtras().getString("v_dob"));
                        intent.putExtra("v_doi", getIntent().getExtras().getString("v_doi"));
                        intent.putExtra("v_doe", getIntent().getExtras().getString("v_doe"));
                        intent.putExtra("v_poissuance", getIntent().getExtras().getString("v_poissuance"));
                        intent.putExtra("v_mealplan", getIntent().getExtras().getString("v_mealplan"));
                        intent.putExtra("v_hub", getIntent().getExtras().getString("v_hub"));
                        intent.putExtra("v_special", getIntent().getExtras().getString("v_special"));
                        intent.putExtra("object", temp.toString());
                        startActivity(intent);


                    } else {
                        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                                PhotoGraphActivity.this);

                        // set title


                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Error please try again!")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {


                                    }
                                });


                        // create alert dialog
                        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                        TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                        msgTxt.setTypeface(font);
                    }


                }else
                {

                    android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                            PhotoGraphActivity.this);

                    // set title


                    // set dialog message
                    alertDialogBuilder
                            .setMessage("File size should be less than 5 MB")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {


                                }
                            });


                    // create alert dialog
                    android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                    msgTxt.setTypeface(font);



                }


            }catch (Exception e)
            {
                System.out.println("Error"+e);
            }



            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
        }
    }




    public String sendPhoto(Bitmap bitmap,String travelid) throws Exception {

        String aa="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            System.out.println("trvelid"+travelid);

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
           // Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()), (int)(bitmap.getHeight()), true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] data = bos.toByteArray();
            System.out.println("size photo:"+data.length);
            //((ImageView)findViewById(R.id.imgoutput)).setImageBitmap(bmpCompressed);


           // float size=(data.length)/1024.0f*1024.0f;



                    entity.addPart("id", new StringBody(travelid));


                    // sending a Image;0
                    // note here, that you can send more than one image, just add another param, same rule to the String;


                    entity.addPart("photograph", new ByteArrayBody(data, "photograph.jpg"));
                    httpPost.setEntity(entity);
                    HttpResponse response = httpClient.execute(httpPost, localContext);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                    aa = reader.readLine();
                    System.out.println("imageupload" + aa);



        } catch (Exception e) {

            System.out.println("error"+e);
        }

        return aa;

    }



    public String sendPhotouri(Uri bitmapuri,String travelid) throws Exception {

        String aa="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            System.out.println("trvelid"+travelid);

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            // Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()), (int)(bitmap.getHeight()), true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();


            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), bitmapuri);

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] data = bos.toByteArray();
            System.out.println("size photo:"+data.length);
            //((ImageView)findViewById(R.id.imgoutput)).setImageBitmap(bmpCompressed);


            // float size=(data.length)/1024.0f*1024.0f;



            entity.addPart("id", new StringBody(travelid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;


            entity.addPart("photograph", new ByteArrayBody(data, "photograph.jpg"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            aa = reader.readLine();
            System.out.println("imageupload" + aa);

            File f=new File(bitmapuri.getPath().toString());
            if(f.exists()) {
                f.delete();
            }

        } catch (Exception e) {

            System.out.println("error"+e);
        }

        return aa;

    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_photo_graph, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    public class CropOptionAdapter extends ArrayAdapter<CropOption> {
        private ArrayList<CropOption> mOptions;
        private LayoutInflater mInflater;

        public CropOptionAdapter(Context context, ArrayList<CropOption> options) {
            super(context, R.layout.crop_selector, options);

            mOptions = options;

            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup group) {
            if (convertView == null)
                convertView = mInflater.inflate(R.layout.crop_selector, null);

            CropOption item = mOptions.get(position);

            if (item != null) {
                ((ImageView) convertView.findViewById(R.id.iv_icon))
                        .setImageDrawable(item.icon);
                ((TextView) convertView.findViewById(R.id.tv_name))
                        .setText(item.title);

                return convertView;
            }

            return null;
        }
    }

    public class CropOption {
        public CharSequence title;
        public Drawable icon;
        public Intent appIntent;
    }


    private void SaveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images1");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "temp.jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void Savefile(InputStream input) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/pdf");
        myDir.mkdirs();

        String fname = "d.pdf";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream output = new FileOutputStream(file);


         try {
                try {
                    byte[] buffer = new byte[4 * 1024]; // or other buffer size
                    int read;

                    while ((read = input.read(buffer)) != -1) {
                        output.write(buffer, 0, read);
                    }
                    output.flush();
                } finally {
                    output.close();
                }
            } catch (Exception e) {
                e.printStackTrace(); // handle exception, define IOException and others
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);




       // Uri uri = data.getData();
/*

        try
        {
            readTextFromUri(uri);
            //System.out.println("path"+);

        }catch (Exception e)
        {

        }
*/

        if(requestCode == RESULT_LOAD_IMAGE_PHOTOGRAPH && resultCode == RESULT_OK && null != data)
        {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String browerphoto = cursor.getString(columnIndex);
            cursor.close();

            Uri selectedImageURI = data.getData();
          //  File imageFile = new File(getRealPathFromURI(selectedImageURI));

            float a = FileSize(data.getData()) / (1024 * 1024);


           // float a=imageFile.length()/(1024*1024);

            if(a<=4.8)
            {


                bitmapphotouri = data.getData();
               // bitmapphoto= BitmapFactory.decodeFile(browerphoto);

                ((TextView)findViewById(R.id.tvscan_photo_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_photo_flag)).setBackgroundResource(R.drawable.icon_checkmark);


            }else
            {

                //bitmapphoto= null;
                bitmapphotouri=null;
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                        PhotoGraphActivity.this);

                // set title


                // set dialog message
                alertDialogBuilder
                        .setMessage("File size should be less than 5 MB")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        });


                // create alert dialog
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);

            }
            System.out.println("images"+a);

//            photo_flag="Y";


            //((ImageView)findViewById(R.id.imgtest)).setImageBitmap(bitmap);

        }



        if(requestCode == PICK_FROM_CAMERA && resultCode == RESULT_OK )
        {

            setcrop();

        }


        if(requestCode==10)
        {

           // Toast.makeText(getApplicationContext(),"save",Toast.LENGTH_LONG).show();

try {
    String root = Environment.getExternalStorageDirectory().toString();
    File myDir = new File(root + "/temp");
    String fname = "p.jpg";
    File file = new File(myDir, fname);

    bitmapphotouri=Uri.fromFile(file);
   // bitmapphotouri=Uri.parse(file.toString());
   // bitmapphoto = BitmapFactory.decodeFile(file.getAbsolutePath());


    ((TextView) findViewById(R.id.tvscan_photo_flag)).setVisibility(View.VISIBLE);
    ((TextView) findViewById(R.id.tvscan_photo_flag)).setBackgroundResource(R.drawable.icon_checkmark);


/*

    if (file.exists())
        file.delete();
*/


}catch (Exception e)
{

}



        }



        if(requestCode == PICK_FROM_CAMERA &&resultCode == RESULT_CANCELED)
        {
            // Toast.makeText(getApplicationContext(),"cancle",Toast.LENGTH_LONG).show();
        }
        if(requestCode == PICK_FROM_FILE && resultCode == RESULT_OK )
        {
            mImageCaptureUri = data.getData();
        }


        if(requestCode == CROP_FROM_CAMERA && resultCode == RESULT_OK )
        {
            Bundle extras = data.getExtras();

            if (extras != null) {
                //Bitmap photo = extras.getParcelable("data");

                // ((ImageView)findViewById(R.id.opacity)).setImageBitmap(photo);


                  //  bitmapphoto=(Bitmap) data.getExtras().get("data");


                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/saved_images");
                String fname = "Image-photograp.jpg";
                File file = new File (myDir, fname);


                bitmapphoto = BitmapFactory.decodeFile(file.getAbsolutePath());


                    ((TextView)findViewById(R.id.tvscan_photo_flag)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.tvscan_photo_flag)).setBackgroundResource(R.drawable.icon_checkmark);
                //SaveImage(bitmapphoto);


                ((ImageView)findViewById(R.id.imgoutput)).setImageBitmap(bitmapphoto);

//                bitmapphoto



//                    SendHttpRequestTask t = new SendHttpRequestTask();
//                    String[] params = new String[]{url, myPrefs.getString("UserId", "").toString()};
//                    t.execute(params);
            }

            File f = new File(mImageCaptureUri.getPath());

            if (f.exists())
                f.delete();

        }
    }



    public void setcrop()
    {


        Intent intent=new Intent(PhotoGraphActivity.this,CropImageActivity.class);
        intent.putExtra("name","p");
        startActivityForResult(intent, 10);
    }






    private void doCrop() {
        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();
        /**
         * Open image crop app by starting an intent
         * �com.android.camera.action.CROP�.
         */
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        /**
         * Check if there is image cropper app installed.
         */
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(
                intent, 0);

        int size = list.size();

        /**
         * If there is no image cropper app, display warning message
         */
        if (size == 0) {

            Toast.makeText(this, "Can not find image crop app",
                    Toast.LENGTH_SHORT).show();

            return;
        } else {
            /**
             * Specify the image path, crop dimension and scale
             */
            intent.setData(mImageCaptureUri);

           intent.putExtra("outputX", 768);
            intent.putExtra("outputY", 1280);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scaleUpIfNeeded",false);
            intent.putExtra("crop","true");
            intent.putExtra("scale", true);
            intent.putExtra("return-data", false);
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/saved_images");
            myDir.mkdirs();
            //Random generator = new Random();
            //int n = 10000;
            //n = generator.nextInt(n);
            String fname = "Image-photograp.jpg";
            File file = new File (myDir, fname);
            if (file.exists ()) file.delete ();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
            /**
             * There is posibility when more than one image cropper app exist,
             * so we have to check for it first. If there is only one app, open
             * then app.
             */

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);
                i.setComponent(new ComponentName(res.activityInfo.packageName,
                        res.activityInfo.name));

                startActivityForResult(i, CROP_FROM_CAMERA);
            } else {
                /**
                 * If there are several app exist, create a custom chooser to
                 * let user selects the app.
                 */
                for (ResolveInfo res : list) {
                    final CropOption co = new CropOption();

                    co.title = getPackageManager().getApplicationLabel(
                            res.activityInfo.applicationInfo);
                    co.icon = getPackageManager().getApplicationIcon(
                            res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);

                    co.appIntent
                            .setComponent(new ComponentName(
                                    res.activityInfo.packageName,
                                    res.activityInfo.name));

                    cropOptions.add(co);
                }

                CropOptionAdapter adapter = new CropOptionAdapter(
                        getApplicationContext(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose Crop App");
                builder.setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                startActivityForResult(
                                        cropOptions.get(item).appIntent,
                                        CROP_FROM_CAMERA);
                            }
                        });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        try{

                            if (mImageCaptureUri != null) {
                                getContentResolver().delete(mImageCaptureUri, null,
                                        null);
                                mImageCaptureUri = null;
                            }

                        }catch (Exception e)
                        {
                            System.out.println("Cancel Error"+e);
                        }


                    }
                });

                AlertDialog alert = builder.create();

                alert.show();
            }
        }
    }


/*


//Connection callback - on successful connection

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "in onConnected() - we're connected, let's do the work in the background...");
//        build an intent that we'll use to start the open file activity
        */
/*IntentSender intentSender = Drive.DriveApi
                .newOpenFileActivityBuilder()
//                these mimetypes enable these folders/files types to be selected
                .setMimeType(new String[] { DriveFolder.MIME_TYPE, "text/plain", "image/png"})
                .build(googleApiClient);
        try {
            startIntentSenderForResult(
                    intentSender, REQUEST_CODE_SELECT, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            Log.i(TAG, "Unable to send intent", e);
        }*//*

    }



//Connection callback - Called when the client is temporarily in a disconnected state

    @Override
    public void onConnectionSuspended(int i) {
        switch (i) {
            case 1:
                Log.i(TAG, "Connection suspended - Cause: " + "Service disconnected");
                break;
            case 2:
                Log.i(TAG, "Connection suspended - Cause: " + "Connection lost");
                break;
            default:
                Log.i(TAG, "Connection suspended - Cause: " + "Unknown");
                break;
        }
    }

//connection failed callback - Called when there was an error connecting the client to the service

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed - result: " + result.toString());
        if (!result.hasResolution()) {
//            display error dialog
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
            return;
        }

        try {
            Log.i(TAG, "trying to resolve the Connection failed error...");
//            tries to resolve the connection failure by trying to restart this activity
            result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
        } catch (IntentSender.SendIntentException e) {
            Log.i(TAG, "Exception while starting resolution activity", e);
        }
    }

//build the google api client

    private void buildGoogleApiClient() {
        Log.i(TAG, "Building the client");
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
    }


//connect client to Google Play Services

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "In onStart() - connecting...");
        googleApiClient.connect();
    }

//close connection to Google Play Services

    @Override
    protected void onStop() {
        super.onStop();
        if (googleApiClient != null) {
            Log.i(TAG, "In onStop() - disConnecting...");
            googleApiClient.disconnect();
        }
    }
*/



    protected int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else {
            return data.getByteCount();
        }
    }




    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    private void readTextFromUri(Uri uri) throws IOException {
        InputStream inputStream = getContentResolver().openInputStream(uri);
        Savefile(inputStream);
        //return stringBuilder.toString();
    }




    private long FileSize(Uri fileUri)
    {
        // Uri fileUri = data.getData();
        long size;

        if(fileUri.getScheme().equalsIgnoreCase("file"))
        {
            //Toast.makeText(getApplicationContext(),"file",Toast.LENGTH_LONG).show();

            File file=new File(fileUri.getPath());
            size=file.length();

        }else
        {

            Cursor cursor = PhotoGraphActivity.this.getContentResolver().query(fileUri,
                    null, null, null, null);
            cursor.moveToFirst();
            size = cursor.getLong(cursor.getColumnIndex(OpenableColumns.SIZE));
            cursor.close();


        }


        //Toast.makeText(getApplicationContext(),"content"+size,Toast.LENGTH_LONG).show();
        return size;

    }




}
