package com.queppelin.universal;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Window;


import com.tool.HttpclasswithoutThrade;
import com.tool.StaticMethod;
import com.viewpager.CirclePageIndicator;
import com.viewpager.TestFragmentAdapter;

import org.json.JSONArray;
import org.json.JSONObject;


public class FirstTimeinfoactivity extends FragmentActivity {



    TestFragmentAdapter mAdapter;
    ViewPager mPager;
    CirclePageIndicator mIndicator;
    private SharedPreferences mPrefs;

    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_first_timeinfoactivity);
        mAdapter = new TestFragmentAdapter(getSupportFragmentManager());
        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);
        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        if(StaticMethod.setting_flag.equalsIgnoreCase("N"))
        {
            if(StaticMethod.isInternetAvailable(FirstTimeinfoactivity.this)) {
                new GetsettingOrignal(getApplicationContext()).execute();
            }else
            {
                return;
            }
        }


    }

    private class GetsettingOrignal extends AsyncTask<Void, Void, String[]>
    {

        String mgetThingsResponse;


        Context context;

        JSONArray jsonarray;

        public GetsettingOrignal(Context context) {
            super();
            this.context=context;


        }
        @Override
        protected String[] doInBackground(Void... params) {

            String [] mStrings=null;

            try {

                String[] pro_key = new String[]{};
                String[] pro_value = new String[]{};
                mgetThingsResponse =(new HttpclasswithoutThrade(context,"getSettings", RestClient.RequestMethod.GET,pro_key,pro_value
                )).getResponsedetails();






            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return mStrings;
        }
        @Override
        protected void onPostExecute(String[] result) {

            try
            {
                System.out.println("getSettings_firsttime:"+mgetThingsResponse);

                if(!mgetThingsResponse.equalsIgnoreCase(""))
                {
                    JSONObject object=new JSONObject(mgetThingsResponse);

                    String email=object.getString("admin_email");
                    String mobile=object.getString("admin_telephone");//admin_mobile
                    String mobile2=object.getString("admin_telephone_2");

                    SharedPreferences.Editor prefsEditor = mPrefs
                            .edit();
                    prefsEditor
                            .putString("admin_email",email);
                    prefsEditor.putString("admin_number", mobile);
                    prefsEditor.putString("admin_number2", mobile2);

                    prefsEditor.putString("experience_title", object.getString("experience_title"));
                    prefsEditor.putString("experience_description", object.getString("experience_description"));
                    prefsEditor.putString("ten_thing_title", object.getString("ten_thing_title"));
                    prefsEditor.putString("ten_thing_desc", object.getString("ten_thing_desc"));

                    prefsEditor.putString("bannerArray", object.getJSONArray("banner").toString());
                    prefsEditor.putString("AppSlideshow", object.getJSONArray("AppSlideshow").toString());

                    prefsEditor.commit();


                    StaticMethod.setting_flag="Y";
                }




            }catch (Exception e)
            {


                StaticMethod.setting_flag="N";
                System.out.println("error"+e);

            }



            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
        }
    }


}


