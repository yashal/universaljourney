package com.queppelin.universal;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.image.loader.ImageLoader;
import com.tool.AppConfig;
import com.tool.HttpClass;
import com.tool.HttpClassVersion;
import com.tool.StaticMethod;
import com.viewpager.CirclePageIndicator;
import com.viewpager.ImageSliderAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class ExpirenceDetailsActivity extends AppCompatActivity {

    String product_id;

    public int currentimageindex=0;
    Timer timer;
    TimerTask task;
    ImageView slidingimage;
    ImageLoader imageLoader;

    private SharedPreferences mPrefs;

    ImageSliderAdapter mAdapter;
    ViewPager mPager;
    CirclePageIndicator mIndicator;
    String jsonArray;
    Typeface font;
    String user_id;
    String action_flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_expirence_details);
        font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
        imageLoader=new ImageLoader(this);
        product_id=getIntent().getExtras().getString("product_id");

        StaticMethod.mLogin_flag="N";
        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);


        action_flag=getIntent().getExtras().getString("action_flag");

        if(action_flag.equalsIgnoreCase("Y"))
        {
            try {

                    String message=getIntent().getExtras().getString("marry").toString();

                       JSONObject jb=new JSONObject(message);

                if(jb.getString("msgType").equalsIgnoreCase("broadcast"))
                {
                    if(StaticMethod.isInternetAvailable(this)) {


                        getPackage();
                        new SetReadThreadOnNotification(ExpirenceDetailsActivity.this,getIntent().getExtras().getString("message_id"),getIntent().getExtras().getString("marry").toString()).execute();

                    }else
                    {
                        return;
                    }




                }else
                {

                    if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
                    {





                        if(StaticMethod.isInternetAvailable(this)) {


                            getPackage();

                            new SetReadThreadOnNotification(ExpirenceDetailsActivity.this, getIntent().getExtras().getString("message_id"), getIntent().getExtras().getString("marry").toString()).execute();
                        }else
                        {
                            return;
                        }


                    }else
                    {
                        Intent intent=new Intent(ExpirenceDetailsActivity.this,LoginActivity.class);
                        intent.putExtra("action_flag","Y");
                        startActivity(intent);
                        finish();
                    }
                }



                  //  new SetReadThreadOnNotification(ExpirenceDetailsActivity.this,getIntent().getExtras().getString("message_id"),getIntent().getExtras().getString("marry").toString()).execute();


            }catch (Exception e)
            {

            }
        }else
        {

            if(StaticMethod.isInternetAvailable(this)) {


                getPackage();
            }else
            {
                return;
            }
        }
        NotificationManager manager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
        manager.cancel(Integer.parseInt(product_id));

        System.out.println("Product_id"+product_id);

       // StaticMethod.notifiaction_text.setText("2");

        ((TextView)findViewById(R.id.tv_inclusions)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_inclusions)).setTypeface(font);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(actionBar.getDisplayOptions()
                | ActionBar.DISPLAY_SHOW_CUSTOM);

        LinearLayout layout = new LinearLayout(actionBar.getThemedContext());
        layout.setOrientation(LinearLayout.HORIZONTAL);

        ImageView imageViewlogin = new ImageView(actionBar.getThemedContext());
        imageViewlogin.setScaleType(ImageView.ScaleType.CENTER);
        imageViewlogin.setImageResource(R.drawable.ic_menu_login);

        ImageView imageViewLogout = new ImageView(actionBar.getThemedContext());
        imageViewLogout.setScaleType(ImageView.ScaleType.CENTER);
        imageViewLogout.setImageResource(R.drawable.ic_logout);
        imageViewLogout.setVisibility(View.VISIBLE);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
                | Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin = 10;
        //imageViewLogout.setLayoutParams(layoutParams);





        ImageView imageViewmytrip = new ImageView(actionBar.getThemedContext());
        imageViewmytrip.setScaleType(ImageView.ScaleType.CENTER);
        imageViewmytrip.setImageResource(R.drawable.ic_menu_trips);

        //imageViewmytrip.setLayoutParams(layoutParams);





        imageViewlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), LoginActivity.class);
                intent.putExtra("action_flag","N");
                intent.putExtra("position",0);
                startActivity(intent);

            }
        });



        imageViewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Toast.makeText(getApplicationContext(),"on logout",Toast.LENGTH_LONG).show();




                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        ExpirenceDetailsActivity.this);
                alertDialogBuilder
                        .setMessage("Do you want to logout?")
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                String userValue=mPrefs.getString("userObject","").toString();
                                String user_id;
                                try
                                {
                                    JSONObject object=new JSONObject(userValue);
                                    JSONObject userObject=object.getJSONObject("result");
                                    user_id=userObject.getString("customer_id");//customer_id
                                    String[] pro_key = new String[]{"deviceId","userId","deviceType"};
                                    String[] pro_value = new String[]{mPrefs.getString("deviceId",""),user_id, AppConfig.DEVIECS_TYPE};
                                    new HttpClassVersion(ExpirenceDetailsActivity.this,"setPushLogoutCustomer", RestClient.RequestMethod.POST,pro_key,pro_value
                                    ).start();
                                    StaticMethod.returnProgressBar(
                                            ExpirenceDetailsActivity.this).setOnDismissListener(
                                            new DialogInterface.OnDismissListener() {
                                                public void onDismiss(DialogInterface dialog) {
                                                    if (HttpClassVersion.ResponseVector != null && !HttpClassVersion.ResponseVector.equalsIgnoreCase("")) {


                                                        try {

                                                            System.out.println("out_new"+HttpClassVersion.ResponseVector);

                                                            JSONObject jsonObject=new JSONObject(HttpClassVersion.ResponseVector);

                                                            if(jsonObject.has("success")) {

                                                                boolean type=jsonObject.getBoolean("success");
                                                                if(type) {


                                                        SharedPreferences.Editor prefsEditor = mPrefs
                                                                .edit();
                                                        prefsEditor
                                                                .putString("userObject","");
                                                        prefsEditor.putString("isLogin", "N");
                                                        prefsEditor.commit();


                                                        for(int i=0;i< StaticMethod.state.size();i++)
                                                        {
                                                            StaticMethod.state.get(i).finish();
                                                        }
                                                        Intent intent=new Intent(getApplicationContext(),NewDashboardActivity.class);
                                                        startActivity(intent);
                                                        finish();

                                                                }else
                                                                {
                                                                    Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                                                                }
                                                            }
                                                        }catch (Exception e)
                                                        {

                                                        }





                                                    }
                                                }
                                            });


                                }catch (Exception e)
                                {

                                }




                            }
                        })
                        .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);



            }
        });



        imageViewmytrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), MyTripsActivity.class);
                intent.putExtra("position",0);
                startActivity(intent);


            }
        });

        layout.setLayoutParams(layoutParams);
       /* layout.addView(imageViewlogin);
        layout.addView(imageViewLogout);
        layout.addView(imageViewmytrip);

*/


        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
        {
            layout.addView(imageViewLogout);
            //layout.addView(imageViewmytrip);

        }else
        {
            layout.addView(imageViewlogin);
        }






        actionBar.setCustomView(layout);





        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);



    }

    public void getPackage()
    {
        String[] pro_key = new String[]{"product_id"};
        String[] pro_value = new String[]{product_id};
        new HttpClassVersion(ExpirenceDetailsActivity.this,"getPackage", RestClient.RequestMethod.POST,pro_key,pro_value
        ).start();
        StaticMethod.returnProgressBar(
                ExpirenceDetailsActivity.this).setOnDismissListener(
                new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        if (HttpClassVersion.ResponseVector != null && !HttpClassVersion.ResponseVector.equalsIgnoreCase("")) {

                            // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                            // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                            //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                            try {

                                System.out.println("getBlog"+HttpClassVersion.ResponseVector);
                                ((LinearLayout)findViewById(R.id.alllayoute)).setVisibility(View.VISIBLE);
                                JSONObject jsonObject=new JSONObject(HttpClassVersion.ResponseVector);
                                // JSONObject jsonRecmandation=jsonObject.getJSONObject("recommended");
                                //   JSONArray jsonArray=jsonObject.getJSONArray("result");

                                //  imageLoader.DisplayImage(jsonRecmandation.getString("image"),((ImageView)findViewById(R.id.imgmorocco)));


                                if(jsonObject.has("images"))
                                {

                                    final JSONArray image_array=jsonObject.getJSONArray("images");
                                    if(image_array.length()>0)
                                    {


                                        mAdapter = new ImageSliderAdapter(getSupportFragmentManager(),image_array.toString());

                                        mPager = (ViewPager)findViewById(R.id.pager);
                                        mPager.setAdapter(mAdapter);

                                        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
                                        mIndicator.setViewPager(mPager);


                                        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                            @Override
                                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


                                            }

                                            @Override
                                            public void onPageSelected(int position) {



//                                                ((TextView)findViewById(R.id.tv_image_count)).setText((position+1)+"/"+image_array.length());

                                            }

                                            @Override
                                            public void onPageScrollStateChanged(int state) {

                                            }
                                        });




                                        //
                                        //
                                        // ImageSlider(image_array);
                                    }

                                }
                                setLayout(jsonObject);

                            } catch (Exception e) {
                                System.err.println(e);
                            }


                        }
                    }
                });

    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
      //  getMenuInflater().inflate(R.menu.menu_expirence_details, menu);



        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
        {
            getMenuInflater().inflate(R.menu.after_login, menu);
        }else
        {
            getMenuInflater().inflate(R.menu.before_login, menu);
            menu.findItem(R.id.menu_mytrip).setVisible(false);
        }



        return true;
    }
*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id==android.R.id.home)
        {
            if(action_flag.equalsIgnoreCase("Y"))
            {
                Intent intent1=new Intent(ExpirenceDetailsActivity.this,NewDashboardActivity.class);
                startActivity(intent1);
                finish();

            }else
            {
                finish();
            }
        }

        if(id==R.id.menu_logout)
        {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    ExpirenceDetailsActivity.this);
            alertDialogBuilder
                    .setMessage("Do you want to logout?")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {

                            SharedPreferences.Editor prefsEditor = mPrefs
                                    .edit();
                            prefsEditor
                                    .putString("userObject","");
                            prefsEditor.putString("isLogin", "N");
                            prefsEditor.commit();

                            for(int i=0;i< StaticMethod.state.size();i++)
                            {
                                StaticMethod.state.get(i).finish();
                            }
                            Intent intent=new Intent(getApplicationContext(),NewDashboardActivity.class);
                            startActivity(intent);
                            finish();

                        }
                    })
                    .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTypeface(font);

        }
        if(id==R.id.menu_mytrip)
        {


            intent=new Intent(getApplicationContext(), MyTripsActivity.class);
            intent.putExtra("position",0);
            startActivity(intent);

        }
        if(id==R.id.menu_login)
        {
            intent=new Intent(getApplicationContext(), LoginActivity.class);
            intent.putExtra("position",0);
            intent.putExtra("action_flag","N");
            startActivity(intent);
        }




        return super.onOptionsItemSelected(item);
    }


    Boolean[] attribute_flag;

    ArrayList<WebView> openrow;
    ArrayList<ImageView> openimage;

    public void setLayout(final JSONObject jsonObject)
    {
        openrow=new ArrayList<>();
        openimage=new ArrayList<>();
        try
        {



            ((TextView)findViewById(R.id.tv_expirence_title)).setText(Html.fromHtml(jsonObject.getString("name")));
            ((TextView)findViewById(R.id.tv_expirence_title)).setTypeface(font);

           // ((TextView)findViewById(R.id.tv_image_count)).setText(Html.fromHtml(jsonObject.getString("name")));
            ((TextView)findViewById(R.id.tv_top_dec)).setText(Html.fromHtml(jsonObject.getString("description")));

            ((TextView)findViewById(R.id.tv_top_dec)).setTypeface(font);
            LinearLayout inclusion_layout=null;
            inclusion_layout=(LinearLayout)findViewById(R.id.linclusion);
            inclusion_layout.removeAllViews();
            View circle_p;

            JSONArray jsonInclu=jsonObject.getJSONArray("inclusions");
            for(int i=0;i<jsonInclu.length();i++)
            {
                circle_p=getLayoutInflater().inflate(R.layout.black_circle_row, null);
               String temp= jsonInclu.getJSONObject(i).getString("icon");




try {
    Resources res = getResources();
    String mDrawableName = temp.replace("-", "_");
    int resID = res.getIdentifier(mDrawableName, "drawable", getPackageName());
    Drawable drawable = res.getDrawable(resID);
    ColorFilter filter = new LightingColorFilter(Color.WHITE, Color.WHITE);
    drawable.setColorFilter(filter);

    ((ImageView) circle_p.findViewById(R.id.imginclusion)).setImageDrawable(drawable);
}catch (Exception e)
{

}
                /*
                if(temp.equalsIgnoreCase("ico-plain"))
                {


                    Drawable myIcon = getResources().getDrawable( R.drawable.ico_plain );
                    ColorFilter filter = new LightingColorFilter( Color.BLACK, Color.BLACK );
                    myIcon.setColorFilter(filter);
                    ((ImageView)circle_p.findViewById(R.id.imginclusion)).setImageDrawable(myIcon);




                }else if(temp.equalsIgnoreCase("ico-car"))
                {

                    Drawable myIcon = getResources().getDrawable( R.drawable.ico_car );
                    ColorFilter filter = new LightingColorFilter( Color.BLACK, Color.BLACK );
                    myIcon.setColorFilter(filter);
                    ((ImageView)circle_p.findViewById(R.id.imginclusion)).setImageDrawable(myIcon);


                }else if(temp.equalsIgnoreCase("ico-cruise"))
                {

                    Drawable myIcon = getResources().getDrawable( R.drawable.ico_cruise );
                    ColorFilter filter = new LightingColorFilter( Color.BLACK, Color.BLACK );
                    myIcon.setColorFilter(filter);
                    ((ImageView)circle_p.findViewById(R.id.imginclusion)).setImageDrawable(myIcon);


                }else if(temp.equalsIgnoreCase("ico-allmeals"))
                {

                    Drawable myIcon = getResources().getDrawable( R.drawable.ico_allmeals );
                    ColorFilter filter = new LightingColorFilter( Color.BLACK, Color.BLACK );
                    myIcon.setColorFilter(filter);
                    ((ImageView)circle_p.findViewById(R.id.imginclusion)).setImageDrawable(myIcon);



                }else
                {
                    Drawable myIcon = getResources().getDrawable( R.drawable.ico_plain);
                    ColorFilter filter = new LightingColorFilter( Color.BLACK, Color.BLACK );
                    myIcon.setColorFilter(filter);
                    ((ImageView)circle_p.findViewById(R.id.imginclusion)).setImageDrawable(myIcon);

                }

*/




                inclusion_layout.addView(circle_p);
            }








            LinearLayout attributelayout=null;
            attributelayout=(LinearLayout)findViewById(R.id.renaming_layout);
            attributelayout.removeAllViews();


           final JSONArray jsonAtrribute=jsonObject.getJSONArray("attributes");


            attribute_flag=new Boolean[jsonAtrribute.length()];

            for(int i=0;i<jsonAtrribute.length();i++)
            {
                final View parent=getLayoutInflater().inflate(R.layout.attribute_row, null);
                parent.setId(i);


                attribute_flag[i]=true;

                ((RelativeLayout)parent.findViewById(R.id.rclick)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {






                       // Toast.makeText(getApplicationContext(),"onclick",Toast.LENGTH_LONG).show();


                        for(int a=0;a<openrow.size();a++)
                        {
                            openrow.get(a).setVisibility(View.GONE);
                            openimage.get(a).setBackgroundResource(R.drawable.ic_plus);

                        }


                        if(attribute_flag[parent.getId()])
                        {


                            attribute_flag[parent.getId()]=false;
                            ((ImageView)parent.findViewById(R.id.imgplusminus)).setBackgroundResource(R.drawable.ic_minus);
                            openrow.add((WebView) parent.findViewById(R.id.webview));
                            openimage.add(((ImageView)parent.findViewById(R.id.imgplusminus)));
                            ((WebView)parent.findViewById(R.id.webview)).setVisibility(View.VISIBLE);

                        }else
                        {


                            attribute_flag[parent.getId()]=true;
                            ((ImageView)parent.findViewById(R.id.imgplusminus)).setBackgroundResource(R.drawable.ic_plus);
                            openrow.remove((WebView)parent.findViewById(R.id.webview));
                            openimage.remove(((ImageView) parent.findViewById(R.id.imgplusminus)));
                            ((WebView)parent.findViewById(R.id.webview)).setVisibility(View.GONE);
                        }



                    }
                });

                ((TextView)parent.findViewById(R.id.tvtitle)).setText(Html.fromHtml(jsonAtrribute.getJSONObject(i).getString("name")));

                ((TextView)parent.findViewById(R.id.tvtitle)).setTypeface(font);



                String des=jsonAtrribute.getJSONObject(i).getString("html").trim();



                String pish = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/Nunito_Light.ttf\")}body {font-family: MyFont;font-size: medium;text-align: justify;}</style></head><body>";
                String pas = "</body></html>";
                String myHtmlString = pish + des + pas;
              //  wv.loadDataWithBaseURL(null,myHtmlString, "text/html", "UTF-8", null);


                ((WebView)parent.findViewById(R.id.webview)).loadDataWithBaseURL(null,myHtmlString , "text/html",
                        "UTF-8", null);


                if(i==0)
                {
                    ((WebView)parent.findViewById(R.id.webview)).setVisibility(View.VISIBLE);

                    attribute_flag[i]=false;
                    ((ImageView)parent.findViewById(R.id.imgplusminus)).setBackgroundResource(R.drawable.ic_minus);
                    openrow.add((WebView) parent.findViewById(R.id.webview));
                    openimage.add(((ImageView)parent.findViewById(R.id.imgplusminus)));


                }else
                {
                    ((WebView)parent.findViewById(R.id.webview)).setVisibility(View.GONE);
                }





                attributelayout.addView(parent);
            }




            setSupport();

        }catch (Exception e)
        {

            System.out.println("Errorp"+e);

        }


    }


    public void ImageSlider(final JSONArray jsonArray)
    {
        try
        {

            final Handler mHandler = new Handler();

            // Create runnable for posting
            final Runnable mUpdateResults = new Runnable() {
                public void run() {

                    AnimateandSlideShow(jsonArray);

                }
            };

            int delay = 1000; // delay for 1 sec.

            int period = 8000; // repeat every 4 sec.

            Timer timer = new Timer();

            timer.scheduleAtFixedRate(new TimerTask() {

                public void run() {

                    mHandler.post(mUpdateResults);

                }

            }, delay, period);


        }catch (Exception e)
        {

        }


    }

    private void AnimateandSlideShow(JSONArray jsonArray) {

        slidingimage = (ImageView)findViewById(R.id.img_banner);
     //   slidingimage.setImageResource(IMAGE_IDS[currentimageindex%IMAGE_IDS.length]);

        try
        {

            imageLoader.DisplayImage(jsonArray.getString(currentimageindex%jsonArray.length()),slidingimage);



            currentimageindex++;
           // ((TextView)findViewById(R.id.tv_image_count)).setText((currentimageindex)%jsonArray.length()+1+"/"+jsonArray.length());

        }catch (Exception e)
        {

        }


        Animation rotateimage = AnimationUtils.loadAnimation(this, R.anim.righttoleft);

        slidingimage.startAnimation(rotateimage);

    }


    public void setSupport()
    {

        ((EditText) findViewById(R.id.etbphone)).setTypeface(font);


        View view = ExpirenceDetailsActivity.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

     final   String admin_number=mPrefs.getString("admin_number","").toString();
        ((TextView)findViewById(R.id.tvnumber)).setText(admin_number);
        ((TextView)findViewById(R.id.tvnumber)).setTypeface(font);

        ((TextView)findViewById(R.id.tvcallat)).setText("Call us at-"+admin_number);
        ((TextView)findViewById(R.id.tvcallat)).setTypeface(font);

        ((TextView)findViewById(R.id.tvcallat)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                Intent intent1 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + admin_number));
                startActivity(intent1);

            }
        });


        try
        {



            if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y")) {

                String userObjet = mPrefs.getString("userObject", "");
                JSONObject userJson = new JSONObject(userObjet);

                JSONObject userinfo = null;

                if (userJson.has("user")) {

                    userinfo = userJson.getJSONObject("user");
                } else if (userJson.has("result")) {
                    userinfo = userJson.getJSONObject("result");
                }


                ((EditText) findViewById(R.id.etbphone)).setText(userinfo.getString("telephone"));


                if (userinfo.getString("telephone").length() >= 8 && userinfo.getString("telephone").length() <= 15) {


                    //((Button) findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_active));
                    ((Button) findViewById(R.id.tvcallme)).setEnabled(true);
                    ((Button)findViewById(R.id.tvcallme)).setTypeface(font);

                } else {


                    ((Button) findViewById(R.id.tvcallme)).setEnabled(false);
                    //((Button) findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_hide));
                    ((Button)findViewById(R.id.tvcallme)).setTypeface(font);
                }


            }else
            {
                ((EditText)findViewById(R.id.etbphone)).setText("");
                //((Button) findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_hide));
                ((Button) findViewById(R.id.tvcallme)).setEnabled(false);
                ((Button)findViewById(R.id.tvcallme)).setTypeface(font);

            }


        }catch (Exception e)
        {
            ((EditText)findViewById(R.id.etbphone)).setText("");
        }





            ((EditText)findViewById(R.id.etbphone)).addTextChangedListener(passwordWatcher);





                ((Button) findViewById(R.id.tvcallme)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        try {

                            String getMobileNumber = ((EditText) findViewById(R.id.etbphone)).getText().toString();


                            if (!getMobileNumber.equalsIgnoreCase("")) {

                                if (getMobileNumber.length() >= 8 && getMobileNumber.length() <= 15) {


                                    getCallMe(getMobileNumber);

                                } else {

                                    // Toast.makeText(getApplicationContext(), "false", Toast.LENGTH_LONG).show();
                                }

                            } else {
                                // Toast.makeText(getApplicationContext(), "Please enter mobile no.", Toast.LENGTH_LONG).show();
                            }


                        } catch (Exception e) {

                        }


                    }
                });


    }



    public void getCallMe(String number)
    {

        String[] pro_key = new String[]{"mobile","entity","entity_id","deviceType"};
        String[] pro_value = new String[]{number,"product",product_id,"ANDROID"};//user_id
        new HttpClass(ExpirenceDetailsActivity.this,"registerCall", RestClient.RequestMethod.POST,pro_key,pro_value
        ).start();
        StaticMethod.returnProgressBar(
                ExpirenceDetailsActivity.this).setOnDismissListener(
                new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        if (HttpClass.ResponseVector != null && !HttpClass.ResponseVector.equalsIgnoreCase("")) {

                            // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                            // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                            //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                            try {

                                System.out.println("registerCall"+HttpClass.ResponseVector);

                                JSONObject jsonObject=new JSONObject(HttpClass.ResponseVector);

                                String message="";

                                if(jsonObject.has("success"))
                                {


//                                    Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();

                                    message=jsonObject.getString("message");






                                }else if(jsonObject.has("error"))
                                {

                                    message=jsonObject.getString("Message");
                                    //Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_LONG).show();


                                }

                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                        ExpirenceDetailsActivity.this);

                                // set title


                                // set dialog message
                                alertDialogBuilder
                                        .setMessage(message)
                                        .setCancelable(false)
                                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {



                                            }
                                        });


                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                msgTxt.setTypeface(font);




                            } catch (Exception e) {
                                System.err.println(e);
                            }

                        }
                    }
                });



    }


    private final TextWatcher passwordWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           // textView.setVisibility(View.VISIBLE);
        }

        public void afterTextChanged(Editable s) {
            if (s.length() >= 8&&s.length()<=15) {

                ((Button) findViewById(R.id.tvcallme)).setEnabled(true);
                //((Button)findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_active));
                ((Button)findViewById(R.id.tvcallme)).setTypeface(font);

            } else{

                ((Button) findViewById(R.id.tvcallme)).setEnabled(false);
                //((Button)findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_hide));
                ((Button)findViewById(R.id.tvcallme)).setTypeface(font);
            }
        }
    };


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(action_flag.equalsIgnoreCase("Y"))
        {
            Intent intent=new Intent(ExpirenceDetailsActivity.this,NewDashboardActivity.class);
            startActivity(intent);
            finish();

        }else
        {
            super.onBackPressed();
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {

            startActivity(getIntent());
            finish();
        }
    }





}
