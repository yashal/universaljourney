package com.queppelin.universal;




import android.content.Context;
import android.content.Intent;

public final class CommonUtilities {
	
	// give your server registration url here
    static final String SERVER_URL = "http://www.umustakeholidays.com/api/v2/registerDevice";


   // static final String SERVER_URL = "http://staging.umustakeholidays.com/api/v2/registerDevice";

    // Google project id
    public static final String SENDER_ID = "138253874982"; //138253874982 for Piron tech.//663601561745 for flexiguru

    //for flexiguru    663601561745
    
    /**
     * Tag used on log messages.
     */
    static final String TAG = "Univesal GCM";

  public   static final String DISPLAY_MESSAGE_ACTION =
            "com.queppelin.universal.DISPLAY_MESSAGE";

    public static final String EXTRA_MESSAGE = "message";



    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
   public  static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
}
