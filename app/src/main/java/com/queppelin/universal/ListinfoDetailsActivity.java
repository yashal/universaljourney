package com.queppelin.universal;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.image.loader.ImageLoader;
import com.tool.AppConfig;
import com.tool.HttpClass;
import com.tool.HttpClassVersion;
import com.tool.StaticMethod;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListinfoDetailsActivity extends AppCompatActivity {


    String cid;
    ImageLoader imageLoader;
    JSONArray jsonArrayOriginally;
    JSONArray jsonArrayCopy;

    JSONArray FilterArray;


    Boolean flag_back=false;

    public static ArrayList<String> selection_flag;
    public static ArrayList<Integer> selection_day_flag;
    private SharedPreferences mPrefs;

    Typeface font;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listinfo_details);

        imageLoader=new ImageLoader(ListinfoDetailsActivity.this);
        font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");

        ((TextView)findViewById(R.id.tvnodatafound)).setTypeface(font);



        selection_flag=new ArrayList<>();
        selection_day_flag=new ArrayList<>();

        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);



        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(actionBar.getDisplayOptions()
                | ActionBar.DISPLAY_SHOW_CUSTOM);

        LinearLayout layout = new LinearLayout(actionBar.getThemedContext());
        layout.setOrientation(LinearLayout.HORIZONTAL);
        getSupportActionBar().setTitle(getIntent().getExtras().getString("titile"));




        ImageView imageViewlogin = new ImageView(actionBar.getThemedContext());
        imageViewlogin.setScaleType(ImageView.ScaleType.CENTER);
        imageViewlogin.setImageResource(R.drawable.ic_menu_login);




        ImageView imageViewLogout = new ImageView(actionBar.getThemedContext());
        imageViewLogout.setScaleType(ImageView.ScaleType.CENTER);
        imageViewLogout.setImageResource(R.drawable.ic_logout);
        imageViewLogout.setVisibility(View.VISIBLE);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
                | Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin = 10;
        //imageViewLogout.setLayoutParams(layoutParams);

        ImageView imageViewmytrip = new ImageView(actionBar.getThemedContext());
        imageViewmytrip.setScaleType(ImageView.ScaleType.CENTER);
        imageViewmytrip.setImageResource(R.drawable.ic_menu_trips);

        //imageViewmytrip.setLayoutParams(layoutParams);





        imageViewlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), LoginActivity.class);
                intent.putExtra("action_flag","N");
                intent.putExtra("position",0);
                startActivity(intent);

            }
        });



        imageViewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Toast.makeText(getApplicationContext(),"on logout",Toast.LENGTH_LONG).show();




                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        ListinfoDetailsActivity.this);
                alertDialogBuilder
                        .setMessage("Do you want to logout?")
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                String userValue=mPrefs.getString("userObject","").toString();
                                String user_id;
                                try
                                {
                                    JSONObject object=new JSONObject(userValue);
                                    JSONObject userObject=object.getJSONObject("result");
                                    user_id=userObject.getString("customer_id");//customer_id
                                    String[] pro_key = new String[]{"deviceId","userId","deviceType"};
                                    String[] pro_value = new String[]{mPrefs.getString("deviceId",""),user_id, AppConfig.DEVIECS_TYPE};
                                    new HttpClassVersion(ListinfoDetailsActivity.this,"setPushLogoutCustomer", RestClient.RequestMethod.POST,pro_key,pro_value
                                    ).start();
                                    StaticMethod.returnProgressBar(
                                            ListinfoDetailsActivity.this).setOnDismissListener(
                                            new DialogInterface.OnDismissListener() {
                                                public void onDismiss(DialogInterface dialog) {
                                                    if (HttpClassVersion.ResponseVector != null && !HttpClassVersion.ResponseVector.equalsIgnoreCase("")) {

                                                        try {

                                                            JSONObject jsonObject=new JSONObject(HttpClassVersion.ResponseVector);

                                                            if(jsonObject.has("success")) {

                                                                boolean type=jsonObject.getBoolean("success");
                                                                if(type) {




                                                                    SharedPreferences.Editor prefsEditor = mPrefs
                                                                .edit();
                                                        prefsEditor
                                                                .putString("userObject","");
                                                        prefsEditor.putString("isLogin", "N");
                                                        prefsEditor.commit();


                                                        for(int i=0;i< StaticMethod.state.size();i++)
                                                        {
                                                            StaticMethod.state.get(i).finish();
                                                        }
                                                        Intent intent=new Intent(getApplicationContext(),NewDashboardActivity.class);
                                                        startActivity(intent);
                                                        finish();

                                                    }else
                                                    {
                                                        Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }catch (Exception e)
                                    {

                                    }





                                }
                                                }
                                            });


                                }catch (Exception e)
                                {

                                }




                            }
                        })
                        .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);




            }
        });



        imageViewmytrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), MyTripsActivity.class);
                intent.putExtra("position",0);
                startActivity(intent);


            }
        });

        layout.setLayoutParams(layoutParams);
       /* layout.addView(imageViewlogin);
        layout.addView(imageViewLogout);
        layout.addView(imageViewmytrip);

*/


        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
        {
            layout.addView(imageViewLogout);
//            layout.addView(imageViewmytrip);

        }else
        {
            layout.addView(imageViewlogin);
        }






        actionBar.setCustomView(layout);






        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getIntent().getExtras().getString("titile"));

        ((RelativeLayout)findViewById(R.id.rfilter)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(ListinfoDetailsActivity.this,FilterActivity.class);
                startActivityForResult(intent, 1);

            }
        });



        cid=getIntent().getExtras().getString("catId");


        if(StaticMethod.isInternetAvailable(ListinfoDetailsActivity.this)) {

            String[] pro_key = new String[]{"cid"};
            String[] pro_value = new String[]{cid};
            new HttpClass(ListinfoDetailsActivity.this, "getPackages", RestClient.RequestMethod.POST, pro_key, pro_value
            ).start();
            StaticMethod.returnProgressBar(
                    ListinfoDetailsActivity.this).setOnDismissListener(
                    new DialogInterface.OnDismissListener() {
                        public void onDismiss(DialogInterface dialog) {
                            if (HttpClass.ResponseVector != null && !HttpClass.ResponseVector.equalsIgnoreCase("")) {

                                // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                                // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                                //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                                try {

                                    System.out.println("getPackages" + HttpClass.ResponseVector);

                                    JSONObject jsonObject = new JSONObject(HttpClass.ResponseVector);
                                    // JSONObject jsonRecmandation=jsonObject.getJSONObject("recommended");
                                    jsonArrayOriginally = jsonObject.getJSONArray("result");
                                    jsonArrayCopy = jsonArrayOriginally;
                                    //  imageLoader.DisplayImage(jsonRecmandation.getString("image"),((ImageView)findViewById(R.id.imgmorocco)));


                                    if (jsonArrayOriginally.length() > 0) {

                                        ((RelativeLayout) findViewById(R.id.rnotresult)).setVisibility(View.GONE);
                                        setlayout(jsonArrayOriginally);
                                        flag_back = false;


                                    } else {
                                        ((RelativeLayout) findViewById(R.id.rnotresult)).setVisibility(View.VISIBLE);
                                        flag_back = true;
                                    }


                                } catch (Exception e) {
                                    System.err.println(e);
                                }


                            }
                        }
                    });


        }else
        {
            return;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

       /* mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
        {
            getMenuInflater().inflate(R.menu.after_login, menu);
        }else
        {
            getMenuInflater().inflate(R.menu.before_login, menu);
            menu.findItem(R.id.menu_mytrip).setVisible(false);
        }
*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id==android.R.id.home)
        {
            if(flag_back)
            {

                setlayout(jsonArrayOriginally);
                flag_back=false;
                ((RelativeLayout)findViewById(R.id.rnotresult)).setVisibility(View.GONE);

            }else
            {
                finish();
               // super.onBackPressed();
            }

        }

        if(id==R.id.menu_logout)
        {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    ListinfoDetailsActivity.this);
            alertDialogBuilder
                    .setMessage("Do you want to logout?")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {

                            SharedPreferences.Editor prefsEditor = mPrefs
                                    .edit();
                            prefsEditor
                                    .putString("userObject","");
                            prefsEditor.putString("isLogin", "N");
                            prefsEditor.commit();

                            for(int i=0;i< StaticMethod.state.size();i++)
                            {
                                StaticMethod.state.get(i).finish();
                            }
                            Intent intent=new Intent(getApplicationContext(),NewDashboardActivity.class);
                            startActivity(intent);
                            finish();

                        }
                    })
                    .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTypeface(font);

        }
        if(id==R.id.menu_mytrip)
        {


            intent=new Intent(getApplicationContext(), MyTripsActivity.class);
            intent.putExtra("position",0);
            startActivity(intent);

        }
        if(id==R.id.menu_login)
        {
            intent=new Intent(getApplicationContext(), LoginActivity.class);
            intent.putExtra("action_flag","N");
            intent.putExtra("position",0);
            startActivity(intent);
        }




        return super.onOptionsItemSelected(item);
    }

    public void setlayout(final JSONArray jsonArray)
    {

        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");

        try
        {
            LinearLayout alllayout=null;
            alllayout=(LinearLayout)findViewById(R.id.llistall);
            alllayout.removeAllViews();
            View parentview;
            for(int i=0;i<jsonArray.length();i++)
            {
                parentview=getLayoutInflater().inflate(R.layout.new_package_listing_row, null);
                parentview.setId(i);

                parentview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        try
                        {
                            Intent intent=new Intent(ListinfoDetailsActivity.this,ExpirenceDetailsActivity.class);
                            intent.putExtra("product_id",jsonArray.getJSONObject(view.getId()).getString("product_id"));
                            intent.putExtra("action_flag","N");
                            startActivity(intent);



                        }catch (Exception e)
                        {

                        }

                    }
                });

                imageLoader.DisplayImage(jsonArray.getJSONObject(i).getString("app_image"),((ImageView)parentview.findViewById(R.id.pimg)));
                ((TextView)parentview.findViewById(R.id.tvpackagename)).setText(jsonArray.getJSONObject(i).getString("name"));


                ((TextView)parentview.findViewById(R.id.tvpackagename)).setTypeface(font);
                ((TextView)parentview.findViewById(R.id.tvexplore)).setTypeface(font);


/*


                LinearLayout horizontal=(LinearLayout)parentview.findViewById(R.id.lhorizontalcircle);
                horizontal.removeAllViews();
                View chield;
                JSONArray optionArray=jsonArray.getJSONObject(i).getJSONArray("attributes");

                for(int j=0;j<optionArray.length();j++)
                {
                    chield=getLayoutInflater().inflate(R.layout.circle_row, null);
                    chield.setId(j);


                    String temp= optionArray.getJSONObject(j).getString("text");

                try {

                    Resources res = getResources();
                    String mDrawableName = temp.replace("-", "_");
                    int resID = res.getIdentifier(mDrawableName, "drawable", getPackageName());
                    Drawable drawable = res.getDrawable(resID);
                    ColorFilter filter = new LightingColorFilter(Color.WHITE, Color.WHITE);
                    drawable.setColorFilter(filter);

                    ((ImageView) chield.findViewById(R.id.imginclusion)).setImageDrawable(drawable);
                }catch (Exception e)
                {

                }







               *//*     if(temp.equalsIgnoreCase("ico-plain"))
                    {


                        Drawable myIcon = getResources().getDrawable( R.drawable.ico_plain );
                        ColorFilter filter = new LightingColorFilter( Color.WHITE, Color.WHITE );
                        myIcon.setColorFilter(filter);
                        ((ImageView)chield.findViewById(R.id.imginclusion)).setImageDrawable(myIcon);




                    }else if(temp.equalsIgnoreCase("ico-car"))
                    {

                        Drawable myIcon = getResources().getDrawable( R.drawable.ico_car );
                        ColorFilter filter = new LightingColorFilter( Color.WHITE, Color.WHITE );
                        myIcon.setColorFilter(filter);
                        ((ImageView)chield.findViewById(R.id.imginclusion)).setImageDrawable(myIcon);


                    }else if(temp.equalsIgnoreCase("ico-cruise"))
                    {

                        Drawable myIcon = getResources().getDrawable( R.drawable.ico_cruise );
                        ColorFilter filter = new LightingColorFilter( Color.WHITE, Color.WHITE );
                        myIcon.setColorFilter(filter);
                        ((ImageView)chield.findViewById(R.id.imginclusion)).setImageDrawable(myIcon);


                    }else if(temp.equalsIgnoreCase("ico-allmeals"))
                    {

                        Drawable myIcon = getResources().getDrawable( R.drawable.ico_allmeals );
                        ColorFilter filter = new LightingColorFilter( Color.WHITE, Color.WHITE );
                        myIcon.setColorFilter(filter);
                        ((ImageView)chield.findViewById(R.id.imginclusion)).setImageDrawable(myIcon);



                    }else if(temp.equalsIgnoreCase("ico-hotairballooning"))
                    {

                    }

                    else
                    {
                        Drawable myIcon = getResources().getDrawable( R.drawable.ico_plain);
                        ColorFilter filter = new LightingColorFilter( Color.WHITE, Color.WHITE );
                        myIcon.setColorFilter(filter);
                        ((ImageView)chield.findViewById(R.id.imginclusion)).setImageDrawable(myIcon);

                    }
*//*
                 //   horizontal.addView(chield);

                }*/





                LinearLayout.LayoutParams llpline = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 15);
                View line=new View(this);
                llpline.setMargins(0, 0, 0, 0);
                line.setLayoutParams(llpline);
                line.setBackgroundColor(Color.parseColor("#00000000"));//add sperater
                if(i==0)
                {

                }else
                {
                    alllayout.addView(line);
                }




                alllayout.addView(parentview);

            }




        }catch (Exception e)
        {
            System.out.println("Error filter"+e);

        }

    }




    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {

                    String result=data.getStringExtra("flag_selection");

                 if(result.equalsIgnoreCase("S"))
                 {

                    ArrayList<String> durationID = new ArrayList<>();
                    ArrayList<String> insusionId = new ArrayList<>();
                    selection_flag = new ArrayList<>();
                    //selection_day_flag=new ArrayList<>();
                    String DurationFilter = "";
                    ArrayList<String> InclusionFilter = new ArrayList<>();
                    try {

                        if (FilterActivity.selectDuration.size() > 0) {
                            JSONObject objectduration = FilterActivity.selectDuration.get(0);
                            DurationFilter = objectduration.getString("name");

                        }
                        System.out.println("aaa" + DurationFilter);

                        for (int i = 0; i < FilterActivity.selectInclusion.size(); i++) {
                            InclusionFilter.add(FilterActivity.selectInclusion.get(i).getString("name"));
                            selection_flag.add(FilterActivity.selectInclusion.get(i).getString("name"));
                        }


                        System.out.println("Duration" + DurationFilter);
                        System.out.println("Incusion" + InclusionFilter);
                        System.out.println("select Incusion" + selection_flag);


                    } catch (Exception e) {
                        System.out.println("out" + e);
                    }


                    FilterArray = new JSONArray();

                    for (int i = 0; i < jsonArrayCopy.length(); i++) {


                        ArrayList<String> mduration = new ArrayList<>();
                        ArrayList<String> minclusion = new ArrayList<>();
                        try {

                            JSONArray tempD = jsonArrayCopy.getJSONObject(i).getJSONArray("duration");
                            JSONArray tempI = jsonArrayCopy.getJSONObject(i).getJSONArray("inclusions");
                            for (int j = 0; j < tempD.length(); j++) {
                                mduration.add(tempD.getString(j));
                            }
                            for (int k = 0; k < tempI.length(); k++) {
                                minclusion.add(tempI.getString(k));
                            }

                            Boolean dflag = false;
                            Boolean iflag = false;


                            System.out.println("check1" + DurationFilter);
                            System.out.println("check2" + InclusionFilter);


                            if (!(DurationFilter.equalsIgnoreCase("")) && InclusionFilter.size() > 0) {

                                // Toast.makeText(getApplicationContext(),"both",Toast.LENGTH_LONG).show();


                                if ((compDays(mduration, DurationFilter)) || (checkValue(minclusion, InclusionFilter))) {
                                    FilterArray.put(jsonArrayCopy.getJSONObject(i));

                                }


                            } else if (!(DurationFilter.equalsIgnoreCase("")) && InclusionFilter.size() == 0) {
                                // Toast.makeText(getApplicationContext(),"only Duration",Toast.LENGTH_LONG).show();

                            /* if(mduration.contains(DurationFilter))
                             {


                                 FilterArray.put(jsonArrayCopy.getJSONObject(i));


                               }*/


                                if (compDays(mduration, DurationFilter)) {


                                    FilterArray.put(jsonArrayCopy.getJSONObject(i));


                                }


                            } else if ((DurationFilter.equalsIgnoreCase("")) && (InclusionFilter.size() > 0)) {
                                //Toast.makeText(getApplicationContext(),"only Inclusion",Toast.LENGTH_LONG).show();

                                if (checkValue(minclusion, InclusionFilter)) {
                                    FilterArray.put(jsonArrayCopy.getJSONObject(i));
                                }


                            }

                        } catch (Exception e) {

                            System.out.println("Error after" + e);

                        }


                    }


                    if (FilterArray.length() > 0) {

                        ((RelativeLayout) findViewById(R.id.rnotresult)).setVisibility(View.GONE);
                        setlayout(FilterArray);
                        flag_back = false;

                    } else {
                        ((LinearLayout) findViewById(R.id.llistall)).removeAllViews();
                        ((RelativeLayout) findViewById(R.id.rnotresult)).setVisibility(View.VISIBLE);
                        flag_back = true;
                    }


                    // Toast.makeText(getApplicationContext(),"after click filter",Toast.LENGTH_LONG).show();

                }else {


                  //   Toast.makeText(getApplicationContext(), "full", Toast.LENGTH_LONG).show();
                     ((RelativeLayout) findViewById(R.id.rnotresult)).setVisibility(View.GONE);
                     selection_day_flag.clear();
                     selection_day_flag=new ArrayList<>();
                     flag_back = false;
                     setlayout(jsonArrayOriginally);

                 }
                }


                break;
        }
    }





    public static Boolean checkValue(ArrayList<String> firstList,ArrayList<String> secondList)
    {
        Boolean flag=false;

        List<Integer> comparingList = new ArrayList<Integer>();

        for (int a = 0; a < firstList.size(); a++) {
            comparingList.add(0);

        }

        for (int counter = 0; counter < firstList.size(); counter++) {
            if (secondList.contains(firstList.get(counter))) {
                comparingList.set(counter, 1);
            }
        }




        Collections.frequency(comparingList, 1);



        if( Collections.frequency(comparingList, 1)==secondList.size())
        {

            flag=true;

        }


        return flag;
    }



    public Boolean compDays(ArrayList<String> dayslist,String selectDays)
    {
        Boolean flag=false;
        ArrayList<String> aa=new ArrayList<>();

        for(int i=0;i<dayslist.size();i++)
        {

            int list=Integer.parseInt(dayslist.get(i));
            int select=Integer.parseInt(selectDays);
            if(list<=select)
            {
                aa.add("");
            }

        }


        if(aa.size()>0)
        {
            flag=true;
        }



        return flag;
    }


    @Override
    public void onBackPressed() {




        if(flag_back)
        {

            setlayout(jsonArrayOriginally);
            flag_back=false;
            ((RelativeLayout)findViewById(R.id.rnotresult)).setVisibility(View.GONE);

        }else
        {
          finish();
            super.onBackPressed();
        }




    }





}
