package com.queppelin.universal;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.image.loader.ImageLoader;
import com.tool.AppConfig;
import com.tool.HttpClassVersion;
import com.tool.StaticMethod;

import org.json.JSONObject;


public class ReadMessageActivity extends AppCompatActivity {

    Typeface font;
    ImageLoader imageLoader;
    private SharedPreferences mPrefs;
    String message_Object;

    String flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_message);

        font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
        imageLoader=new ImageLoader(this);

        StaticMethod.mLogin_flag="N";

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(actionBar.getDisplayOptions()
                | ActionBar.DISPLAY_SHOW_CUSTOM);

        LinearLayout layout = new LinearLayout(actionBar.getThemedContext());
        layout.setOrientation(LinearLayout.HORIZONTAL);

        ImageView imageViewlogin = new ImageView(actionBar.getThemedContext());
        imageViewlogin.setScaleType(ImageView.ScaleType.CENTER);
        imageViewlogin.setImageResource(R.drawable.ic_menu_login);

        ImageView imageViewLogout = new ImageView(actionBar.getThemedContext());
        imageViewLogout.setScaleType(ImageView.ScaleType.CENTER);
        imageViewLogout.setImageResource(R.drawable.ic_logout);
        imageViewLogout.setVisibility(View.VISIBLE);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
                | Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin = 10;
        //imageViewLogout.setLayoutParams(layoutParams);





        ImageView imageViewmytrip = new ImageView(actionBar.getThemedContext());
        imageViewmytrip.setScaleType(ImageView.ScaleType.CENTER);
        imageViewmytrip.setImageResource(R.drawable.ic_menu_trips);

        //imageViewmytrip.setLayoutParams(layoutParams);





        imageViewlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), LoginActivity.class);
                intent.putExtra("action_flag","N");
                intent.putExtra("position",0);
                startActivity(intent);

            }
        });



        imageViewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Toast.makeText(getApplicationContext(),"on logout",Toast.LENGTH_LONG).show();




                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        ReadMessageActivity.this);
                alertDialogBuilder
                        .setMessage("Do you want to logout?")
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                String userValue=mPrefs.getString("userObject","").toString();
                                String user_id;
                                try
                                {
                                    JSONObject object=new JSONObject(userValue);
                                    JSONObject userObject=object.getJSONObject("result");
                                    user_id=userObject.getString("customer_id");//customer_id
                                    String[] pro_key = new String[]{"deviceId","userId","deviceType"};
                                    String[] pro_value = new String[]{mPrefs.getString("deviceId",""),user_id, AppConfig.DEVIECS_TYPE};
                                    new HttpClassVersion(ReadMessageActivity.this,"setPushLogoutCustomer", RestClient.RequestMethod.POST,pro_key,pro_value
                                    ).start();
                                    StaticMethod.returnProgressBar(
                                            ReadMessageActivity.this).setOnDismissListener(
                                            new DialogInterface.OnDismissListener() {
                                                public void onDismiss(DialogInterface dialog) {
                                                    if (HttpClassVersion.ResponseVector != null && !HttpClassVersion.ResponseVector.equalsIgnoreCase("")) {



                                                        try {

                                                            JSONObject jsonObject=new JSONObject(HttpClassVersion.ResponseVector);

                                                            if(jsonObject.has("success")) {

                                                                boolean type=jsonObject.getBoolean("success");
                                                                if(type) {


                                                                    SharedPreferences.Editor prefsEditor = mPrefs
                                                                            .edit();
                                                                    prefsEditor
                                                                            .putString("userObject","");
                                                                    prefsEditor.putString("isLogin", "N");
                                                                    prefsEditor.commit();


                                                                    for(int i=0;i< StaticMethod.state.size();i++)
                                                                    {
                                                                        StaticMethod.state.get(i).finish();
                                                                    }
                                                                    Intent intent=new Intent(getApplicationContext(),NewDashboardActivity.class);
                                                                    startActivity(intent);
                                                                    finish();

                                                                }else
                                                                {
                                                                    Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                                                                }
                                                            }
                                                        }catch (Exception e)
                                                        {

                                                        }




                                                    }
                                                }
                                            });


                                }catch (Exception e)
                                {

                                }



                            }
                        })
                        .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);



            }
        });



        imageViewmytrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), MyTripsActivity.class);
                intent.putExtra("position",0);
                startActivity(intent);


            }
        });

        layout.setLayoutParams(layoutParams);
       /* layout.addView(imageViewlogin);
        layout.addView(imageViewLogout);
        layout.addView(imageViewmytrip);

*/


        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
        {
            //layout.addView(imageViewLogout);
            //layout.addView(imageViewmytrip);

        }else
        {
            //layout.addView(imageViewlogin);
        }
        actionBar.setCustomView(layout);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("MESSAGES DETAILS");


        try{



            JSONObject object=new JSONObject(getIntent().getExtras().getString("message_object"));

            flag=getIntent().getExtras().getString("action_flag");


            if(flag.equalsIgnoreCase("Y"))
            {

             try
             {

                 String message=getIntent().getExtras().getString("marry").toString();

                 JSONObject jb=new JSONObject(message) ;

                 if(jb.getString("msgType").equalsIgnoreCase("broadcast"))
                 {

                     if(StaticMethod.isInternetAvailable(this)) {
                         new SetReadThreadOnNotification(ReadMessageActivity.this, getIntent().getExtras().getString("message_id"), getIntent().getExtras().getString("marry").toString()).execute();
                     }else
                     {
                        return;
                     }

                 }else
                 {

                     if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
                     {

                         if(StaticMethod.isInternetAvailable(this)) {
                             new SetReadThreadOnNotification(ReadMessageActivity.this, getIntent().getExtras().getString("message_id"), getIntent().getExtras().getString("marry").toString()).execute();
                         }else
                         {
                             return;
                         }

                      }else
                     {
                         Intent intent=new Intent(ReadMessageActivity.this,LoginActivity.class);
                         intent.putExtra("action_flag","Y");
                         startActivity(intent);
                         finish();
                     }
                 }


             }catch (Exception e)
             {

             }
               // new SetReadThreadOnNotification(ReadMessageActivity.this,object.getString("msgid"),getIntent().getExtras().getString("message_object").toString()).execute();
            }


            String img_path=object.getString("image");

            if(img_path.equalsIgnoreCase("")||img_path.equalsIgnoreCase("null"))
            {
                ((ImageView)findViewById(R.id.imgnoti)).getLayoutParams().height = 0;
                ((ImageView)findViewById(R.id.imgnoti)).requestLayout();

            }else
            {


                imageLoader.DisplayImage(object.getString("image").replaceAll(" ", "%20"),((ImageView)findViewById(R.id.imgnoti)));
            }


            ((TextView)findViewById(R.id.tvtitle)).setText(Html.fromHtml(object.getString("msg_title")));
            ((TextView)findViewById(R.id.tvdescription)).setText(Html.fromHtml(object.getString("msg")));
            ((TextView)findViewById(R.id.tvtitle)).setTypeface(font);
            ((TextView)findViewById(R.id.tvdescription)).setTypeface(font);





            NotificationManager manager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
            if(!object.getString("msgid").equalsIgnoreCase("")) {
                manager.cancel(Integer.parseInt(object.getString("msgid")));
            }


        }catch (Exception e)
        {
            System.out.println("Error read activity"+e);
        }





    }


    @Override
    public void onBackPressed() {


        if(flag.equalsIgnoreCase("Y"))
        {
            Intent intent=new Intent(ReadMessageActivity.this,NewDashboardActivity.class);
            startActivity(intent);
            finish();

        }else
        {
            super.onBackPressed();
        }




    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id==android.R.id.home)
        {


            if(flag.equalsIgnoreCase("Y"))
            {
                Intent intent=new Intent(ReadMessageActivity.this,NewDashboardActivity.class);
                startActivity(intent);
                finish();

            }else
            {
                finish();
            }


        }


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }






    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {

            startActivity(getIntent());
            finish();
        }
    }




}
