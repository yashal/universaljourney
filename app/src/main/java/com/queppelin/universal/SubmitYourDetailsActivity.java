package com.queppelin.universal;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.tool.StaticMethod;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class SubmitYourDetailsActivity extends AppCompatActivity {

    private static final int DATE_DIALOG_ID_DOB = 1;
    private static final int DATE_DIALOG_ID_ISSUE = 2;
    private static final int DATE_DIALOG_ID_Expiry = 3;

    String [] sexArray={"Select sex","Male","Female"};
    String [] meal_plan={"Select meal","Veg","Jain Veg","Non-Veg"};
    Spinner spinner_sex,spinner_meal;

    String sexvale="";
    String mealvale="";


    int sex_spinner_p=0;
    int meal_spinner_p=0;


    int day;
    int month;
    int year;

    JSONObject obj;
    JSONObject objupdate;

    Bitmap bitmap_passport=null;
    Bitmap bitmap_photo=null;
    Bitmap bitmap_support=null;


    Bitmap bitmap_passport_back=null;
    Bitmap bitmap_support1=null;
    Bitmap bitmap_support2=null;



    byte[] byteArraypassport;
    byte[] byteArraybackpassport;
    byte[] byteArrayphoto;
    byte[] byteArraysupport;
    byte[] byteArraysupport1;
    byte[] byteArraysupport2;
    Typeface font;
    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_your_details);
        getSupportActionBar().hide();
        font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");

        StaticMethod.stateactivity.add(SubmitYourDetailsActivity.this);
        String Object=getIntent().getExtras().getString("travelObject");

        ((TextView)findViewById(R.id.tvtitleheader)).setTypeface(font);
        ((TextView)findViewById(R.id.tvnext)).setTypeface(font);
        ((TextView)findViewById(R.id.tvdoi)).setTypeface(font);
        ((TextView)findViewById(R.id.tvdoe)).setTypeface(font);
        ((TextView)findViewById(R.id.tvdob)).setTypeface(font);
        ((TextView)findViewById(R.id.tvback)).setTypeface(font);


        ((TextView)findViewById(R.id.tvsex)).setTypeface(font);
        ((TextView)findViewById(R.id.tvmealplan)).setTypeface(font);


        // int position=getIntent().getExtras().getInt("position");

      /*  Intent intent = getIntent();
        bitmap_passport = (Bitmap) intent.getParcelableExtra("bitmap_passport");
        bitmap_photo = (Bitmap) intent.getParcelableExtra("bitmap_photo");
        bitmap_support = (Bitmap) intent.getParcelableExtra("bitmap_support");*/



        try
        {
             obj=new JSONObject(Object);
            objupdate=obj;

            System.out.println("Samit"+obj);


           ((EditText)findViewById(R.id.input_surname)).setText(obj.getString("surname"));
           ((EditText)findViewById(R.id.input_fname)).setText(obj.getString("name"));
           ((EditText)findViewById(R.id.input_pnumber)).setText(obj.getString("passport_number"));
           ((EditText)findViewById(R.id.input_poinumber)).setText(obj.getString("place_of_issue"));
           ((EditText)findViewById(R.id.input_hub)).setText(obj.getString("hub_location"));
           ((EditText)findViewById(R.id.input_spneed)).setText(obj.getString("special_need"));



            ((EditText)findViewById(R.id.input_surname)).setTypeface(font);
            ((EditText)findViewById(R.id.input_fname)).setTypeface(font);

            ((EditText)findViewById(R.id.input_pnumber)).setTypeface(font);
            ((EditText)findViewById(R.id.input_poinumber)).setTypeface(font);

            ((EditText)findViewById(R.id.input_hub)).setTypeface(font);
            ((EditText)findViewById(R.id.input_spneed)).setTypeface(font);

            String sex=obj.getString("sex");
            String meal=obj.getString("meal_preference");

            if(sex.equalsIgnoreCase("Male"))
            {
                sex_spinner_p=1;
            }else if(sex.equalsIgnoreCase("Female"))
            {
                sex_spinner_p=2;
            }else
            {
                sex_spinner_p=0;
            }

            if(meal.equalsIgnoreCase("Veg"))
            {
                meal_spinner_p=1;
            }else if(meal.equalsIgnoreCase("Jain Veg"))
            {
                meal_spinner_p=2;
            }else if(meal.equalsIgnoreCase("Non-Veg"))
            {
                meal_spinner_p=3;
            }else
            {
                meal_spinner_p=0;
            }




            String dob=obj.getString("date_of_birth");

            SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy");

            ((TextView)findViewById(R.id.tvselectdob)).setTypeface(font);
            ((TextView)findViewById(R.id.tvselectissues)).setTypeface(font);
            ((TextView)findViewById(R.id.tvselectexpire)).setTypeface(font);
            if(dob.equalsIgnoreCase("")||dob.equalsIgnoreCase("0000-00-00"))
            {
                ((TextView)findViewById(R.id.tvselectdob)).setText("Select date");
            }else
            {

                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");


                try {
                    Date oneWayTripDate = input.parse(dob);

                    dob=output.format(oneWayTripDate);
                    selectdob=dob;


                    // format output
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                ((TextView)findViewById(R.id.tvselectdob)).setText(dob);
            }


            String doi=obj.getString("date_of_issue");

            if(doi.equalsIgnoreCase("")||doi.equalsIgnoreCase("0000-00-00"))
            {

                ((TextView)findViewById(R.id.tvselectissues)).setText("Select date");
            }else
            {
                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    Date oneWayTripDate = input.parse(doi);

                    doi=output.format(oneWayTripDate);
                    selectIssues=doi;
                    // format output
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                ((TextView)findViewById(R.id.tvselectissues)).setText(doi);
            }

            String doe=obj.getString("date_of_expiry");
            if(doe.equalsIgnoreCase("")||doe.equalsIgnoreCase("0000-00-00"))
            {
                ((TextView)findViewById(R.id.tvselectexpire)).setText("Select date");
            }else
            {

                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    Date oneWayTripDate = input.parse(doe);

                    doe=output.format(oneWayTripDate);

                    selectexpire=doe;
                    // format output
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                ((TextView)findViewById(R.id.tvselectexpire)).setText(doe);
            }



        }catch (Exception e)
        {

        }



        ((RelativeLayout)findViewById(R.id.rback)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });



      /*  ((TextView)findViewById(R.id.tvnext)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(SubmitYourDetailsActivity.this,NextSubmitDetailsActivity.class);
                startActivity(intent);
            }
        });*/



        spinner_sex=(Spinner)findViewById(R.id.spsex);

        Drawable spinnerDrawable = spinner_sex.getBackground().getConstantState().newDrawable();

        spinnerDrawable.setColorFilter(getResources().getColor(R.color.black_semi_transparent), PorterDuff.Mode.SRC_ATOP);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            spinner_sex.setBackground(spinnerDrawable);
        }else{
            spinner_sex.setBackgroundDrawable(spinnerDrawable);
        }

        spinner_meal=(Spinner)findViewById(R.id.spmealplan);


        Drawable spinnerDrawablespinner_meal = spinner_meal.getBackground().getConstantState().newDrawable();

        spinnerDrawablespinner_meal.setColorFilter(getResources().getColor(R.color.black_semi_transparent), PorterDuff.Mode.SRC_ATOP);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            spinner_meal.setBackground(spinnerDrawable);
        }else{
            spinner_meal.setBackgroundDrawable(spinnerDrawable);
        }



        //  ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(SubmitYourDetailsActivity.this,
         //       R.layout.spinner_item, sexArray);


        SpinerAdapter adapter_state = new SpinerAdapter(
                getApplicationContext(),
                R.layout.spinner_item,
                sexArray);

        adapter_state
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        spinner_sex.setAdapter(adapter_state);
        spinner_sex.setSelection(sex_spinner_p);
        spinner_sex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {

                sex_spinner_p=arg2;
                if(arg2>0) {

                    sexvale = sexArray[arg2];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

       // ArrayAdapter<String> adapter_state2 = new ArrayAdapter<String>(SubmitYourDetailsActivity.this,
         //       R.layout.spinner_item, meal_plan);


        SpinerAdapter adapter_state2 = new SpinerAdapter(
                getApplicationContext(),
                R.layout.spinner_item,
                meal_plan);



        adapter_state2
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

       // spinner_meal.setSelection(meal_spinner_p);
        spinner_meal.setAdapter(adapter_state2);
        spinner_meal.setSelection(meal_spinner_p);
        spinner_meal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {

if(arg2>0)
{
    mealvale=meal_plan[arg2];
}


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });







        ((TextView)findViewById(R.id.tvnext)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {













                String sure_name=   ((EditText)findViewById(R.id.input_surname)).getText().toString();
                String sure_fname=    ((EditText)findViewById(R.id.input_fname)).getText().toString();
                String sure_pnumber=    ((EditText)findViewById(R.id.input_pnumber)).getText().toString();
                String passportissues=    ((EditText)findViewById(R.id.input_poinumber)).getText().toString();
                String sure_hub=    ((EditText)findViewById(R.id.input_hub)).getText().toString();
                String sure_specialneend=   ((EditText)findViewById(R.id.input_spneed)).getText().toString();


                    //Intent intent=new Intent(SubmitYourDetailsActivity.this,NextSubmitDetailsActivity.class);

                    Intent intent=new Intent(SubmitYourDetailsActivity.this,PhotoGraphActivity.class);
                    intent.putExtra("v_surname",sure_name);
                    intent.putExtra("v_firstname",sure_fname);
                    intent.putExtra("v_sex",sexvale);
                    intent.putExtra("v_passport",sure_pnumber);
                    intent.putExtra("v_dob",selectdob);
                    intent.putExtra("v_doi",selectIssues);
                    intent.putExtra("v_doe",selectexpire);
                    intent.putExtra("v_poissuance",passportissues);
                    intent.putExtra("v_mealplan",mealvale);
                    intent.putExtra("v_hub",sure_hub);
                    intent.putExtra("v_special",sure_specialneend);
                String select_dob=((TextView)findViewById(R.id.tvselectdob)).getText().toString();
                Date doidate=null;
                Date doedate=null;
                Date dobdate=null;

                try
                    {
                        objupdate.put("surname",sure_name);
                        objupdate.put("name",sure_fname);
                        objupdate.put("sex",sexvale);

                        objupdate.put("passport_number",sure_pnumber);
                        objupdate.put("place_of_issue",passportissues);
                        objupdate.put("meal_preference",mealvale);
                        objupdate.put("hub_location",sure_hub);
                        objupdate.put("special_need",sure_specialneend);


                        /*  get date*/


                        String select_doi=((TextView)findViewById(R.id.tvselectissues)).getText().toString();
                        String select_doe=((TextView)findViewById(R.id.tvselectexpire)).getText().toString();

                        try
                        {
                            SimpleDateFormat input = new SimpleDateFormat("dd MMM yyyy");
                            SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");

                             dobdate = input.parse(select_dob);
                            select_dob=output.format(dobdate);

                             doidate = input.parse(select_doi);
                            select_doi=output.format(doidate);

                             doedate = input.parse(select_doe);
                            select_doe=output.format(doedate);



                        }catch (Exception e)
                        {
                               System.out.println("Error date formate"+e);
                        }
                        objupdate.put("date_of_birth",select_dob);
                        objupdate.put("place_of_issue",select_doi);
                        objupdate.put("date_of_expiry",select_doe);
                    }catch (Exception e)
                    {

                    }
                intent.putExtra("object",objupdate.toString());


                if(sex_spinner_p==0)
                {
                   // Toast.makeText(getApplicationContext(),"Please select sex.",Toast.LENGTH_LONG).show();



                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            SubmitYourDetailsActivity.this);

                    // set title


                    // set dialog message
                    alertDialogBuilder
                            .setTitle("There was a problem")
                            .setMessage("Please enter your sex.")
                            .setCancelable(false)
                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, close
                                    // current activity
                                    //MainActivity.this.finish();


                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                    msgTxt.setTypeface(font);




                }else if(select_dob.equalsIgnoreCase("Select date")) {



                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            SubmitYourDetailsActivity.this);

                    // set title


                    // set dialog message
                    alertDialogBuilder
                            .setTitle("There was a problem")
                            .setMessage("Please enter your date of birth.")
                            .setCancelable(false)
                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, close
                                    // current activity
                                    //MainActivity.this.finish();


                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                    msgTxt.setTypeface(font);


                }else if(!(doidate.compareTo(doedate)<0))
                {


                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            SubmitYourDetailsActivity.this);

                    // set title


                    // set dialog message
                    alertDialogBuilder
                            .setTitle("There was a problem")
                            .setMessage("Date of Issuance of Passport should be less than the Date of Expiry.")
                            .setCancelable(false)
                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, close
                                    // current activity
                                    //MainActivity.this.finish();


                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                    msgTxt.setTypeface(font);



                }else if(!(dobdate.compareTo(doidate)<0))
                {



                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            SubmitYourDetailsActivity.this);

                    // set title


                    // set dialog message
                    alertDialogBuilder
                            .setTitle("There was a problem")
                            .setMessage("Date of Birth Date  should be less than of Issuance of Passport.")
                            .setCancelable(false)
                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, close
                                    // current activity
                                    //MainActivity.this.finish();


                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                    msgTxt.setTypeface(font);



                }else
                {


                    startActivity(intent);
                    //Toast.makeText(getApplicationContext(),"done:",Toast.LENGTH_LONG).show();
                }



//




            }
        });





        ((TextView)findViewById(R.id.tvselectdob)).setOnClickListener(listenerdob);
        ((TextView)findViewById(R.id.tvselectissues)).setOnClickListener(listenerdateissues);
        ((TextView)findViewById(R.id.tvselectexpire)).setOnClickListener(listenerexpiry);


    }

    View.OnClickListener listenerdob = new View.OnClickListener() {

        public void onClick(View arg0) {

            String dobdate=((TextView)findViewById(R.id.tvselectdob)).getText().toString();


            if(dobdate.equalsIgnoreCase("Select date"))//
            {
                final Calendar c = Calendar.getInstance();

                c.set(Calendar.YEAR, 1950);
                c.set(Calendar.MONTH, Calendar.JANUARY);
                c.set(Calendar.DAY_OF_MONTH, 1);
               // c.set(Calendar.HOUR_OF_DAY, 16);
               // c.set(Calendar.MINUTE, 58);

                year1 = c.get(Calendar.YEAR);
                month1 = c.get(Calendar.MONTH);
                day1 = c.get(Calendar.DAY_OF_MONTH);

                showDialog(DATE_DIALOG_ID_DOB);

            }else
            {

                try
                {

                    SimpleDateFormat input = new SimpleDateFormat("dd MMM yyyy");
                    Date date = input.parse(dobdate);

                    Calendar cs = Calendar.getInstance();
                    cs.setTime(date);
                    year1 = cs.get(Calendar.YEAR);
                    month1 = cs.get(Calendar.MONTH);
                    day1 = cs.get(Calendar.DAY_OF_MONTH);
                    showDialog(DATE_DIALOG_ID_DOB);

                }catch (Exception e)
                {

                }



            }







        }
    };



    View.OnClickListener listenerdateissues = new View.OnClickListener() {

        public void onClick(View arg0) {

             String dobdate=((TextView)findViewById(R.id.tvselectissues)).getText().toString();

            if(dobdate.equalsIgnoreCase("Select date"))
            {
                final Calendar c = Calendar.getInstance();
                year1 = c.get(Calendar.YEAR);
                month1 = c.get(Calendar.MONTH);
                day1 = c.get(Calendar.DAY_OF_MONTH);
                showDialog(DATE_DIALOG_ID_ISSUE);

            }else
            {

                try
                {

                    SimpleDateFormat input = new SimpleDateFormat("dd MMM yyyy");
                    Date date = input.parse(dobdate);

                    Calendar cs = Calendar.getInstance();
                    cs.setTime(date);
                    year1 = cs.get(Calendar.YEAR);
                    month1 = cs.get(Calendar.MONTH);
                    day1 = cs.get(Calendar.DAY_OF_MONTH);
                    showDialog(DATE_DIALOG_ID_ISSUE);

                }catch (Exception e)
                {

                }


            }






        }
    };



    View.OnClickListener listenerexpiry = new View.OnClickListener() {

        public void onClick(View arg0) {

             String dobdate=((TextView)findViewById(R.id.tvselectexpire)).getText().toString();


            if(dobdate.equalsIgnoreCase("Select date"))
            {
                final Calendar c = Calendar.getInstance();
                year1 = c.get(Calendar.YEAR);
                month1 = c.get(Calendar.MONTH);
                day1 = c.get(Calendar.DAY_OF_MONTH);

                showDialog(DATE_DIALOG_ID_Expiry);

            }else
            {
                try
                {

                    SimpleDateFormat input = new SimpleDateFormat("dd MMM yyyy");
                    Date date = input.parse(dobdate);

                    Calendar cs = Calendar.getInstance();
                    cs.setTime(date);
                    year1 = cs.get(Calendar.YEAR);
                    month1 = cs.get(Calendar.MONTH);
                    day1 = cs.get(Calendar.DAY_OF_MONTH);
                    showDialog(DATE_DIALOG_ID_Expiry);

                }catch (Exception e)
                {

                }

            }





        }
    };



    private String selectdob="";
    private String selectIssues="";
    private String selectexpire="";


    private int year1;
    private int day1;
    private int month1;
    private String dobday;
    private String dobmonth;
    private String dobyear;
    private void updateDisplaydob() {
        selectdob = new StringBuilder().append(month1 + 1).append("/")
                .append(day1).append("/").append(year1).toString();





        SimpleDateFormat input = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy");

        try {
            Date oneWayTripDate = input.parse(selectdob);

            selectdob=output.format(oneWayTripDate);
            // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }




        ((TextView)findViewById(R.id.tvselectdob)).setText(selectdob);

        // Log.i("DATE", currentDate);
    }

    private void updateDisplayissues() {
        selectIssues = new StringBuilder().append(month1 + 1).append("/")
                .append(day1).append("/").append(year1).toString();




        SimpleDateFormat input = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy");

        try {
            Date oneWayTripDate = input.parse(selectIssues);

            selectIssues=output.format(oneWayTripDate);
            // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ((TextView)findViewById(R.id.tvselectissues)).setText(selectIssues);

        // Log.i("DATE", currentDate);
    }
    private void updateDisplayexpiry() {
        selectexpire = new StringBuilder().append(month1 + 1).append("/")
                .append(day1).append("/").append(year1).toString();


        SimpleDateFormat input = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy");

        try {
            Date oneWayTripDate = input.parse(selectexpire);

            selectexpire=output.format(oneWayTripDate);
            // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }




        ((TextView)findViewById(R.id.tvselectexpire)).setText(selectexpire);

        // Log.i("DATE", currentDate);
    }


    DatePickerDialog.OnDateSetListener myDateSetListenerdob = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker datePicker, int i, int j, int k) {
			year1 = i;
            month1 = j;
			day1 = k;
            updateDisplaydob();

        }
    };

    DatePickerDialog.OnDateSetListener myDateSetListenerissues = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker datePicker, int i, int j, int k) {
            year1 = i;
            month1 = j;
            day1 = k;
            updateDisplayissues();

        }
    };


    DatePickerDialog.OnDateSetListener myDateSetListenerexpire = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker datePicker, int i, int j, int k) {
            year1 = i;
            month1 = j;
            day1 = k;
            updateDisplayexpiry();

        }
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID_DOB:
                DatePickerDialog dgdob=new DatePickerDialog(this, myDateSetListenerdob, year1, month1,
                        day1);
                dgdob.getDatePicker().setMaxDate(new Date().getTime());
                return dgdob;

            case DATE_DIALOG_ID_ISSUE:
                DatePickerDialog dgissue=new DatePickerDialog(this, myDateSetListenerissues, year1, month1,
                        day1);
                dgissue.getDatePicker().setMaxDate(new Date().getTime());
                return dgissue;

            case DATE_DIALOG_ID_Expiry:
                DatePickerDialog dgexpiry=new DatePickerDialog(this, myDateSetListenerexpire, year1, month1,
                        day1);
               // dgexpiry.getDatePicker().setMaxDate(new Date().getTime());
                return dgexpiry;




        }
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
