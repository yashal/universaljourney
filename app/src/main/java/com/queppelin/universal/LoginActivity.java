package com.queppelin.universal;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.tool.HttpClass;
import com.tool.StaticMethod;

import org.json.JSONArray;
import org.json.JSONObject;


public class LoginActivity extends AppCompatActivity {


    Typeface font;

    private SharedPreferences mPrefs;
    String action_flag;



    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(action_flag.equalsIgnoreCase("Y"))
        {
            Intent intent=new Intent(LoginActivity.this,NewDashboardActivity.class);
            startActivity(intent);
            finish();

        }else
        {
            super.onBackPressed();
        }


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        font = Typeface.createFromAsset(this.getAssets(), "Nunito_Light.ttf");
        StaticMethod.state.add(LoginActivity.this);

        getSupportActionBar().hide();

        ((RelativeLayout)findViewById(R.id.rback)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);


        action_flag=getIntent().getExtras().getString("action_flag");





        if(action_flag.equalsIgnoreCase("Y"))
        {
            StaticMethod.mLogin_flag="Y";
        }else
        {
            StaticMethod.mLogin_flag="N";
        }


        try
        {

            NotificationManager manager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
            manager.cancel(7777);



        }catch (Exception e)
        {

        }

        try {
            String marray = mPrefs.getString("messObjectlogin","");

            if(!marray.equalsIgnoreCase(""))
            {
                JSONArray j = new JSONArray(marray);
                //new SetReadThreadOnNotification(this,j).execute();
            }

        }catch (Exception e)
        {

        }

        ((TextView)findViewById(R.id.genrat_otp)).setTypeface(font);
        ((EditText)findViewById(R.id.input_mobile)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_sinup)).setTypeface(font);


        ((TextView)findViewById(R.id.tv_title)).setTypeface(font);
        ((TextView)findViewById(R.id.tvback)).setTypeface(font);

        ((TextView)findViewById(R.id.genrat_otp)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final String mNumber=((EditText)findViewById(R.id.input_mobile)).getText().toString();

                if(mNumber.length() >= 10 && mNumber.length() <= 15) {


                    if (!mNumber.equalsIgnoreCase("")) {





                        if(StaticMethod.isInternetAvailable(LoginActivity.this))
                        {

                        String[] pro_key = new String[]{"mobile"};
                        String[] pro_value = new String[]{mNumber};
                        new HttpClass(LoginActivity.this, "getOtp", RestClient.RequestMethod.POST, pro_key, pro_value
                        ).start();
                        StaticMethod.returnProgressBar(
                                LoginActivity.this).setOnDismissListener(
                                new DialogInterface.OnDismissListener() {
                                    public void onDismiss(DialogInterface dialog) {
                                        if (HttpClass.ResponseVector != null && !HttpClass.ResponseVector.equalsIgnoreCase("")) {

                                            // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                                            // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                                            //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                                            try {

                                                System.out.println("outotp" + HttpClass.ResponseVector);

                                                JSONObject jsonObject = new JSONObject(HttpClass.ResponseVector);

                                                if (jsonObject.has("success")) {

                                                    Boolean flag = jsonObject.getBoolean("success");

                                                    if (flag) {

                                                        Intent intent = new Intent(LoginActivity.this, LoginOTPActivity.class);

                                                        intent.putExtra("mobile", mNumber);
                                                        startActivity(intent);

                                                    } else {
                                                        Toast.makeText(getApplicationContext(), "Please try again.", Toast.LENGTH_LONG).show();
                                                    }


                                                } else if (jsonObject.has("error")) {

                                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                                            LoginActivity.this);

                                                    alertDialogBuilder
                                                            .setMessage(jsonObject.getString("message"))
                                                            .setCancelable(false)
                                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {


                                                                }
                                                            });
                                                    // create alert dialog
                                                    AlertDialog alertDialog = alertDialogBuilder.create();

                                                    // show it
                                                    alertDialog.show();


                                                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                                    msgTxt.setTypeface(font);


                                                    //  Toast.makeText(getApplicationContext(),"No user found! Please sign up.",Toast.LENGTH_LONG).show();

                                                }


                                                // JSONObject jsonRecmandation=jsonObject.getJSONObject("recommended");
                                                //  JSONArray jsonArray=jsonObject.getJSONArray("result");

                                                //  imageLoader.DisplayImage(jsonRecmandation.getString("image"),((ImageView)findViewById(R.id.imgmorocco)));


                                            } catch (Exception e) {
                                                System.err.println(e);
                                            }


                                        }
                                    }
                                });


                    }else {

                            return;
                    }



                    } else {

                        Toast.makeText(getApplicationContext(), "Please Enter Mobile.", Toast.LENGTH_LONG).show();

                    }

                }else{


                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            LoginActivity.this);

                    alertDialogBuilder
                            .setMessage("Please Enter valid Mobile no.")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {


                                }
                            });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();


                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                    msgTxt.setTypeface(font);




                }




            }
        });




        ((TextView)findViewById(R.id.tv_sinup)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(getApplicationContext(),SignUpActivity.class);
                startActivity(intent);
            }
        });





    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {

            startActivity(getIntent());
            finish();
        }
    }


}
