package com.queppelin.universal;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.view.View;

import com.tool.HttpclasswithoutThreadVersion;
import com.tool.StaticMethod;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by lenovo on 19-04-2016.
 */
public class SetReadThread  extends AsyncTask<Void, Void, String[]>
{



    String mgetThingsResponseVersion;
    Context context;

    JSONArray jsonarray;
    private SharedPreferences mPrefs;
    String messageObject;

    String msgid;
    public SetReadThread(Context context, String msgid,String messageObject) {
        super();
        this.context=context;
        this.msgid=msgid;
        this.messageObject=messageObject;
        mPrefs = context.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);



    }
    @Override
    protected String[] doInBackground(Void... params) {

        String [] mStrings=null;

        try {




            /*String messageId=mPrefs.getString("getMessageArray","");


            JSONArray jsonArraymessage=new JSONArray(messageId);
            String mId="";
            for(int i=0;i<jsonArraymessage.length();i++)
            {
             String   mid=jsonArraymessage.getJSONObject(i).getString("msgid");


                mId=mId+mid+",";
            }

            mId=removeLastChar(mId);

            System.out.println("Message"+mId);
*/
            if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
            {



                String userValue=mPrefs.getString("userObject","").toString();

                String user_id;
                JSONObject object=new JSONObject(userValue);
                JSONObject userObject=object.getJSONObject("result");
                user_id=userObject.getString("customer_id");//customer_id


                JSONObject objecttype=new JSONObject(messageObject);
                String[] pro_keyVersion = new String[]{"deviceId","userId","msgid"};
                String[] pro_valueVersion = new String[]{mPrefs.getString("deviceId",""),objecttype.getString("user_id"),msgid};

                System.out.println("key_read"+pro_valueVersion);

                mgetThingsResponseVersion =(new HttpclasswithoutThreadVersion(context,"setMessageReadStatus", RestClient.RequestMethod.POST,pro_keyVersion,pro_valueVersion
                )).getResponsedetails();


            }else
            {
                String[] pro_keyVersion = new String[]{"deviceId","userId","msgid"};
                String[] pro_valueVersion = new String[]{mPrefs.getString("deviceId",""),"0",msgid};
                System.out.println("key_read"+pro_valueVersion);
                mgetThingsResponseVersion =(new HttpclasswithoutThreadVersion(context,"setMessageReadStatus", RestClient.RequestMethod.POST,pro_keyVersion,pro_valueVersion
                )).getResponsedetails();

            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mStrings;
    }
    @Override
    protected void onPostExecute(String[] result) {

        try {


            System.out.println("getReadMessagefinal" + mgetThingsResponseVersion);
            new GetCountThread(context).execute();

        }catch (Exception e)
        {

        }



        super.onPostExecute(result);
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub

        super.onPreExecute();
    }

    public String removeLastChar(String s) {
        if (s == null || s.length() == 0) {
            return s;
        }
        return s.substring(0, s.length()-1);
    }
}
