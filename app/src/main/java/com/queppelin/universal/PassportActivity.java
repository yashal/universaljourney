package com.queppelin.universal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.tool.AppConfig;
import com.tool.StaticMethod;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class PassportActivity extends AppCompatActivity {



    private static final int RESULT_LOAD_IMAGE_PASSPORT = 1;
    private static final int RESULT_LOAD_IMAGE_PASSPORT_BACK = 7;


    private static final int RESULT_LOAD_PDF_PASSPORT = 77;
    private static final int RESULT_LOAD_PDF_PASSPORT_BACK = 66;


    Uri PASSPORT_PDF=null;
    Uri PASSPORT_BACK_PDF=null;


    private static final int PICK_FROM_CAMERA = 4;
    private static final int CROP_FROM_CAMERA = 5;
    private static final int PICK_FROM_FILE = 6;
    private AlertDialog dialog;

    private int scan_flag=0;

    JSONObject  object;
    String traveleOject;

    private Uri mImageCaptureUri;


    //Bitmap bitmappassport=null;
   // Bitmap bitmapbackpassport=null;


    Uri bitmappassporturi=null;
    Uri bitmapbackpassporturi=null;



    String passport="";
    String passport_back="";
    String travel_id;



    String passport_flag="";
    String photo_flag="";

    String passportback_flag="";


    boolean max_size=false;


    public static Activity next;

    String imageType="";
    boolean max_size_flag=false;


    Typeface font;


    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passport);

        imageType="";
        font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
        StaticMethod.stateactivity.add(PassportActivity.this);

        next=PassportActivity.this;
        ((RelativeLayout)findViewById(R.id.rback)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });


        getSupportActionBar().hide();




        ((TextView)findViewById(R.id.tvheadertitle)).setTypeface(font);
        ((TextView)findViewById(R.id.tvpassportfront)).setTypeface(font);
        ((TextView)findViewById(R.id.tvpassportfronscan)).setTypeface(font);
        ((TextView)findViewById(R.id.tvpassportscaonbrowse)).setTypeface(font);
        ((TextView)findViewById(R.id.tvphoto)).setTypeface(font);
        ((TextView)findViewById(R.id.tvpassportbackscan)).setTypeface(font);
        ((TextView)findViewById(R.id.tvpassportbackbrowse)).setTypeface(font);
        ((TextView)findViewById(R.id.tvnote)).setTypeface(font);
        ((TextView)findViewById(R.id.tvskip)).setTypeface(font);
        ((TextView)findViewById(R.id.tvsubmit)).setTypeface(font);
        ((TextView)findViewById(R.id.tvback)).setTypeface(font);

        traveleOject=getIntent().getExtras().getString("object");


        try
        {

            object=new JSONObject(traveleOject);


            passport = object.getString("pass_copy");
            passport_back = object.getString("pass_copy_back");

            travel_id=object.getString("id");

        }catch (Exception e)
        {

        }



        if(passport.equalsIgnoreCase("")||passport.equalsIgnoreCase("null"))
        {

            ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setVisibility(View.INVISIBLE);
            ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setBackgroundResource(R.drawable.icon_closemark);
        }else
        {
            ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setBackgroundResource(R.drawable.icon_checkmark);
        }


        if(passport_back.equalsIgnoreCase("")||passport_back.equalsIgnoreCase("null"))
        {

            ((TextView)findViewById(R.id.tvscan_passportback_flag)).setVisibility(View.INVISIBLE);
            ((TextView)findViewById(R.id.tvscan_passportback_flag)).setBackgroundResource(R.drawable.icon_closemark);
        }else
        {
            ((TextView)findViewById(R.id.tvscan_passportback_flag)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_passportback_flag)).setBackgroundResource(R.drawable.icon_checkmark);
        }










        ((TextView)findViewById(R.id.tvskip)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {











                Intent intent=new Intent(PassportActivity.this,SupportDoucumentActivity.class);
                intent.putExtra("v_surname", getIntent().getExtras().getString("v_surname"));
                intent.putExtra("v_firstname", getIntent().getExtras().getString("v_firstname"));
                intent.putExtra("v_sex", getIntent().getExtras().getString("v_sex"));
                intent.putExtra("v_passport", getIntent().getExtras().getString("v_passport"));
                intent.putExtra("v_dob", getIntent().getExtras().getString("v_dob"));
                intent.putExtra("v_doi", getIntent().getExtras().getString("v_doi"));
                intent.putExtra("v_doe", getIntent().getExtras().getString("v_doe"));
                intent.putExtra("v_poissuance", getIntent().getExtras().getString("v_poissuance"));
                intent.putExtra("v_mealplan", getIntent().getExtras().getString("v_mealplan"));
                intent.putExtra("v_hub", getIntent().getExtras().getString("v_hub"));
                intent.putExtra("v_special", getIntent().getExtras().getString("v_special"));
                intent.putExtra("object",object.toString());




                startActivity(intent);

            }
        });



        ((TextView)findViewById(R.id.tvsubmit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(StaticMethod.isInternetAvailable(PassportActivity.this)) {


                    if (bitmappassporturi == null && bitmapbackpassporturi == null&& PASSPORT_PDF==null&&PASSPORT_BACK_PDF==null) {


                        if (passport_back.equalsIgnoreCase("") && passport.equalsIgnoreCase("")) {

                            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                                    PassportActivity.this);
                            alertDialogBuilder
                                    .setMessage("Please select Passport")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                        }
                                    });
                            android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                            // show it
                            alertDialog.show();

                            Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                            msgTxt.setTypeface(font);


                        } else {

                            Intent intent = new Intent(PassportActivity.this, SupportDoucumentActivity.class);
                            intent.putExtra("v_surname", getIntent().getExtras().getString("v_surname"));
                            intent.putExtra("v_firstname", getIntent().getExtras().getString("v_firstname"));
                            intent.putExtra("v_sex", getIntent().getExtras().getString("v_sex"));
                            intent.putExtra("v_passport", getIntent().getExtras().getString("v_passport"));
                            intent.putExtra("v_dob", getIntent().getExtras().getString("v_dob"));
                            intent.putExtra("v_doi", getIntent().getExtras().getString("v_doi"));
                            intent.putExtra("v_doe", getIntent().getExtras().getString("v_doe"));
                            intent.putExtra("v_poissuance", getIntent().getExtras().getString("v_poissuance"));
                            intent.putExtra("v_mealplan", getIntent().getExtras().getString("v_mealplan"));
                            intent.putExtra("v_hub", getIntent().getExtras().getString("v_hub"));
                            intent.putExtra("v_special", getIntent().getExtras().getString("v_special"));
                            intent.putExtra("object", object.toString());


                            startActivity(intent);

                        }


                    } else {
                        if (StaticMethod.isInternetAvailable(PassportActivity.this)) {

                            new GetThings(PassportActivity.this, traveleOject).execute();
                        } else {
                            return;
                        }
                    }

                }else
                {
                    return;
                }

            }
        });


        ((TextView)findViewById(R.id.tvpassportfronscan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageType="S";
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/temp");
                myDir.mkdirs();
                scan_flag = 1;
                String fname = "crap_temp.jpg";
                File file = new File (myDir, fname);
                if (file.exists ()) file.delete ();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, PICK_FROM_CAMERA);
                }








            }
        });



        ((TextView)findViewById(R.id.tvpassportbackscan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                imageType="S";

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/temp");
                myDir.mkdirs();
                scan_flag = 2;
                String fname = "crap_temp.jpg";
                File file = new File (myDir, fname);
                if (file.exists ()) file.delete ();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, PICK_FROM_CAMERA);
                }






            }
        });


        ((TextView)findViewById(R.id.tvpassportscaonbrowse)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageType="B";
//                Intent i = new Intent(
//                        Intent.ACTION_PICK,
//                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(i, RESULT_LOAD_IMAGE_PASSPORT);






                CharSequence colors[] = new CharSequence[] {"Image file", "PDF file"};

                AlertDialog.Builder builder = new AlertDialog.Builder(PassportActivity.this);
                builder.setTitle("Choose ...");
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // the user clicked on colors[which]


                        if(which==0)
                        {
                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, RESULT_LOAD_IMAGE_PASSPORT);


                        }else if(which==1)
                        {



                            openFile("application/pdf",RESULT_LOAD_PDF_PASSPORT);
/*
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.setType("

/*");
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            try {
                                startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), RESULT_LOAD_PDF_PASSPORT);
                            } catch (android.content.ActivityNotFoundException ex) {
                                ex.printStackTrace();
                            }
*/




                        }

                    }
                });
                builder.show();


















            }
        });



        ((TextView)findViewById(R.id.tvpassportbackbrowse)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageType="B";
/*
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE_PASSPORT_BACK);
*/






                CharSequence colors[] = new CharSequence[] {"Image file", "PDF file"};

                AlertDialog.Builder builder = new AlertDialog.Builder(PassportActivity.this);
                builder.setTitle("Choose...");
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // the user clicked on colors[which]


                        if(which==0)
                        {
                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, RESULT_LOAD_IMAGE_PASSPORT_BACK);


                        }else if(which==1)
                        {






                            openFile("application/pdf",RESULT_LOAD_PDF_PASSPORT_BACK);






/*

                            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);


//                           // Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                            Uri uri = Uri.parse(Environment.getRootDirectory().getPath()
//                                    + "");
//                            intent.setDataAndType(uri, "file*/
/*");
                            intent.setType("application/pdf");
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            try {
                                startActivityForResult(Intent.createChooser(intent, "Select a File to Upload_text"), RESULT_LOAD_PDF_PASSPORT_BACK);
                            } catch (android.content.ActivityNotFoundException ex) {
                                ex.printStackTrace();
                            }

*/


                        }

                    }
                });
                builder.show();





            }
        });









    }


    public String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE_PASSPORT && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String browerpassort = cursor.getString(columnIndex);
            cursor.close();


            passport_flag="Y";



            Uri selectedImageURI = data.getData();
           // File imageFile = new File(getRealPathFromURI(selectedImageURI));

            float a = FileSize(data.getData()) / (1024 * 1024);
           // float a=imageFile.length()/(1024*1024);

            if(a<=4.8)
            {
                /*if(bitmappassport!=null)
                {
                    bitmappassport.recycle();
                }
*/
               // bitmappassport= BitmapFactory.decodeFile(browerpassort);

                bitmappassporturi= data.getData();

                ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setBackgroundResource(R.drawable.icon_checkmark);

            }else
            {
                bitmappassporturi= null;
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                        PassportActivity.this);

                // set title


                // set dialog message
                alertDialogBuilder
                        .setMessage("File size should be less than 5 MB")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        });


                // create alert dialog
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);


            }
            System.out.println("images"+a);





            //((ImageView)findViewById(R.id.imgtest)).setImageBitmap(bitmap);



//            Bitmap bitmap= BitmapFactory.decodeFile(picturePath);
//            ByteArrayOutputStream bos = new ByteArrayOutputStream();
//            System.out.println("ImagePath"+picturePath);
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);
//             byteimage = bos.toByteArray();








/*

              private static final int RESULT_LOAD_IMAGE_PASSPORT = 1;
    private static final int RESULT_LOAD_IMAGE_PHOTOGRAPH = 2;
    private static final int RESULT_LOAD_IMAGE_SUPPORT = 3;




            ImageView imageView = (ImageView) findViewById(R.id.imgView);
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));*/

        }


        if(requestCode == RESULT_LOAD_PDF_PASSPORT && resultCode == RESULT_OK && null != data)
        {




            if(FileType(data.getData()).equalsIgnoreCase("application/pdf")) {

                float a = FileSize(data.getData()) / (1024 * 1024);

                if (a <= 4.8) {

                    PASSPORT_PDF = data.getData();
                    ((TextView) findViewById(R.id.tvscan_passportfront_flag)).setVisibility(View.VISIBLE);
                    ((TextView) findViewById(R.id.tvscan_passportfront_flag)).setBackgroundResource(R.drawable.icon_checkmark);


                } else {
                    PASSPORT_PDF = null;
                    android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                            PassportActivity.this);

                    // set title


                    // set dialog message
                    alertDialogBuilder
                            .setMessage("File size should be less than 5 MB")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {


                                }
                            });


                    // create alert dialog
                    android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                    msgTxt.setTypeface(font);


                }


            }else
            {
                Toast.makeText(getApplicationContext(),"Please select valid formate.",Toast.LENGTH_LONG).show();
            }

            //String mfileName=    getFileName(data.getData());
  //          Toast.makeText(getApplicationContext(),"load passport"+mfileName+a,Toast.LENGTH_LONG).show();


        }
        if(requestCode == RESULT_LOAD_PDF_PASSPORT_BACK && resultCode == RESULT_OK && null != data)
        {


            if(FileType(data.getData()).equalsIgnoreCase("application/pdf")) {

            float a= FileSize(data.getData())/(1024*1024);

            if(a<=4.8)
            {



                 PASSPORT_BACK_PDF=data.getData();
                ((TextView)findViewById(R.id.tvscan_passportback_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_passportback_flag)).setBackgroundResource(R.drawable.icon_checkmark);


            }else
            {

                PASSPORT_BACK_PDF=null;

                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                        PassportActivity.this);

                // set title


                // set dialog message
                alertDialogBuilder
                        .setMessage("File size should be less than 5 MB")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        });


                // create alert dialog
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);



            }

            }else
            {
                Toast.makeText(getApplicationContext(),"Please select valid formate.",Toast.LENGTH_LONG).show();
            }


           // String mfileName=    getFileName(data.getData());
//            Toast.makeText(getApplicationContext(),"load passport back."+mfileName+a,Toast.LENGTH_LONG).show();

        }

        if (requestCode == RESULT_LOAD_IMAGE_PASSPORT_BACK && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String browerpassort = cursor.getString(columnIndex);
            cursor.close();


            passportback_flag="Y";


            Uri selectedImageURI = data.getData();
            //File imageFile = new File(getRealPathFromURI(selectedImageURI));

            float a = FileSize(data.getData()) / (1024 * 1024);
            //float a=imageFile.length()/(1024*1024);

            if(a<=4.8)
            {


//                if(bitmapbackpassport!=null)
//                {
//                    bitmapbackpassport.recycle();
//                }

               // bitmapbackpassport= BitmapFactory.decodeFile(browerpassort);
                bitmapbackpassporturi= data.getData();
                ((TextView)findViewById(R.id.tvscan_passportback_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_passportback_flag)).setBackgroundResource(R.drawable.icon_checkmark);


            }else
            {

                bitmapbackpassporturi= null;


                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                        PassportActivity.this);

                // set title


                // set dialog message
                alertDialogBuilder
                        .setMessage("File size should be less than 5 MB")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        });


                // create alert dialog
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);



            }
            System.out.println("images"+a);

        }

        if(requestCode == PICK_FROM_CAMERA && resultCode == RESULT_OK )
        {

            if(scan_flag==1)
            {

                Intent intent=new Intent(PassportActivity.this,CropImageActivity.class);
                intent.putExtra("name","pf");
                startActivityForResult(intent, 10);


            }else if(scan_flag==2)
            {
                Intent intent=new Intent(PassportActivity.this,CropImageActivity.class);
                intent.putExtra("name","pb");
                startActivityForResult(intent, 11);


            }



        }



        if(requestCode==10)
        {

            //Toast.makeText(getApplicationContext(),"savefront",Toast.LENGTH_LONG).show();

            try
            {

                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/temp");
                String fname = "pf.jpg";
                File file = new File(myDir, fname);


                bitmappassporturi = Uri.fromFile(file);

                passport_flag="Y";

                ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setBackgroundResource(R.drawable.icon_checkmark);





            }catch (Exception e)
            {

            }



        }
        if(requestCode==11)
        {

            //Toast.makeText(getApplicationContext(),"saveback",Toast.LENGTH_LONG).show();


try{

    String root = Environment.getExternalStorageDirectory().toString();
    File myDir = new File(root + "/temp");
    String fname = "pb.jpg";
    File file = new File(myDir, fname);


    bitmapbackpassporturi = Uri.fromFile(file);


    passportback_flag="Y";


    //bitmapbackpassport=(Bitmap) data.getExtras().get("data");
    ((TextView)findViewById(R.id.tvscan_passportback_flag)).setVisibility(View.VISIBLE);
    ((TextView)findViewById(R.id.tvscan_passportback_flag)).setBackgroundResource(R.drawable.icon_checkmark);


}catch (Exception e)
{

}



        }




        if(requestCode == PICK_FROM_CAMERA &&resultCode == RESULT_CANCELED)
        {
            // Toast.makeText(getApplicationContext(),"cancle",Toast.LENGTH_LONG).show();
        }
        if(requestCode == PICK_FROM_FILE && resultCode == RESULT_OK )
        {
            mImageCaptureUri = data.getData();
        }


        if(requestCode == CROP_FROM_CAMERA && resultCode == RESULT_OK )
        {
            Bundle extras = data.getExtras();


        }
    }



    private void doCrop() {
        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();
        /**
         * Open image crop app by starting an intent
         * �com.android.camera.action.CROP�.
         */
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        /**
         * Check if there is image cropper app installed.
         */
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(
                intent, 0);

        int size = list.size();

        /**
         * If there is no image cropper app, display warning message
         */
        if (size == 0) {

            Toast.makeText(this, "Can not find image crop app",
                    Toast.LENGTH_SHORT).show();

            return;
        } else {
            /**
             * Specify the image path, crop dimension and scale
             */
            intent.setData(mImageCaptureUri);

            intent.putExtra("outputX", 236);
            intent.putExtra("outputY", 330.5);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            intent.putExtra("return-data", true);
            /**
             * There is posibility when more than one image cropper app exist,
             * so we have to check for it first. If there is only one app, open
             * then app.
             */

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName,
                        res.activityInfo.name));

                startActivityForResult(i, CROP_FROM_CAMERA);
            } else {
                /**
                 * If there are several app exist, create a custom chooser to
                 * let user selects the app.
                 */
                for (ResolveInfo res : list) {
                    final CropOption co = new CropOption();

                    co.title = getPackageManager().getApplicationLabel(
                            res.activityInfo.applicationInfo);
                    co.icon = getPackageManager().getApplicationIcon(
                            res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);

                    co.appIntent
                            .setComponent(new ComponentName(
                                    res.activityInfo.packageName,
                                    res.activityInfo.name));

                    cropOptions.add(co);
                }

                CropOptionAdapter adapter = new CropOptionAdapter(
                        getApplicationContext(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose Crop App");
                builder.setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                startActivityForResult(
                                        cropOptions.get(item).appIntent,
                                        CROP_FROM_CAMERA);
                            }
                        });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        try{

                            if (mImageCaptureUri != null) {
                                getContentResolver().delete(mImageCaptureUri, null,
                                        null);
                                mImageCaptureUri = null;
                            }

                        }catch (Exception e)
                        {
                            System.out.println("Cancel Error"+e);
                        }


                    }
                });

                AlertDialog alert = builder.create();

                alert.show();
            }
        }
    }




    public class CropOptionAdapter extends ArrayAdapter<CropOption> {
        private ArrayList<CropOption> mOptions;
        private LayoutInflater mInflater;

        public CropOptionAdapter(Context context, ArrayList<CropOption> options) {
            super(context, R.layout.crop_selector, options);

            mOptions = options;

            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup group) {
            if (convertView == null)
                convertView = mInflater.inflate(R.layout.crop_selector, null);

            CropOption item = mOptions.get(position);

            if (item != null) {
                ((ImageView) convertView.findViewById(R.id.iv_icon))
                        .setImageDrawable(item.icon);
                ((TextView) convertView.findViewById(R.id.tv_name))
                        .setText(item.title);

                return convertView;
            }

            return null;
        }
    }

    public class CropOption {
        public CharSequence title;
        public Drawable icon;
        public Intent appIntent;
    }

    private class GetThings extends AsyncTask<Void, Void, String[]>
    {

        String mgetThingsResponse="";
        Context context;
        String jsonObject;
        JSONArray jsonarray;
        ProgressDialog pd;
        Boolean flag=true;
        String send_passport_result="";
        String send_backpassport_result="";
        String send_passport_result_pdf="";
        String send_backpassport_result_pdf="";
        String send_photo_result="";
        String send_support_result="";
        String send_support_result2="";
        String send_support_result3="";


        public GetThings(Context context,String jsonObject) {
            super();
            this.context=context;
            this.jsonObject=jsonObject;
            max_size=false;
            pd= new ProgressDialog(context).show(context, "", "Uploading document..");
            pd.show();
        }
        @Override
        protected String[] doInBackground(Void... params) {

            String [] mStrings=null;

            try {



               /* if(bitmappassport!=null)
                {
                    send_passport_result= sendpassport(bitmappassport,travel_id);
                }


                if(bitmapbackpassport!=null)
                {
                    send_backpassport_result= sendBackpassport(bitmapbackpassport,travel_id);
                }
*/




                if(bitmappassporturi!=null)
                {
                    send_passport_result= sendpassporturi(bitmappassporturi,travel_id);
                }


                if(bitmapbackpassporturi!=null)
                {
                    send_backpassport_result= sendBackpassporturi(bitmapbackpassporturi,travel_id);
                }




                if(PASSPORT_PDF!=null)
                {
                    send_passport_result_pdf= sendpdfpassport(inputStreamFromUri(PASSPORT_PDF),travel_id);
                }
                if(PASSPORT_BACK_PDF!=null)
                {
                    send_backpassport_result_pdf= sendpdfBackpassport(inputStreamFromUri(PASSPORT_BACK_PDF),travel_id);
                }




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

                flag=false;
            }
            return mStrings;
        }
        @Override
        protected void onPostExecute(String[] result) {

            try
            {
                if(pd.isShowing())
                {
                    pd.dismiss();
                }

                if(true) {

                    JSONObject savedata;
                    JSONObject savefilepassport;
                    JSONObject savefilephoto;
                    JSONObject savefilesupport;

                    JSONObject savefilebackpassport;
                    JSONObject savefilesupport2;
                    JSONObject savefilesupport3;


                    JSONObject temp = null;
                    temp = new JSONObject(jsonObject);


                    if (!send_passport_result.equalsIgnoreCase("")) {
                        savefilepassport = new JSONObject(send_passport_result);


                        if (savefilepassport.has("success")) {
                            temp.put("pass_copy", "y");

                            flag = true;
                        } else {
                            flag = false;
                        }

                    }

                    if (!send_backpassport_result.equalsIgnoreCase("")) {
                        savefilebackpassport = new JSONObject(send_backpassport_result);


                        if (savefilebackpassport.has("success")) {
                            temp.put("pass_copy_back", "y");

                            flag = true;
                        } else {
                            flag = false;
                        }

                    }


                    if (!send_passport_result_pdf.equalsIgnoreCase("")) {
                        JSONObject savefilepassportpdf = new JSONObject(send_passport_result_pdf);


                        if (savefilepassportpdf.has("success")) {
                            temp.put("pass_copy", "y");

                            flag = true;
                        } else {
                            flag = false;
                        }

                    }

                    if (!send_backpassport_result_pdf.equalsIgnoreCase("")) {
                        JSONObject savefilebackpassportpdf = new JSONObject(send_backpassport_result_pdf);


                        if (savefilebackpassportpdf.has("success")) {
                            temp.put("pass_copy_back", "y");

                            flag = true;
                        } else {
                            flag = false;
                        }

                    }



                    if (flag) {



                        Intent intent = new Intent(PassportActivity.this, SupportDoucumentActivity.class);
                        intent.putExtra("v_surname", getIntent().getExtras().getString("v_surname"));
                        intent.putExtra("v_firstname", getIntent().getExtras().getString("v_firstname"));
                        intent.putExtra("v_sex", getIntent().getExtras().getString("v_sex"));
                        intent.putExtra("v_passport", getIntent().getExtras().getString("v_passport"));
                        intent.putExtra("v_dob", getIntent().getExtras().getString("v_dob"));
                        intent.putExtra("v_doi", getIntent().getExtras().getString("v_doi"));
                        intent.putExtra("v_doe", getIntent().getExtras().getString("v_doe"));
                        intent.putExtra("v_poissuance", getIntent().getExtras().getString("v_poissuance"));
                        intent.putExtra("v_mealplan", getIntent().getExtras().getString("v_mealplan"));
                        intent.putExtra("v_hub", getIntent().getExtras().getString("v_hub"));
                        intent.putExtra("v_special", getIntent().getExtras().getString("v_special"));
                        intent.putExtra("object", temp.toString());


                        startActivity(intent);


                    } else {
                        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                                PassportActivity.this);

                        // set title


                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Error please try again!")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {


                                    }
                                });


                        // create alert dialog
                        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();

                        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                        TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                        msgTxt.setTypeface(font);

                    }


                }else
                {


                    android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                            PassportActivity.this);

                    // set title


                    // set dialog message
                    alertDialogBuilder
                            .setMessage("File size should be less than 5 MB")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {


                                }
                            });


                    // create alert dialog
                    android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                    msgTxt.setTypeface(font);


                }


            }catch (Exception e)
            {
                System.out.println("Error"+e);
            }



            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
        }
    }









    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_passport, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    public String sendpassport(Bitmap bitmap,String traveleid) throws Exception {

        String aa="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            // Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
           // Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
           // bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] data = bos.toByteArray();

                    entity.addPart("id", new StringBody(traveleid));


                    // sending a Image;0
                    // note here, that you can send more than one image, just add another param, same rule to the String;
                    entity.addPart("pass_copy", new ByteArrayBody(data, "pass_copy.jpg"));
                    httpPost.setEntity(entity);
                    HttpResponse response = httpClient.execute(httpPost, localContext);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                    aa = reader.readLine();

                    System.out.println("imageupload" + aa);


        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return aa;

    }


    public String sendpassporturi(Uri bitmapuri,String traveleid) throws Exception {

        String aa="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            // Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            // Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            // bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), bitmapuri);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] data = bos.toByteArray();

            entity.addPart("id", new StringBody(traveleid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("pass_copy", new ByteArrayBody(data, "pass_copy.jpg"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            aa = reader.readLine();

            System.out.println("imageupload" + aa);


            File f=new File(bitmapuri.getPath().toString());
            if(f.exists()) {
                f.delete();
            }


        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return aa;

    }


    public String sendBackpassport(Bitmap bitmap,String traveleid) throws Exception {

        String aa="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            // Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
           // Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
           // bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] data = bos.toByteArray();


                    entity.addPart("id", new StringBody(traveleid));


                    // sending a Image;0
                    // note here, that you can send more than one image, just add another param, same rule to the String;
                    entity.addPart("pass_copy_back", new ByteArrayBody(data, "pass_copy_back.jpg"));
                    httpPost.setEntity(entity);
                    HttpResponse response = httpClient.execute(httpPost, localContext);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                    aa = reader.readLine();

                    System.out.println("imageupload" + aa);


        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return aa;

    }



    public String sendBackpassporturi(Uri bitmapuri,String traveleid) throws Exception {

        String aa="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            // Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            // Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), bitmapuri);
            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            // bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] data = bos.toByteArray();


            entity.addPart("id", new StringBody(traveleid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("pass_copy_back", new ByteArrayBody(data, "pass_copy_back.jpg"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            aa = reader.readLine();

            System.out.println("imageupload" + aa);


            File f=new File(bitmapuri.getPath().toString());
            if(f.exists()) {
                f.delete();
            }

        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return aa;

    }



    public String sendpdfpassport(InputStream inputStream,String traveleid) throws Exception {

        String aa="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            // Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            // Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);


            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            // bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);


            byte[] data =readFully(inputStream);

            entity.addPart("id", new StringBody(traveleid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("pass_copy", new ByteArrayBody(data, "pass_copy.pdf"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            aa = reader.readLine();

            System.out.println("imageuploadpdf" + aa);


        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return aa;

    }


    public String sendpdfBackpassport(InputStream inputStream,String traveleid) throws Exception {

        String aa="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            // Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            // Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);


            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            // bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);

            byte[] data =readFully(inputStream);


            entity.addPart("id", new StringBody(traveleid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("pass_copy_back", new ByteArrayBody(data, "pass_copy_back.pdf"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            aa = reader.readLine();

            System.out.println("imageupload" + aa);


        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return aa;

    }





    private InputStream inputStreamFromUri(Uri uri) throws IOException {
        InputStream inputStream = getContentResolver().openInputStream(uri);
       // Savefile(inputStream,"a");
        return inputStream;
    }

    private void readTextFromUri(Uri uri) throws IOException {
        InputStream inputStream = getContentResolver().openInputStream(uri);
        Savefile(inputStream,"a");
        //return stringBuilder.toString();
    }



    public void openFile(String minmeType,int REQUEST_CODE) {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(minmeType);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // special intent for Samsung file manager
        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
        // if you want any file type, you can skip next line
        sIntent.putExtra("CONTENT_TYPE", minmeType);
        sIntent.addCategory(Intent.CATEGORY_DEFAULT);

        Intent chooserIntent;
        if (getPackageManager().resolveActivity(sIntent, 0) != null){
            // it is device with samsung file manager
            chooserIntent = Intent.createChooser(sIntent, "Open file");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { intent});
        }
        else {
            chooserIntent = Intent.createChooser(intent, "Open file");
        }

        try {
            startActivityForResult(chooserIntent, REQUEST_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), "No suitable File Manager was found.", Toast.LENGTH_SHORT).show();
        }
    }





    private void Savefile(InputStream input,String type) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/tempfile");
        myDir.mkdirs();

        String fname = "d.pdf";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream output = new FileOutputStream(file);


            try {
                try {
                    byte[] buffer = new byte[4 * 1024]; // or other buffer size
                    int read;

                    while ((read = input.read(buffer)) != -1) {
                        output.write(buffer, 0, read);
                    }
                    output.flush();
                } finally {
                    output.close();
                }
            } catch (Exception e) {
                e.printStackTrace(); // handle exception, define IOException and others
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }




    private long FileSize(Uri fileUri)
    {
        // Uri fileUri = data.getData();
        long size;

        if(fileUri.getScheme().equalsIgnoreCase("file"))
        {
            //Toast.makeText(getApplicationContext(),"file",Toast.LENGTH_LONG).show();

            File file=new File(fileUri.getPath());
            size=file.length();

        }else
        {

            Cursor cursor = PassportActivity.this.getContentResolver().query(fileUri,
                    null, null, null, null);
            cursor.moveToFirst();
            size = cursor.getLong(cursor.getColumnIndex(OpenableColumns.SIZE));
            cursor.close();


        }


        //Toast.makeText(getApplicationContext(),"content"+size,Toast.LENGTH_LONG).show();
        return size;

    }



    public byte[] readFully(InputStream inputStream) throws IOException {
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }



    public static byte[] readFullytemp(InputStream stream) throws IOException
    {
        byte[] buffer = new byte[16384];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int bytesRead;
        while ((bytesRead = stream.read(buffer)) != -1)
        {
            baos.write(buffer, 0, bytesRead);
        }
        return baos.toByteArray();
    }


    private String FileType(Uri fileUri)
    {
        // Uri fileUri = data.getData();
        String type="";

        if(fileUri.getScheme().equalsIgnoreCase("file"))
        {
            String uri = fileUri.toString();
            if(uri.contains(".")) {
                String extension = uri.substring(uri.lastIndexOf("."));
                System.out.println("type"+extension);
                if(extension.equalsIgnoreCase(".pdf"))
                {
                    type="application/pdf";
                }else
                {
                    type="";
                }
                //type

            }else
            {
                type="";
            }
        }else
        {


            String aa=  getContentResolver().getType(fileUri);
            if(!aa.equalsIgnoreCase("null")||aa!=null)
            {
                type=aa;

            }else
            {
                type="";

            }

        }
        //Toast.makeText(getApplicationContext(),"content"+size,Toast.LENGTH_LONG).show();
        return type;

    }

}
