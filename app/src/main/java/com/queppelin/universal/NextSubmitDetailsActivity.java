package com.queppelin.universal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.tool.StaticMethod;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class NextSubmitDetailsActivity extends AppCompatActivity {


    private static final int RESULT_LOAD_IMAGE_PASSPORT = 1;
    private static final int RESULT_LOAD_IMAGE_PHOTOGRAPH = 2;
    private static final int RESULT_LOAD_IMAGE_SUPPORT = 3;


    private static final int RESULT_LOAD_IMAGE_PASSPORT_BACK = 7;
    private static final int RESULT_LOAD_IMAGE_SUPPORT_1 = 8;
    private static final int RESULT_LOAD_IMAGE_SUPPORT_2 = 9;


    private static final int PICK_FROM_CAMERA = 4;
    private static final int CROP_FROM_CAMERA = 5;
    private static final int PICK_FROM_FILE = 6;
    private AlertDialog dialog;

    private int scan_flag=0;

    private Uri mImageCaptureUri;


    Bitmap bitmappassport=null;
    Bitmap bitmapphoto=null;
    Bitmap bitmapsupport=null;

    Bitmap bitmapbackpassport=null;
    Bitmap bitmapsupport1=null;
    Bitmap bitmapsupport2=null;



    String passport="";
    String photo="";
    String support="";

    String passportback="";
    String support1="";
    String support2="";

    String passport_flag="";
    String photo_flag="";
    String support_flag="";
    String passportback_flag="";
    String support_flag1="";
    String support_flag2="";

    public static Activity next;

    byte[] byteArraypassort;
    byte[] byteArraybackpassort;
    byte[] byteArrayphoto;
    byte[] byteArraysuppot;
    byte[] byteArraysuppot1;
    byte[] byteArraysuppot2;


    byte[] byteArraypassport;
    byte[] byteArraypassport_back;
    byte[] byteArrayphoto1;
    byte[] byteArraysupport;

    byte[] byteArraysupport1;
    byte[] byteArraysupport2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_submit_details);


        StaticMethod.stateactivity.add(NextSubmitDetailsActivity.this);

        next=NextSubmitDetailsActivity.this;
        ((RelativeLayout)findViewById(R.id.rback)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });


        getSupportActionBar().hide();



        byteArraypassport = getIntent().getByteArrayExtra("bitmap_passport");
        if(byteArraypassport!=null)
        {
            bitmappassport = BitmapFactory.decodeByteArray(byteArraypassport, 0, byteArraypassport.length);
        }else
        {
            bitmappassport=null;
        }


        byteArraypassport_back = getIntent().getByteArrayExtra("bitmap_passport_back");
        if(byteArraypassport_back!=null)
        {
            bitmapbackpassport = BitmapFactory.decodeByteArray(byteArraypassport_back, 0, byteArraypassport_back.length);
        }else
        {
            bitmapbackpassport=null;
        }




        byteArrayphoto1 = getIntent().getByteArrayExtra("bitmap_photo");

        if(byteArrayphoto1!=null)
        {
            bitmapphoto = BitmapFactory.decodeByteArray(byteArrayphoto1, 0, byteArrayphoto1.length);
        }else
        {
            bitmapphoto = null;
        }

        byteArraysupport = getIntent().getByteArrayExtra("bitmap_support");

        if(byteArraysupport!=null)
        {
            bitmapsupport = BitmapFactory.decodeByteArray(byteArraysupport, 0, byteArraysupport.length);
        }else
        {
            bitmapsupport=null;
        }



        byteArraysupport1 = getIntent().getByteArrayExtra("bitmap_support1");

        if(byteArraysupport1!=null)
        {
            bitmapsupport1 = BitmapFactory.decodeByteArray(byteArraysupport1, 0, byteArraysupport1.length);
        }else
        {
            bitmapsupport1=null;
        }


        byteArraysupport2 = getIntent().getByteArrayExtra("bitmap_support2");

        if(byteArraysupport2!=null)
        {
            bitmapsupport2 = BitmapFactory.decodeByteArray(byteArraysupport2, 0, byteArraysupport2.length);
        }else
        {
            bitmapsupport2=null;
        }





        String object=getIntent().getExtras().getString("object");

        try
        {
            JSONObject jobject=new JSONObject(object);

            passport = jobject.getString("pass_copy");
            photo = jobject.getString("photograph");
            support = jobject.getString("supp_doc");


            passportback = jobject.getString("pass_copy_back");
            support1 = jobject.getString("supp_doc_2");
            support2 = jobject.getString("supp_doc_3");



        }catch (Exception e)
        {

        }



        if(passport.equalsIgnoreCase("")||passport.equalsIgnoreCase("null"))
        {
            ((TextView)findViewById(R.id.tvscan_pass_flag)).setBackgroundResource(R.drawable.icon_closemark);

            passport_flag="";

        }else
        {
            ((TextView)findViewById(R.id.tvscan_pass_flag)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_pass_flag)).setBackgroundResource(R.drawable.icon_checkmark);

            passport_flag="Y";

        }


        if(passportback.equalsIgnoreCase("")||passportback.equalsIgnoreCase("null"))
        {
            ((TextView)findViewById(R.id.tvscan_pass_flag_back)).setBackgroundResource(R.drawable.icon_closemark);

            passportback_flag="";

        }else
        {
            ((TextView)findViewById(R.id.tvscan_pass_flag_back)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_pass_flag_back)).setBackgroundResource(R.drawable.icon_checkmark);
            passportback_flag="Y";

        }




        if(photo.equalsIgnoreCase("")||photo.equalsIgnoreCase("null"))
        {
             photo_flag="";

            ((TextView)findViewById(R.id.tvscan_photo_flag)).setBackgroundResource(R.drawable.icon_closemark);
        }else
        {
            photo_flag="Y";
            ((TextView)findViewById(R.id.tvscan_photo_flag)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_photo_flag)).setBackgroundResource(R.drawable.icon_checkmark);
        }

        if(support.equalsIgnoreCase("")||support.equalsIgnoreCase("null"))
        {
            support_flag="";
            ((TextView)findViewById(R.id.tvscan_suppot_flag_1)).setBackgroundResource(R.drawable.icon_closemark);

        }else
        {
            support_flag="Y";
            ((TextView)findViewById(R.id.tvscan_suppot_flag_1)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_suppot_flag_1)).setBackgroundResource(R.drawable.icon_checkmark);
        }


        if(support1.equalsIgnoreCase("")||support1.equalsIgnoreCase("null"))
        {
            support_flag1="";
            ((TextView)findViewById(R.id.tvscan_suppot_flag_2)).setBackgroundResource(R.drawable.icon_closemark);

        }else
        {
            support_flag1="Y";
            ((TextView)findViewById(R.id.tvscan_suppot_flag_2)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_suppot_flag_2)).setBackgroundResource(R.drawable.icon_checkmark);
        }


        if(support2.equalsIgnoreCase("")||support2.equalsIgnoreCase("null"))
        {
            support_flag2="";
            ((TextView)findViewById(R.id.tvscan_suppot_flag_3)).setBackgroundResource(R.drawable.icon_closemark);

        }else
        {
            support_flag2="Y";
            ((TextView)findViewById(R.id.tvscan_suppot_flag_3)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_suppot_flag_3)).setBackgroundResource(R.drawable.icon_checkmark);

        }




        ((TextView)findViewById(R.id.tvsubmit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try
                {

                    Intent intent = new Intent(NextSubmitDetailsActivity.this, SubmissionSummryDetails.class);
                    intent.putExtra("v_surname", getIntent().getExtras().getString("v_surname"));
                    intent.putExtra("v_firstname", getIntent().getExtras().getString("v_firstname"));
                    intent.putExtra("v_sex", getIntent().getExtras().getString("v_sex"));
                    intent.putExtra("v_passport", getIntent().getExtras().getString("v_passport"));
                    intent.putExtra("v_dob", getIntent().getExtras().getString("v_dob"));
                    intent.putExtra("v_doi", getIntent().getExtras().getString("v_doi"));
                    intent.putExtra("v_doe", getIntent().getExtras().getString("v_doe"));
                    intent.putExtra("v_poissuance", getIntent().getExtras().getString("v_poissuance"));
                    intent.putExtra("v_mealplan", getIntent().getExtras().getString("v_mealplan"));
                    intent.putExtra("v_hub", getIntent().getExtras().getString("v_hub"));
                    intent.putExtra("v_special", getIntent().getExtras().getString("v_special"));



                    if(bitmappassport!=null) {

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmappassport.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                        byteArraypassort = stream.toByteArray();
                        stream.close();

                    }


                    if(bitmapbackpassport!=null) {

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmapbackpassport.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                        byteArraybackpassort = stream.toByteArray();
                        stream.close();

                    }



                    if(bitmapphoto!=null)
                    {
                        ByteArrayOutputStream streamphoto = new ByteArrayOutputStream();
                        bitmapphoto.compress(Bitmap.CompressFormat.JPEG, 50, streamphoto);
                        byteArrayphoto = streamphoto.toByteArray();
                        streamphoto.close();

                    }

                    if(bitmapsupport!=null)
                    {
                        ByteArrayOutputStream streamsupport = new ByteArrayOutputStream();
                        bitmapsupport.compress(Bitmap.CompressFormat.JPEG, 50, streamsupport);
                        byteArraysuppot = streamsupport.toByteArray();
                        streamsupport.close();


                    }

                    if(bitmapsupport1!=null)
                    {
                        ByteArrayOutputStream streamsupport = new ByteArrayOutputStream();
                        bitmapsupport1.compress(Bitmap.CompressFormat.JPEG, 50, streamsupport);
                        byteArraysuppot1 = streamsupport.toByteArray();
                        streamsupport.close();


                    }

                    if(bitmapsupport2!=null)
                    {
                        ByteArrayOutputStream streamsupport = new ByteArrayOutputStream();
                        bitmapsupport2.compress(Bitmap.CompressFormat.JPEG, 50, streamsupport);
                        byteArraysuppot2 = streamsupport.toByteArray();
                        streamsupport.close();


                    }






                    intent.putExtra("bitmap_passport", byteArraypassort);
                    intent.putExtra("bitmap_photo", byteArrayphoto);
                    intent.putExtra("bitmap_support", byteArraysuppot);

                    intent.putExtra("bitmap_passport_back", byteArraybackpassort);
                    intent.putExtra("bitmap_support1", byteArraysuppot1);
                    intent.putExtra("bitmap_support2", byteArraysuppot2);



                    intent.putExtra("object", getIntent().getExtras().getString("object"));


                    startActivity(intent);






                }catch (Exception e)
                {
                    System.out.println("Error submit"+e);
                }



            }
        });




        ((TextView)findViewById(R.id.tvpasportbrowse)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE_PASSPORT);

            }
        });

        ((TextView)findViewById(R.id.tvpasportbrowse_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



               // Toast.makeText(getApplicationContext(),"Onclicklast",Toast.LENGTH_LONG).show();


                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE_PASSPORT_BACK);

            }
        });
        ((TextView)findViewById(R.id.tvphotobrowse)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE_PHOTOGRAPH);

            }
        });
        ((TextView)findViewById(R.id.tvsupportbrowse)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE_SUPPORT);

            }
        });



        ((TextView)findViewById(R.id.tvsupportbrowse_1)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE_SUPPORT_1);

            }
        });

        ((TextView)findViewById(R.id.tvsupportbrowse_2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE_SUPPORT_2);

            }
        });




        ((TextView)findViewById(R.id.tvpasportscan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                mImageCaptureUri = Uri.fromFile(new File(Environment
                        .getExternalStorageDirectory(), "tmp_avatar_"
                        + String.valueOf(System.currentTimeMillis())
                        + ".jpg"));
                scan_flag=1;
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        mImageCaptureUri);

                try {
                    intent.putExtra("return-data", true);

                    startActivityForResult(intent, PICK_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }


            }
        });


        ((TextView)findViewById(R.id.tvpasportscan_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                mImageCaptureUri = Uri.fromFile(new File(Environment
                        .getExternalStorageDirectory(), "tmp_avatar_"
                        + String.valueOf(System.currentTimeMillis())
                        + ".jpg"));
                scan_flag=4;
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        mImageCaptureUri);

                try {
                    intent.putExtra("return-data", true);

                    startActivityForResult(intent, PICK_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }


            }
        });




        ((TextView)findViewById(R.id.tvphotoscan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                scan_flag=2;

                             mImageCaptureUri = Uri.fromFile(new File(Environment
                        .getExternalStorageDirectory(), "tmp_avatar_"
                        + String.valueOf(System.currentTimeMillis())
                        + ".jpg"));

                try {
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        mImageCaptureUri);
                    intent.putExtra("return-data", true);
                    startActivityForResult(intent, PICK_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }

            }
        });


        ((TextView)findViewById(R.id.tvsupportscan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                scan_flag=3;
                mImageCaptureUri = Uri.fromFile(new File(Environment
                        .getExternalStorageDirectory(), "tmp_avatar_"
                        + String.valueOf(System.currentTimeMillis())
                        + ".jpg"));

                try {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            mImageCaptureUri);
                    intent.putExtra("return-data", true);

                    startActivityForResult(intent, PICK_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
               // captureImageInitialization();


            }
        });

        ((TextView)findViewById(R.id.tvsupportscan_1)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                scan_flag=5;
                mImageCaptureUri = Uri.fromFile(new File(Environment
                        .getExternalStorageDirectory(), "tmp_avatar_"
                        + String.valueOf(System.currentTimeMillis())
                        + ".jpg"));

                try {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            mImageCaptureUri);
                    intent.putExtra("return-data", true);

                    startActivityForResult(intent, PICK_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                // captureImageInitialization();


            }
        });


        ((TextView)findViewById(R.id.tvsupportscan_2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                scan_flag=6;
                mImageCaptureUri = Uri.fromFile(new File(Environment
                        .getExternalStorageDirectory(), "tmp_avatar_"
                        + String.valueOf(System.currentTimeMillis())
                        + ".jpg"));

                try {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            mImageCaptureUri);
                    intent.putExtra("return-data", true);

                    startActivityForResult(intent, PICK_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                // captureImageInitialization();


            }
        });





    }

    private void doCrop() {
        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();
        /**
         * Open image crop app by starting an intent
         * �com.android.camera.action.CROP�.
         */
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        /**
         * Check if there is image cropper app installed.
         */
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(
                intent, 0);

        int size = list.size();

        /**
         * If there is no image cropper app, display warning message
         */
        if (size == 0) {

            Toast.makeText(this, "Can not find image crop app",
                    Toast.LENGTH_SHORT).show();

            return;
        } else {
            /**
             * Specify the image path, crop dimension and scale
             */
            intent.setData(mImageCaptureUri);

            intent.putExtra("outputX", 200);
            intent.putExtra("outputY", 200);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            intent.putExtra("return-data", true);
            /**
             * There is posibility when more than one image cropper app exist,
             * so we have to check for it first. If there is only one app, open
             * then app.
             */

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName,
                        res.activityInfo.name));

                startActivityForResult(i, CROP_FROM_CAMERA);
            } else {
                /**
                 * If there are several app exist, create a custom chooser to
                 * let user selects the app.
                 */
                for (ResolveInfo res : list) {
                    final CropOption co = new CropOption();

                    co.title = getPackageManager().getApplicationLabel(
                            res.activityInfo.applicationInfo);
                    co.icon = getPackageManager().getApplicationIcon(
                            res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);

                    co.appIntent
                            .setComponent(new ComponentName(
                                    res.activityInfo.packageName,
                                    res.activityInfo.name));

                    cropOptions.add(co);
                }

                CropOptionAdapter adapter = new CropOptionAdapter(
                        getApplicationContext(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose Crop App");
                builder.setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                startActivityForResult(
                                        cropOptions.get(item).appIntent,
                                        CROP_FROM_CAMERA);
                            }
                        });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        try{

                            if (mImageCaptureUri != null) {
                                getContentResolver().delete(mImageCaptureUri, null,
                                        null);
                                mImageCaptureUri = null;
                            }

                        }catch (Exception e)
                        {
                            System.out.println("Cancel Error"+e);
                        }


                    }
                });

                AlertDialog alert = builder.create();

                alert.show();

            }
        }
    }
    byte[] byteimage;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE_PASSPORT && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
           String browerpassort = cursor.getString(columnIndex);
            cursor.close();

            bitmappassport= BitmapFactory.decodeFile(browerpassort);
            passport_flag="Y";

            ((TextView)findViewById(R.id.tvscan_pass_flag)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_pass_flag)).setBackgroundResource(R.drawable.icon_checkmark);


            //((ImageView)findViewById(R.id.imgtest)).setImageBitmap(bitmap);



//            Bitmap bitmap= BitmapFactory.decodeFile(picturePath);
//            ByteArrayOutputStream bos = new ByteArrayOutputStream();
//            System.out.println("ImagePath"+picturePath);
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);
//             byteimage = bos.toByteArray();








/*

              private static final int RESULT_LOAD_IMAGE_PASSPORT = 1;
    private static final int RESULT_LOAD_IMAGE_PHOTOGRAPH = 2;
    private static final int RESULT_LOAD_IMAGE_SUPPORT = 3;




            ImageView imageView = (ImageView) findViewById(R.id.imgView);
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));*/

        }

        if(requestCode == RESULT_LOAD_IMAGE_PHOTOGRAPH && resultCode == RESULT_OK && null != data)
        {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String browerphoto = cursor.getString(columnIndex);
            cursor.close();

            bitmapphoto= BitmapFactory.decodeFile(browerphoto);
photo_flag="Y";

            ((TextView)findViewById(R.id.tvscan_photo_flag)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_photo_flag)).setBackgroundResource(R.drawable.icon_checkmark);
            //((ImageView)findViewById(R.id.imgtest)).setImageBitmap(bitmap);

        }
        if(requestCode == RESULT_LOAD_IMAGE_SUPPORT && resultCode == RESULT_OK && null != data)
        {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
           String  browersupport = cursor.getString(columnIndex);
            cursor.close();




            bitmapsupport= BitmapFactory.decodeFile(browersupport);

            support_flag="Y";
            ((TextView)findViewById(R.id.tvscan_suppot_flag_1)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_suppot_flag_1)).setBackgroundResource(R.drawable.icon_checkmark);

            //((ImageView)findViewById(R.id.imgtest)).setImageBitmap(bitmap);

        }




        if (requestCode == RESULT_LOAD_IMAGE_PASSPORT_BACK && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String browerpassort = cursor.getString(columnIndex);
            cursor.close();

            bitmapbackpassport= BitmapFactory.decodeFile(browerpassort);
            passportback_flag="Y";

            ((TextView)findViewById(R.id.tvscan_pass_flag_back)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_pass_flag_back)).setBackgroundResource(R.drawable.icon_checkmark);


        }


        if (requestCode == RESULT_LOAD_IMAGE_SUPPORT_1 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String browerpassort = cursor.getString(columnIndex);

            cursor.close();

            bitmapsupport1= BitmapFactory.decodeFile(browerpassort);
            support_flag1="Y";

            ((TextView)findViewById(R.id.tvscan_suppot_flag_2)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_suppot_flag_2)).setBackgroundResource(R.drawable.icon_checkmark);


        }

        if (requestCode == RESULT_LOAD_IMAGE_SUPPORT_2 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String browerpassort = cursor.getString(columnIndex);

            cursor.close();

            bitmapsupport2= BitmapFactory.decodeFile(browerpassort);
            support_flag2="Y";

            ((TextView)findViewById(R.id.tvscan_suppot_flag_3)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_suppot_flag_3)).setBackgroundResource(R.drawable.icon_checkmark);


        }



        if(requestCode == PICK_FROM_CAMERA && resultCode == RESULT_OK )
        {
            //Toast.makeText(getApplicationContext(),"Onclick cemra",Toast.LENGTH_LONG).show();
            doCrop();

        }

        if(requestCode == PICK_FROM_CAMERA &&resultCode == RESULT_CANCELED)
        {
           // Toast.makeText(getApplicationContext(),"cancle",Toast.LENGTH_LONG).show();
        }
        if(requestCode == PICK_FROM_FILE && resultCode == RESULT_OK )
        {
            mImageCaptureUri = data.getData();
        }


        if(requestCode == CROP_FROM_CAMERA && resultCode == RESULT_OK )
        {
            Bundle extras = data.getExtras();

            if (extras != null) {
                //Bitmap photo = extras.getParcelable("data");

                // ((ImageView)findViewById(R.id.opacity)).setImageBitmap(photo);


                if(scan_flag==1)
                {

                    bitmappassport=(Bitmap) data.getExtras().get("data");
                    passport_flag="Y";

                    ((TextView)findViewById(R.id.tvscan_pass_flag)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.tvscan_pass_flag)).setBackgroundResource(R.drawable.icon_checkmark);

//                    Toast.makeText(getApplicationContext(),"passport",Toast.LENGTH_LONG).show();

                }else if(scan_flag==2)
                {

                    photo_flag="Y";
                    bitmapphoto=(Bitmap) data.getExtras().get("data");
                    ((TextView)findViewById(R.id.tvscan_photo_flag)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.tvscan_photo_flag)).setBackgroundResource(R.drawable.icon_checkmark);
                }else if(scan_flag==3)
                {
                    support_flag="Y";
                    bitmapsupport=(Bitmap) data.getExtras().get("data");
                    ((TextView)findViewById(R.id.tvscan_suppot_flag_1)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.tvscan_suppot_flag_1)).setBackgroundResource(R.drawable.icon_checkmark);

                }else if(scan_flag==4)
                {
                    passportback_flag="Y";
                    bitmapbackpassport=(Bitmap) data.getExtras().get("data");
                    ((TextView)findViewById(R.id.tvscan_pass_flag_back)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.tvscan_pass_flag_back)).setBackgroundResource(R.drawable.icon_checkmark);

                }else if(scan_flag==5)
                {
                    support_flag1="Y";
                    bitmapsupport1=(Bitmap) data.getExtras().get("data");
                    ((TextView)findViewById(R.id.tvscan_suppot_flag_2)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.tvscan_suppot_flag_2)).setBackgroundResource(R.drawable.icon_checkmark);

                }else if(scan_flag==6)
                {
                    support_flag2="Y";
                    bitmapsupport2=(Bitmap) data.getExtras().get("data");
                    ((TextView)findViewById(R.id.tvscan_suppot_flag_3)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.tvscan_suppot_flag_3)).setBackgroundResource(R.drawable.icon_checkmark);

                }





//                    SendHttpRequestTask t = new SendHttpRequestTask();
//                    String[] params = new String[]{url, myPrefs.getString("UserId", "").toString()};
//                    t.execute(params);
            }

            File f = new File(mImageCaptureUri.getPath());

            if (f.exists())
                f.delete();

        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_next_submit_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public class CropOptionAdapter extends ArrayAdapter<CropOption> {
        private ArrayList<CropOption> mOptions;
        private LayoutInflater mInflater;

        public CropOptionAdapter(Context context, ArrayList<CropOption> options) {
            super(context, R.layout.crop_selector, options);

            mOptions = options;

            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup group) {
            if (convertView == null)
                convertView = mInflater.inflate(R.layout.crop_selector, null);

            CropOption item = mOptions.get(position);

            if (item != null) {
                ((ImageView) convertView.findViewById(R.id.iv_icon))
                        .setImageDrawable(item.icon);
                ((TextView) convertView.findViewById(R.id.tv_name))
                        .setText(item.title);

                return convertView;
            }

            return null;
        }
    }

    public class CropOption {
        public CharSequence title;
        public Drawable icon;
        public Intent appIntent;
    }



}
