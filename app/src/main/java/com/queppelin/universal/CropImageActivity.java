package com.queppelin.universal;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.isseiaoki.simplecropview.CropImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Random;


public class CropImageActivity extends AppCompatActivity {


    Bitmap bitmapphoto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);
        getSupportActionBar().hide();

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/temp");
        String fname = "crap_temp.jpg";
        File file = new File (myDir, fname);
        //File f=new File(path, "profile.jpg");
        try
        {
        //   bitmapphoto = BitmapFactory.decodeStream(new FileInputStream(file));
        //   bitmapphoto = BitmapFactory.decodeFile(file.getAbsolutePath());


        }catch (Exception e)
        {

        }

        final CropImageView cropImageView = (CropImageView)findViewById(R.id.cropImageView);
        final ImageView croppedImageView = (ImageView)findViewById(R.id.croppedImageView);

    //    cropImageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);        // Set image for cropping

//      Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmapphoto, (int)(bitmapphoto.getWidth()*0.5), (int)(bitmapphoto.getHeight()*0.5), true);

      //  Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmapphoto, 2048, 2048, true);

/*vvvvvvvvvvvvvvvvvvvvvvv*/

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);

        int srcWidth = options.outWidth;
        int srcHeight = options.outHeight;

        int desiredWidth=3920;

// Only scale if the source is big enough. This code is just trying to fit a image into a certain width.
        if(desiredWidth > srcWidth)
            desiredWidth = srcWidth;



// Calculate the correct inSampleSize/scale value. This helps reduce memory use. It should be a power of 2
// from: http://stackoverflow.com/questions/477572/android-strange-out-of-memory-issue/823966#823966
        int inSampleSize = 1;
        while(srcWidth / 2 > desiredWidth){
            srcWidth /= 2;
            srcHeight /= 2;
            inSampleSize *= 2;
        }

        float desiredScale = (float) desiredWidth / srcWidth;

// Decode with inSampleSize
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inSampleSize = inSampleSize;
        options.inScaled = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap sampledSrcBitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);

// Resize
        Matrix matrix = new Matrix();
        matrix.postScale(desiredScale, desiredScale);
        Bitmap scaledBitmap = Bitmap.createBitmap(sampledSrcBitmap, 0, 0, sampledSrcBitmap.getWidth(), sampledSrcBitmap.getHeight(), matrix, true);
        sampledSrcBitmap = null;

      //  SaveImage(scaledBitmap);
        try {

        ExifInterface ei = new ExifInterface(file.getAbsolutePath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

        switch(orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                scaledBitmap= rotateImage(scaledBitmap, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                scaledBitmap= rotateImage(scaledBitmap, 180);
                break;
            // etc.
        }

        }catch (Exception e)
        {

        }
        cropImageView.setImageBitmap(scaledBitmap);


        TextView cropButton = (TextView)findViewById(R.id.crop_button);
        cropButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get cropped image, and show result.

                /*for photo*/
                if((getIntent().getExtras().getString("name")).equalsIgnoreCase("p"))
                {
                    SaveImage(cropImageView.getCroppedBitmap(),"p");
                }
               /*for passport*/
                if((getIntent().getExtras().getString("name")).equalsIgnoreCase("pf"))
                {
                    SaveImage(cropImageView.getCroppedBitmap(),"pf");
                }
                if((getIntent().getExtras().getString("name")).equalsIgnoreCase("pb"))
                {
                    SaveImage(cropImageView.getCroppedBitmap(),"pb");
                }
                /*support document*/

                if((getIntent().getExtras().getString("name")).equalsIgnoreCase("s1"))
                {
                    SaveImage(cropImageView.getCroppedBitmap(),"s1");
                }
                if((getIntent().getExtras().getString("name")).equalsIgnoreCase("s2"))
                {
                    SaveImage(cropImageView.getCroppedBitmap(),"s2");
                }
                if((getIntent().getExtras().getString("name")).equalsIgnoreCase("s3"))
                {
                    SaveImage(cropImageView.getCroppedBitmap(),"s3");
                }



                try
                {

                    String root = Environment.getExternalStorageDirectory().toString();
                    File myDir = new File(root + "/temp");
                    String fname = "crap_temp.jpg";
                    File file = new File (myDir, fname);
                    if (file.exists())
                        file.delete();

                    setResult(RESULT_OK);
                    finish();
                }catch (Exception e)
                {

                }

               // croppedImageView.setImageBitmap(cropImageView.getCroppedBitmap());
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();


        try
        {

            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/temp");
            String fname = "crap_temp.jpg";
            File file = new File (myDir, fname);
            if (file.exists())
                file.delete();

        }catch (Exception e)
        {

        }



    }

    private void SaveImage(Bitmap finalBitmap,String name) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/temp");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = name+".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

        return retVal;
    }

}
