package com.queppelin.universal;

import static com.queppelin.universal.CommonUtilities.SERVER_URL;
import static com.queppelin.universal.CommonUtilities.TAG;
import static com.queppelin.universal.CommonUtilities.displayMessage;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.tool.AppConfig;
import com.tool.HttpClass;
import com.tool.StaticMethod;

import org.json.JSONObject;


public final class ServerUtilities {
    private static final int MAX_ATTEMPTS = 5;
    private static final int BACKOFF_MILLI_SECONDS = 2000;
    private static final Random random = new Random();

   static Context context1;


    private  static SharedPreferences mPrefs;
    /**
     * Register this account/device pair within the server.
     *
     */
    public static void register(final Context context, String name, String email, final String regId) {
        Log.i(TAG, "registering device (regId = " + regId + ")");
        try
        {

            context1=context;
        String serverUrl = SERVER_URL;
        Map<String, String> params = new HashMap<String, String>();
        params.put("deviceId", regId);


            String  versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        params.put("deviceType", "ANDROID");
        params.put("versionName", versionName);




            //int  versionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;




        mPrefs = context.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        // Once GCM returns a registration id, we need to register on our server
        // As the server might be down, we will retry it a couple
        // times.
        for (int i = 1; i <= MAX_ATTEMPTS; i++) {
            Log.d(TAG, "Attempt #" + i + " to register");
            try {
                displayMessage(context, context.getString(
                        R.string.server_registering, i, MAX_ATTEMPTS));
                post(serverUrl, params);
                GCMRegistrar.setRegisteredOnServer(context, true);
                String message = context.getString(R.string.server_registered);
                CommonUtilities.displayMessage(context, message);
                return;
            } catch (IOException e) {
                // Here we are simplifying and retrying on any error; in a real
                // application, it should retry only on unrecoverable errors
                // (like HTTP error code 503).
                Log.e(TAG, "Failed to register on attempt " + i + ":" + e);
                if (i == MAX_ATTEMPTS) {
                    break;
                }
                try {
                    Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
                    Thread.sleep(backoff);
                } catch (InterruptedException e1) {
                    // Activity finished before we complete - exit.
                    Log.d(TAG, "Thread interrupted: abort remaining retries!");
                    Thread.currentThread().interrupt();
                    return;
                }
                // increase backoff exponentially
                backoff *= 2;
            }
        }
        String message = context.getString(R.string.server_register_error,
                MAX_ATTEMPTS);
        CommonUtilities.displayMessage(context, message);
        }catch (Exception e)
        {

        }


    }

    /**
     * Unregister this account/device pair within the server.
     */
    public static void unregister(final Context context, final String regId) {
        Log.i(TAG, "unregistering device (regId = " + regId + ")");
        String serverUrl = SERVER_URL + "/unregister";
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        try {
            post(serverUrl, params);
            GCMRegistrar.setRegisteredOnServer(context, false);
            String message = context.getString(R.string.server_unregistered);
            CommonUtilities.displayMessage(context, message);
        } catch (IOException e) {
            // At this point the device is unregistered from GCM, but still
            // registered in the server.
            // We could try to unregister again, but it is not necessary:
            // if the server tries to send a message to the device, it will get
            // a "NotRegistered" error message and should unregister the device.
            String message = context.getString(R.string.server_unregister_error,
                    e.getMessage());
            CommonUtilities.displayMessage(context, message);
        }
    }

    /**
     * Issue a POST request to the server.
     *
     * @param endpoint POST address.
     * @param params request parameters.
     *
     * @throws IOException propagated from POST.
     */
    private static void post(String endpoint, Map<String, String> params)
            throws IOException {




        String[] pro_key = new String[]{"deviceId","deviceType","deviceVersion","nativeDeviceId"};
        String[] pro_value = new String[]{(String)params.get("deviceId"),(String)params.get("deviceType"),(String)params.get("versionName"),StaticMethod.getUniqueId(context1)};


        try
        {
            RestClient client = new RestClient(endpoint,context1);
            client.AddHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            client.AddHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);
            for(int i=0;i<pro_key.length;i++)
            {
                client.AddParam(pro_key[i], pro_value[i]);
                 System.out.println("input::=>" + pro_value[i]);
            }
            client.Execute(RestClient.RequestMethod.POST);
            String ResponseVector = client.getResponse();

            System.out.println("output::" + ResponseVector);



            JSONObject jsonObject=new JSONObject(ResponseVector);


            if(jsonObject.has("success")) {


                Boolean flag = jsonObject.getBoolean("success");

                if (flag) {

                    SharedPreferences.Editor prefsEditor = mPrefs
                            .edit();
                    prefsEditor
                            .putString("deviceId", (String) params.get("deviceId"));
                    prefsEditor.putString("deviceType", (String) params.get("deviceType"));
                    prefsEditor.commit();


                }
            }














    }catch (Exception e)
        {


        }

    }
}
