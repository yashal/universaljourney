package com.queppelin.universal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.tool.AppConfig;
import com.tool.StaticMethod;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;







public class SupportDoucumentActivity extends AppCompatActivity {


    private static final int RESULT_LOAD_IMAGE_SUPPORT = 3;
    private static final int RESULT_LOAD_IMAGE_SUPPORT_1 = 8;
    private static final int RESULT_LOAD_IMAGE_SUPPORT_2 = 9;



    private static final int RESULT_LOAD_PDF_SUPPORT = 13;
    private static final int RESULT_LOAD_PDF_SUPPORT_1 = 18;
    private static final int RESULT_LOAD_PDF_SUPPORT_2 = 19;


    Uri PDF_SUPPORT=null;
    Uri PDF_SUPPORT_1=null;
    Uri PDF_SUPPORT_2=null;


    private static final int PICK_FROM_CAMERA = 4;
    private static final int CROP_FROM_CAMERA = 5;
    private static final int PICK_FROM_FILE = 6;
    private AlertDialog dialog;

    private int scan_flag=0;

    private Uri mImageCaptureUri;


    JSONObject  object;
    String traveleOject;


    /*Bitmap bitmapsupport=null;
    Bitmap bitmapsupport1=null;
    Bitmap bitmapsupport2=null;
*/


    Uri bitmapsupporturi=null;
    Uri bitmapsupport1uri=null;
    Uri bitmapsupport2uri=null;



    String passport="";
    String photo="";
    String support="";
    String travel_id;
    String passportback="";
    String support1="";
    String support2="";

    String passport_flag="";
    String photo_flag="";
    String support_flag="";
    String passportback_flag="";
    String support_flag1="";
    String support_flag2="";

    public static Activity next;

    byte[] byteArraypassort;
    byte[] byteArraybackpassort;
    byte[] byteArrayphoto;
    byte[] byteArraysuppot;
    byte[] byteArraysuppot1;
    byte[] byteArraysuppot2;


    byte[] byteArraypassport;
    byte[] byteArraypassport_back;
    byte[] byteArrayphoto1;
    byte[] byteArraysupport;

    byte[] byteArraysupport1;
    byte[] byteArraysupport2;

    boolean max_size=false;
    Typeface font;

    String imageType="";
    boolean max_size_flag=false;


    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_doucument);
        font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");

        imageType="";

        StaticMethod.stateactivity.add(SupportDoucumentActivity.this);



        ((TextView)findViewById(R.id.tvheadertitle)).setTypeface(font);
        ((TextView)findViewById(R.id.tvpassportfront)).setTypeface(font);
        ((TextView)findViewById(R.id.tvpassportfronscan)).setTypeface(font);
        ((TextView)findViewById(R.id.tvpassportscaonbrowse)).setTypeface(font);
        ((TextView)findViewById(R.id.tvphoto)).setTypeface(font);
        ((TextView)findViewById(R.id.tvpassportbackscan)).setTypeface(font);
        ((TextView)findViewById(R.id.tvpassportbackbrowse)).setTypeface(font);



        ((TextView)findViewById(R.id.tvrleftsupport3)).setTypeface(font);
        ((TextView)findViewById(R.id.tvsupport3scan)).setTypeface(font);
        ((TextView)findViewById(R.id.tvsupport3browse)).setTypeface(font);

        ((TextView)findViewById(R.id.tvnote)).setTypeface(font);
        ((TextView)findViewById(R.id.tvskip)).setTypeface(font);
        ((TextView)findViewById(R.id.tvsubmit)).setTypeface(font);

        ((TextView)findViewById(R.id.tvback)).setTypeface(font);




        next=SupportDoucumentActivity.this;
        ((RelativeLayout)findViewById(R.id.rback)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });


        getSupportActionBar().hide();

        traveleOject=getIntent().getExtras().getString("object");


        try
        {

            object=new JSONObject(traveleOject);
            support = object.getString("supp_doc");
            support1 = object.getString("supp_doc_2");
            support2 = object.getString("supp_doc_3");
            travel_id=object.getString("id");

        }catch (Exception e)
        {

        }



        if(support.equalsIgnoreCase("")||support.equalsIgnoreCase("null"))
        {

            ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setVisibility(View.INVISIBLE);
            ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setBackgroundResource(R.drawable.icon_closemark);
        }else
        {
            ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setBackgroundResource(R.drawable.icon_checkmark);
        }


        if(support1.equalsIgnoreCase("")||support1.equalsIgnoreCase("null"))
        {

            ((TextView)findViewById(R.id.tvscan_passportback_flag)).setVisibility(View.INVISIBLE);
            ((TextView)findViewById(R.id.tvscan_passportback_flag)).setBackgroundResource(R.drawable.icon_closemark);
        }else
        {
            ((TextView)findViewById(R.id.tvscan_passportback_flag)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_passportback_flag)).setBackgroundResource(R.drawable.icon_checkmark);
        }



        if(support2.equalsIgnoreCase("")||support2.equalsIgnoreCase("null"))
        {

            ((TextView)findViewById(R.id.tvscan_tvsupport3browse_flag)).setVisibility(View.INVISIBLE);
            ((TextView)findViewById(R.id.tvscan_tvsupport3browse_flag)).setBackgroundResource(R.drawable.icon_closemark);
        }else
        {
            ((TextView)findViewById(R.id.tvscan_tvsupport3browse_flag)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tvscan_tvsupport3browse_flag)).setBackgroundResource(R.drawable.icon_checkmark);
        }




        ((TextView)findViewById(R.id.tvpassportfronscan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                imageType="S";
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/temp");
                myDir.mkdirs();
                scan_flag = 1;
                String fname = "crap_temp.jpg";
                File file = new File (myDir, fname);
                if (file.exists ()) file.delete ();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, PICK_FROM_CAMERA);
                }




/*
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                scan_flag=1;
                mImageCaptureUri = Uri.fromFile(new File(Environment
                        .getExternalStorageDirectory(), "tmp_avatar_"
                        + String.valueOf(System.currentTimeMillis())
                        + ".jpg"));

                try {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            mImageCaptureUri);
                    intent.putExtra("return-data", true);

                    startActivityForResult(intent, PICK_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                // captureImageInitialization();

*/



            }
        });



        ((TextView)findViewById(R.id.tvpassportbackscan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                imageType="S";
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/temp");
                myDir.mkdirs();
                scan_flag = 2;
                String fname = "crap_temp.jpg";
                File file = new File (myDir, fname);
                if (file.exists ()) file.delete ();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, PICK_FROM_CAMERA);
                }



              /*  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                scan_flag=2;
                mImageCaptureUri = Uri.fromFile(new File(Environment
                        .getExternalStorageDirectory(), "tmp_avatar_"
                        + String.valueOf(System.currentTimeMillis())
                        + ".jpg"));

                try {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            mImageCaptureUri);
                    intent.putExtra("return-data", true);

                    startActivityForResult(intent, PICK_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                // captureImageInitialization();

*/



            }
        });


        ((TextView)findViewById(R.id.tvsupport3scan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageType="S";

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/temp");
                myDir.mkdirs();
                scan_flag = 3;
                String fname = "crap_temp.jpg";
                File file = new File (myDir, fname);
                if (file.exists ()) file.delete ();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, PICK_FROM_CAMERA);
                }





/*
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                scan_flag=3;
                mImageCaptureUri = Uri.fromFile(new File(Environment
                        .getExternalStorageDirectory(), "tmp_avatar_"
                        + String.valueOf(System.currentTimeMillis())
                        + ".jpg"));

                try {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            mImageCaptureUri);
                    intent.putExtra("return-data", true);

                    startActivityForResult(intent, PICK_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
*/
                // captureImageInitialization();

            }
        });



        ((TextView)findViewById(R.id.tvpassportscaonbrowse)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageType="B";
//                Intent i = new Intent(
//                        Intent.ACTION_PICK,
//                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(i, RESULT_LOAD_IMAGE_SUPPORT);



                CharSequence colors[] = new CharSequence[] {"Image file", "PDF file"};

                AlertDialog.Builder builder = new AlertDialog.Builder(SupportDoucumentActivity.this);
                builder.setTitle("Choose ...");
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // the user clicked on colors[which]


                        if(which==0)
                        {
                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, RESULT_LOAD_IMAGE_SUPPORT);


                        }else if(which==1)
                        {


                            openFile("application/pdf",RESULT_LOAD_PDF_SUPPORT);


/*
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.setType("application/pdf");
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            try {
                                startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), RESULT_LOAD_PDF_SUPPORT);
                            } catch (android.content.ActivityNotFoundException ex) {
                                ex.printStackTrace();
                            }*/



                        }

                    }
                });
                builder.show();




            }
        });



        ((TextView)findViewById(R.id.tvpassportbackbrowse)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageType="B";

//                Intent i = new Intent(
//                        Intent.ACTION_PICK,
//                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(i, RESULT_LOAD_IMAGE_SUPPORT_1);


                CharSequence colors[] = new CharSequence[] {"Image file", "PDF file"};

                AlertDialog.Builder builder = new AlertDialog.Builder(SupportDoucumentActivity.this);
                builder.setTitle("Choose ...");
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // the user clicked on colors[which]


                        if(which==0)
                        {
                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, RESULT_LOAD_IMAGE_SUPPORT_1);


                        }else if(which==1)
                        {

                            openFile("application/pdf",RESULT_LOAD_PDF_SUPPORT_1);

                         /*   Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.setType("application/pdf");
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            try {
                                startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), RESULT_LOAD_PDF_SUPPORT_1);
                            } catch (android.content.ActivityNotFoundException ex) {
                                ex.printStackTrace();
                            }
*/


                        }

                    }
                });
                builder.show();







            }
        });


        ((TextView)findViewById(R.id.tvsupport3browse)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                imageType="B";
//                Intent i = new Intent(
//                        Intent.ACTION_PICK,
//                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(i, RESULT_LOAD_IMAGE_SUPPORT_2);





                CharSequence colors[] = new CharSequence[] {"Image file", "PDF file"};

                AlertDialog.Builder builder = new AlertDialog.Builder(SupportDoucumentActivity.this);
                builder.setTitle("Choose ...");
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // the user clicked on colors[which]


                        if(which==0)
                        {
                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, RESULT_LOAD_IMAGE_SUPPORT_2);


                        }else if(which==1)
                        {

                            openFile("application/pdf",RESULT_LOAD_PDF_SUPPORT_2);
/*
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.setType("application/pdf");
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            try {
                                startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), RESULT_LOAD_PDF_SUPPORT_2);
                            } catch (android.content.ActivityNotFoundException ex) {
                                ex.printStackTrace();
                            }

*/


                        }

                    }
                });
                builder.show();






            }
        });





        ((TextView)findViewById(R.id.tvskip)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(SupportDoucumentActivity.this,SubmissionSummryDetails.class);
                intent.putExtra("v_surname", getIntent().getExtras().getString("v_surname"));
                intent.putExtra("v_firstname", getIntent().getExtras().getString("v_firstname"));
                intent.putExtra("v_sex", getIntent().getExtras().getString("v_sex"));
                intent.putExtra("v_passport", getIntent().getExtras().getString("v_passport"));
                intent.putExtra("v_dob", getIntent().getExtras().getString("v_dob"));
                intent.putExtra("v_doi", getIntent().getExtras().getString("v_doi"));
                intent.putExtra("v_doe", getIntent().getExtras().getString("v_doe"));
                intent.putExtra("v_poissuance", getIntent().getExtras().getString("v_poissuance"));
                intent.putExtra("v_mealplan", getIntent().getExtras().getString("v_mealplan"));
                intent.putExtra("v_hub", getIntent().getExtras().getString("v_hub"));
                intent.putExtra("v_special", getIntent().getExtras().getString("v_special"));
                intent.putExtra("object",object.toString());


                startActivity(intent);


            }
        });


        ((TextView)findViewById(R.id.tvsubmit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {





                if(StaticMethod.isInternetAvailable(SupportDoucumentActivity.this)) {

                    if (bitmapsupporturi == null && bitmapsupport1uri == null && bitmapsupport2uri == null&&PDF_SUPPORT==null &&PDF_SUPPORT_1==null && PDF_SUPPORT_2==null) {


                        if (support2.equalsIgnoreCase("") && support1.equalsIgnoreCase("") && support.equalsIgnoreCase("")) {

                            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                                    SupportDoucumentActivity.this);

                            // set title


                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("Please select photo graph")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {


                                        }
                                    });


                            // create alert dialog
                            android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                            // show it
                            alertDialog.show();
                            Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                            msgTxt.setTypeface(font);


                        } else {


                            Intent intent = new Intent(SupportDoucumentActivity.this, SubmissionSummryDetails.class);
                            intent.putExtra("v_surname", getIntent().getExtras().getString("v_surname"));
                            intent.putExtra("v_firstname", getIntent().getExtras().getString("v_firstname"));
                            intent.putExtra("v_sex", getIntent().getExtras().getString("v_sex"));
                            intent.putExtra("v_passport", getIntent().getExtras().getString("v_passport"));
                            intent.putExtra("v_dob", getIntent().getExtras().getString("v_dob"));
                            intent.putExtra("v_doi", getIntent().getExtras().getString("v_doi"));
                            intent.putExtra("v_doe", getIntent().getExtras().getString("v_doe"));
                            intent.putExtra("v_poissuance", getIntent().getExtras().getString("v_poissuance"));
                            intent.putExtra("v_mealplan", getIntent().getExtras().getString("v_mealplan"));
                            intent.putExtra("v_hub", getIntent().getExtras().getString("v_hub"));
                            intent.putExtra("v_special", getIntent().getExtras().getString("v_special"));
                            intent.putExtra("object", object.toString());


                            startActivity(intent);


                        }


                    } else {
                        if (StaticMethod.isInternetAvailable(SupportDoucumentActivity.this)) {


                            new GetThings(SupportDoucumentActivity.this, traveleOject).execute();
                        } else {
                            return;
                        }
                    }

                }else
                {
                    return;
                }



            }
        });


    }





    private void doCrop() {
        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();
        /**
         * Open image crop app by starting an intent
         * �com.android.camera.action.CROP�.
         */
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        /**
         * Check if there is image cropper app installed.
         */
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(
                intent, 0);

        int size = list.size();

        /**
         * If there is no image cropper app, display warning message
         */
        if (size == 0) {

            Toast.makeText(this, "Can not find image crop app",
                    Toast.LENGTH_SHORT).show();

            return;
        } else {
            /**
             * Specify the image path, crop dimension and scale
             */
            intent.setData(mImageCaptureUri);

            intent.putExtra("outputX", 264.5);
            intent.putExtra("outputY", 374);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            intent.putExtra("return-data", true);
            /**
             * There is posibility when more than one image cropper app exist,
             * so we have to check for it first. If there is only one app, open
             * then app.
             */

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName,
                        res.activityInfo.name));

                startActivityForResult(i, CROP_FROM_CAMERA);
            } else {
                /**
                 * If there are several app exist, create a custom chooser to
                 * let user selects the app.
                 */
                for (ResolveInfo res : list) {
                    final CropOption co = new CropOption();

                    co.title = getPackageManager().getApplicationLabel(
                            res.activityInfo.applicationInfo);
                    co.icon = getPackageManager().getApplicationIcon(
                            res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);

                    co.appIntent
                            .setComponent(new ComponentName(
                                    res.activityInfo.packageName,
                                    res.activityInfo.name));

                    cropOptions.add(co);
                }

                CropOptionAdapter adapter = new CropOptionAdapter(
                        getApplicationContext(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose Crop App");
                builder.setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                startActivityForResult(
                                        cropOptions.get(item).appIntent,
                                        CROP_FROM_CAMERA);
                            }
                        });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        try{

                            if (mImageCaptureUri != null) {
                                getContentResolver().delete(mImageCaptureUri, null,
                                        null);
                                mImageCaptureUri = null;
                            }

                        }catch (Exception e)
                        {
                            System.out.println("Cancel Error"+e);
                        }


                    }
                });

                AlertDialog alert = builder.create();

                alert.show();
            }
        }
    }




    public class CropOptionAdapter extends ArrayAdapter<CropOption> {
        private ArrayList<CropOption> mOptions;
        private LayoutInflater mInflater;

        public CropOptionAdapter(Context context, ArrayList<CropOption> options) {
            super(context, R.layout.crop_selector, options);

            mOptions = options;

            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup group) {
            if (convertView == null)
                convertView = mInflater.inflate(R.layout.crop_selector, null);

            CropOption item = mOptions.get(position);

            if (item != null) {
                ((ImageView) convertView.findViewById(R.id.iv_icon))
                        .setImageDrawable(item.icon);
                ((TextView) convertView.findViewById(R.id.tv_name))
                        .setText(item.title);

                return convertView;
            }

            return null;
        }
    }

    public class CropOption {
        public CharSequence title;
        public Drawable icon;
        public Intent appIntent;
    }


    public String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    protected void
    onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RESULT_LOAD_IMAGE_SUPPORT && resultCode == RESULT_OK && null != data)
        {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String  browersupport = cursor.getString(columnIndex);
            cursor.close();

            Uri selectedImageURI = data.getData();
            //File imageFile = new File(getRealPathFromURI(selectedImageURI));

            float a = FileSize(data.getData()) / (1024 * 1024);
           // float a=imageFile.length()/(1024*1024);

            if(a<=4.8)
            {



                bitmapsupporturi= data.getData();
                support_flag="Y";
                ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setBackgroundResource(R.drawable.icon_checkmark);

            }else
            {
                bitmapsupporturi=null;
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                        SupportDoucumentActivity.this);

                // set title


                // set dialog message
                alertDialogBuilder
                        .setMessage("File size should be less than 5 MB")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        });


                // create alert dialog
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);

            }
            System.out.println("images"+a);



            //((ImageView)findViewById(R.id.imgtest)).setImageBitmap(bitmap);

        }


        if(requestCode == RESULT_LOAD_PDF_SUPPORT && resultCode == RESULT_OK && null != data)
        {

            if(FileType(data.getData()).equalsIgnoreCase("application/pdf")) {
            float a= FileSize(data.getData())/(1024*1024);

            if(a<=4.8)
            {

                 PDF_SUPPORT=data.getData();
                ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setBackgroundResource(R.drawable.icon_checkmark);


            }else
            {
                PDF_SUPPORT=null;
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                        SupportDoucumentActivity.this);
                alertDialogBuilder
                        .setMessage("File size should be less than 5 MB")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        });
                // create alert dialog
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);



            }

            }else
            {
                Toast.makeText(getApplicationContext(),"Please select valid formate.",Toast.LENGTH_LONG).show();
            }

        }
        if(requestCode == RESULT_LOAD_PDF_SUPPORT_1 && resultCode == RESULT_OK && null != data)
        {
            if(FileType(data.getData()).equalsIgnoreCase("application/pdf")) {

            float a= FileSize(data.getData())/(1024*1024);

            if(a<=4.8)
            {


                PDF_SUPPORT_1=data.getData();
                ((TextView)findViewById(R.id.tvscan_passportback_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_passportback_flag)).setBackgroundResource(R.drawable.icon_checkmark);


            }else
            {
                PDF_SUPPORT_1=null;
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                        SupportDoucumentActivity.this);

                // set title


                // set dialog message
                alertDialogBuilder
                        .setMessage("File size should be less than 5 MB")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        });


                // create alert dialog
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);



            }

        }else
        {
            Toast.makeText(getApplicationContext(),"Please select valid formate.",Toast.LENGTH_LONG).show();
        }


    }
        if(requestCode == RESULT_LOAD_PDF_SUPPORT_2 && resultCode == RESULT_OK && null != data)
        {
            if(FileType(data.getData()).equalsIgnoreCase("application/pdf")) {

            float a= FileSize(data.getData())/(1024*1024);

            if(a<=4.8)
            {


                PDF_SUPPORT_2=data.getData();
                ((TextView)findViewById(R.id.tvscan_tvsupport3browse_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_tvsupport3browse_flag)).setBackgroundResource(R.drawable.icon_checkmark);

            }else
            {
                PDF_SUPPORT_2=null;
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                        SupportDoucumentActivity.this);

                // set title


                // set dialog message
                alertDialogBuilder
                        .setMessage("File size should be less than 5 MB")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        });


                // create alert dialog
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);



            }


            }else
            {
                Toast.makeText(getApplicationContext(),"Please select valid formate.",Toast.LENGTH_LONG).show();
            }

        }





        if (requestCode == RESULT_LOAD_IMAGE_SUPPORT_1 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String browerpassort = cursor.getString(columnIndex);

            cursor.close();


            Uri selectedImageURI = data.getData();
            //File imageFile = new File(getRealPathFromURI(selectedImageURI));

            float a = FileSize(data.getData()) / (1024 * 1024);
          //  float a=imageFile.length()/(1024*1024);

            if(a<=4.8)
            {


                bitmapsupport1uri= data.getData();
                support_flag1="Y";

                ((TextView)findViewById(R.id.tvscan_passportback_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_passportback_flag)).setBackgroundResource(R.drawable.icon_checkmark);

            }else
            {
                bitmapsupport1uri=null;
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                        SupportDoucumentActivity.this);

                // set title


                // set dialog message
                alertDialogBuilder
                        .setMessage("File size should be less than 5 MB")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        });


                // create alert dialog
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);
            }
            System.out.println("images"+a);



        }

        if (requestCode == RESULT_LOAD_IMAGE_SUPPORT_2 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String browerpassort = cursor.getString(columnIndex);

            cursor.close();


            Uri selectedImageURI = data.getData();
            //File imageFile = new File(getRealPathFromURI(selectedImageURI));

            float a = FileSize(data.getData()) / (1024 * 1024);
            //float a=imageFile.length()/(1024*1024);

            if(a<=4.8)
            {


                bitmapsupport2uri= data.getData();
                support_flag2="Y";

                ((TextView)findViewById(R.id.tvscan_tvsupport3browse_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_tvsupport3browse_flag)).setBackgroundResource(R.drawable.icon_checkmark);

            }else
            {
                bitmapsupport2uri=null;
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                        SupportDoucumentActivity.this);

                // set title


                // set dialog message
                alertDialogBuilder
                        .setMessage("File size should be less than 5 MB")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        });


                // create alert dialog
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);
            }
            System.out.println("images"+a);



        }



        if(requestCode == PICK_FROM_CAMERA && resultCode == RESULT_OK )
        {
            //Toast.makeText(getApplicationContext(),"Onclick cemra",Toast.LENGTH_LONG).show();
            //doCrop();

            if(scan_flag==1)
            {

                Intent intent=new Intent(SupportDoucumentActivity.this,CropImageActivity.class);
                intent.putExtra("name","s1");
                startActivityForResult(intent, 10);
            }else if(scan_flag==2)
            {
                Intent intent=new Intent(SupportDoucumentActivity.this,CropImageActivity.class);
                intent.putExtra("name","s2");
                startActivityForResult(intent, 11);

            }else if(scan_flag==3)
            {
                Intent intent=new Intent(SupportDoucumentActivity.this,CropImageActivity.class);
                intent.putExtra("name","s3");
                startActivityForResult(intent, 12);
            }






        }

        if(requestCode==10)
        {
            try
            {



                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/temp");
                String fname = "s1.jpg";
                File file = new File(myDir, fname);


                bitmapsupporturi = Uri.fromFile(file);


               // bitmapsupport=(Bitmap) data.getExtras().get("data");
                ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_passportfront_flag)).setBackgroundResource(R.drawable.icon_checkmark);


            }catch (Exception e)
            {

            }


        }
        if(requestCode==11)
        {

            try{


                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/temp");
                String fname = "s2.jpg";
                File file = new File(myDir, fname);


                bitmapsupport1uri = Uri.fromFile(file);




               // bitmapsupport1=(Bitmap) data.getExtras().get("data");
                ((TextView)findViewById(R.id.tvscan_passportback_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_passportback_flag)).setBackgroundResource(R.drawable.icon_checkmark);


            }catch (Exception e)
            {

            }


        }
        if(requestCode==12)
        {

            try
            {



                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/temp");
                String fname = "s3.jpg";
                File file = new File(myDir, fname);


                bitmapsupport2uri = Uri.fromFile(file);





               // bitmapsupport2=(Bitmap) data.getExtras().get("data");
                ((TextView)findViewById(R.id.tvscan_tvsupport3browse_flag)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvscan_tvsupport3browse_flag)).setBackgroundResource(R.drawable.icon_checkmark);



            }catch (Exception e)
            {

            }


        }

        if(requestCode == PICK_FROM_CAMERA &&resultCode == RESULT_CANCELED)
        {
            // Toast.makeText(getApplicationContext(),"cancle",Toast.LENGTH_LONG).show();
        }
        if(requestCode == PICK_FROM_FILE && resultCode == RESULT_OK )
        {
            mImageCaptureUri = data.getData();
        }


        if(requestCode == CROP_FROM_CAMERA && resultCode == RESULT_OK )
        {
            Bundle extras = data.getExtras();


        }
    }



    private class GetThings extends AsyncTask<Void, Void, String[]>
    {

        String mgetThingsResponse="";
        Context context;
        String jsonObject;
        JSONArray jsonarray;
        ProgressDialog pd;
        Boolean flag=true;
        String send_passport_result="";
        String send_backpassport_result="";
        String send_photo_result="";
        String send_support_result="";
        String send_support_result2="";
        String send_support_result3="";


        String send_support_pdf_result="";
        String send_support_pdf_result2="";
        String send_support_pdf_result3="";



        public GetThings(Context context,String jsonObject) {
            super();
            this.context=context;
            this.jsonObject=jsonObject;

            max_size=false;
            pd= new ProgressDialog(context).show(context, "", "Uploading document..");
            pd.show();
        }
        @Override
        protected String[] doInBackground(Void... params) {

            String [] mStrings=null;

            try {

               /* if(bitmapsupport!=null)
                {
                    send_support_result=   sendDataSupport(bitmapsupport,travel_id);
                }

                if(bitmapsupport1!=null)
                {
                    send_support_result2=   sendDataSupport2(bitmapsupport1,travel_id);
                }

                if(bitmapsupport2!=null)
                {
                    send_support_result3=   sendDataSupport3(bitmapsupport2,travel_id);
                }*/



                if(bitmapsupporturi!=null)
                {
                    send_support_result=   sendDataSupporturi(bitmapsupporturi,travel_id);
                }

                if(bitmapsupport1uri!=null)
                {
                    send_support_result2=   sendDataSupport2uri(bitmapsupport1uri,travel_id);
                }

                if(bitmapsupport2uri!=null)
                {
                    send_support_result3=   sendDataSupport3uri(bitmapsupport2uri,travel_id);
                }


                if(PDF_SUPPORT!=null)
                {
                    send_support_pdf_result=   sendpdfDataSupport(inputStreamFromUri(PDF_SUPPORT),travel_id);
                }

                if(PDF_SUPPORT_1!=null)
                {
                    send_support_pdf_result2=   sendpdfDataSupport2(inputStreamFromUri(PDF_SUPPORT_1),travel_id);
                }


                if(PDF_SUPPORT_2!=null)
                {
                    send_support_pdf_result3=   sendpdfDataSupport3(inputStreamFromUri(PDF_SUPPORT_2),travel_id);
                }





            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

                flag=false;
            }
            return mStrings;
        }
        @Override
        protected void onPostExecute(String[] result) {

            try
            {
                if(pd.isShowing())
                {
                    pd.dismiss();
                }

                if(true) {

                    JSONObject savedata;
                    JSONObject savefilepassport;
                    JSONObject savefilephoto;
                    JSONObject savefilesupport;

                    JSONObject savefilebackpassport;
                    JSONObject savefilesupport2;
                    JSONObject savefilesupport3;


                    JSONObject temp = null;
                    temp = new JSONObject(jsonObject);


                    if (!send_support_result.equalsIgnoreCase("")) {
                        savefilesupport = new JSONObject(send_support_result);

                        if (savefilesupport.has("success")) {


                            temp.put("supp_doc", "y");
                            flag = true;
                        } else {
                            flag = false;
                        }


                    }


                    if (!send_support_result2.equalsIgnoreCase("")) {
                        savefilesupport2 = new JSONObject(send_support_result2);

                        if (savefilesupport2.has("success")) {
                            temp.put("supp_doc_2", "y");
                            flag = true;
                        } else {
                            flag = false;
                        }


                    }


                    if (!send_support_result3.equalsIgnoreCase("")) {
                        savefilesupport3 = new JSONObject(send_support_result3);

                        if (savefilesupport3.has("success")) {
                            temp.put("supp_doc_3", "y");
                            flag = true;
                        } else {
                            flag = false;
                        }


                    }




                    if (!send_support_pdf_result.equalsIgnoreCase("")) {
                        JSONObject savefilesupportpdf = new JSONObject(send_support_pdf_result);

                        if (savefilesupportpdf.has("success")) {


                            temp.put("supp_doc", "y");
                            flag = true;
                        } else {
                            flag = false;
                        }


                    }



                    if (!send_support_pdf_result2.equalsIgnoreCase("")) {
                        JSONObject savefilesupport2pdf = new JSONObject(send_support_pdf_result2);

                        if (savefilesupport2pdf.has("success")) {
                            temp.put("supp_doc_2", "y");
                            flag = true;
                        } else {
                            flag = false;
                        }


                    }



                    if (!send_support_pdf_result3.equalsIgnoreCase("")) {
                        JSONObject  savefilesupport3pdf = new JSONObject(send_support_pdf_result3);

                        if (savefilesupport3pdf.has("success")) {
                            temp.put("supp_doc_3", "y");
                            flag = true;
                        } else {
                            flag = false;
                        }


                    }

                    if (flag) {


                        Intent intent = new Intent(SupportDoucumentActivity.this, SubmissionSummryDetails.class);

                        intent.putExtra("v_surname", getIntent().getExtras().getString("v_surname"));
                        intent.putExtra("v_firstname", getIntent().getExtras().getString("v_firstname"));
                        intent.putExtra("v_sex", getIntent().getExtras().getString("v_sex"));
                        intent.putExtra("v_passport", getIntent().getExtras().getString("v_passport"));
                        intent.putExtra("v_dob", getIntent().getExtras().getString("v_dob"));
                        intent.putExtra("v_doi", getIntent().getExtras().getString("v_doi"));
                        intent.putExtra("v_doe", getIntent().getExtras().getString("v_doe"));
                        intent.putExtra("v_poissuance", getIntent().getExtras().getString("v_poissuance"));
                        intent.putExtra("v_mealplan", getIntent().getExtras().getString("v_mealplan"));
                        intent.putExtra("v_hub", getIntent().getExtras().getString("v_hub"));
                        intent.putExtra("v_special", getIntent().getExtras().getString("v_special"));
                        intent.putExtra("object", temp.toString());


                        startActivity(intent);


                    } else {
                        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                                SupportDoucumentActivity.this);

                        // set title


                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Error please try again!")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {


                                    }
                                });


                        // create alert dialog
                        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                        TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                        msgTxt.setTypeface(font);

                    }

                }else
                {
                    android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                            SupportDoucumentActivity.this);

                    // set title


                    // set dialog message
                    alertDialogBuilder
                            .setMessage("File size should be less than 5 MB")
                                .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {


                                }
                            });


                    // create alert dialog
                    android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                    msgTxt.setTypeface(font);

                }



            }catch (Exception e)
            {
                System.out.println("Error"+e);
            }



            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
        }
    }


    public String sendDataSupport(Bitmap bitmap,String travelid) throws Exception {

        String sResponse="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            //Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            //bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[]
                    data = bos.toByteArray();




                    // sending a String param;
                    // entity.addPart("id",new UrlEncodedFormEntity(params, HTTP.UTF_8));
                    entity.addPart("id", new StringBody(travelid));


                    // sending a Image;0
                    // note here, that you can send more than one image, just add another param, same rule to the String;
                    entity.addPart("supp_doc", new ByteArrayBody(data, "supp_doc.jpg"));
                    httpPost.setEntity(entity);
                    HttpResponse response = httpClient.execute(httpPost, localContext);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                    sResponse = reader.readLine();

                    System.out.println("imageupload" + sResponse);



        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return sResponse;
    }




    public String sendDataSupporturi(Uri bitmapuri,String travelid) throws Exception {

        String sResponse="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            //Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            //bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), bitmapuri);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[]
                    data = bos.toByteArray();




            // sending a String param;
            // entity.addPart("id",new UrlEncodedFormEntity(params, HTTP.UTF_8));
            entity.addPart("id", new StringBody(travelid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("supp_doc", new ByteArrayBody(data, "supp_doc.jpg"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            sResponse = reader.readLine();

            System.out.println("imageupload" + sResponse);

            File f=new File(bitmapuri.getPath().toString());
            if(f.exists()) {
                f.delete();
            }


        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return sResponse;
    }




    public String sendDataSupport2(Bitmap bitmap,String travelid) throws Exception {

        String sResponse="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            //Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
         //   bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] data = bos.toByteArray();

                    entity.addPart("id", new StringBody(travelid));


                    // sending a Image;0
                    // note here, that you can send more than one image, just add another param, same rule to the String;
                    entity.addPart("supp_doc_2", new ByteArrayBody(data, "supp_doc_2.jpg"));
                    httpPost.setEntity(entity);
                    HttpResponse response = httpClient.execute(httpPost, localContext);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                    sResponse = reader.readLine();

                    System.out.println("imageupload" + sResponse);



        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return sResponse;
    }



    public String sendDataSupport2uri(Uri bitmapuri,String travelid) throws Exception {

        String sResponse="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            //Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), bitmapuri);
            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            //   bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] data = bos.toByteArray();

            entity.addPart("id", new StringBody(travelid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("supp_doc_2", new ByteArrayBody(data, "supp_doc_2.jpg"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            sResponse = reader.readLine();

            System.out.println("imageupload" + sResponse);


            File f=new File(bitmapuri.getPath().toString());
            if(f.exists()) {
                f.delete();
            }


        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return sResponse;
    }



    public String sendDataSupport3(Bitmap bitmap,String travelid) throws Exception {

        String sResponse="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
           // Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
       //     bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] data = bos.toByteArray();

                // sending a String param;
                // entity.addPart("id",new UrlEncodedFormEntity(params, HTTP.UTF_8));
                entity.addPart("id", new StringBody(travelid));


                // sending a Image;0
                // note here, that you can send more than one image, just add another param, same rule to the String;
                entity.addPart("supp_doc_3", new ByteArrayBody(data, "supp_doc_3.jpg"));
                httpPost.setEntity(entity);
                HttpResponse response = httpClient.execute(httpPost, localContext);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                sResponse = reader.readLine();

                System.out.println("imageupload" + sResponse);

        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return sResponse;
    }



    public String sendDataSupport3uri(Uri bitmapuri,String travelid) throws Exception {

        String sResponse="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            // Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            //     bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), bitmapuri);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] data = bos.toByteArray();

            // sending a String param;
            // entity.addPart("id",new UrlEncodedFormEntity(params, HTTP.UTF_8));
            entity.addPart("id", new StringBody(travelid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("supp_doc_3", new ByteArrayBody(data, "supp_doc_3.jpg"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            sResponse = reader.readLine();

            System.out.println("imageupload" + sResponse);


            File f=new File(bitmapuri.getPath().toString());
            if(f.exists()) {
                f.delete();
            }


        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return sResponse;
    }




    /**/

    public String sendpdfDataSupport(InputStream inputStream,String travelid) throws Exception {

        String sResponse="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            //Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);


            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            //bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);


            byte[] data = readFully(inputStream);




            // sending a String param;
            // entity.addPart("id",new UrlEncodedFormEntity(params, HTTP.UTF_8));
            entity.addPart("id", new StringBody(travelid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("supp_doc", new ByteArrayBody(data, "supp_doc.pdf"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            sResponse = reader.readLine();

            System.out.println("imageupload" + sResponse);



        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return sResponse;
    }


    public String sendpdfDataSupport2(InputStream inputStream,String travelid) throws Exception {

        String sResponse="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            //Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);

            byte[] data = readFully(inputStream);

            entity.addPart("id", new StringBody(travelid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("supp_doc_2", new ByteArrayBody(data, "supp_doc_2.pdf"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            sResponse = reader.readLine();

            System.out.println("imageupload" + sResponse);



        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return sResponse;
    }

    public String sendpdfDataSupport3(InputStream inputStream,String travelid) throws Exception {

        String sResponse="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            // Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            byte[] data = readFully(inputStream);

            // sending a String param;
            // entity.addPart("id",new UrlEncodedFormEntity(params, HTTP.UTF_8));
            entity.addPart("id", new StringBody(travelid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("supp_doc_3", new ByteArrayBody(data, "supp_doc_3.pdf"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            sResponse = reader.readLine();

            System.out.println("imageupload" + sResponse);

        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return sResponse;
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_support_doucument, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    private long FileSize(Uri fileUri)
    {
        // Uri fileUri = data.getData();
        long size;

        if(fileUri.getScheme().equalsIgnoreCase("file"))
        {
            //Toast.makeText(getApplicationContext(),"file",Toast.LENGTH_LONG).show();
            File file=new File(fileUri.getPath());
            size=file.length();
        }else
        {
            Cursor cursor = SupportDoucumentActivity.this.getContentResolver().query(fileUri,
                    null, null, null, null);
            cursor.moveToFirst();
            size = cursor.getLong(cursor.getColumnIndex(OpenableColumns.SIZE));
            cursor.close();
        }


        //Toast.makeText(getApplicationContext(),"content"+size,Toast.LENGTH_LONG).show();
        return size;

    }



    public byte[] readFully(InputStream inputStream) throws IOException {
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }


    private InputStream inputStreamFromUri(Uri uri) throws IOException {
        InputStream inputStream = getContentResolver().openInputStream(uri);
        // Savefile(inputStream,"a");
        return inputStream;
    }






    public void openFile(String minmeType,int REQUEST_CODE) {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(minmeType);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // special intent for Samsung file manager
        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
        // if you want any file type, you can skip next line
        sIntent.putExtra("CONTENT_TYPE", minmeType);
        sIntent.addCategory(Intent.CATEGORY_DEFAULT);

        Intent chooserIntent;
        if (getPackageManager().resolveActivity(sIntent, 0) != null){
            // it is device with samsung file manager
            chooserIntent = Intent.createChooser(sIntent, "Open file");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { intent});
        }
        else {
            chooserIntent = Intent.createChooser(intent, "Open file");
        }

        try {
            startActivityForResult(chooserIntent, REQUEST_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), "No suitable File Manager was found.", Toast.LENGTH_SHORT).show();
        }
    }

    private String FileType(Uri fileUri)
    {
        // Uri fileUri = data.getData();
        String type="";

        if(fileUri.getScheme().equalsIgnoreCase("file"))
        {
            String uri = fileUri.toString();
            if(uri.contains(".")) {
                String extension = uri.substring(uri.lastIndexOf("."));
                System.out.println("type"+extension);
                if(extension.equalsIgnoreCase(".pdf"))
                {
                    type="application/pdf";
                }else
                {
                    type="";
                }
                //type

            }else
            {
                type="";
            }
        }else
        {


            String aa=  getContentResolver().getType(fileUri);
            if(!aa.equalsIgnoreCase("null")||aa!=null)
            {
                type=aa;

            }else
            {
                type="";

            }

        }


        //Toast.makeText(getApplicationContext(),"content"+size,Toast.LENGTH_LONG).show();
        return type;

    }


}
