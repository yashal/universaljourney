package com.queppelin.universal;

import static com.queppelin.universal.CommonUtilities.SENDER_ID;
import static com.queppelin.universal.CommonUtilities.displayMessage;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;


import com.google.android.gcm.GCMBaseIntentService;
import com.image.loader.ImageLoader;
import com.tool.HttpClassVersion;

public class GCMIntentService extends GCMBaseIntentService {
	
    private static int notificationId;
	ImageLoader imageLoder;
	private static final String TAG = "GCMIntentService";
	public static String show_msg;
	public static String expert_details_flag="";
    private SharedPreferences mPrefs;
	
	public static String askId;
     
	public static  String ques;
     
	public static String postername;
	
    public GCMIntentService() {
        super(SENDER_ID);
    }

    /**
     * Method called on device registered
     **/
    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        displayMessage(getApplicationContext(), "Your device registred with GCM");
        Log.d("NAME", "Amit kumar");
        ServerUtilities.register(context, "Amit kumar", "jssamitkumar@gmail.com", registrationId);
    }
    /**
     * Method called on device un registred
     * */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        displayMessage(context, getString(R.string.gcm_unregistered));
        ServerUtilities.unregister(context, registrationId);
    }

    /**
     * Method called on Receiving a new message
     * */
    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message");
        String message = intent.getExtras().getString("message");
        String messageObj = intent.getExtras().getString("messageObj");
        String title = intent.getExtras().getString("title");

        mPrefs = context.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        String bundle=intent.getExtras().toString();
        imageLoder=new ImageLoader(context);
        System.out.println("gcm_message"+bundle);
        show_msg=message;
        new GetCountThread(context).execute();
        displayMessage(context, message);
        notificationId= getCounter();
        generateNotification(context, message,title,messageObj);

        // notifies user
       
    }

    /**
     * Method called on receiving a deleted message
     * */
    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        displayMessage(context, message);
        // notifies user
     //   generateNotification(context, message);
    }

    /**
     * Method called on Error
     * */

     @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        displayMessage(context, getString(R.string.gcm_recoverable_error,
                errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private  void generateNotification(Context context, String message,String title,String messageObj) {


       try
       {
           JSONArray jsonArray=new JSONArray(messageObj);




           SharedPreferences.Editor prefsEditor = mPrefs
                   .edit();
           prefsEditor
                   .putString("messObjectlogin", messageObj);

           prefsEditor.commit();





           JSONObject object=jsonArray.getJSONObject(0);
           String action_type=object.getString("action");
           if(action_type.equalsIgnoreCase("MYTRIP"))
           {


               int icon = R.drawable.ic_notification_icon;
               long when = System.currentTimeMillis();


               NotificationManager notificationManager = (NotificationManager)
                       context.getSystemService(Context.NOTIFICATION_SERVICE);

               Notification notification = new Notification(icon, message, when);

               Intent notificationIntent = new Intent(context, MyTripsActivity.class);

               TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack
               stackBuilder.addParentStack(NewDashboardActivity.class);
// Adds the Intent to the top of the stack
               stackBuilder.addNextIntentWithParentStack(notificationIntent);

               // set intent so it does not start a new activity
 	       /* notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
 	                Intent.FLAG_ACTIVITY_SINGLE_TOP);*/

               int n_id=Integer.parseInt(object.getString("customer_group_id"));

               notificationIntent.putExtra("customer_group_id",object.getString("customer_group_id"));
               notificationIntent.putExtra("action_flag","Y");
               notificationIntent.putExtra("marry",object+"");
               notificationIntent.putExtra("message_id",object.getString("msgid"));











               //PendingIntent intent =
                //       PendingIntent.getActivity(context, n_id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


               PendingIntent intent =  stackBuilder.getPendingIntent(n_id, PendingIntent.FLAG_UPDATE_CURRENT);

               notificationIntent.putExtra("customer_group_id",object.getString("customer_group_id"));
               notificationIntent.putExtra("action_flag","Y");

//               notification.setLatestEventInfo(context, title, message, intent);
               notification.flags |= Notification.FLAG_AUTO_CANCEL;
               // Play default notification sound
               notification.defaults |= Notification.DEFAULT_SOUND;
               //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");

               // Vibrate if vibrate is enabled nnn
               notification.defaults |= Notification.DEFAULT_VIBRATE;
               notificationManager.notify(n_id, notification);








             /*  intent=new Intent(NotificationListActivity.this,MyTripsActivity.class);
               intent.putExtra("customer_group_id",object.getString("customer_group_id"));
               intent.putExtra("action_flag","Y");
               startActivity(intent);*/



           }else if(action_type.equalsIgnoreCase("LOGIN"))
           {

               int icon = R.drawable.ic_notification_icon;
               long when = System.currentTimeMillis();




               NotificationManager notificationManager = (NotificationManager)
                       context.getSystemService(Context.NOTIFICATION_SERVICE);
               Notification notification = new Notification(icon, message, when);

               Intent notificationIntent = new Intent(context, LoginActivity.class);

               notificationIntent.putExtra("action_flag","Y");
               notificationIntent.putExtra("message_id",object.getString("msgid"));
               notificationIntent.putExtra("marry",object+"");

               TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack
               stackBuilder.addParentStack(NewDashboardActivity.class);
// Adds the Intent to the top of the stack
               stackBuilder.addNextIntentWithParentStack(notificationIntent);



               // set intent so it does not start a new activity
 	       /* notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
 	                Intent.FLAG_ACTIVITY_SINGLE_TOP);*/
              // PendingIntent intent =
               //        PendingIntent.getActivity(context, notificationId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


               PendingIntent intent =  stackBuilder.getPendingIntent(7777, PendingIntent.FLAG_UPDATE_CURRENT);



//               notification.setLatestEventInfo(context, title, message, intent);
               notification.flags |= Notification.FLAG_AUTO_CANCEL;
               // Play default notification sound
               notification.defaults |= Notification.DEFAULT_SOUND;
               //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");
               System.out.println("");
               // Vibrate if vibrate is enabled
               notification.defaults |= Notification.DEFAULT_VIBRATE;
               notificationManager.notify(7777, notification);






           }else if(action_type.equalsIgnoreCase("PACKAGE"))
           {

               int icon = R.drawable.ic_notification_icon;
               long when = System.currentTimeMillis();
               NotificationManager notificationManager = (NotificationManager)
                       context.getSystemService(Context.NOTIFICATION_SERVICE);
               Notification notification = new Notification(icon, message, when);

               Intent notificationIntent = new Intent(context, ExpirenceDetailsActivity.class);

               // set intent so it does not start a new activity
 	       /* notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
 	                Intent.FLAG_ACTIVITY_SINGLE_TOP);*/


               TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack
               stackBuilder.addParentStack(NewDashboardActivity.class);
// Adds the Intent to the top of the stack
               stackBuilder.addNextIntentWithParentStack(notificationIntent);


               notificationIntent.putExtra("product_id",object.getString("product_id"));

               notificationIntent.putExtra("action_flag","Y");
               notificationIntent.putExtra("marry",object+"");
               notificationIntent.putExtra("message_id",object.getString("msgid"));
               String notificationID=object.getString("product_id");
               int n_ID=Integer.parseInt(notificationID);

               /*PendingIntent intent =
                       PendingIntent.getActivity(context, n_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
*/

               PendingIntent intent =  stackBuilder.getPendingIntent(n_ID, PendingIntent.FLAG_UPDATE_CURRENT);




               NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
               builder.setContentIntent(intent);
//               notification.setLatestEventInfo(context, title, message, intent);
               notification.flags |= Notification.FLAG_AUTO_CANCEL;
               // Play default notification sound
               notification.defaults |= Notification.DEFAULT_SOUND;
               //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");

               // Vibrate if vibrate is enabled
               notification.defaults |= Notification.DEFAULT_VIBRATE;
               notificationManager.notify(n_ID,  notification);



           }else if(action_type.equalsIgnoreCase("NOACTION"))
           {

               int icon = R.drawable.ic_notification_icon;
               long when = System.currentTimeMillis();




               String message1="";

               int messageChar=message.length();
               if(messageChar>120)
               {
                   message1=message.substring(0, 120);

               }else
               {
                   message1=message;

               }





               NotificationManager notificationManager = (NotificationManager)
                       context.getSystemService(Context.NOTIFICATION_SERVICE);
               Notification notification = new Notification(icon, message1, when);

               Intent notificationIntent = new Intent(context, ReadMessageActivity.class);

               TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack
               stackBuilder.addParentStack(NewDashboardActivity.class);
// Adds the Intent to the top of the stack
               stackBuilder.addNextIntentWithParentStack(notificationIntent);


               notificationIntent.putExtra("action_flag","Y");
               notificationIntent.putExtra("message_object",object+"");
               notificationIntent.putExtra("message_id",object.getString("msgid"));
                int n_id1=Integer.parseInt(object.getString("msgid"));
               notificationIntent.putExtra("marry",object+"");
               // set intent so it does not start a new activity
 	       /* notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
 	                Intent.FLAG_ACTIVITY_SINGLE_TOP);*/
             //  PendingIntent intent =
               //        PendingIntent.getActivity(context, n_id1, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);



               PendingIntent intent =  stackBuilder.getPendingIntent(n_id1, PendingIntent.FLAG_UPDATE_CURRENT);

//               notification.setLatestEventInfo(context, title, message1, intent);
               notification.flags |= Notification.FLAG_AUTO_CANCEL;
               // Play default notification sound
               notification.defaults |= Notification.DEFAULT_SOUND;
               //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");

               // Vibrate if vibrate is enabled
               notification.defaults |= Notification.DEFAULT_VIBRATE;
               notificationManager.notify(n_id1, notification);



           }else
           {


               int icon = R.drawable.ic_notification_icon;
               long when = System.currentTimeMillis();




               NotificationManager notificationManager = (NotificationManager)
                       context.getSystemService(Context.NOTIFICATION_SERVICE);
               Notification notification = new Notification(icon, message, when);

               Intent notificationIntent = new Intent(context, NotificationListActivity.class);
               // set intent so it does not start a new activity
 	       /* notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
 	                Intent.FLAG_ACTIVITY_SINGLE_TOP);*/

               TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack
               stackBuilder.addParentStack(NewDashboardActivity.class);
// Adds the Intent to the top of the stack
               stackBuilder.addNextIntentWithParentStack(notificationIntent);


               notificationIntent.putExtra("marry",object+"");
          //     PendingIntent intent =
               //        PendingIntent.getActivity(context, notificationId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


               PendingIntent intent =  stackBuilder.getPendingIntent(notificationId, PendingIntent.FLAG_UPDATE_CURRENT);

//               notification.setLatestEventInfo(context, title, message, intent);
               notification.flags |= Notification.FLAG_AUTO_CANCEL;
               // Play default notification sound
               notification.defaults |= Notification.DEFAULT_SOUND;
               //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");

               // Vibrate if vibrate is enabled
               notification.defaults |= Notification.DEFAULT_VIBRATE;
               notificationManager.notify(notificationId, notification);





           }




       }catch (Exception e)
       {

           System.out.println("ErrorN"+e);
       }

    }



    private int getCounter()
    {
        SharedPreferences prefs = getSharedPreferences("mUniversalPrefs", MODE_PRIVATE);
        int counter = prefs.getInt("Counter", 0);
        SharedPreferences.Editor edit = prefs.edit();
        counter = counter + 1;
        edit.putInt("Counter", counter);
        edit.commit();
        return counter;
    }





}
