package com.queppelin.universal;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.tool.HttpClass;
import com.tool.StaticMethod;

import org.json.JSONObject;


public class FeedBackActivity extends AppCompatActivity {

    private SharedPreferences mPrefs;



    String[] drop_down1={"Feedback on Packages","Feedback on Group","Feedback on app"};

    String[] drop_down2={"Convenient time to get in touch","10 to 11 AM","11 to 12 PM","2 to 3 PM","3 to 4 PM","4 to 5 PM","5 to 6 PM"};

Spinner sp1,sp2;

    String sp1vale="";
    String sp2vale="";
    String user_id;
    Typeface font;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);

        getSupportActionBar().hide();
        font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        ((TextView)findViewById(R.id.tvback)).setTypeface(font);

        ((TextView)findViewById(R.id.tvfeedback)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_error)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_submit)).setTypeface(font);
        ((EditText)findViewById(R.id.input_surname)).setTypeface(font);



        sp1=(Spinner)findViewById(R.id.shelp);
        sp2=(Spinner)findViewById(R.id.spefern);

//
//        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(FeedBackActivity.this,
//                android.R.layout.simple_spinner_item, drop_down1);


       /* ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(FeedBackActivity.this,
                R.layout.spinner_item, drop_down1);
*/

        SpinerAdapterFeedBack adapter_state = new SpinerAdapterFeedBack(
                getApplicationContext(),
                R.layout.spinner_item,
                drop_down1);

        adapter_state
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp1.setAdapter(adapter_state);


        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {


                    sp1vale = drop_down1[arg2];

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });



//        ArrayAdapter<String> adapter_state2 = new ArrayAdapter<String>(FeedBackActivity.this,
//                R.layout.spinner_item, drop_down2);

        SpinerAdapterFeedBack adapter_state2 = new SpinerAdapterFeedBack(
                getApplicationContext(),
                R.layout.spinner_item,
                drop_down2);



        adapter_state2
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp2.setAdapter(adapter_state2);


        sp2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {



                sp2vale = drop_down2[arg2];

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });






        ((RelativeLayout)findViewById(R.id.rback)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });



        ((TextView)findViewById(R.id.tv_submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                String userValue=mPrefs.getString("userObject","").toString();

                try
                {
                    JSONObject object=new JSONObject(userValue);
                    JSONObject userObject=object.getJSONObject("result");
                    user_id=userObject.getString("customer_id");//customer_id

                }catch (Exception e)
                {

                }


                if(!sp2vale.equalsIgnoreCase(""))
                {


                    String feed= ((EditText)findViewById(R.id.input_surname)).getText().toString();

                    if(feed.equalsIgnoreCase(""))
                    {
                        if(StaticMethod.isInternetAvailable(FeedBackActivity.this)) {
                            sendFeedback(user_id, sp1vale, sp2vale);
                        }else{
                            return;
                        }
                    }else
                    {

                        if(feed.length()>20&&feed.length()<1000)
                        {
                            if(StaticMethod.isInternetAvailable(FeedBackActivity.this)) {
                                sendFeedback(user_id, sp1vale, sp2vale);
                                ((TextView) findViewById(R.id.tv_error)).setVisibility(View.GONE);
                            }else
                            {
                                return;
                            }
                        }else
                        {
                            ((TextView)findViewById(R.id.tv_error)).setVisibility(View.VISIBLE);
                        }

                    }






                }else
                {
                    Toast.makeText(getApplicationContext(),"Please select drop-down",Toast.LENGTH_LONG).show();
                }






            }
        });



        ((LinearLayout)findViewById(R.id.touchid)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {



                    InputMethodManager imm = (InputMethodManager)FeedBackActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);


                return false;
            }
        });

        ((LinearLayout)findViewById(R.id.touchid)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {



                InputMethodManager imm = (InputMethodManager)FeedBackActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);


                return false;
            }
        });




    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_feed_back, menu);
        return true;
    }
*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }




        return super.onOptionsItemSelected(item);
    }


    public void sendFeedback(String userid,String packages,String time)
    {





       String feed= ((EditText)findViewById(R.id.input_surname)).getText().toString();

        String[] pro_key = new String[]{"user_id","package","feedback","time"};
        String[] pro_value = new String[]{userid,packages,feed,time};
        new HttpClass(FeedBackActivity.this,"setFeedback", RestClient.RequestMethod.POST,pro_key,pro_value
        ).start();
        StaticMethod.returnProgressBar(
                FeedBackActivity.this).setOnDismissListener(
                new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        if (HttpClass.ResponseVector != null && !HttpClass.ResponseVector.equalsIgnoreCase("")) {

                            // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                            // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                            //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                            try {

                                System.out.println("setFeedback"+HttpClass.ResponseVector);
                               // ((LinearLayout)findViewById(R.id.alllayoute)).setVisibility(View.VISIBLE);
                                JSONObject jsonObject=new JSONObject(HttpClass.ResponseVector);


                                if(jsonObject.has("success"))
                                {

                                    Boolean flag=jsonObject.getBoolean("success");

                                    if(flag)
                                    {

                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                                FeedBackActivity.this);

                                        // set title


                                        // set dialog message
                                        alertDialogBuilder
                                                .setMessage(jsonObject.getString("message"))
                                                .setCancelable(false)
                                                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,int id) {

                                                        ((EditText)findViewById(R.id.input_surname)).setText("");

                                                    }
                                                });


                                        // create alert dialog
                                        AlertDialog alertDialog = alertDialogBuilder.create();

                                        // show it
                                        alertDialog.show();

                                        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                        TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                        msgTxt.setTypeface(font);






                                    }else
                                    {
                                        Toast.makeText(getApplicationContext(),"Please try again.",Toast.LENGTH_LONG).show();
                                    }



                                }else if(jsonObject.has("error"))
                                {
                                   // Toast.makeText(getApplicationContext(),"Please try again!",Toast.LENGTH_LONG).show();



                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                            FeedBackActivity.this);

                                    // set title


                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(jsonObject.getString("message"))
                                            .setCancelable(false)
                                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,int id) {

                                                    ((EditText)findViewById(R.id.input_surname)).setText("");

                                                }
                                            });


                                    // create alert dialog
                                    AlertDialog alertDialog = alertDialogBuilder.create();

                                    // show it
                                    alertDialog.show();

                                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                    msgTxt.setTypeface(font);


                                }




















                            } catch (Exception e) {
                                System.err.println(e);
                            }


                        }
                    }
                });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {

            startActivity(getIntent());
            finish();
        }
    }

}
