package com.queppelin.universal;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.image.loader.ImageLoader;
import com.tool.AppConfig;
import com.tool.HttpClass;
import com.tool.HttpClassVersion;
import com.tool.HttpclasswithoutThrade;
import com.tool.LeftMenuClickListener;
import com.tool.StaticMethod;
import com.viewpager.BannerFragmentAdapter;
import com.viewpager.CirclePageIndicator;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

import javax.xml.transform.Templates;

/**
 * Created by lenovo on 24-03-2016.
 */
public class NewDashboardActivity extends AppCompatActivity {

    private SharedPreferences mPrefs;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    ImageLoader imageLoader;

    TextView notification_count;
    ArrayList<RelativeLayout> tempinfo;
    ArrayList<RelativeLayout> rdes;
    ArrayList<ImageView> img;

    ArrayList<RelativeLayout> tempinfo1;
    ArrayList<RelativeLayout> rdes1;
    ArrayList<ImageView> img1;


    BannerFragmentAdapter bannerAdapter;

    ViewPager mPager;
    CirclePageIndicator mIndicator;


    public static ScrollView sv;
    public static RelativeLayout sr;

    Typeface font;


    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void moveDrawerToTop() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DrawerLayout drawer = (DrawerLayout) inflater.inflate(R.layout.new_dashboard, null); // "null" is important.

        // HACK: "steal" the first child of decor view
        ViewGroup decor = (ViewGroup) getWindow().getDecorView();
        drawer.findViewById(R.id.dra).setPadding(0, getStatusBarHeight(), 0, 0);

        // Make the drawer replace the first child
        decor.addView(drawer);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_dashboard);

        StaticMethod.mLogin_flag="N";

       // moveDrawerToTop();
        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);






        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(actionBar.getDisplayOptions()
                | ActionBar.DISPLAY_SHOW_CUSTOM);

        LinearLayout layout = new LinearLayout(actionBar.getThemedContext());
        layout.setOrientation(LinearLayout.HORIZONTAL);
        getSupportActionBar().setTitle("Universal Journeys");

         font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");

        tempinfo=new ArrayList<>();
        rdes=new ArrayList<>();
        img=new ArrayList<>();


        tempinfo1=new ArrayList<>();
        rdes1=new ArrayList<>();
        img1=new ArrayList<>();




        ((TextView)findViewById(R.id.tvlefttitle)).setTypeface(font);
        ((TextView)findViewById(R.id.id_login)).setTypeface(font);
        ((TextView)findViewById(R.id.id_signup)).setTypeface(font);
        ((TextView)findViewById(R.id.tvusername)).setTypeface(font);

        ((TextView)findViewById(R.id.tvemailid)).setTypeface(font);
        ((TextView)findViewById(R.id.tvLogout)).setTypeface(font);
        ((TextView)findViewById(R.id.tvnumber)).setTypeface(font);







        ImageView imageViewlogin = new ImageView(actionBar.getThemedContext());
        imageViewlogin.setScaleType(ImageView.ScaleType.CENTER);
        imageViewlogin.setImageResource(R.drawable.ic_menu_login);



        TextView Mtile = new TextView(actionBar.getThemedContext());
        Mtile.setGravity(Gravity.CENTER);
        Mtile.setText("");
        Mtile.setTextColor(Color.parseColor("#ffffff"));


        bannerAdapter=new BannerFragmentAdapter(getSupportFragmentManager());
        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setAdapter(bannerAdapter);
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);



        if(StaticMethod.setting_flag.equalsIgnoreCase("N"))
        {



            if(StaticMethod.isInternetAvailable(NewDashboardActivity.this)) {
                new GetsettingOrignal(getApplicationContext()).execute();
            }else
            {
                return;
            }


        }

        ImageView imageViewLogout = new ImageView(actionBar.getThemedContext());
        imageViewLogout.setScaleType(ImageView.ScaleType.CENTER);
        imageViewLogout.setImageResource(R.drawable.ic_logout);
        imageViewLogout.setVisibility(View.VISIBLE);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
                | Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin = 10;
        //imageViewLogout.setLayoutParams(layoutParams);

        ImageView imageViewmytrip = new ImageView(actionBar.getThemedContext());
        imageViewmytrip.setScaleType(ImageView.ScaleType.CENTER);
        imageViewmytrip.setImageResource(R.drawable.ic_menu_trips);

        //imageViewmytrip.setLayoutParams(layoutParams);

        imageViewlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), LoginActivity.class);
                intent.putExtra("action_flag","N");
                intent.putExtra("position",0);
                startActivity(intent);

            }
        });
        imageViewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Toast.makeText(getApplicationContext(),"on logout",Toast.LENGTH_LONG).show();

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        NewDashboardActivity.this);
                alertDialogBuilder
                        .setMessage("Do you want to logout?")
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {






                                String userValue=mPrefs.getString("userObject","").toString();
                                String user_id;
                                try
                                {
                                    JSONObject object=new JSONObject(userValue);
                                    JSONObject userObject=object.getJSONObject("result");
                                    user_id=userObject.getString("customer_id");//customer_id
                                    String[] pro_key = new String[]{"deviceId","userId","deviceType"};
                                    String[] pro_value = new String[]{mPrefs.getString("deviceId",""),user_id, AppConfig.DEVIECS_TYPE};
                                    new HttpClassVersion(NewDashboardActivity.this,"setPushLogoutCustomer", RestClient.RequestMethod.POST,pro_key,pro_value
                                    ).start();
                                    StaticMethod.returnProgressBar(
                                            NewDashboardActivity.this).setOnDismissListener(
                                            new DialogInterface.OnDismissListener() {
                                                public void onDismiss(DialogInterface dialog) {
                                                    if (HttpClassVersion.ResponseVector != null && !HttpClassVersion.ResponseVector.equalsIgnoreCase("")) {

                                                        try {
                                                            JSONObject jsonObject=new JSONObject(HttpClassVersion.ResponseVector);
                                                            if(jsonObject.has("success")) {

                                                                boolean type=jsonObject.getBoolean("success");
                                                                if(type) {

                                                                    SharedPreferences.Editor prefsEditor = mPrefs
                                                                            .edit();
                                                                    prefsEditor
                                                                            .putString("userObject", "");
                                                                    prefsEditor.putString("isLogin", "N");
                                                                    prefsEditor.commit();
                                                                    for (int i = 0; i < StaticMethod.state.size(); i++) {
                                                                        StaticMethod.state.get(i).finish();
                                                                    }
                                                                    Intent intent = new Intent(getApplicationContext(), NewDashboardActivity.class);
                                                                    startActivity(intent);
                                                                    finish();
                                                                }else
                                                                {
                                                                    Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                                                                }
                                                            }
                                                        }catch (Exception e)
                                                        {

                                                        }



                                                    }
                                                }
                                            });


                                }catch (Exception e)
                                {

                                }

                            }
                        })
                        .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.show();

                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);

            }
        });
        imageViewmytrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), MyTripsActivity.class);
                intent.putExtra("customer_group_id","");
                intent.putExtra("action_flag","N");
                intent.putExtra("position",0);
                startActivity(intent);


            }
        });

        layout.setLayoutParams(layoutParams);
       /* layout.addView(imageViewlogin);
        layout.addView(imageViewLogout);
        layout.addView(imageViewmytrip);

*/


        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        layout.addView(Mtile);
        if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
        {
            layout.addView(imageViewLogout);
            //layout.addView(imageViewmytrip);

        }else
        {
            layout.addView(imageViewlogin);
        }


        actionBar.setCustomView(layout);
        imageLoader=new ImageLoader(this);
        StaticMethod.state.add(NewDashboardActivity.this);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

//        mDrawerLayout.setStatusBarBackgroundColor(
//                getResources().getColor(R.color.white));
        mActivityTitle = getTitle().toString();
        addDrawerItems();
        setupDrawer();
        sr=(RelativeLayout)findViewById(R.id.rex);
        sv=((ScrollView)findViewById(R.id.mscroll));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        if(StaticMethod.isInternetAvailable(NewDashboardActivity.this)) {
            new GetCountThread(this).execute();
        }else
        {
            return;
        }

        setFloatingButtonControls();



      //  setExpirence();
      //  setThings();

        if(StaticMethod.isInternetAvailable(NewDashboardActivity.this)) {

            new GetThings(NewDashboardActivity.this).execute();


            String[] pro_key = new String[]{};
            String[] pro_value = new String[]{};

            new HttpClass(NewDashboardActivity.this, "getExperiences", RestClient.RequestMethod.GET, pro_key, pro_value
            ).start();
            StaticMethod.returnProgressBar(
                    NewDashboardActivity.this).setOnDismissListener(
                    new DialogInterface.OnDismissListener() {
                        public void onDismiss(DialogInterface dialog) {
                            if (HttpClass.ResponseVector != null && !HttpClass.ResponseVector.equalsIgnoreCase("")) {

                                ((LinearLayout) findViewById(R.id.allview)).setVisibility(View.VISIBLE);
                                // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                                //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                                try {

                                    System.out.println("outamit" + HttpClass.ResponseVector);


                                    JSONObject jsonObject = new JSONObject(HttpClass.ResponseVector);
                                    // final JSONObject jsonRecmandation=jsonObject.getJSONObject("recommended");
                                    JSONArray jsonArrayExperi = jsonObject.getJSONArray("experiences");


                                    JSONArray AppSlider = new JSONArray(mPrefs.getString("AppSlideshow", ""));


                                    if (mPrefs.getString("isLogin", "").equalsIgnoreCase("Y")) {


                                        //  imageLoader.DisplayImage(AppSlider.getJSONObject(0).getString("image"),((ImageView)findViewById(R.id.imgbanner)));

                                    } else {

                                        //imageLoader.DisplayImage(AppSlider.getJSONObject(1).getString("image"),((ImageView)findViewById(R.id.imgbanner)));
                                    }


                                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");


                                    ((TextView) findViewById(R.id.tvtitle)).setText(Html.fromHtml(mPrefs.getString("experience_title", "")));
                                    ((TextView) findViewById(R.id.tvtitle)).setTypeface(font);


                                    ((TextView) findViewById(R.id.tvdescription)).setText(Html.fromHtml(mPrefs.getString("experience_description", "")));
                                    ((TextView) findViewById(R.id.tvdescription)).setTypeface(font);

                                    ((TextView) findViewById(R.id.tvthingstitle)).setText(Html.fromHtml(mPrefs.getString("ten_thing_title", "")));
                                    ((TextView) findViewById(R.id.tvthingstitle)).setTypeface(font);
                                    ((TextView) findViewById(R.id.tvthingsdescription)).setText(Html.fromHtml(mPrefs.getString("ten_thing_desc", "")));
                                    ((TextView) findViewById(R.id.tvthingsdescription)).setTypeface(font);


                           /*     imageLoader.DisplayImage(jsonRecmandation.getString("image"),((ImageView)findViewById(R.id.imgmorocco)));
                                ((TextView)findViewById(R.id.tv_morocco)).setText(Html.fromHtml(jsonRecmandation.getString("name")));
                                //((TextView)findViewById(R.id.tv_umth)).setText(Html.fromHtml(jsonRecmandation.getString("image")));

                                ((RelativeLayout)findViewById(R.id.rrrecommonded1)).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {


                                        try
                                        {

                                            Intent intent=new Intent(DashboardActivity.this,ExpirenceDetailsActivity.class);
                                            intent.putExtra("product_id",jsonRecmandation.getString("product_id"));
                                            startActivity(intent);

                                        }catch (Exception e)
                                        {

                                        }

                                    }
                                });*/

                                    setExpirence(jsonArrayExperi);


                                } catch (Exception e) {
                                    System.err.println(e);
                                }


                            }
                        }
                    });

        }else
        {
            return;
        }



    }

    private class GetThings extends AsyncTask<Void, Void, String[]>
    {

        String mgetThingsResponse;
        Context context;

        JSONArray jsonarray;

        public GetThings(Context context) {
            super();
            this.context=context;


        }
        @Override
        protected String[] doInBackground(Void... params) {

            String [] mStrings=null;

            try {

                String[] pro_key = new String[]{};
                String[] pro_value = new String[]{};
                mgetThingsResponse =(new HttpclasswithoutThrade(NewDashboardActivity.this,"getTenThings", RestClient.RequestMethod.GET,pro_key,pro_value
                )).getResponsedetails();



            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return mStrings;
        }
        @Override
        protected void onPostExecute(String[] result) {

            try
            {
                System.out.println("things:"+mgetThingsResponse);

                if(mgetThingsResponse.equalsIgnoreCase("")||mgetThingsResponse.equalsIgnoreCase("null"))
                {

                }else
                {
                    JSONObject object=new JSONObject(mgetThingsResponse);
                    JSONArray jsonArray=object.getJSONArray("result");
                    setThings(jsonArray);
                }



            }catch (Exception e)
            {

            }



            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
        }
    }





    public  void addDrawerItems() {


        LinearLayout leftmenu=null;
        leftmenu=(LinearLayout)findViewById(R.id.leftmenulayout);
        leftmenu.removeAllViews();
        View parentview;


        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
        {

            ((RelativeLayout)findViewById(R.id.ruserinfobeforelogin)).setVisibility(View.GONE);
            ((RelativeLayout)findViewById(R.id.ruserinfoafterlogin)).setVisibility(View.VISIBLE);

            try
            {
                String userObjet=mPrefs.getString("userObject","");
                JSONObject userJson=new JSONObject(userObjet);

                JSONObject userinfo=null;

                if(userJson.has("user"))
                {

                    userinfo=userJson.getJSONObject("user");
                }else if(userJson.has("result"))
                {
                    userinfo=userJson.getJSONObject("result");
                }



                ((TextView)findViewById(R.id.tvusername)).setText(userinfo.getString("firstname")+"\u0020"+userinfo.getString("lastname"));
                ((TextView)findViewById(R.id.tvemailid)).setText(userinfo.getString("email"));
                ((TextView)findViewById(R.id.tvnumber)).setText(userinfo.getString("telephone"));



            }catch (Exception e)
            {

            }
            ((TextView)findViewById(R.id.tvLogout)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mDrawerLayout.closeDrawers();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            NewDashboardActivity.this);
                    alertDialogBuilder
                            .setMessage("Do you want to logout?")
                            .setCancelable(false)
                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {

                                    SharedPreferences.Editor prefsEditor = mPrefs
                                            .edit();
                                    prefsEditor
                                            .putString("userObject","");
                                    prefsEditor.putString("isLogin", "N");
                                    prefsEditor.commit();

                                    for(int i=0;i<StaticMethod.state.size();i++)
                                    {
                                        StaticMethod.state.get(i).finish();
                                    }
                                    Intent intent=new Intent(getApplicationContext(),NewDashboardActivity.class);
                                    startActivity(intent);
                                    finish();

                                }
                            })
                            .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                    msgTxt.setTypeface(font);

                }
            });
        }else
        {
            ((RelativeLayout)findViewById(R.id.ruserinfobeforelogin)).setVisibility(View.VISIBLE);
            ((RelativeLayout)findViewById(R.id.ruserinfoafterlogin)).setVisibility(View.GONE);
            ((TextView)findViewById(R.id.id_login)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                    intent.putExtra("action_flag","N");
                    startActivity(intent);

                }
            });
            ((TextView)findViewById(R.id.id_signup)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(getApplicationContext(),SignUpActivity.class);
                    startActivity(intent);

                }
            });

        }
        for(int i=0;i<StaticMethod.leftMenuTitle.length;i++)
        {
            parentview=getLayoutInflater().inflate(R.layout.left_menu_row, null);
            parentview.setId(i);
            //    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Roboto-Regular.ttf");
            ((TextView)parentview.findViewById(R.id.tvtitile)).setText(StaticMethod.leftMenuTitle[i]);
            ((TextView)parentview.findViewById(R.id.tvtitile)).setTypeface(font);
            ((ImageView)parentview.findViewById(R.id.img)).setBackgroundResource(StaticMethod.leftMenuicon[i]);



            if(i==2)
            {
                StaticMethod.notifiaction_r=((RelativeLayout)parentview.findViewById(R.id.rwidge_count));
                StaticMethod.notifiaction_text = ((TextView) parentview.findViewById(R.id.tvnotification_count));

                if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
                {

                    try {
                        StaticMethod.notifiaction_r.setVisibility(View.VISIBLE);


                        String getMessage = mPrefs.getString("getMessageArray", "");
                        int count=0 ;
                        if(!getMessage.equalsIgnoreCase("")) {
                            JSONArray jsonArray = new JSONArray(getMessage);
                            count=StaticMethod.getUnreadMessageCount(jsonArray);
                        }

                        StaticMethod.notifiaction_text.setText(count+"");
                    }catch (Exception e)
                    {

                    }

                }else
                {

                    StaticMethod.notifiaction_r.setVisibility(View.GONE);
                }



            }else
            {
                ((RelativeLayout)parentview.findViewById(R.id.rwidge_count)).setVisibility(View.GONE);
            }


            //  ((TextView)parentview.findViewById(R.id.tvtitile)).setTypeface(font);

            parentview.setOnClickListener(new LeftMenuClickListener(NewDashboardActivity.this,mDrawerLayout));
            LinearLayout.LayoutParams llpline = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1);
            View line=new View(this);
            llpline.setMargins(0, 0, 0, 0);
            line.setLayoutParams(llpline);
            line.setBackgroundColor(Color.parseColor("#00000000"));//add sperater
            if(i==0)
            {

            }else
            {
                leftmenu.addView(line);
            }
            leftmenu.addView(parentview);

        }







    }


    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // getSupportActionBar().setTitle("Navigation!");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //   getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }







    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }



        return super.onOptionsItemSelected(item);
    }





    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    boolean expirenceFlag[];
    ImageLoader imageLoaderExp;
    public void setExpirence(final JSONArray jsonArray)
    {
        imageLoaderExp=new ImageLoader(NewDashboardActivity.this);
        LinearLayout leftmenu=null;
        leftmenu=(LinearLayout)findViewById(R.id.lexpirence);
        leftmenu.removeAllViews();
        Typeface font = Typeface.createFromAsset(NewDashboardActivity.this.getAssets(), "Nunito_Light.ttf");
        expirenceFlag= new boolean[jsonArray.length()];
        for(int i=0;i<jsonArray.length();i++)
        {

            expirenceFlag[i]=true;
            final View  parentview=getLayoutInflater().inflate(R.layout.new_dashboard_row, null);
            parentview.setId(i);
            ((TextView)parentview.findViewById(R.id.tvexplore)).setText("EXPLORE");
            ((TextView)parentview.findViewById(R.id.tvexplore)).setTypeface(font);
            try{

                ((TextView)parentview.findViewById(R.id.tvpackagename)).setText(jsonArray.getJSONObject(i).getString("name"));
                ((TextView)parentview.findViewById(R.id.tvpackagename)).setTypeface(font);
                ((TextView)parentview.findViewById(R.id.tvinfo)).setText(Html.fromHtml(jsonArray.getJSONObject(i).getString("app_short_desc")));
                ((TextView)parentview.findViewById(R.id.tvinfo)).setTypeface(font);
                imageLoaderExp.DisplayImage(jsonArray.getJSONObject(i).getString("app_image"), ((ImageView) parentview.findViewById(R.id.pimg)));

            }catch (Exception e)
            {

            }

            ((RelativeLayout)parentview.findViewById(R.id.rinfo)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {





                    if (expirenceFlag[parentview.getId()]) {


                      //  Toast.makeText(getApplicationContext(),"true"+parentview.getId(),Toast.LENGTH_LONG).show();


                        expirenceFlag[parentview.getId()] = false;

                        for(int a=0;a<tempinfo.size();a++)
                        {
                            tempinfo.get(a).setBackgroundResource(R.drawable.i_button_non_active);
                            rdes.get(a).setVisibility(View.GONE);
                            img.get(a).setVisibility(View.GONE);

                        }


                        ((RelativeLayout) parentview.findViewById(R.id.rinfo)).setBackgroundResource(R.drawable.i_button_active);
                        ((ImageView) parentview.findViewById(R.id.imgop)).setVisibility(View.VISIBLE);
                        ((RelativeLayout) parentview.findViewById(R.id.rtextdes)).setVisibility(View.VISIBLE);



                        tempinfo.add(((RelativeLayout) parentview.findViewById(R.id.rinfo)));
                        rdes.add(((RelativeLayout) parentview.findViewById(R.id.rtextdes)));
                        img.add(((ImageView) parentview.findViewById(R.id.imgop)));





                    } else {
                        expirenceFlag[parentview.getId()] = true;
                        ((RelativeLayout) parentview.findViewById(R.id.rinfo)).setBackgroundResource(R.drawable.i_button_non_active);
                        ((ImageView) parentview.findViewById(R.id.imgop)).setVisibility(View.GONE);
                        ((RelativeLayout) parentview.findViewById(R.id.rtextdes)).setVisibility(View.GONE);

                        //Toast.makeText(getApplicationContext(),"false"+parentview.getId(),Toast.LENGTH_LONG).show();


                        tempinfo.remove(((RelativeLayout) parentview.findViewById(R.id.rinfo)));
                        rdes.remove(((RelativeLayout) parentview.findViewById(R.id.rtextdes)));
                        img.remove(((ImageView) parentview.findViewById(R.id.imgop)));

                        //Toast.makeText(getApplicationContext(),"false"+parentview.getId(),Toast.LENGTH_LONG).show();



                    }

                }
            });


            ((TextView)parentview.findViewById(R.id.tvexplore)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    try
                    {

                        Intent intent=new Intent(NewDashboardActivity.this,ListinfoDetailsActivity.class);
                        intent.putExtra("titile",jsonArray.getJSONObject(parentview.getId()).getString("name"));
                        intent.putExtra("catId",jsonArray.getJSONObject(parentview.getId()).getString("category_id"));
                        startActivity(intent);


                    }catch (Exception e)
                    {

                    }


                }
            });


            ((RelativeLayout)parentview.findViewById(R.id.rall)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    try
                    {

                        Intent intent=new Intent(NewDashboardActivity.this,ListinfoDetailsActivity.class);
                        intent.putExtra("titile",jsonArray.getJSONObject(parentview.getId()).getString("name"));
                        intent.putExtra("catId",jsonArray.getJSONObject(parentview.getId()).getString("category_id"));
                        startActivity(intent);

                    }catch (Exception e)
                    {

                    }


                }
            });

            LinearLayout.LayoutParams llpline = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 15);
            View line=new View(this);
            llpline.setMargins(0, 0, 0, 0);
            line.setLayoutParams(llpline);
            line.setBackgroundColor(Color.parseColor("#00000000"));//add sperater
            if(i==0)
            {

            }else
            {
                leftmenu.addView(line);
            }

            leftmenu.addView(parentview);

        }

    }






    boolean thingFlag[];
    ImageLoader imageLoaderThing;
    String thingarray;
    public void setThings(JSONArray jsonArray)
    {
        thingarray=jsonArray.toString();
        imageLoaderThing=new ImageLoader(NewDashboardActivity.this);
        LinearLayout leftmenu=null;
        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
        leftmenu=(LinearLayout)findViewById(R.id.lthings);
        leftmenu.removeAllViews();
        thingFlag=new boolean[jsonArray.length()];
        for(int i=0;i<jsonArray.length();i++)
        {
            thingFlag[i]=true;
        final View parentview=getLayoutInflater().inflate(R.layout.new_dashboard_row, null);
            parentview.setId(i);
            ((TextView)parentview.findViewById(R.id.tvexplore)).setText("FIND MORE");
            ((TextView)parentview.findViewById(R.id.tvexplore)).setTypeface(font);
        try{

            ((TextView)parentview.findViewById(R.id.tvpackagename)).setText(jsonArray.getJSONObject(i).getString("short_title"));

            ((TextView)parentview.findViewById(R.id.tvpackagename)).setTypeface(font);
            ((TextView)parentview.findViewById(R.id.tvinfo)).setText(Html.fromHtml(jsonArray.getJSONObject(i).getString("app_short_desc")));
            ((TextView)parentview.findViewById(R.id.tvinfo)).setTypeface(font);
            imageLoaderThing.DisplayImage(jsonArray.getJSONObject(i).getString("app_image"),((ImageView)parentview.findViewById(R.id.pimg)));

            }catch (Exception e)
            {

            }



            ((RelativeLayout)parentview.findViewById(R.id.rinfo)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(thingFlag[parentview.getId()])
                    {


                        for(int a=0;a<tempinfo.size();a++)
                        {
                            tempinfo.get(a).setBackgroundResource(R.drawable.i_button_non_active);
                            rdes.get(a).setVisibility(View.GONE);
                            img.get(a).setVisibility(View.GONE);

                        }



                        ((RelativeLayout)parentview.findViewById(R.id.rinfo)).setBackgroundResource(R.drawable.i_button_active);
                        ((ImageView)parentview.findViewById(R.id.imgop)).setVisibility(View.VISIBLE);
                        ((RelativeLayout)parentview.findViewById(R.id.rtextdes)).setVisibility(View.VISIBLE);
                        thingFlag[parentview.getId()]=false;


                        tempinfo.add(((RelativeLayout) parentview.findViewById(R.id.rinfo)));
                        rdes.add(((RelativeLayout) parentview.findViewById(R.id.rtextdes)));
                        img.add(((ImageView) parentview.findViewById(R.id.imgop)));



                    }else
                    {
                        ((RelativeLayout)parentview.findViewById(R.id.rinfo)).setBackgroundResource(R.drawable.i_button_non_active);
                        ((ImageView)parentview.findViewById(R.id.imgop)).setVisibility(View.GONE);
                        ((RelativeLayout)parentview.findViewById(R.id.rtextdes)).setVisibility(View.GONE);
                        thingFlag[parentview.getId()]=true;

                        tempinfo.remove(((RelativeLayout) parentview.findViewById(R.id.rinfo)));
                        rdes.remove(((RelativeLayout) parentview.findViewById(R.id.rtextdes)));
                        img.remove(((ImageView) parentview.findViewById(R.id.imgop)));





                    }

                }
            });



            ((TextView)parentview.findViewById(R.id.tvexplore)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try
                    {
                        Intent intent=new Intent(NewDashboardActivity.this,SliderTenTodo.class);
                        intent.putExtra("jsonArray",thingarray);
                        intent.putExtra("blog_id",parentview.getId());
                        startActivity(intent);


                    }catch (Exception e)
                    {

                    }


                }
            });
            ((RelativeLayout)parentview.findViewById(R.id.rall)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try
                    {
                        Intent intent=new Intent(NewDashboardActivity.this,SliderTenTodo.class);
                        intent.putExtra("jsonArray",thingarray);
                        intent.putExtra("blog_id",parentview.getId());
                        startActivity(intent);

                    }catch (Exception e)
                    {

                    }

                }
            });

            LinearLayout.LayoutParams llpline = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 15);
            View line=new View(this);
            llpline.setMargins(0, 0, 0, 0);
            line.setLayoutParams(llpline);
            line.setBackgroundColor(Color.parseColor("#00000000"));//add sperater
            if(i==0)
            {

            }else
            {
                leftmenu.addView(line);
            }

            leftmenu.addView(parentview);

        }

    }




    private void setFloatingButtonControls() {
        final View bckgroundDimmer = findViewById(R.id.background_dimmer);
        final FloatingActionsMenu floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.multiple_actions);




        bckgroundDimmer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                floatingActionsMenu.collapse();
                return false;
            }
        });

        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                bckgroundDimmer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMenuCollapsed() {
                bckgroundDimmer.setVisibility(View.GONE);
            }
        });


        ((FloatingActionButton)findViewById(R.id.action_a)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                floatingActionsMenu.collapse();

                if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
                {
                    Intent intent=new Intent(getApplicationContext(), MyTripsActivity.class);
                    intent.putExtra("customer_group_id","");
                    intent.putExtra("action_flag","N");
                    intent.putExtra("position",0);
                    startActivity(intent);

                }else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            NewDashboardActivity.this);

                    // set title


                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Please login to visit your trips section.")
                            .setCancelable(false)
                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, close
                                    // current activity
                                    //MainActivity.this.finish();
                                    // drawerLayout.closeDrawers();

                                    Intent intent=new Intent(NewDashboardActivity.this,LoginActivity.class);
                                    intent.putExtra("action_flag","N");
                                    startActivity(intent);







                                }
                            })
                            .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                    msgTxt.setTypeface(font);


                }

            }
        });



        ((FloatingActionButton)findViewById(R.id.action_b)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                floatingActionsMenu.collapse();
                NewDashboardActivity.setScrollbottum();

            }
        });


        ((FloatingActionButton)findViewById(R.id.action_c)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                floatingActionsMenu.collapse();

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        NewDashboardActivity.this);

                // set title


                // set dialog message
                alertDialogBuilder
                        .setMessage("Website will be opened in External Browser?")
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, close
                                // current activity
                                //MainActivity.this.finish();
                                // drawerLayout.closeDrawers();




                                Intent intent = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse("http://www.umustakeholidays.com/"));
                                startActivity(intent);



                            }
                        })
                        .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);


            }
        });

    }



    public static void setScrollup()
    {
        sv.fullScroll(View.FOCUS_UP);

        sv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {


                sv.post(new Runnable() {
                    public void run() {
                        // sv.fullScroll(View.FOCUS_UP);
                        //sv.smoothScrollTo(0, 0);
                    }
                });


            }
        });

    }



    public static void setScrollbottum()
    {





        sv.smoothScrollTo(0,sr.getTop());
        //  sv.smoothScrollTo(0,(height- 100));

       // System.out.println("Height"+height);



        sv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                sv.post(new Runnable() {
                    public void run() {


                        //sv.fullScroll(View.FOCUS_DOWN);



                    }
                });
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {

            startActivity(getIntent());
            finish();
        }
    }


    private class GetsettingOrignal extends AsyncTask<Void, Void, String[]>
    {

        String mgetThingsResponse;


        Context context;

        JSONArray jsonarray;

        public GetsettingOrignal(Context context) {
            super();
            this.context=context;


        }
        @Override
        protected String[] doInBackground(Void... params) {

            String [] mStrings=null;

            try {

                String[] pro_key = new String[]{};
                String[] pro_value = new String[]{};
                mgetThingsResponse =(new HttpclasswithoutThrade(context,"getSettings", RestClient.RequestMethod.GET,pro_key,pro_value
                )).getResponsedetails();


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return mStrings;
        }
        @Override
        protected void onPostExecute(String[] result) {

            try
            {
                System.out.println("getSettings_firsttime:"+mgetThingsResponse);

                if(!mgetThingsResponse.equalsIgnoreCase(""))
                {
                    JSONObject object=new JSONObject(mgetThingsResponse);

                    String email=object.getString("admin_email");
                    String mobile=object.getString("admin_telephone");//admin_mobile
                    String mobile2=object.getString("admin_telephone_2");

                    SharedPreferences.Editor prefsEditor = mPrefs
                            .edit();
                    prefsEditor
                            .putString("admin_email",email);
                    prefsEditor.putString("admin_number", mobile);
                    prefsEditor.putString("admin_number2", mobile2);

                    prefsEditor.putString("experience_title", object.getString("experience_title"));
                    prefsEditor.putString("experience_description", object.getString("experience_description"));
                    prefsEditor.putString("ten_thing_title", object.getString("ten_thing_title"));
                    prefsEditor.putString("ten_thing_desc", object.getString("ten_thing_desc"));

                    prefsEditor.putString("bannerArray", object.getJSONArray("banner").toString());
                    prefsEditor.putString("AppSlideshow", object.getJSONArray("AppSlideshow").toString());

                    prefsEditor.commit();


                    StaticMethod.setting_flag="Y";




                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");


                    ((TextView) findViewById(R.id.tvtitle)).setText(Html.fromHtml(mPrefs.getString("experience_title", "")));
                    ((TextView) findViewById(R.id.tvtitle)).setTypeface(font);


                    ((TextView) findViewById(R.id.tvdescription)).setText(Html.fromHtml(mPrefs.getString("experience_description", "")));
                    ((TextView) findViewById(R.id.tvdescription)).setTypeface(font);

                    ((TextView) findViewById(R.id.tvthingstitle)).setText(Html.fromHtml(mPrefs.getString("ten_thing_title", "")));
                    ((TextView) findViewById(R.id.tvthingstitle)).setTypeface(font);
                    ((TextView) findViewById(R.id.tvthingsdescription)).setText(Html.fromHtml(mPrefs.getString("ten_thing_desc", "")));
                    ((TextView) findViewById(R.id.tvthingsdescription)).setTypeface(font);





                }




            }catch (Exception e)
            {


                StaticMethod.setting_flag="N";
                System.out.println("error"+e);

            }



            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
        }
    }




}

