package com.queppelin.universal;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.tool.HttpClass;
import com.tool.HttpClassVersion;
import com.tool.StaticMethod;

import org.json.JSONObject;

/**
 * Created by lenovo on 23-01-2016.
 */
public class LoginOTPActivity extends AppCompatActivity {


    String full_name;
    String email_id;
    String mobile;

    private SharedPreferences mPrefs;

    Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        font = Typeface.createFromAsset(this.getAssets(), "Nunito_Light.ttf");
        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        getSupportActionBar().hide();



        ((EditText)findViewById(R.id.input_mobile)).setTypeface(font);
        ((TextView)findViewById(R.id.genrat_otp)).setTypeface(font);


        ((TextView)findViewById(R.id.tv_title)).setTypeface(font);
        ((TextView)findViewById(R.id.tvback)).setTypeface(font);



        mobile=getIntent().getExtras().getString("mobile");

        ((RelativeLayout)findViewById(R.id.rback)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        ((TextView)findViewById(R.id.genrat_otp)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mOTP=((EditText)findViewById(R.id.input_mobile)).getText().toString();


                if(!mOTP.equalsIgnoreCase(""))
                {

                    if(StaticMethod.isInternetAvailable(LoginOTPActivity.this)) {
                        VerifyOTP(mOTP, mobile);
                    }else
                    {
                        return;
                    }
                }else
                {

                    Toast.makeText(getApplicationContext(), "Please enter OTP", Toast.LENGTH_LONG).show();
                }



            }
        });






    }


    public void VerifyOTP(final String otp,final String mobile)
    {

        if(!otp.equalsIgnoreCase(""))
        {

            String[] pro_key = new String[]{"mobile","code","deviceId","deviceType"};
            String[] pro_value = new String[]{mobile,otp,mPrefs.getString("deviceId",""),mPrefs.getString("deviceType","")};
            new HttpClassVersion(LoginOTPActivity.this,"validateOtp", RestClient.RequestMethod.POST,pro_key,pro_value
            ).start();
            StaticMethod.returnProgressBar(
                    LoginOTPActivity.this).setOnDismissListener(
                    new DialogInterface.OnDismissListener() {
                        public void onDismiss(DialogInterface dialog) {
                            if (HttpClassVersion.ResponseVector != null && !HttpClassVersion.ResponseVector.equalsIgnoreCase("")) {

                                // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                                // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                                //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                                try {

                                    System.out.println("signUp"+HttpClassVersion.ResponseVector);

                                    JSONObject jsonObject=new JSONObject(HttpClassVersion.ResponseVector);

                                    if(jsonObject.has("success"))
                                    {


                                        Boolean flag=jsonObject.getBoolean("success");

                                        if(flag)
                                        {
                                            SharedPreferences.Editor prefsEditor = mPrefs
                                                    .edit();
                                            prefsEditor
                                                    .putString("userObject",HttpClassVersion.ResponseVector);
                                            prefsEditor.putString("isLogin", "Y");
                                            prefsEditor.commit();

                                            for(int i=0;i<StaticMethod.state.size();i++)
                                            {
                                                StaticMethod.state.get(i).finish();
                                            }

                                            if(StaticMethod.mLogin_flag.equalsIgnoreCase("N")) {

                                                Intent intent = new Intent(getApplicationContext(), NewDashboardActivity.class);
                                                intent.putExtra("position", 0);
                                                startActivity(intent);
                                                finish();
                                            }else
                                            {
                                                Intent intent=new Intent(getApplicationContext(),NotificationListActivity.class);
                                                intent.putExtra("action_flag","Y");
                                                startActivity(intent);
                                                finish();


                                            }

                                        }else
                                        {

                                            Toast.makeText(getApplicationContext(), "Please try again.", Toast.LENGTH_LONG).show();

                                        }



                                    }else if(jsonObject.has("error"))
                                    {
                                        ((EditText)findViewById(R.id.input_mobile)).setText("");
                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                                LoginOTPActivity.this);

                                        alertDialogBuilder
                                                .setMessage(jsonObject.getString("message"))
                                                .setCancelable(false)
                                                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,int id) {



                                                    }
                                                });
                                        // create alert dialog
                                        AlertDialog alertDialog = alertDialogBuilder.create();

                                        // show it
                                        alertDialog.show();
                                        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                        TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                        msgTxt.setTypeface(font);


                                    }


























                                } catch (Exception e) {
                                    System.err.println(e);
                                }


                            }
                        }
                    });





        }else
        {

            Toast.makeText(getApplicationContext(), "Please Enter Mobile.", Toast.LENGTH_LONG).show();

        }



    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_otp, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {

            startActivity(getIntent());
            finish();
        }
    }

}
