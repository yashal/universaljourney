package com.queppelin.universal;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.tool.HttpClass;
import com.tool.StaticMethod;

import org.json.JSONObject;


public class SignUpActivity extends AppCompatActivity {

    public static Activity activty;
    Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        getSupportActionBar().hide();
        font = Typeface.createFromAsset(this.getAssets(), "Nunito_Light.ttf");
        activty=SignUpActivity.this;
        StaticMethod.state.add(SignUpActivity.this);

        ((RelativeLayout)findViewById(R.id.rback)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });


        ((TextView)findViewById(R.id.tv_login)).setTypeface(font);
        ((EditText)findViewById(R.id.input_name)).setTypeface(font);
        ((EditText)findViewById(R.id.input_email)).setTypeface(font);
        ((EditText)findViewById(R.id.input_mobile)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_sign_up)).setTypeface(font);


        ((TextView)findViewById(R.id.tv_title)).setTypeface(font);
        ((TextView)findViewById(R.id.tvback)).setTypeface(font);

        ((TextView)findViewById(R.id.tv_term_condiition)).setTypeface(font);
        ((TextView)findViewById(R.id.tvterm)).setTypeface(font);
        ((TextView)findViewById(R.id.and)).setTypeface(font);
        ((TextView)findViewById(R.id.privacy)).setTypeface(font);








        String term="<u>Terms of service</u>";
        String privacy="<u>privacy Policy.</u>";



                ((TextView)findViewById(R.id.privacy)) .setText(Html.fromHtml(privacy));
        ((TextView)findViewById(R.id.tvterm)) .setText(Html.fromHtml(term));


        ((TextView)findViewById(R.id.privacy)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Intent intent=new Intent(SignUpActivity.this, PrivacyPolicyActivity.class);
                intent.putExtra("position",0);
                startActivity(intent);

            }
        });

        ((TextView)findViewById(R.id.tvterm)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               Intent intent=new Intent(SignUpActivity.this, TermAndCondition.class);
                intent.putExtra("position",0);
                startActivity(intent);


            }
        });


        ((TextView)findViewById(R.id.tv_sign_up)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String fullname = ((EditText) findViewById(R.id.input_name)).getText().toString();
                String email = ((EditText) findViewById(R.id.input_email)).getText().toString();
                String mobile = ((EditText) findViewById(R.id.input_mobile)).getText().toString();

                if (fullname.equalsIgnoreCase("")) {

                    StaticMethod.showSnackBar(SignUpActivity.this, "Please enter name");


                } else if (email.equalsIgnoreCase("")) {

                    StaticMethod.showSnackBar(SignUpActivity.this, "Please enter email");

                } else if (!isValidEmail(email)) {
                    StaticMethod.showSnackBar(SignUpActivity.this, "Please enter a valid email");
                } else if (mobile.equalsIgnoreCase("")) {
                    StaticMethod.showSnackBar(SignUpActivity.this, "Please enter mobile");
                } else {

                    if(StaticMethod.isInternetAvailable(SignUpActivity.this)) {

                        RegisterMobile(fullname, email, mobile);
                    }
                }


            }
        });


        ((TextView)findViewById(R.id.tv_login)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(SignUpActivity.this,LoginActivity.class);
                intent.putExtra("action_flag","N");
                startActivity(intent);
                finish();

            }
        });




    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void RegisterMobile(final String name,final String email,final String mNumber)
    {

        if(!mNumber.equalsIgnoreCase(""))
        {

            String[] pro_key = new String[]{"mobile","email","name"};
            String[] pro_value = new String[]{mNumber,email,name};
            new HttpClass(SignUpActivity.this,"registerOtp", RestClient.RequestMethod.POST,pro_key,pro_value
            ).start();
            StaticMethod.returnProgressBar(
                    SignUpActivity.this).setOnDismissListener(
                    new DialogInterface.OnDismissListener() {
                        public void onDismiss(DialogInterface dialog) {
                            if (HttpClass.ResponseVector != null && !HttpClass.ResponseVector.equalsIgnoreCase("")) {

                                // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                                // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                                //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                                try {

                                    System.out.println("registerOtp"+HttpClass.ResponseVector);

                                     JSONObject jsonObject=new JSONObject(HttpClass.ResponseVector);


                                    if(jsonObject.has("success"))
                                    {

                                        Boolean flag=jsonObject.getBoolean("success");

                                        if(flag)
                                        {

                                            Intent intent=new Intent(SignUpActivity.this,OtpActivity.class);
                                            intent.putExtra("full_name",name);
                                            intent.putExtra("email",email);
                                            intent.putExtra("mobile",mNumber);
                                            startActivity(intent);


                                        }else
                                        {

                                            Toast.makeText(getApplicationContext(),"Please try again.",Toast.LENGTH_LONG).show();

                                        }

                                    }else if(jsonObject.has("error"))
                                    {

                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                                SignUpActivity.this);

                                        alertDialogBuilder
                                                .setMessage(jsonObject.getString("message"))
                                                .setCancelable(false)
                                                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,int id) {

                                                     ((EditText)findViewById(R.id.input_name)).setText("");
                                                        ((EditText)findViewById(R.id.input_email)).setText("");
                                                        ((EditText)findViewById(R.id.input_mobile)).setText("");

                                                    }
                                                });
                                        // create alert dialog
                                        AlertDialog alertDialog = alertDialogBuilder.create();

                                        // show it
                                        alertDialog.show();

                                        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                        TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                        msgTxt.setTypeface(font);


                                    }













                                } catch (Exception e) {
                                    System.err.println(e);
                                }


                            }
                        }
                    });





        }else
        {

            Toast.makeText(getApplicationContext(), "Please Enter Mobile.", Toast.LENGTH_LONG).show();

        }



    }


    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target)
                && android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                .matches();
    }


}
