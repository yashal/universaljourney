package com.queppelin.universal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.tool.AppConfig;
import com.tool.HttpClass;
import com.tool.HttpClassVersion;
import com.tool.StaticMethod;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;



public class MyTripsActivity extends AppCompatActivity {

    byte[] bitmap_passport;
    byte[] bitmap_photo;
    byte[] bitmap_support;
    private SharedPreferences mPrefs;
    String user_id;

    String customer_group_id;
    Boolean itratery=true;
    Boolean airticket=true;
    Boolean hotelcommunication=true;


    String action_flag;

    ProgressBar pb;
    Dialog dialog;
    int downloadedSize = 0;
    int totalSize = 0;
    TextView cur_val;
    String dwnload_file_path = "\u0020 \u0020  \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020 \u0020";

    String ItineraryPDFLink="";
    String AirTicketPDFLink="";
    String HotelConfirmationPDFLink="";
    Typeface font;





    @Override
    protected void onDestroy() {
        super.onDestroy();

    System.gc();



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_trips);

         font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
         mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);


        StaticMethod.mLogin_flag="N";
        String userValue=mPrefs.getString("userObject","").toString();

        try
        {
            JSONObject object=new JSONObject(userValue);
            JSONObject userObject=object.getJSONObject("result");
            user_id=userObject.getString("customer_id");//customer_id

        }catch (Exception e)
        {

        }


        customer_group_id=getIntent().getExtras().getString("customer_group_id");

        action_flag=getIntent().getExtras().getString("action_flag");

        if(action_flag.equalsIgnoreCase("Y"))
        {
         try {
             String message=getIntent().getExtras().getString("marry").toString();

             JSONObject jb=new JSONObject(message) ;

             if(jb.getString("msgType").equalsIgnoreCase("broadcast"))
             {

                 if(StaticMethod.isInternetAvailable(this)) {

                     new SetReadThreadOnNotification(MyTripsActivity.this, getIntent().getExtras().getString("message_id"), getIntent().getExtras().getString("marry").toString()).execute();

                 }
                 {
                     return;
                 }
             }else
             {

                 if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
                 {

                     if(StaticMethod.isInternetAvailable(this)) {

                         new SetReadThreadOnNotification(MyTripsActivity.this, getIntent().getExtras().getString("message_id"), getIntent().getExtras().getString("marry").toString()).execute();

                         getTrip();

                     }else
                     {
                         return;
                     }
                 }else
                 {
                     Intent intent=new Intent(MyTripsActivity.this,LoginActivity.class);
                     intent.putExtra("action_flag","Y");
                     startActivity(intent);
                     finish();
                 }
             }

         }catch (Exception e)
            {

            }
         }else
        {

            if(StaticMethod.isInternetAvailable(this)) {


                getTrip();
            }else
            {
                return;
            }
        }
        NotificationManager manager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
       if(!customer_group_id.equalsIgnoreCase("")) {
           manager.cancel(Integer.parseInt(customer_group_id));
       }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(actionBar.getDisplayOptions()
                | ActionBar.DISPLAY_SHOW_CUSTOM);

        LinearLayout layout = new LinearLayout(actionBar.getThemedContext());
        layout.setOrientation(LinearLayout.HORIZONTAL);





        ImageView imageViewlogin = new ImageView(actionBar.getThemedContext());
        imageViewlogin.setScaleType(ImageView.ScaleType.CENTER);
        imageViewlogin.setImageResource(R.drawable.ic_menu_login);




        ImageView imageViewLogout = new ImageView(actionBar.getThemedContext());
        imageViewLogout.setScaleType(ImageView.ScaleType.CENTER);
        imageViewLogout.setImageResource(R.drawable.ic_logout);
        imageViewLogout.setVisibility(View.VISIBLE);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
                | Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin = 10;
        //imageViewLogout.setLayoutParams(layoutParams);





        ImageView imageViewmytrip = new ImageView(actionBar.getThemedContext());
        imageViewmytrip.setScaleType(ImageView.ScaleType.CENTER);
        imageViewmytrip.setImageResource(R.drawable.ic_menu_trips);

        //imageViewmytrip.setLayoutParams(layoutParams);





        imageViewlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), LoginActivity.class);
                intent.putExtra("action_flag","N");
                intent.putExtra("position",0);
                startActivity(intent);

            }
        });



        imageViewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Toast.makeText(getApplicationContext(),"on logout",Toast.LENGTH_LONG).show();

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MyTripsActivity.this);
                alertDialogBuilder
                        .setMessage("Do you want to logout?")
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                String userValue=mPrefs.getString("userObject","").toString();
                                String user_id;
                                try
                                {
                                    JSONObject object=new JSONObject(userValue);
                                    JSONObject userObject=object.getJSONObject("result");
                                    user_id=userObject.getString("customer_id");//customer_id
                                    String[] pro_key = new String[]{"deviceId","userId","deviceType"};
                                    String[] pro_value = new String[]{mPrefs.getString("deviceId",""),user_id, AppConfig.DEVIECS_TYPE};
                                    new HttpClassVersion(MyTripsActivity.this,"setPushLogoutCustomer", RestClient.RequestMethod.POST,pro_key,pro_value
                                    ).start();
                                    StaticMethod.returnProgressBar(
                                            MyTripsActivity.this).setOnDismissListener(
                                            new DialogInterface.OnDismissListener() {
                                                public void onDismiss(DialogInterface dialog) {
                                                    if (HttpClassVersion.ResponseVector != null && !HttpClassVersion.ResponseVector.equalsIgnoreCase("")) {


                                                        try {

                                                            JSONObject jsonObject=new JSONObject(HttpClassVersion.ResponseVector);

                                                            if(jsonObject.has("success")) {

                                                                boolean type=jsonObject.getBoolean("success");
                                                                if(type) {




                                                                    SharedPreferences.Editor prefsEditor = mPrefs
                                                                .edit();
                                                        prefsEditor
                                                                .putString("userObject","");
                                                        prefsEditor.putString("isLogin", "N");
                                                        prefsEditor.commit();


                                                        for(int i=0;i< StaticMethod.state.size();i++)
                                                        {
                                                            StaticMethod.state.get(i).finish();
                                                        }
                                                        Intent intent=new Intent(getApplicationContext(),NewDashboardActivity.class);
                                                        startActivity(intent);
                                                        finish();

                                                                }else
                                                                {
                                                                    Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                                                                }
                                                            }
                                                        }catch (Exception e)
                                                        {

                                                        }




                                                    }
                                                }
                                            });


                                }catch (Exception e)
                                {

                                }



                            }
                        })
                        .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);



            }
        });



        imageViewmytrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), MyTripsActivity.class);
                intent.putExtra("position",0);
                startActivity(intent);


            }
        });

        layout.setLayoutParams(layoutParams);
       /* layout.addView(imageViewlogin);
        layout.addView(imageViewLogout);
        layout.addView(imageViewmytrip);

*/



        if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
        {
            layout.addView(imageViewLogout);


        }else
        {
            layout.addView(imageViewlogin);
        }






        actionBar.setCustomView(layout);



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        String help="<u>Need help!</u>";

        ((TextView)findViewById(R.id.tvneedhelp)).setText(Html.fromHtml(help));
        ((TextView)findViewById(R.id.tvneedhelp)).setTypeface(font);
        ((TextView)findViewById(R.id.tvneedhelp)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MyTripsActivity.this);
                // set title
                LayoutInflater inflater = MyTripsActivity.this.getLayoutInflater();

                final View parent_view = inflater.inflate(R.layout.dailog_need_help, null);

                // set dialog message
                alertDialogBuilder

                        .setCancelable(false)

                        .setView(parent_view)
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });


                // create alert dialog
                final AlertDialog alertDialog = alertDialogBuilder.create();


                alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                String admin_number = mPrefs.getString("admin_number2", "").toString();

                ((TextView) parent_view.findViewById(R.id.tvcallus)).setText("Call us at-" + admin_number);
                ((TextView) parent_view.findViewById(R.id.tvcallus)).setTypeface(font);
                ((Button) parent_view.findViewById(R.id.tvcallme)).setTypeface(font);
                ((EditText) parent_view.findViewById(R.id.etbphone)).setTypeface(font);
                ((Button) parent_view.findViewById(R.id.tvcallme)).setEnabled(true);
                //((Button) parent_view.findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_active));


                try {

                    String userObjet = mPrefs.getString("userObject", "");

                    JSONObject userJson = new JSONObject(userObjet);

                    JSONObject userinfo = null;

                    if (userJson.has("user")) {

                        userinfo = userJson.getJSONObject("user");
                    } else if (userJson.has("result")) {
                        userinfo = userJson.getJSONObject("result");
                    }


                    ((EditText) parent_view.findViewById(R.id.etbphone)).setText(userinfo.getString("telephone"));


                } catch (Exception e) {

                }


                ((EditText) parent_view.findViewById(R.id.etbphone)).addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {


                        if (editable.length() >= 8 && editable.length() <= 15) {

                            ((Button) parent_view.findViewById(R.id.tvcallme)).setEnabled(true);
                           // ((Button) parent_view.findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_active));


                        } else {

                            ((Button) parent_view.findViewById(R.id.tvcallme)).setEnabled(false);
                         //   ((Button) parent_view.findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_hide));
                        }


                    }
                });


                final String getMobileNumber = ((EditText) parent_view.findViewById(R.id.etbphone)).getText().toString();

                ((Button) parent_view.findViewById(R.id.tvcallme)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        try {

                            String getMobileNumber = ((EditText) parent_view.findViewById(R.id.etbphone)).getText().toString();

                            if (!getMobileNumber.equalsIgnoreCase("")) {


                                if (getMobileNumber.length() >= 8 && getMobileNumber.length() <= 15) {


                                    getCallMe(getMobileNumber, alertDialog);

                                } else {

                                    // Toast.makeText(getApplicationContext(), "false", Toast.LENGTH_LONG).show();
                                }

                                //             getCallMe(getMobileNumber, alertDialog);


                            } else {
                                //Toast.makeText(getApplicationContext(),"Please enter mobile number",Toast.LENGTH_LONG).show();
                            }


                        } catch (Exception e) {
                            ((EditText) parent_view.findViewById(R.id.etbphone)).setText("");
                        }


                    }
                });


                ((TextView) parent_view.findViewById(R.id.tvcallus)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        String admin_number = mPrefs.getString("admin_number2", "").toString();

                        ((TextView) parent_view.findViewById(R.id.tvcallus)).setText("Call us at-" + admin_number);
                        Intent intent1 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + admin_number));
                        startActivity(intent1);
                        alertDialog.dismiss();

                    }
                });


                // show it
                alertDialog.show();


            }
        });


    }


    public void getTrip()
    {

        String[] pro_key = new String[]{"user_id"};
        String[] pro_value = new String[]{user_id};//user_id
        new HttpClass(MyTripsActivity.this,"getMyTrips", RestClient.RequestMethod.POST,pro_key,pro_value
        ).start();
        StaticMethod.returnProgressBar(
                MyTripsActivity.this).setOnDismissListener(
                new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        if (HttpClass.ResponseVector != null && !HttpClass.ResponseVector.equalsIgnoreCase("")) {

                            // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                            // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                            //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                            try {


                                ((LinearLayout)findViewById(R.id.alllayouttrip)).setVisibility(View.VISIBLE);

                                System.out.println("getMyTrips"+HttpClass.ResponseVector);

                                JSONObject jsonObject=new JSONObject(HttpClass.ResponseVector);

                                if(jsonObject.has("success"))
                                {

                                    Boolean flag=jsonObject.getBoolean("success");
                                    ((LinearLayout)findViewById(R.id.lsupport)).setVisibility(View.GONE);
                                    ((TextView)findViewById(R.id.tvmessage)).setVisibility(View.GONE);
                                    ((TextView)findViewById(R.id.tvmessage)).setTypeface(font);

                                    if(flag)
                                    {

                                        ((TextView)findViewById(R.id.tvneedhelp)).setVisibility(View.VISIBLE);
                                        JSONObject tripsObject=jsonObject.getJSONObject("trips");
                                        setLayout(tripsObject);

                                    }else
                                    {
                                        Toast.makeText(getApplicationContext(), "Please try again.", Toast.LENGTH_LONG).show();
                                    }



                                }else if(jsonObject.has("error"))
                                {

                                    // Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();


                                    ((TextView)findViewById(R.id.tvneedhelp)).setVisibility(View.GONE);

                                    ((LinearLayout)findViewById(R.id.lsupport)).setVisibility(View.VISIBLE);
                                    String temp="It looks like you aren't enrolled with any of our experiences.Please get in touch with  us using below section!";
                                    ((TextView)findViewById(R.id.tvmessage)).setText(temp);
                                    ((TextView)findViewById(R.id.tvmessage)).setTypeface(font);
                                    //((TextView)findViewById(R.id.tvmessage)).setText(jsonObject.getString("message"));
                                    ((TextView)findViewById(R.id.tvmessage)).setVisibility(View.VISIBLE);
                                    setSupport();
                                }

                            } catch (Exception e) {
                                System.err.println(e);
                            }

                        }
                    }
                });

    }


    Boolean[] flag;
    ArrayList<String> keyValue;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void setLayout(final JSONObject jobject)
    {
        int i;

        try
        {


            keyValue=new ArrayList<>();

            Iterator<String> a=   jobject.keys();

            while(a.hasNext())
            {
                String aa=a.next();
                keyValue.add(aa);

            }

            LinearLayout allLayout=null;
            allLayout=(LinearLayout)findViewById(R.id.lalllayout);
            allLayout.removeAllViews();

            flag=new Boolean[keyValue.size()];

            for(i=0;i<keyValue.size();i++)
            {

                if(i==0)
                {
                    flag[i]=false;
                }else
                {
                    flag[i]=true;
                }

                final View parent=getLayoutInflater().inflate(R.layout.mytrip_row, null);
                parent.setId(i);

                final LinearLayout layout=(LinearLayout)parent.findViewById(R.id.lexpendsview);
                layout.removeAllViews();

                final JSONArray jsonTravel=jobject.getJSONObject(keyValue.get(i)).getJSONArray("travellers");

                System.out.println("Key" + keyValue.get(i));

                ((TextView)parent.findViewById(R.id.tvtitle)).setText(Html.fromHtml(jobject.getJSONObject(keyValue.get(i)).getString("customer_group")));
                ((TextView)parent.findViewById(R.id.tvtitle)).setTypeface(font);

                ((TextView) parent.findViewById(R.id.tvexpends)).setTypeface(font);
                ((RelativeLayout)parent.findViewById(R.id.rrow)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (flag[parent.getId()]) {
                            layout.setVisibility(View.VISIBLE);
                            ((TextView) parent.findViewById(R.id.tvexpends)).setText("-");
                            flag[parent.getId()] = false;
                        } else {
                            flag[parent.getId()] = true;
                            ((TextView) parent.findViewById(R.id.tvexpends)).setText("+");
                            layout.setVisibility(View.GONE);

                        }
                    }
                });
//
//                if(docobject!=null)
//                {
//
//                    Toast.makeText(getApplicationContext(),"not null",Toast.LENGTH_LONG).show();
//
//                }else
//                {
//                    Toast.makeText(getApplicationContext()," value",Toast.LENGTH_LONG).show();
//                }
//
//
                for(int j=0;j<jsonTravel.length();j++)
                {
                    final View chield=getLayoutInflater().inflate(R.layout.sub_group_row, null);
                    chield.setId(j);
                    String is_dependent=jsonTravel.getJSONObject(j).getString("is_dependent");
                    ((TextView)chield.findViewById(R.id.tvdepart)).setTypeface(font);
                    if(is_dependent.equalsIgnoreCase("0"))
                    {
                        ((TextView)chield.findViewById(R.id.tvdepart)).setText("Self");

                    }else if(is_dependent.equalsIgnoreCase("1"))
                    {

                        ((TextView)chield.findViewById(R.id.tvdepart)).setText("Dependent");
                    }


                    String client_name=jsonTravel.getJSONObject(j).getString("initial")+".\u0020"+jsonTravel.getJSONObject(j).getString("name")+"\u0020"+jsonTravel.getJSONObject(j).getString("surname");

                    ((TextView)chield.findViewById(R.id.tvclient_name)).setText(client_name);

                    ((TextView)chield.findViewById(R.id.tvclient_name)).setTypeface(font);

                    ((TextView)chield.findViewById(R.id.tviti)).setTypeface(font);
                    ((TextView)chield.findViewById(R.id.tvh)).setTypeface(font);
                    ((TextView)chield.findViewById(R.id.tvat)).setTypeface(font);



                    String departure=jsonTravel.getJSONObject(j).getString("customer_group_departure");
                    String arrivaldate=jsonTravel.getJSONObject(j).getString("customer_group_arrival");
                    String detailssubmit=jsonTravel.getJSONObject(j).getString("progress");

                    String date ="";
                    String arrival="";
                    SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy");
                    try {
                      Date oneWayTripDate = input.parse(departure);
                        Date onearrival = input.parse(arrivaldate);// parse input
                        date=output.format(oneWayTripDate);
                        arrival=output.format(onearrival); // format output
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }



                    String flag_download_flag="";
                    JSONObject docobject=jobject.getJSONObject(keyValue.get(i));


                    String flag_itery="";
                    String flag_airtickit="";
                    String flag_airhotel="";



                  /*  Drawable myIcon = getResources().getDrawable( R.drawable.d_itinery );
                    ColorFilter filter = new LightingColorFilter( Color.BLACK, Color.BLACK);
                    myIcon.setColorFilter(filter);


                    ((ImageView)chield.findViewById(R.id.icircleiterater)).setBackground(myIcon);

                    Drawable myIcon1 = getResources().getDrawable( R.drawable.d_airticket);
                    ColorFilter filter1 = new LightingColorFilter( Color.BLACK, Color.BLACK);
                    myIcon1.setColorFilter(filter1);
                    ((ImageView)chield.findViewById(R.id.icirairticketc)).setBackground(myIcon1);

                    Drawable myIcon2 = getResources().getDrawable( R.drawable.d_hotel );
                    ColorFilter filter2 = new LightingColorFilter( Color.BLACK, Color.BLACK);
                    myIcon2.setColorFilter(filter2);

                    ((ImageView)chield.findViewById(R.id.iciratvhotelcommunication)).setBackground(myIcon2);
*/
                    if(docobject.isNull("docs"))
                    {
                        flag_download_flag="Y";


                    }else
                    {
                        flag_download_flag="N";

                        try
                        {
                            JSONObject pdfobject=docobject.getJSONObject("docs");

                            ItineraryPDFLink=pdfobject.getString("ItineraryPDFLink");



                            AirTicketPDFLink=pdfobject.getString("AirTicketPDFLink");


                            HotelConfirmationPDFLink=pdfobject.getString("HotelConfirmationPDFLink");





                        }catch (Exception e)
                        {

                        }


                    }



                    ((TextView) chield.findViewById(R.id.tvupdate)).setTypeface(font);
                    ((TextView)chield.findViewById(R.id.tvdownlad)).setTypeface(font);
                    if(flag_download_flag.equalsIgnoreCase("Y")) {


                        ((TextView) chield.findViewById(R.id.tvupdate)).setVisibility(View.VISIBLE);

                        ((TextView)chield.findViewById(R.id.tvdownlad)).setVisibility(View.GONE);

                        ((RelativeLayout) chield.findViewById(R.id.tvdownloaditetrary)).setVisibility(View.GONE);
                        ((RelativeLayout) chield.findViewById(R.id.tvairticket)).setVisibility(View.GONE);
                        ((RelativeLayout) chield.findViewById(R.id.tvhotelcommunication)).setVisibility(View.GONE);


                    }else
                    {
                            ((TextView)chield.findViewById(R.id.tvdownlad)).setVisibility(View.VISIBLE);
                            ((TextView)chield.findViewById(R.id.tvupdate)).setVisibility(View.GONE);


                        ((RelativeLayout)chield.findViewById(R.id.tvdownloaditetrary)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                //    Toast.makeText(getApplicationContext(),"iterater",Toast.LENGTH_LONG).show();

                                try {

                                    final String ItineraryPDFLink = jobject.getJSONObject(keyValue.get(parent.getId())).getJSONObject("docs").getString("ItineraryPDFLink");




                                    String mGroupId=jobject.getJSONObject(keyValue.get(parent.getId())).getString("customer_group_id");
                                    String mGroupName=jobject.getJSONObject(keyValue.get(parent.getId())).getString("customer_group");

                                    final  String mfileName="ItineraryPDF_"+mGroupName+"_"+mGroupId+".pdf";


//                                 Toast.makeText(getApplicationContext(),"Onclic"+ItineraryPDFLink,Toast.LENGTH_LONG).show();


                                    if (!checkFileAvailble(mfileName)) {

                                        if (ItineraryPDFLink.equalsIgnoreCase("") || ItineraryPDFLink.equalsIgnoreCase("null")) {



                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                                    MyTripsActivity.this);
                                            alertDialogBuilder
                                                    .setMessage("Sorry! file is not available.")
                                                    .setCancelable(false)
                                                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog,int id) {


                                                        }
                                                    });
                                            AlertDialog alertDialog = alertDialogBuilder.create();
                                            alertDialog.show();
                                            Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                            msgTxt.setTypeface(font);



                                        } else {
                                            showProgress(dwnload_file_path);

                                            new Thread(new Runnable() {
                                                public void run() {
                                                    downloadFile(ItineraryPDFLink, mfileName, "I");
                                                }
                                            }).start();
                                        }

                                    } else {
                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                                MyTripsActivity.this);
                                        alertDialogBuilder
                                                .setMessage("The file is available in UNUMTH folder,click ok to open the file.")
                                                .setCancelable(false)
                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        penPdfFile(mfileName);
                                                    }
                                                });
                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        alertDialog.show();
                                        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                        TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                        msgTxt.setTypeface(font);


                                    }

                                } catch (Exception e) {

                                    Toast.makeText(getApplicationContext(), "Error" + e, Toast.LENGTH_LONG).show();

                                }


                            }
                        });

                        ((RelativeLayout)chield.findViewById(R.id.tvairticket)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                //Toast.makeText(getApplicationContext(),"airticket",Toast.LENGTH_LONG).show();


try
{

    final    String AirTicketPDFLink=jobject.getJSONObject(keyValue.get(parent.getId())).getJSONObject("docs").getString("AirTicketPDFLink");


    String mGroupId=jobject.getJSONObject(keyValue.get(parent.getId())).getString("customer_group_id");
    String mGroupName=jobject.getJSONObject(keyValue.get(parent.getId())).getString("customer_group");
    final  String mfileName="AirTicketPDF_"+mGroupName+"_"+mGroupId+".pdf";
                    if(!checkFileAvailble(mfileName))
                                {


                                if(AirTicketPDFLink.equalsIgnoreCase("")||AirTicketPDFLink.equalsIgnoreCase("null"))
                                {


                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                            MyTripsActivity.this);
                                    alertDialogBuilder
                                            .setMessage("Sorry! file is not available.")
                                            .setCancelable(false)
                                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,int id) {


                                                }
                                            });
                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                    msgTxt.setTypeface(font);




                                }else
                                {
                                    showProgress(dwnload_file_path);

                                    new Thread(new Runnable() {
                                        public void run() {
                                            downloadFile(AirTicketPDFLink,mfileName,"A");
                                        }
                                    }).start();

                                }

                                }else
                                {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                            MyTripsActivity.this);
                                    alertDialogBuilder
                                            .setMessage("The file is available in UNUMTH folder,click ok to open the file.")
                                            .setCancelable(false)
                                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,int id) {
                                                    penPdfFile(mfileName);

                                                }
                                            });
                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();
                                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                    msgTxt.setTypeface(font);


                                }

}catch (Exception e)
{

}


                            }
                        });

                        ((RelativeLayout)chield.findViewById(R.id.tvhotelcommunication)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                //Toast.makeText(getApplicationContext(),"hotel communication",Toast.LENGTH_LONG).show();

                                try
                                {
                                    final    String HotelConfirmationPDFLink=jobject.getJSONObject(keyValue.get(parent.getId())).getJSONObject("docs").getString("HotelConfirmationPDFLink");


                                    String mGroupId=jobject.getJSONObject(keyValue.get(parent.getId())).getString("customer_group_id");
                                    String mGroupName=jobject.getJSONObject(keyValue.get(parent.getId())).getString("customer_group");



                                  final  String mfileName="HotelConfirmationPDF_"+mGroupName+"_"+mGroupId+".pdf";






                                    if(!checkFileAvailble(mfileName))
                                {




                                if(HotelConfirmationPDFLink.equalsIgnoreCase("")||HotelConfirmationPDFLink.equalsIgnoreCase("null"))
                                {


                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                            MyTripsActivity.this);
                                    alertDialogBuilder
                                            .setMessage("Sorry! file is not available.")
                                            .setCancelable(false)
                                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,int id) {


                                                }
                                            });
                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();
                                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                    msgTxt.setTypeface(font);


                                  //  Toast.makeText(getApplicationContext(),"hotel communication"+HotelConfirmationPDFLink,Toast.LENGTH_LONG).show();

                                }else
                                {
                                    showProgress(dwnload_file_path);

                                    new Thread(new Runnable() {
                                        public void run() {
                                            downloadFile(HotelConfirmationPDFLink,mfileName,"H");

                                        }
                                    }).start();


                                }
                                }else
                                {

                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                            MyTripsActivity.this);
                                    alertDialogBuilder
                                            .setMessage("The file is available in UNUMTH folder,click ok to open the file.")
                                            .setCancelable(false)
                                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,int id) {

                                                    penPdfFile(mfileName);

                                                }
                                            });
                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();
                                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                    msgTxt.setTypeface(font);



                                }

                                }catch (Exception e)
                                {

                                    Toast.makeText(getApplicationContext(),"er"+e,Toast.LENGTH_LONG).show();

                                }


                            }
                        });










                        }

















                    ((TextView)chield.findViewById(R.id.tvdeparture)).setText("Departure -" + date);
                    ((TextView)chield.findViewById(R.id.tvarrivalback)).setText("Arrival Back -" + arrival);
                    ((TextView)chield.findViewById(R.id.tvdetailssumbit)).setText("Details submitted -"+detailssubmit+"%");


                    ((TextView)chield.findViewById(R.id.tvdeparture)).setTypeface(font);
                    ((TextView)chield.findViewById(R.id.tvarrivalback)).setTypeface(font);
                    ((TextView)chield.findViewById(R.id.tvdetailssumbit)).setTypeface(font);
                    ((TextView)chield.findViewById(R.id.tvupdate)).setTypeface(font);

                    ((TextView)chield.findViewById(R.id.tvupdate)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                           // Toast.makeText(getApplicationContext(),"update",Toast.LENGTH_LONG).show();
                        try {
                                Intent intent = new Intent(MyTripsActivity.this, SubmitYourDetailsActivity.class);
                                intent.putExtra("travelObject", jsonTravel.getJSONObject(chield.getId()).toString());
                                intent.putExtra("bitmap_passport", bitmap_passport);
                                intent.putExtra("bitmap_photo", bitmap_photo);
                                intent.putExtra("bitmap_support", bitmap_support);
                                intent.putExtra("position", chield.getId());
                                startActivity(intent);
                        }catch (Exception e)
                        {

                        }
                                        }
                    });

                    LinearLayout.LayoutParams llpline = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 10);
                    View line=new View(this);
                    llpline.setMargins(0, 0, 0, 0);
                    line.setLayoutParams(llpline);
                    line.setBackgroundColor(Color.parseColor("#00000000"));//add sperater
                    if(j==0)
                    {

                    }else
                    {
                        layout.addView(line);
                    }


                    layout.addView(chield);
                }

                if(action_flag.equalsIgnoreCase("Y"))
                {

                    if(keyValue.get(i).equalsIgnoreCase(customer_group_id))
                    {
                        layout.setVisibility(View.VISIBLE);
                        ((TextView) parent.findViewById(R.id.tvexpends)).setText("-");
                        ((TextView) parent.findViewById(R.id.tvexpends)).setTypeface(font);

                    }else
                    {
                        layout.setVisibility(View.GONE);
                    }




                }else {



                    if(customer_group_id.equalsIgnoreCase(""))
                    {

                        if (i == 0) {

                            layout.setVisibility(View.VISIBLE);
                            ((TextView) parent.findViewById(R.id.tvexpends)).setText("-");
                            ((TextView) parent.findViewById(R.id.tvexpends)).setTypeface(font);

                        } else {
                            layout.setVisibility(View.GONE);
                        }


                    }else
                    {


                        if(keyValue.get(i).equalsIgnoreCase(customer_group_id))
                        {
                            layout.setVisibility(View.VISIBLE);
                            ((TextView) parent.findViewById(R.id.tvexpends)).setText("-");
                            ((TextView) parent.findViewById(R.id.tvexpends)).setTypeface(font);

                        }else
                        {
                            layout.setVisibility(View.GONE);
                        }


                    }


                }    System.out.println("j");

                allLayout.addView(parent);
            }

        }catch (Exception e)
        {

        }

    }







    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==android.R.id.home)
        {
            if(action_flag.equalsIgnoreCase("Y"))
            {
                Intent intent=new Intent(MyTripsActivity.this,NewDashboardActivity.class);
                startActivity(intent);
                finish();

            }else
            {
                finish();
            }
        }




        return super.onOptionsItemSelected(item);

    }


    public void getCallMe(String number,final AlertDialog alertDialog)
    {

        String[] pro_key = new String[]{"mobile","entity","entity_id","deviceType"};
        String[] pro_value = new String[]{number,"mytrips",user_id,"ANDROID"};//user_id
        new HttpClass(MyTripsActivity.this,"registerCall", RestClient.RequestMethod.POST,pro_key,pro_value
        ).start();
        StaticMethod.returnProgressBar(
                MyTripsActivity.this).setOnDismissListener(
                new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        if (HttpClass.ResponseVector != null && !HttpClass.ResponseVector.equalsIgnoreCase("")) {

                            // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                            // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                            //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                            try {

                                System.out.println("registerCall"+HttpClass.ResponseVector);

                                JSONObject jsonObject=new JSONObject(HttpClass.ResponseVector);

                                String message="";

                                if(jsonObject.has("success"))
                                {
                                    message=jsonObject.getString("message");

                                    //Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                                    alertDialog.dismiss();


                                }else if(jsonObject.has("error"))
                                {
                                    message=jsonObject.getString("Message");

                                    //Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_LONG).show();


                                }



                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                        MyTripsActivity.this);

                                // set title


                                // set dialog message
                                alertDialogBuilder
                                        .setMessage(message)
                                        .setCancelable(false)
                                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {



                                            }
                                        });


                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();

                                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                msgTxt.setTypeface(font);




                            } catch (Exception e) {
                                System.err.println(e);
                            }

                        }
                    }
                });



    }

    void downloadFile(String file_path,final String fileName, final String type){
        downloadedSize=0;
        try {
            URL url = new URL(decode(file_path));
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);

            //connect
            urlConnection.connect();

            //set the path where we want to save the file
            File SDCardRoot = Environment.getExternalStorageDirectory();
            File myDir = new File(SDCardRoot + "/UJUMTH");
            if(!myDir.isDirectory())
            myDir.mkdirs();
            File file = new File(myDir,fileName);

            FileOutputStream fileOutput = new FileOutputStream(file);

            //Stream used for reading the data from the internet
            InputStream inputStream = urlConnection.getInputStream();

            //this is the total size of the file which we are <span id="IL_AD3" class="IL_AD">downloading</span>
            totalSize = urlConnection.getContentLength();

            runOnUiThread(new Runnable() {
                public void run() {
                    pb.setMax(totalSize);
                }
            });

            //create a buffer...
            byte[] buffer = new byte[1024];
            int bufferLength = 0;

            while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
                fileOutput.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
                // update the progressbar //
                runOnUiThread(new Runnable() {
                    public void run() {
                        pb.setProgress(downloadedSize);
                        float per = ((float)downloadedSize/totalSize) * 100;
                        cur_val.setText("Downloaded " + downloadedSize + "KB / " + totalSize + "KB (" + (int)per + "%)" );
                    }
                });
            }
            //close the output stream when complete //
            fileOutput.close();
            runOnUiThread(new Runnable() {
                public void run() {

                    dialog.dismiss();

                    /*if(type.equalsIgnoreCase("I"))
                    {*/

                       // itratery=false;

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                MyTripsActivity.this);
                        alertDialogBuilder
                                .setMessage("Downloaded file completed.")
                                .setCancelable(false)
                                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {


                                        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/UJUMTH/"+fileName);
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent);

                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                    TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                    msgTxt.setTypeface(font);


                    /*}else if(type.equalsIgnoreCase("A"))
                    {



                         airticket=false;

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                MyTripsActivity.this);
                        alertDialogBuilder
                                .setMessage("Downloaded file completed.")
                                .setCancelable(false)
                                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {


                                        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/AirTicketPDF.pdf.pdf");
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent);


                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();




                    }else if(type.equalsIgnoreCase("H"))
                    {



                       hotelcommunication=false;
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                MyTripsActivity.this);
                        alertDialogBuilder
                                .setMessage("Downloaded file completed.")
                                .setCancelable(false)
                                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {


                                        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/HotelConfirmationPDF.pdf");
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent);




                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();



                    }
*/

                    // System.out.println("Complet File Downlaod.");
                     // if you want close it..
                }
            });

        } catch (final MalformedURLException e) {
            showError("Error : MalformedURLException " + e);
            e.printStackTrace();
        } catch (final IOException e) {
            showError("Error : IOException " + e);
            e.printStackTrace();
        }
        catch (final Exception e) {
            showError("Error : Please check your internet connection" + e);
        }
    }

    void showError(final String err){
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(MyTripsActivity.this, err, Toast.LENGTH_LONG).show();
            }
        });
    }

    void showProgress(String file_path){
        dialog = new Dialog(MyTripsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.myprogressdialog);
        dialog.setTitle("Download Progress");

        TextView text = (TextView) dialog.findViewById(R.id.tv1);
        text.setText("Downloading file from ... " + file_path);
        text.setTypeface(font);
        cur_val = (TextView) dialog.findViewById(R.id.cur_pg_tv);
        cur_val.setText("Starting download...");
        cur_val.setTypeface(font);
        dialog.show();

        pb = (ProgressBar)dialog.findViewById(R.id.progress_bar);
        pb.setProgress(0);
        pb.setProgressDrawable(getResources().getDrawable(R.drawable.green_progress));
    }



    private String decode(String url)
    {
        return url.replace("&amp;", "&");
    }







    public void setSupport()
    {


        View view = MyTripsActivity.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)MyTripsActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        final   String admin_number=mPrefs.getString("admin_number2","").toString();
        ((TextView)findViewById(R.id.tvcallat)).setText("Call us at-"+admin_number);
        ((TextView)findViewById(R.id.tvcallat)).setTypeface(font);
        ((EditText) findViewById(R.id.etbphone)).setTypeface(font);
        ((Button) findViewById(R.id.tvcallme)).setTypeface(font);


        ((TextView)findViewById(R.id.tvcallat)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                Intent intent1 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + admin_number));
                startActivity(intent1);

            }
        });


        try {

            if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
            {

                String userObjet = mPrefs.getString("userObject", "");
                JSONObject userJson = new JSONObject(userObjet);

                JSONObject userinfo = null;

                if (userJson.has("user")) {

                    userinfo = userJson.getJSONObject("user");
                } else if (userJson.has("result")) {
                    userinfo = userJson.getJSONObject("result");
                }


                ((EditText) findViewById(R.id.etbphone)).setText(userinfo.getString("telephone"));


                if (userinfo.getString("telephone").length() >= 8 && userinfo.getString("telephone").length() <= 15) {

                    ((Button) findViewById(R.id.tvcallme)).setEnabled(true);
                   // ((Button) findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_active));


                } else {

                    ((Button) findViewById(R.id.tvcallme)).setEnabled(false);
                   // ((Button)findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_hide));
                }

            }else
            {
                ((EditText)findViewById(R.id.etbphone)).setText("");
            }
        }catch (Exception e)
        {

        }

        ((EditText)findViewById(R.id.etbphone)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {


                if (editable.length() >= 8&&editable.length()<=15) {

                    ((Button)findViewById(R.id.tvcallme)).setEnabled(true);
                   // ((Button)findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_active));


                } else{

                    ((Button)findViewById(R.id.tvcallme)).setEnabled(false);
                   // ((Button)findViewById(R.id.tvcallme)).setBackgroundColor(getResources().getColor(R.color.call_hide));
                }



            }
        });





        ((Button)findViewById(R.id.tvcallme)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try
                {

                    String getMobileNumber=((EditText)findViewById(R.id.etbphone)).getText().toString();
                    if (!getMobileNumber.equalsIgnoreCase("")) {

                        if (getMobileNumber.length() >= 8 && getMobileNumber.length() <= 15) {


                            getCallMe(getMobileNumber);
                            View view1 = MyTripsActivity.this.getCurrentFocus();
                            if (view1 != null) {
                                InputMethodManager imm = (InputMethodManager)MyTripsActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view1.getWindowToken(), 0);
                            }

                        } else {


                        }

                    } else {

                    }                }catch (Exception e)
                {

                }



            }
        });


    }



    public void getCallMe(String number)
    {

        String[] pro_key = new String[]{"mobile","entity","entity_id","deviceType"};
        String[] pro_value = new String[]{number,"mytrips",user_id,"ANDROID"};//user_id
        new HttpClass(MyTripsActivity.this,"registerCall", RestClient.RequestMethod.POST,pro_key,pro_value
        ).start();
        StaticMethod.returnProgressBar(
                MyTripsActivity.this).setOnDismissListener(
                new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        if (HttpClass.ResponseVector != null && !HttpClass.ResponseVector.equalsIgnoreCase("")) {

                            // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                            // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                            //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                            try {

                                System.out.println("registerCall"+HttpClass.ResponseVector);

                                JSONObject jsonObject=new JSONObject(HttpClass.ResponseVector);

                                String message="";

                                if(jsonObject.has("success"))
                                {
                                    message=jsonObject.getString("message");

                                }else if(jsonObject.has("error"))
                                {

                                    message=jsonObject.getString("Message");

                                }
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                        MyTripsActivity.this);
                                alertDialogBuilder
                                        .setMessage(message)
                                        .setCancelable(false)
                                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {



                                            }
                                        });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                                msgTxt.setTypeface(font);


                            } catch (Exception e) {
                                System.err.println(e);
                            }

                        }
                    }
                });



    }





    public boolean checkFileAvailble(String mfileName)
    {

        File SDCardRoot = Environment.getExternalStorageDirectory();
        File myDir = new File(SDCardRoot + "/UJUMTH");
        File file = new File(myDir,mfileName);
        return file.exists();
    }


    public void penPdfFile(String fileName)
    {

        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/UJUMTH/"+fileName);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(action_flag.equalsIgnoreCase("Y"))
        {
            Intent intent=new Intent(MyTripsActivity.this,NewDashboardActivity.class);
            startActivity(intent);
            finish();

        }else
        {
            super.onBackPressed();
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {

            startActivity(getIntent());
            finish();
        }
    }



}
