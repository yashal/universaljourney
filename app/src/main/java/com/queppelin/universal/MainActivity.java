package com.queppelin.universal;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;


import static com.queppelin.universal.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.queppelin.universal.CommonUtilities.EXTRA_MESSAGE;
import static com.queppelin.universal.CommonUtilities.SENDER_ID;

import com.google.android.gcm.GCMRegistrar;
import com.tool.StaticMethod;


public class MainActivity extends Activity {



    final String PREFS_NAME = "MyApp";
    SharedPreferences settings;


    AsyncTask<Void, Void, Void> mRegisterTask;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
         settings = getSharedPreferences(PREFS_NAME, 0);

        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
        ((TextView)findViewById(R.id.up)).setTypeface(font);
        ((TextView)findViewById(R.id.below)).setTypeface(font);

        if(StaticMethod.isInternetAvailable(MainActivity.this))
        {


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {



                    if (settings.getBoolean("my_first_time", true)) {
                        //the app is being launched for first time, do something
                        Intent intent=new Intent(MainActivity.this,FirstTimeinfoactivity.class);
                        startActivity(intent);
                        finish();

                        settings.edit().putBoolean("my_first_time", false).commit();
                    }else
                    {
                        //Intent intent=new Intent(MainActivity.this,DashboardActivity.class);
                        Intent intent=new Intent(MainActivity.this,NewDashboardActivity.class);
                        intent.putExtra("position",0);
                        startActivity(intent);
                        finish();
                    }




                }
            }, 2000);


        }else
        {
            return;
            // Toast.makeText(this,"net",Toast.LENGTH_LONG).show();

        }







       /* StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        try {


            RestClient client = new RestClient("http://staging.umustakeholidays.com/api/v1/getExperiences");

            client.AddHeader("Developer-Key", "test");
            client.AddHeader("Developer-Secret", "testing123");

            try {
                client.Execute(RestClient.RequestMethod.GET);
            } catch (Exception e) {
                e.printStackTrace();
            }

            String response = client.getResponse();
            System.out.println("output"+response);

            Toast.makeText(getApplicationContext(),"out put"+response,Toast.LENGTH_LONG).show();


        }catch (Exception e)
        {
System.out.print("error"+e);
        }
*/





        // Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(getApplicationContext());
        // Make sure the manifest was properly set - comment out this line
        // while developing the app, then uncomment it when it's ready.
        GCMRegistrar.checkManifest(getApplicationContext());
        registerReceiver(mHandleMessageReceiver, new IntentFilter(
                DISPLAY_MESSAGE_ACTION));
        final String regId = GCMRegistrar.getRegistrationId(getApplicationContext());
        //GCMRegistrar.unregister(this);
        // Check if regid already presents
        if (regId.equals("")) {
            // Registration is not present, register now with GCM

            GCMRegistrar.register(this, SENDER_ID);
        } else {
            // Device is already registered on GCM
            if (GCMRegistrar.isRegisteredOnServer(getApplicationContext())) {//
                // Skips registration.
                //GCMRegistrar.unregister(this);
                //ServerUtilities.unregister(this, regId);
                //Toast.makeText(getApplicationContext(), "Already registered with GCM", Toast.LENGTH_LONG).show();
            } else {
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.
                final Context context = getApplicationContext();
                mRegisterTask = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        // Register on our server
                        // On server creates a new user
                        ServerUtilities.register(context, "Amit", "jssamitkumar@gmail.com", regId);


                        return null;
                    }
                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }
                };
                mRegisterTask.execute(null, null, null);
            }
        }




    }






    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());

            /**
             * Take appropriate action on this message
             * depending upon your app requirement
             * For now i am just displaying it on the screen
             * */

            // Showing received message

            //Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();

            // Releasing wake lock
            WakeLocker.release();
        }
    };

    @Override
    protected void onDestroy() {

        System.gc();
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try {
            unregisterReceiver(mHandleMessageReceiver);
            GCMRegistrar.onDestroy(getApplicationContext());
        } catch (Exception e) {
            Log.e("UnRegister Recei Error", "> " + e.getMessage());
        }
        super.onDestroy();
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {

            startActivity(new Intent(MainActivity.this,MainActivity.class));
            finish();
        }
    }


}
