package com.queppelin.universal;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.tool.AppConfig;
import com.tool.HttpclasswithoutThrade;
import com.tool.StaticMethod;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;


public class SubmissionSummryDetails extends AppCompatActivity {


    byte[] temp;

    Bitmap bitmap_passport = null;
    Bitmap bitmap_passport_back = null;
    Bitmap bitmap_photo = null;
    Bitmap bitmap_support = null;
    Bitmap bitmap_support1 = null;
    Bitmap bitmap_support2 = null;


    String passport="";
    String passport_back="";
    String photo="";
    String support="";
    String support1="";
    String support2="";


    String travel_id;


    String traveleOject;







    String v_surname;
    String v_firstname;
    String v_sex;
    String v_passport;
    String v_dob;
    String v_doi;
    String v_doe;
    String v_poissuance;
    String v_mealplan;
    String v_hub;
    String v_special;


    JSONObject  object;


    byte[] byteArraypassport;
    byte[] byteArraybackpassport;
    byte[] byteArrayphoto;
    byte[] byteArraysupport;
    byte[] byteArraysupport1;
    byte[] byteArraysupport2;

    Typeface font;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submission_summry_details);
        getSupportActionBar().hide();

        font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
        StaticMethod.stateactivity.add(SubmissionSummryDetails.this);

        ((TextView)findViewById(R.id.tvsurename)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_surename)).setTypeface(font);

        ((TextView)findViewById(R.id.tvgivename)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_fname)).setTypeface(font);

        ((TextView)findViewById(R.id.tvsex1)).setTypeface(font);
        ((TextView)findViewById(R.id.tvsex)).setTypeface(font);

        ((TextView)findViewById(R.id.tv_passport_no1)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_passport_no)).setTypeface(font);

        ((TextView)findViewById(R.id.tv_dob1)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_dob)).setTypeface(font);


        ((TextView)findViewById(R.id.tv_doi1)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_doi)).setTypeface(font);



        ((TextView)findViewById(R.id.doe1)).setTypeface(font);
        ((TextView)findViewById(R.id.doe)).setTypeface(font);

        ((TextView)findViewById(R.id.tv_poa1)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_poa)).setTypeface(font);

        ((TextView)findViewById(R.id.meal_plan1)).setTypeface(font);
        ((TextView)findViewById(R.id.meal_plan)).setTypeface(font);

        ((TextView)findViewById(R.id.tv_hub1)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_hub)).setTypeface(font);

        ((TextView)findViewById(R.id.tv_special_need1)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_special_need)).setTypeface(font);

        ((TextView)findViewById(R.id.tv_passport1)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_passport_back1)).setTypeface(font);

        ((TextView)findViewById(R.id.tv_photograph1)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_supporting01)).setTypeface(font);

        ((TextView)findViewById(R.id.tv_supporting21)).setTypeface(font);
        ((TextView)findViewById(R.id.tv_supporting31)).setTypeface(font);

        ((TextView)findViewById(R.id.tvedit)).setTypeface(font);
        ((TextView)findViewById(R.id.tvsubmit)).setTypeface(font);
        ((TextView)findViewById(R.id.tvback)).setTypeface(font);












       /* intent.putExtra("v_surname",sure_name);
        intent.putExtra("v_firstname",sure_fname);
        intent.putExtra("v_sex",sexvale);
        intent.putExtra("v_passport",sure_pnumber);
        intent.putExtra("v_dob",selectdob);
        intent.putExtra("v_doi",selectIssues);
        intent.putExtra("v_doe",selectexpire);
        intent.putExtra("v_poissuance",passportissues);
        intent.putExtra("v_mealplan",mealvale);
        intent.putExtra("v_hub",sure_hub);
        intent.putExtra("v_special",sure_specialneend);*/




         v_surname=getIntent().getExtras().getString("v_surname");
         v_firstname=getIntent().getExtras().getString("v_firstname");
         v_sex=getIntent().getExtras().getString("v_sex");
         v_passport=getIntent().getExtras().getString("v_passport");
         v_dob=getIntent().getExtras().getString("v_dob");
         v_doi=getIntent().getExtras().getString("v_doi");
         v_doe=getIntent().getExtras().getString("v_doe");
         v_poissuance=getIntent().getExtras().getString("v_poissuance");
         v_mealplan=getIntent().getExtras().getString("v_mealplan");
         v_hub=getIntent().getExtras().getString("v_hub");
         v_special=getIntent().getExtras().getString("v_special");



        ((TextView)findViewById(R.id.tv_surename)).setText(v_surname);
        ((TextView)findViewById(R.id.tv_fname)).setText(v_firstname);

        ((TextView)findViewById(R.id.tvsex)).setText(v_sex);

        ((TextView)findViewById(R.id.tv_passport_no)).setText(v_passport);

        ((TextView)findViewById(R.id.tv_dob)).setText(v_dob);
        ((TextView)findViewById(R.id.tv_doi)).setText(v_doi);
        ((TextView)findViewById(R.id.doe)).setText(v_doe);


        try
        {
            SimpleDateFormat input = new SimpleDateFormat("dd MMM yyyy");
            SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");

            Date dobdate = input.parse(v_dob);
            v_dob=output.format(dobdate);

            Date doidate = input.parse(v_doi);
            v_doi=output.format(doidate);

            Date doedate = input.parse(v_doe);
            v_doe=output.format(doedate);



        }catch (Exception e)
        {

        }







        ((TextView)findViewById(R.id.tv_poa)).setText(v_poissuance);
        ((TextView)findViewById(R.id.meal_plan)).setText(v_mealplan);
        ((TextView)findViewById(R.id.tv_hub)).setText(v_hub);
        ((TextView)findViewById(R.id.tv_special_need)).setText(v_special);

/*
        Intent intent = getIntent();
     bitmap_passport = (Bitmap) intent.getParcelableExtra("bitmap_passport");
     bitmap_photo = (Bitmap) intent.getParcelableExtra("bitmap_photo");
     bitmap_support = (Bitmap) intent.getParcelableExtra("bitmap_support");
*/

        /*byteArraypassport = getIntent().getByteArrayExtra("bitmap_passport");
        if(byteArraypassport!=null)
        {
            bitmap_passport = BitmapFactory.decodeByteArray(byteArraypassport, 0, byteArraypassport.length);
        }else
        {
            bitmap_passport=null;
        }



        byteArraybackpassport = getIntent().getByteArrayExtra("bitmap_passport_back");
        if(byteArraybackpassport!=null)
        {
            bitmap_passport_back = BitmapFactory.decodeByteArray(byteArraybackpassport, 0, byteArraybackpassport.length);
        }else
        {
            bitmap_passport=null;
        }







        byteArrayphoto = getIntent().getByteArrayExtra("bitmap_photo");

        if(byteArrayphoto!=null)
        {
            bitmap_photo = BitmapFactory.decodeByteArray(byteArrayphoto, 0, byteArrayphoto.length);
        }else
        {
            bitmap_photo = null;
        }

        byteArraysupport = getIntent().getByteArrayExtra("bitmap_support");

        if(byteArraysupport!=null)
        {
            bitmap_support = BitmapFactory.decodeByteArray(byteArraysupport, 0, byteArraysupport.length);
        }else
        {
            bitmap_support=null;
        }



        byteArraysupport1 = getIntent().getByteArrayExtra("bitmap_support1");

        if(byteArraysupport1!=null)
        {
            bitmap_support1 = BitmapFactory.decodeByteArray(byteArraysupport1, 0, byteArraysupport1.length);
        }else
        {
            bitmap_support1=null;
        }

        byteArraysupport2 = getIntent().getByteArrayExtra("bitmap_support2");

        if(byteArraysupport2!=null)
        {
            bitmap_support2 = BitmapFactory.decodeByteArray(byteArraysupport2, 0, byteArraysupport2.length);
        }else
        {
            bitmap_support2=null;
        }



*/

/*
        byteArraypassport = getIntent().getByteArrayExtra("bitmap_passport");
        bitmap_passport = BitmapFactory.decodeByteArray(byteArraypassport, 0, byteArraypassport.length);

        byteArrayphoto = getIntent().getByteArrayExtra("bitmap_photo");
        bitmap_photo = BitmapFactory.decodeByteArray(byteArrayphoto, 0, byteArrayphoto.length);
        byteArraysupport = getIntent().getByteArrayExtra("bitmap_support");

        bitmap_support = BitmapFactory.decodeByteArray(byteArraysupport, 0, byteArraysupport.length);

*/


        traveleOject=getIntent().getExtras().getString("object");


        try
        {

           object=new JSONObject(traveleOject);


            passport = object.getString("pass_copy");
            photo = object.getString("photograph");
            support = object.getString("supp_doc");

            passport_back = object.getString("pass_copy_back");
            support1 = object.getString("supp_doc_2");
            support2 = object.getString("supp_doc_3");

            travel_id=object.getString("id");

        }catch (Exception e)
        {

        }


            if(passport.equalsIgnoreCase("")||passport.equalsIgnoreCase("null"))
            {
                ((TextView)findViewById(R.id.tv_passport)).setBackgroundResource(R.drawable.icon_closemark);



            }else
            {
                ((TextView)findViewById(R.id.tv_passport)).setBackgroundResource(R.drawable.icon_checkmark);



            }



            if(passport_back.equalsIgnoreCase("")||passport_back.equalsIgnoreCase("null"))
            {
                ((TextView)findViewById(R.id.tv_passport_back)).setBackgroundResource(R.drawable.icon_closemark);



            }else
            {
                ((TextView)findViewById(R.id.tv_passport_back)).setBackgroundResource(R.drawable.icon_checkmark);



            }




            if(photo.equalsIgnoreCase("")||photo.equalsIgnoreCase("null"))
            {


                ((TextView)findViewById(R.id.tv_photograph)).setBackgroundResource(R.drawable.icon_closemark);
            }else
            {

                ((TextView)findViewById(R.id.tv_photograph)).setBackgroundResource(R.drawable.icon_checkmark);
            }


            if(support.equalsIgnoreCase("")||support.equalsIgnoreCase("null"))
            {

                ((TextView)findViewById(R.id.tv_supporting)).setBackgroundResource(R.drawable.icon_closemark);

            }else
            {

                ((TextView)findViewById(R.id.tv_supporting)).setBackgroundResource(R.drawable.icon_checkmark);
            }



            if(support1.equalsIgnoreCase("")||support1.equalsIgnoreCase("null"))
            {

                ((TextView)findViewById(R.id.tv_supporting2)).setBackgroundResource(R.drawable.icon_closemark);

            }else
            {

                ((TextView)findViewById(R.id.tv_supporting2)).setBackgroundResource(R.drawable.icon_checkmark);
            }


            if(support2.equalsIgnoreCase("")||support2.equalsIgnoreCase("null"))
            {

                ((TextView)findViewById(R.id.tv_supporting3)).setBackgroundResource(R.drawable.icon_closemark);

            }else
            {

                ((TextView)findViewById(R.id.tv_supporting3)).setBackgroundResource(R.drawable.icon_checkmark);
            }


        ((RelativeLayout) findViewById(R.id.rback)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        ((TextView)findViewById(R.id.tvedit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for(Activity a:StaticMethod.stateactivity)
                {
                    a.finish();
                }


                Intent intent=new Intent(SubmissionSummryDetails.this,SubmitYourDetailsActivity.class);

                intent.putExtra("bitmap_passport", temp);
                intent.putExtra("bitmap_photo", temp);
                intent.putExtra("bitmap_support", temp);

                intent.putExtra("bitmap_passport_back", temp);
                intent.putExtra("bitmap_support1", temp);
                intent.putExtra("bitmap_support2", temp);

                intent.putExtra("travelObject",object.toString());
                startActivity(intent);
                finish();

            }
        });

        ((TextView)findViewById(R.id.tvsubmit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


              //  Intent intent=new Intent(SubmissionSummryDetails.this,ThankuSubmitionActivity.class);
             //  startActivity(intent);


                if(StaticMethod.isInternetAvailable(SubmissionSummryDetails.this)) {


                    new GetThings(SubmissionSummryDetails.this, traveleOject).execute();
                }else
                {
                    return;
                }

            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_submission_summry_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





    private class GetThings extends AsyncTask<Void, Void, String[]>
    {

        String mgetThingsResponse="";
        Context context;
        String jsonObject;
        JSONArray jsonarray;
        ProgressDialog pd;
        Boolean flag=true;
        String send_passport_result="";
       String send_backpassport_result="";
        String send_photo_result="";
        String send_support_result="";
        String send_support_result2="";
        String send_support_result3="";


        public GetThings(Context context,String jsonObject) {
            super();
            this.context=context;
            this.jsonObject=jsonObject;

            pd= new ProgressDialog(context).show(context, "", "Loading...");
            pd.show();
        }
        @Override
        protected String[] doInBackground(Void... params) {

            String [] mStrings=null;

            try {

                JSONObject Object=new JSONObject(jsonObject);

                String[] pro_key = new String[]{"id","customer_id","customer_group_id","is_dependent","cp_code","cp_location","hub_location","initial","surname","name","passport_number","date_of_birth","place_of_birth","place_of_issue","date_of_issue","date_of_expiry","meal_preference","mobile_number","alt_mobile_number","email_address","comp_status","created","updated","sex","special_need","customer_group","customer_group_departure","customer_group_arrival","customer_group_description","progress"};
                String[] pro_value = new String[]{Object.getString("id"),Object.getString("customer_id"),Object.getString("customer_group_id"),Object.getString("is_dependent"),Object.getString("cp_code"),Object.getString("cp_location"),v_hub,Object.getString("initial"),v_surname,v_firstname,v_passport,v_dob,Object.getString("place_of_birth"),v_poissuance,v_doi,v_doe,v_mealplan,Object.getString("mobile_number"),Object.getString("alt_mobile_number"),Object.getString("email_address"),Object.getString("comp_status"),Object.getString("created"),Object.getString("updated"),v_sex,v_special,Object.getString("customer_group"),Object.getString("customer_group_departure"),Object.getString("customer_group_arrival"),Object.getString("customer_group_description"),Object.getString("progress")};





                mgetThingsResponse =(new HttpclasswithoutThrade(SubmissionSummryDetails.this,"saveMyTrips", RestClient.RequestMethod.POST,pro_key,pro_value
                )).getResponsedetails();


/*

                if(bitmap_passport!=null)
                {
                    send_passport_result= sendpassport(bitmap_passport,travel_id);
                }


                if(bitmap_passport_back!=null)
                {
                    send_backpassport_result= sendBackpassport(bitmap_passport_back,travel_id);
                }


                if(bitmap_photo!=null)
                {
                    send_photo_result=   sendPhoto(bitmap_photo,travel_id);
                }
                if(bitmap_support!=null)
                {
                    send_support_result=   sendDataSupport(bitmap_support,travel_id);
                }

                if(bitmap_support1!=null)
                {
                    send_support_result2=   sendDataSupport2(bitmap_support1,travel_id);
                }

                if(bitmap_support2!=null)
                {
                    send_support_result3=   sendDataSupport3(bitmap_support2,travel_id);
                }
*/




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

                flag=false;
            }
            return mStrings;
        }
        @Override
        protected void onPostExecute(String[] result) {

            try
            {
                if(pd.isShowing())
                {
                    pd.dismiss();
                }

                JSONObject savedata;
                JSONObject savefilepassport;
                JSONObject savefilephoto;
                JSONObject savefilesupport;

                JSONObject savefilebackpassport;
                JSONObject savefilesupport2;
                JSONObject savefilesupport3;





                if(!mgetThingsResponse.equalsIgnoreCase(""))
                {
                    savedata=new JSONObject(mgetThingsResponse);
                    if(savedata.has("success"))
                    {
                        flag=true;
                    }else
                    {
                        flag=false;
                    }

                }
/*
                System.out.println("ffj");
                if(!send_passport_result.equalsIgnoreCase(""))
                {
                    savefilepassport=new JSONObject(send_passport_result);


                    if(savefilepassport.has("success"))
                    {
                        flag=true;
                    }else
                    {
                        flag=false;
                    }

                }

                if(!send_backpassport_result.equalsIgnoreCase(""))
                {
                    savefilebackpassport=new JSONObject(send_backpassport_result);


                    if(savefilebackpassport.has("success"))
                    {
                        flag=true;
                    }else
                    {
                        flag=false;
                    }

                }



                if(!send_photo_result.equalsIgnoreCase(""))
                {
                    savefilephoto=new JSONObject(send_photo_result);

                    if(savefilephoto.has("success"))
                    {
                        flag=true;
                    }else
                    {
                        flag=false;
                    }


                }
                 if(!send_support_result.equalsIgnoreCase(""))
                 {
                     savefilesupport=new JSONObject(send_support_result);

                     if(savefilesupport.has("success"))
                     {
                           flag=true;
                     }else
                     {
                         flag=false;
                     }


                 }


                if(!send_support_result2.equalsIgnoreCase(""))
                {
                    savefilesupport2=new JSONObject(send_support_result2);

                    if(savefilesupport2.has("success"))
                    {
                        flag=true;
                    }else
                    {
                        flag=false;
                    }


                }



                if(!send_support_result3.equalsIgnoreCase(""))
                {
                    savefilesupport3=new JSONObject(send_support_result3);

                    if(savefilesupport3.has("success"))
                    {
                        flag=true;
                    }else
                    {
                        flag=false;
                    }


                }


*/




                if(flag)
               {
                   Intent intent =new Intent(SubmissionSummryDetails.this,ThankuSubmitionActivity.class);
                   startActivity(intent);

                   for(Activity a:StaticMethod.stateactivity)
                   {
                       a.finish();
                   }


               }else
               {
                   AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                           SubmissionSummryDetails.this);

                   // set title


                   // set dialog message
                   alertDialogBuilder
                           .setMessage("Error please try again!")
                           .setCancelable(false)
                           .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                               public void onClick(DialogInterface dialog,int id) {



                               }
                           });


                   // create alert dialog
                   AlertDialog alertDialog = alertDialogBuilder.create();

                   // show it

                   alertDialog.show();
                   Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                   TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                   msgTxt.setTypeface(font);


               }





            }catch (Exception e)
            {
                System.out.println("Error"+e);
            }



            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
        }
    }


    public String sendpassport(Bitmap bitmap,String traveleid) throws Exception {

       String aa="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
           // Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            byte[] data = bos.toByteArray();


            // sending a String param;
            // entity.addPart("id",new UrlEncodedFormEntity(params, HTTP.UTF_8));
            entity.addPart("id",new StringBody(traveleid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("pass_copy", new ByteArrayBody(data, "pass_copy.jpg"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(   response.getEntity().getContent(), "UTF-8"));
            aa = reader.readLine();

            System.out.println("imageupload"+aa);



        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return aa;

    }



    public String sendBackpassport(Bitmap bitmap,String traveleid) throws Exception {

        String aa="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            // Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            byte[] data = bos.toByteArray();


            // sending a String param;
            // entity.addPart("id",new UrlEncodedFormEntity(params, HTTP.UTF_8));
            entity.addPart("id",new StringBody(traveleid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("pass_copy_back", new ByteArrayBody(data, "pass_copy_back.jpg"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(   response.getEntity().getContent(), "UTF-8"));
            aa = reader.readLine();

            System.out.println("imageupload"+aa);



        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return aa;

    }





    public String sendPhoto(Bitmap bitmap,String travelid) throws Exception {

      String aa="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            byte[] data = bos.toByteArray();


            // sending a String param;
            // entity.addPart("id",new UrlEncodedFormEntity(params, HTTP.UTF_8));
            entity.addPart("id",new StringBody(travelid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("photograph", new ByteArrayBody(data, "photograph.jpg"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(   response.getEntity().getContent(), "UTF-8"));
            aa = reader.readLine();

            System.out.println("imageupload"+aa);



        } catch (Exception e) {

            System.out.println("error"+e);
        }

        return aa;

    }


    public String sendDataSupport(Bitmap bitmap,String travelid) throws Exception {

        String sResponse="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            byte[] data = bos.toByteArray();


            // sending a String param;
            // entity.addPart("id",new UrlEncodedFormEntity(params, HTTP.UTF_8));
            entity.addPart("id",new StringBody(travelid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("supp_doc", new ByteArrayBody(data, "supp_doc.jpg"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(   response.getEntity().getContent(), "UTF-8"));
             sResponse = reader.readLine();

            System.out.println("imageupload"+sResponse);



        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return sResponse;
    }


    public String sendDataSupport2(Bitmap bitmap,String travelid) throws Exception {

        String sResponse="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            byte[] data = bos.toByteArray();


            // sending a String param;
            // entity.addPart("id",new UrlEncodedFormEntity(params, HTTP.UTF_8));
            entity.addPart("id",new StringBody(travelid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("supp_doc_2", new ByteArrayBody(data, "supp_doc_2.jpg"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(   response.getEntity().getContent(), "UTF-8"));
            sResponse = reader.readLine();

            System.out.println("imageupload"+sResponse);



        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return sResponse;
    }

    public String sendDataSupport3(Bitmap bitmap,String travelid) throws Exception {

        String sResponse="";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();

            // here, change it to your php;
            HttpPost httpPost = new HttpPost(AppConfig.APP_URL+"saveTripFiles");

            httpPost.addHeader(AppConfig.HEADER_PRAMETER, AppConfig.HEADER_PRAMETER_VALUE);
            httpPost.addHeader(AppConfig.HEADER_PRAMETER1, AppConfig.HEADER_PRAMETER1_VALUE);

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //Bitmap  bitmap = BitmapFactory.decodeFile(fileNmae);

            // you can change the format of you image compressed for what do you want;
            // now it is set up to 640 x 480;
            Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // CompressFormat set up to JPG, you can change to PNG or whatever you want;
            bmpCompressed.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            byte[] data = bos.toByteArray();


            // sending a String param;
            // entity.addPart("id",new UrlEncodedFormEntity(params, HTTP.UTF_8));
            entity.addPart("id",new StringBody(travelid));


            // sending a Image;0
            // note here, that you can send more than one image, just add another param, same rule to the String;
            entity.addPart("supp_doc_3", new ByteArrayBody(data, "supp_doc_3.jpg"));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(   response.getEntity().getContent(), "UTF-8"));
            sResponse = reader.readLine();

            System.out.println("imageupload"+sResponse);



        } catch (Exception e) {

            System.out.println("error"+e);
        }
        return sResponse;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {

            startActivity(getIntent());
            finish();
        }
    }




}
