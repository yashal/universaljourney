package com.queppelin.universal;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.tool.AppConfig;
import com.tool.HttpClassVersion;
import com.tool.StaticMethod;
import com.viewpager.CirclePageIndicator;
import com.viewpager.ThingsFragmentAdapter;

import org.json.JSONObject;


public class SliderTenTodo extends AppCompatActivity {

    private SharedPreferences mPrefs;
    int blog_id;

    ThingsFragmentAdapter mAdapter;
    ViewPager mPager;
    CirclePageIndicator mIndicator;
    String jsonArray;


    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.gc();
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider_ten_todo);

        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);


        blog_id=getIntent().getExtras().getInt("blog_id");
        jsonArray=getIntent().getExtras().getString("jsonArray");


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(actionBar.getDisplayOptions()
                | ActionBar.DISPLAY_SHOW_CUSTOM);

        LinearLayout layout = new LinearLayout(actionBar.getThemedContext());
        layout.setOrientation(LinearLayout.HORIZONTAL);


        ImageView imageViewlogin = new ImageView(actionBar.getThemedContext());
        imageViewlogin.setScaleType(ImageView.ScaleType.CENTER);
        imageViewlogin.setImageResource(R.drawable.ic_menu_login);




        ImageView imageViewLogout = new ImageView(actionBar.getThemedContext());
        imageViewLogout.setScaleType(ImageView.ScaleType.CENTER);
        imageViewLogout.setImageResource(R.drawable.ic_logout);
        imageViewLogout.setVisibility(View.VISIBLE);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
                | Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin = 10;
        //imageViewLogout.setLayoutParams(layoutParams);


        ImageView imageViewmytrip = new ImageView(actionBar.getThemedContext());
        imageViewmytrip.setScaleType(ImageView.ScaleType.CENTER);
        imageViewmytrip.setImageResource(R.drawable.ic_menu_trips);

        //imageViewmytrip.setLayoutParams(layoutParams);


        imageViewlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), LoginActivity.class);
                intent.putExtra("action_flag","N");
                intent.putExtra("position",0);
                startActivity(intent);

            }
        });



        imageViewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Toast.makeText(getApplicationContext(),"on logout",Toast.LENGTH_LONG).show();

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        SliderTenTodo.this);
                alertDialogBuilder
                        .setMessage("Do you want to logout?")
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                String userValue=mPrefs.getString("userObject","").toString();
                                String user_id;
                                try
                                {
                                    JSONObject object=new JSONObject(userValue);
                                    JSONObject userObject=object.getJSONObject("result");
                                    user_id=userObject.getString("customer_id");//customer_id
                                    String[] pro_key = new String[]{"deviceId","userId","deviceType"};
                                    String[] pro_value = new String[]{mPrefs.getString("deviceId",""),user_id, AppConfig.DEVIECS_TYPE};
                                    new HttpClassVersion(SliderTenTodo.this,"setPushLogoutCustomer", RestClient.RequestMethod.POST,pro_key,pro_value
                                    ).start();
                                    StaticMethod.returnProgressBar(
                                            SliderTenTodo.this).setOnDismissListener(
                                            new DialogInterface.OnDismissListener() {
                                                public void onDismiss(DialogInterface dialog) {
                                                    if (HttpClassVersion.ResponseVector != null && !HttpClassVersion.ResponseVector.equalsIgnoreCase("")) {



                                                        try {

                                                            JSONObject jsonObject=new JSONObject(HttpClassVersion.ResponseVector);

                                                            if(jsonObject.has("success")) {

                                                                boolean type=jsonObject.getBoolean("success");
                                                                if(type) {


                                                                    SharedPreferences.Editor prefsEditor = mPrefs
                                                                .edit();
                                                        prefsEditor
                                                                .putString("userObject","");
                                                        prefsEditor.putString("isLogin", "N");
                                                        prefsEditor.commit();


                                                        for(int i=0;i< StaticMethod.state.size();i++)
                                                        {
                                                            StaticMethod.state.get(i).finish();
                                                        }
                                                        Intent intent=new Intent(getApplicationContext(),NewDashboardActivity.class);
                                                        startActivity(intent);
                                                        finish();

                                                                }else
                                                                {
                                                                    Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                                                                }
                                                            }
                                                        }catch (Exception e)
                                                        {

                                                        }



                                                    }
                                                }
                                            });


                                }catch (Exception e)
                                {

                                }



                            }
                        })
                        .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);

            }
        });



        imageViewmytrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), MyTripsActivity.class);
                intent.putExtra("position",0);
                startActivity(intent);


            }
        });

        layout.setLayoutParams(layoutParams);
       /* layout.addView(imageViewlogin);
        layout.addView(imageViewLogout);
        layout.addView(imageViewmytrip);

*/


        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
        {
            layout.addView(imageViewLogout);
           // layout.addView(imageViewmytrip);

        }else
        {
            layout.addView(imageViewlogin);
        }






        actionBar.setCustomView(layout);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);



        mPager = (ViewPager)findViewById(R.id.pager);

        mAdapter = new ThingsFragmentAdapter(getSupportFragmentManager(),jsonArray,mPager);


        mPager.setAdapter(mAdapter);
        mPager.setCurrentItem(blog_id);
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);



    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //  getMenuInflater().inflate(R.menu.menu_expirence_details, menu);



        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                                       Activity.MODE_PRIVATE);

    if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
    {
        getMenuInflater().inflate(R.menu.after_login, menu);
    }else
    {
        getMenuInflater().inflate(R.menu.before_login, menu);
        menu.findItem(R.id.menu_mytrip).setVisible(false);
    }



    return true;
}*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id==android.R.id.home)
        {
            finish();
        }

        if(id==R.id.menu_logout)
        {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    SliderTenTodo.this);
            alertDialogBuilder
                    .setMessage("Do you want to logout?")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {

                            SharedPreferences.Editor prefsEditor = mPrefs
                                    .edit();
                            prefsEditor
                                    .putString("userObject","");
                            prefsEditor.putString("isLogin", "N");
                            prefsEditor.commit();

                            for(int i=0;i< StaticMethod.state.size();i++)
                            {
                                StaticMethod.state.get(i).finish();
                            }
                            Intent intent=new Intent(getApplicationContext(),NewDashboardActivity.class);
                            startActivity(intent);
                            finish();

                        }
                    })
                    .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }
        if(id==R.id.menu_mytrip)
        {


            intent=new Intent(getApplicationContext(), MyTripsActivity.class);
            intent.putExtra("position",0);
            startActivity(intent);

        }
        if(id==R.id.menu_login)
        {
            intent=new Intent(getApplicationContext(), LoginActivity.class);
            intent.putExtra("action_flag","N");
            intent.putExtra("position",0);
            startActivity(intent);
        }




        return super.onOptionsItemSelected(item);
    }

}
