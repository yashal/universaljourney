package com.queppelin.universal;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import com.google.android.gcm.GCMRegistrar;
import static com.queppelin.universal.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.queppelin.universal.CommonUtilities.EXTRA_MESSAGE;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.image.loader.ImageLoader;
import com.tool.AppConfig;
import com.tool.HttpClass;
import com.tool.HttpClassVersion;
import com.tool.HttpclasswithoutThrade;
import com.tool.HttpclasswithoutThreadVersion;
import com.tool.StaticMethod;
import com.viewpager.CirclePageIndicator;
import com.viewpager.ImageSliderAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;


public class NotificationListActivity extends AppCompatActivity {


    Typeface font;
    ImageLoader imageLoader;
    private SharedPreferences mPrefs;

    ConnectionDetector cd;

    String action_flag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);

        font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
        imageLoader=new ImageLoader(this);




        cd = new ConnectionDetector(getApplicationContext());




        StaticMethod.mLogin_flag="N";
        action_flag=getIntent().getExtras().getString("action_flag");
        ((TextView)findViewById(R.id.tvnodata)).setTypeface(font);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(actionBar.getDisplayOptions()
                | ActionBar.DISPLAY_SHOW_CUSTOM);

        LinearLayout layout = new LinearLayout(actionBar.getThemedContext());
        layout.setOrientation(LinearLayout.HORIZONTAL);

        ImageView imageViewlogin = new ImageView(actionBar.getThemedContext());
        imageViewlogin.setScaleType(ImageView.ScaleType.CENTER);
        imageViewlogin.setImageResource(R.drawable.ic_menu_login);

        ImageView imageViewLogout = new ImageView(actionBar.getThemedContext());
        imageViewLogout.setScaleType(ImageView.ScaleType.CENTER);
        imageViewLogout.setImageResource(R.drawable.ic_logout);
        imageViewLogout.setVisibility(View.VISIBLE);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
                | Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin = 10;
        //imageViewLogout.setLayoutParams(layoutParams);





        ImageView imageViewmytrip = new ImageView(actionBar.getThemedContext());
        imageViewmytrip.setScaleType(ImageView.ScaleType.CENTER);
        imageViewmytrip.setImageResource(R.drawable.ic_menu_trips);

        //imageViewmytrip.setLayoutParams(layoutParams);





        imageViewlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), LoginActivity.class);
                intent.putExtra("action_flag","N");
                intent.putExtra("position",0);
                startActivity(intent);

            }
        });


        ((TextView)findViewById(R.id.tvgotohome)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), NewDashboardActivity.class);
                intent.putExtra("position", 0);
                startActivity(intent);

            }
        });




        ((TextView)findViewById(R.id.tvgotohome)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(NotificationListActivity.this,NewDashboardActivity.class);
                startActivity(intent);

            }
        });





        imageViewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Toast.makeText(getApplicationContext(),"on logout",Toast.LENGTH_LONG).show();




                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        NotificationListActivity.this);
                alertDialogBuilder
                        .setMessage("Do you want to logout?")
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                String userValue=mPrefs.getString("userObject","").toString();
                                String user_id;
                                try
                                {
                                    JSONObject object=new JSONObject(userValue);
                                    JSONObject userObject=object.getJSONObject("result");
                                    user_id=userObject.getString("customer_id");//customer_id
                                    String[] pro_key = new String[]{"deviceId","userId","deviceType"};
                                    String[] pro_value = new String[]{mPrefs.getString("deviceId",""),user_id, AppConfig.DEVIECS_TYPE};
                                    new HttpClassVersion(NotificationListActivity.this,"setPushLogoutCustomer", RestClient.RequestMethod.POST,pro_key,pro_value
                                    ).start();
                                    StaticMethod.returnProgressBar(
                                            NotificationListActivity.this).setOnDismissListener(
                                            new DialogInterface.OnDismissListener() {
                                                public void onDismiss(DialogInterface dialog) {
                                                    if (HttpClassVersion.ResponseVector != null && !HttpClassVersion.ResponseVector.equalsIgnoreCase("")) {



                                                        try {

                                                            JSONObject jsonObject=new JSONObject(HttpClassVersion.ResponseVector);

                                                            if(jsonObject.has("success")) {

                                                                boolean type=jsonObject.getBoolean("success");
                                                                if(type) {


                                                        SharedPreferences.Editor prefsEditor = mPrefs
                                                                .edit();
                                                        prefsEditor
                                                                .putString("userObject","");
                                                        prefsEditor.putString("isLogin", "N");
                                                        prefsEditor.commit();


                                                        for(int i=0;i< StaticMethod.state.size();i++)
                                                        {
                                                            StaticMethod.state.get(i).finish();
                                                        }
                                                        Intent intent=new Intent(getApplicationContext(),NewDashboardActivity.class);
                                                        startActivity(intent);
                                                        finish();

                                                    }else
                                                    {
                                                        Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }catch (Exception e)
                                    {

                                    }




                                                    }
                                                }
                                            });


                                }catch (Exception e)
                                {

                                }



                            }
                        })
                        .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
                TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
                msgTxt.setTypeface(font);



            }
        });



        imageViewmytrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent=new Intent(getApplicationContext(), MyTripsActivity.class);
                intent.putExtra("position",0);
                startActivity(intent);


            }
        });

        layout.setLayoutParams(layoutParams);
       /* layout.addView(imageViewlogin);
        layout.addView(imageViewLogout);
        layout.addView(imageViewmytrip);

*/
        ((RelativeLayout)findViewById(R.id.rnodata)).setVisibility(View.GONE);


        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
        {
            //layout.addView(imageViewLogout);
            //layout.addView(imageViewmytrip);

        }else
        {
            //layout.addView(imageViewlogin);
        }
        actionBar.setCustomView(layout);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("MESSAGES");

        if(StaticMethod.isInternetAvailable(this)) {


            setGetAllMessage();
        }else
        {
            return;
        }


        registerReceiver(mHandleMessageReceiver1, new IntentFilter(
                DISPLAY_MESSAGE_ACTION));


    }




    private class GetBackgroundMessage extends AsyncTask<Void, Void, String[]>
    {

        String mgetThingsResponse;
        Context context;

        JSONArray jsonarray;

        public GetBackgroundMessage(Context context) {
            super();
            this.context=context;


        }
        @Override
        protected String[] doInBackground(Void... params) {

            String [] mStrings=null;

            try {



                mPrefs = context.getSharedPreferences("mUniversalPrefs",
                        Activity.MODE_PRIVATE);
                String[] pro_key = new String[]{"userId","deviceType","isLoggedIn","deviceId"};
                String[] pro_value=null;

                if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
                {

                    String userValue=mPrefs.getString("userObject","").toString();

                    try
                    {
                        JSONObject object=new JSONObject(userValue);
                        JSONObject userObject=object.getJSONObject("result");
                        String  user_id=userObject.getString("customer_id");//customer_id

                        pro_value = new String[]{user_id,"ANDROID","1",mPrefs.getString("deviceId","")};
                    }catch (Exception e)
                    {

                    }
                }else
                {

                    pro_value = new String[]{"0","ANDROID","0",mPrefs.getString("deviceId","")};

                }

                mgetThingsResponse =(new HttpclasswithoutThreadVersion(NotificationListActivity.this,"getAllMessages", RestClient.RequestMethod.POST,pro_key,pro_value
                )).getResponsedetails();



            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return mStrings;
        }
        @Override
        protected void onPostExecute(String[] result) {

            try {

                System.out.println("getAllMessages_Background"+mgetThingsResponse);
                //       ((LinearLayout)findViewById(R.id.alllayoute)).setVisibility(View.VISIBLE);


                ((LinearLayout)findViewById(R.id.allnotification)).removeAllViews();

                if((!mgetThingsResponse.equalsIgnoreCase(""))&&(!mgetThingsResponse.equalsIgnoreCase("null")))
                {

                JSONObject jsonObject=new JSONObject(mgetThingsResponse);
                if(jsonObject.has("result"))
                {
                    Boolean flag = jsonObject.getBoolean("success");
                    if(flag)
                    {

                        try {
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            ((RelativeLayout) findViewById(R.id.rnodata)).setVisibility(View.GONE);

                                        /*((TextView)findViewById(R.id.tvnodata)).setText("No Message Available");
                                        ((TextView)findViewById(R.id.tvnodata)).setTypeface(font);
                                        */
                            ((LinearLayout)findViewById(R.id.allnotification)).removeAllViews();

                           // setLayout(jsonArray);

                            if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
                            {
                                setLayout(jsonArray);
                            }else
                            {





                                JSONArray jsontemp=removePersnalMessage(jsonArray);
                                if(jsontemp.length()>0)
                                {
                                    setLayout(jsontemp);
                                }else {

                                    ((RelativeLayout) findViewById(R.id.rnodata)).setVisibility(View.VISIBLE);
                                    ((TextView)findViewById(R.id.tvnodata)).setText("No Message Available");
                                    ((TextView)findViewById(R.id.tvnodata)).setTypeface(font);
                                }

                            }


                        }catch (Exception e)
                        {
                            ((RelativeLayout) findViewById(R.id.rnodata)).setVisibility(View.VISIBLE);
                            ((TextView)findViewById(R.id.tvnodata)).setText("No Message Available");
                            ((TextView)findViewById(R.id.tvnodata)).setTypeface(font);
                        }
                    }

                }else
                {
                    ((RelativeLayout) findViewById(R.id.rnodata)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.tvnodata)).setText("No Message Available");
                    ((TextView)findViewById(R.id.tvnodata)).setTypeface(font);
                }
//

                }else{


                    ((RelativeLayout) findViewById(R.id.rnodata)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.tvnodata)).setText("No Message Available");
                    ((TextView)findViewById(R.id.tvnodata)).setTypeface(font);




                }



            } catch (Exception e) {
                System.err.println(e);

                ((RelativeLayout)findViewById(R.id.rnodata)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tvnodata)).setText("No Message Available");
                ((TextView)findViewById(R.id.tvnodata)).setTypeface(font);

            }


            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();
        }
    }




    private final BroadcastReceiver mHandleMessageReceiver1 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());
            //System.out.println("check recever()");


            try {


                System.out.println("notification recives");


                if(StaticMethod.isInternetAvailable(NotificationListActivity.this)) {


                    new GetBackgroundMessage(NotificationListActivity.this).execute();

                }else
                {
                    return;
                }
                WakeLocker.release();

            } catch (Exception e) {
                // TODO: handle exception
            }




        }
    };


    @Override
    public void onRestart() {
        super.onRestart();

        //System.out.println("onRestart()");


        if(StaticMethod.isInternetAvailable(this)) {


            setGetAllMessage();
        }else
        {
            return;
        }

    }


    public void setGetAllMessage()
    {

        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);
        String[] pro_key = new String[]{"userId","deviceType","isLoggedIn","deviceId"};
        String[] pro_value=null;

        if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
        {

            String userValue=mPrefs.getString("userObject","").toString();

            try
            {
                JSONObject object=new JSONObject(userValue);
                JSONObject userObject=object.getJSONObject("result");
                String  user_id=userObject.getString("customer_id");//customer_id

                pro_value = new String[]{user_id,"ANDROID","1",mPrefs.getString("deviceId","")};
            }catch (Exception e)
            {

            }
        }else
        {

            pro_value = new String[]{"0","ANDROID","0",mPrefs.getString("deviceId","")};

        }

        new HttpClassVersion(NotificationListActivity.this,"getAllMessages", RestClient.RequestMethod.POST,pro_key,pro_value
        ).start();
        StaticMethod.returnProgressBar(
                NotificationListActivity.this).setOnDismissListener(
                new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        if (HttpClass.ResponseVector != null && !HttpClass.ResponseVector.equalsIgnoreCase("")) {

                            // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                            // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                            //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                            try {

                                System.out.println("getAllMessages" + HttpClassVersion.ResponseVector);
                                //       ((LinearLayout)findViewById(R.id.alllayoute)).setVisibility(View.VISIBLE);

                                String output_url = HttpClassVersion.ResponseVector;

                                if ((!output_url.equalsIgnoreCase("")) && (!output_url.equalsIgnoreCase("null"))) {


                                JSONObject jsonObject = new JSONObject(HttpClassVersion.ResponseVector);

                                ((LinearLayout) findViewById(R.id.allnotification)).removeAllViews();

                                if (jsonObject.has("result")) {
                                    Boolean flag = jsonObject.getBoolean("success");
                                    if (flag) {

                                        try {
                                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                                            ((RelativeLayout) findViewById(R.id.rnodata)).setVisibility(View.GONE);

                                        /*((TextView)findViewById(R.id.tvnodata)).setText("No Message Available");
                                        ((TextView)findViewById(R.id.tvnodata)).setTypeface(font);
                                        */
                                            ((LinearLayout) findViewById(R.id.allnotification)).removeAllViews();

                                            if (mPrefs.getString("isLogin", "").equalsIgnoreCase("Y")) {
                                                setLayout(jsonArray);
                                            } else {

                                                JSONArray jsontemp = removePersnalMessage(jsonArray);
                                                if (jsontemp.length() > 0) {
                                                    setLayout(jsontemp);
                                                } else {

                                                    ((RelativeLayout) findViewById(R.id.rnodata)).setVisibility(View.VISIBLE);
                                                    ((TextView) findViewById(R.id.tvnodata)).setText("No Message Available");
                                                    ((TextView) findViewById(R.id.tvnodata)).setTypeface(font);
                                                }


                                            }

                                        } catch (Exception e) {
                                            ((RelativeLayout) findViewById(R.id.rnodata)).setVisibility(View.VISIBLE);
                                            ((TextView) findViewById(R.id.tvnodata)).setText("No Message Available");
                                            ((TextView) findViewById(R.id.tvnodata)).setTypeface(font);
                                        }
                                    }

                                } else {
                                    ((RelativeLayout) findViewById(R.id.rnodata)).setVisibility(View.VISIBLE);
                                    ((TextView) findViewById(R.id.tvnodata)).setText("No Message Available");
                                    ((TextView) findViewById(R.id.tvnodata)).setTypeface(font);
                                }
//


                            }else
                            {


                                ((RelativeLayout) findViewById(R.id.rnodata)).setVisibility(View.VISIBLE);
                                ((TextView) findViewById(R.id.tvnodata)).setText("No Message Available");
                                ((TextView) findViewById(R.id.tvnodata)).setTypeface(font);


                            }





                            } catch (Exception e) {
                                System.err.println(e);

                                ((RelativeLayout)findViewById(R.id.rnodata)).setVisibility(View.VISIBLE);
                                ((TextView)findViewById(R.id.tvnodata)).setText("No Message Available");
                                ((TextView)findViewById(R.id.tvnodata)).setTypeface(font);

                            }
                        }
                    }
                });



    }





    public void setLayout(final JSONArray jsonArray)
    {
        LinearLayout notification_Layout=null;
        notification_Layout=(LinearLayout)findViewById(R.id.allnotification);
        notification_Layout.removeAllViews();
        View notification;
        try
        {

            for(int i=0;i<jsonArray.length();i++)
            {

                String action_type=jsonArray.getJSONObject(i).getString("action");

                String read_status=jsonArray.getJSONObject(i).getString("is_read");

                if(action_type.equalsIgnoreCase("MYTRIP")||action_type.equalsIgnoreCase("LOGIN"))
                {

                    notification=getLayoutInflater().inflate(R.layout.notification_row_p, null);
                    String type=jsonArray.getJSONObject(i).getString("action");

                    if(type.equalsIgnoreCase("LOGIN"))
                    {

                      //  ((TextView)notification.findViewById(R.id.tvread)).setText("LOGIN");
                        ((TextView)notification.findViewById(R.id.tvexp)).setText(jsonArray.getJSONObject(i).getString("cta"));
                    }else if(type.equalsIgnoreCase("MYTRIP"))
                    {

                      //  ((TextView)notification.findViewById(R.id.tvread)).setText("MYTRIPS");
                        ((TextView)notification.findViewById(R.id.tvexp)).setText(jsonArray.getJSONObject(i).getString("cta"));
                    }
                    ((TextView)notification.findViewById(R.id.tvexp)).setTypeface(font);
                    LinearLayout aa=(LinearLayout)notification.findViewById(R.id.lnall);
                    if(read_status.equalsIgnoreCase("0"))
                    {

                       // anll.setBackgroundColor(Color.parseColor("#00000000"));

                        aa.setBackgroundResource(R.drawable.rect_white);

                    }else if(read_status.equalsIgnoreCase("1"))
                    {

                      //  anll.setBackgroundColor(Color.parseColor("#e7e7e7"));
                        aa.setBackgroundResource(R.drawable.read_rectgle);

                    }




                    String img_path=jsonArray.getJSONObject(i).getString("image");
                    if(img_path.equalsIgnoreCase("")||img_path.equalsIgnoreCase("null"))
                    {
                        ((ImageView)notification.findViewById(R.id.imgnotification)).getLayoutParams().height = 0;
                        ((ImageView)notification.findViewById(R.id.imgnotification)).requestLayout();

                    }else
                    {


                        imageLoader.DisplayImage(jsonArray.getJSONObject(i).getString("image").replaceAll(" ", "%20"),((ImageView)notification.findViewById(R.id.imgnotification)));
                    }

                    ((TextView)notification.findViewById(R.id.tvdescription)).setText(Html.fromHtml(jsonArray.getJSONObject(i).getString("msg")));

                }else if(action_type.equalsIgnoreCase("PACKAGE"))
                {

                     notification=getLayoutInflater().inflate(R.layout.notification_row_p, null);
//jsonArray.getJSONObject(i).getString("image")
                  //  imageLoader.DisplayImage(jsonArray.getJSONObject(i).getString("image"),((ImageView)notification.findViewById(R.id.imgnotification)));
                    ((TextView)notification.findViewById(R.id.tvexp)).setText(jsonArray.getJSONObject(i).getString("cta"));

                    //((TextView)notification.findViewById(R.id.tvexp)).setText("EXPLORE");
                    ((TextView)notification.findViewById(R.id.tvexp)).setTypeface(font);



                    LinearLayout aa=(LinearLayout)notification.findViewById(R.id.lnall);

                    if(read_status.equalsIgnoreCase("0"))
                    {

                        // anll.setBackgroundColor(Color.parseColor("#00000000"));



                    }else if(read_status.equalsIgnoreCase("1"))
                    {

                        //  anll.setBackgroundColor(Color.parseColor("#e7e7e7"));
                        aa.setBackgroundResource(R.drawable.read_rectgle);

                    }


                    String img_path=jsonArray.getJSONObject(i).getString("image");
                    if(img_path.equalsIgnoreCase("")||img_path.equalsIgnoreCase("null"))
                    {
                        ((ImageView)notification.findViewById(R.id.imgnotification)).getLayoutParams().height = 0;
                        ((ImageView)notification.findViewById(R.id.imgnotification)).requestLayout();

                    }else
                    {


                        imageLoader.DisplayImage(jsonArray.getJSONObject(i).getString("image").replaceAll(" ", "%20"),((ImageView)notification.findViewById(R.id.imgnotification)));
                    }


                    ((TextView)notification.findViewById(R.id.tvdescription)).setText(Html.fromHtml(jsonArray.getJSONObject(i).getString("msg")));

                }else if (action_type.equalsIgnoreCase("NOACTION"))

                {

                    notification=getLayoutInflater().inflate(R.layout.notification_row_p, null);


                    ((TextView)notification.findViewById(R.id.tvexp)).setText(jsonArray.getJSONObject(i).getString("cta"));
                    ((TextView)notification.findViewById(R.id.tvexp)).setTypeface(font);

                    LinearLayout aa=(LinearLayout)notification.findViewById(R.id.lnall);

                    String img_path=jsonArray.getJSONObject(i).getString("image");
                    if(img_path.equalsIgnoreCase("")||img_path.equalsIgnoreCase("null"))
                    {
                        ((ImageView)notification.findViewById(R.id.imgnotification)).getLayoutParams().height = 0;
                        ((ImageView)notification.findViewById(R.id.imgnotification)).requestLayout();

                    }else
                    {


                        imageLoader.DisplayImage(jsonArray.getJSONObject(i).getString("image").replaceAll(" ", "%20"),((ImageView)notification.findViewById(R.id.imgnotification)));
                    }


                    if(read_status.equalsIgnoreCase("0"))
                    {

                        // anll.setBackgroundColor(Color.parseColor("#00000000"));

                        aa.setBackgroundResource(R.drawable.rect_white);

                    }else if(read_status.equalsIgnoreCase("1"))
                    {

                        //  anll.setBackgroundColor(Color.parseColor("#e7e7e7"));
                        aa.setBackgroundResource(R.drawable.read_rectgle);

                    }


                    String shot_mess=jsonArray.getJSONObject(i).getString("msg");




                    String message1="";

                    int messageChar=shot_mess.length();
                    if(messageChar>120)
                    {
                        message1=shot_mess.substring(0, 120);

                    }else
                    {
                        message1=shot_mess;

                    }



                    ((TextView)notification.findViewById(R.id.tvdescription)).setText(Html.fromHtml(message1));

                }
                else
                {

                    notification=getLayoutInflater().inflate(R.layout.notification_row_p, null);

                    LinearLayout aa=(LinearLayout)notification.findViewById(R.id.lnall);

                    if(read_status.equalsIgnoreCase("0"))
                    {

                        // anll.setBackgroundColor(Color.parseColor("#00000000"));
                        aa.setBackgroundResource(R.drawable.rect_white);

                    }else if(read_status.equalsIgnoreCase("1"))
                    {
                        //  anll.setBackgroundColor(Color.parseColor("#e7e7e7"));
                        aa.setBackgroundResource(R.drawable.read_rectgle);
                    }

                    String img_path=jsonArray.getJSONObject(i).getString("image");
                    if(img_path.equalsIgnoreCase("")||img_path.equalsIgnoreCase("null"))
                    {
                        ((ImageView)notification.findViewById(R.id.imgnotification)).getLayoutParams().height = 0;
                        ((ImageView)notification.findViewById(R.id.imgnotification)).requestLayout();

                    }else
                    {


                        imageLoader.DisplayImage(jsonArray.getJSONObject(i).getString("image").replaceAll(" ", "%20"),((ImageView)notification.findViewById(R.id.imgnotification)));
                    }

                    ((TextView)notification.findViewById(R.id.tvdescription)).setText(Html.fromHtml(jsonArray.getJSONObject(i).getString("msg")));

                }

                imageLoader.DisplayImage((jsonArray.getJSONObject(i).getString("image")).replaceAll(" ", "%20"),((ImageView)notification.findViewById(R.id.imgnotification)));

                ((TextView)notification.findViewById(R.id.tvtitle)).setText(Html.fromHtml(jsonArray.getJSONObject(i).getString("msg_title")));


                ((TextView)notification.findViewById(R.id.tvtitle)).setTypeface(font);
                ((TextView)notification.findViewById(R.id.tvdescription)).setTypeface(font);
                ((TextView)notification.findViewById(R.id.tvtime)).setText(getTimeDifrence(jsonArray.getJSONObject(i).getString("created_on")));
                ((TextView)notification.findViewById(R.id.tvtime)).setTypeface(font);




                notification.setId(i);
                notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        try
                        {

                            String type=jsonArray.getJSONObject(view.getId()).getString("action");
                            if(StaticMethod.isInternetAvailable(NotificationListActivity.this)) {
                                new SetReadThread(NotificationListActivity.this, jsonArray.getJSONObject(view.getId()).getString("msgid"), jsonArray.getJSONObject(view.getId()) + "").execute();
                            }else
                            {
                                return;
                            }


                            Intent intent;

                            if(type.equalsIgnoreCase("PACKAGE"))
                            {

                                intent=new Intent(NotificationListActivity.this,ExpirenceDetailsActivity.class);
                                intent.putExtra("product_id",jsonArray.getJSONObject(view.getId()).getString("product_id"));
                                intent.putExtra("action_flag","N");
                                startActivity(intent);

                            }else if(type.equalsIgnoreCase("MYTRIP"))
                            {

                                intent=new Intent(NotificationListActivity.this,MyTripsActivity.class);
                                intent.putExtra("customer_group_id",jsonArray.getJSONObject(view.getId()).getString("customer_group_id"));
                                intent.putExtra("action_flag","N");
                                startActivity(intent);

                            }else if(type.equalsIgnoreCase("LOGIN"))
                            {

                                StaticMethod.mLogin_flag="Y";

                                intent=new Intent(NotificationListActivity.this,LoginActivity.class);
                                intent.putExtra("action_flag","Y");
                                //intent.putExtra("product_id",jsonArray.getJSONObject(view.getId()).getString("product_id"));
                                startActivity(intent);

                            }else if(type.equalsIgnoreCase("NOACTION"))
                            {

                                intent=new Intent(NotificationListActivity.this,ReadMessageActivity.class);
                                intent.putExtra("action_flag","N");
                                intent.putExtra("message_object",jsonArray.getJSONObject(view.getId())+"");
                                startActivity(intent);

                            }

                            //Toast.makeText(getApplicationContext(),"OnCLick"+type,Toast.LENGTH_LONG).show();

                        }catch (Exception e)
                        {

                        }


                    }
                });






                LinearLayout.LayoutParams llpline = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 10);
                View line=new View(this);
                llpline.setMargins(0, 0, 0, 0);
                line.setLayoutParams(llpline);
                line.setBackgroundColor(Color.parseColor("#00000000"));//add sperater
                if(i==0)
                {

                }else
                {
                    notification_Layout.addView(line);
                }




                notification_Layout.addView(notification);


            }



        }catch (Exception e)
        {


           System.out.println("getAllError"+e);

        }




    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }


        if(id==android.R.id.home)
        {
            if(action_flag.equalsIgnoreCase("Y"))
            {
                Intent intent=new Intent(NotificationListActivity.this,NewDashboardActivity.class);
                startActivity(intent);
                finish();

            }else
            {
                finish();
            }

        }


        return super.onOptionsItemSelected(item);
    }



    public String getTimeDifrence(String dateStart) {
        String diffTime = "";
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

       String dateStop=format.format(new Date());



            Date d1 = null;
            Date d2 = null;
            try {
                d1 = format.parse(dateStart);
                d2 = format.parse(dateStop);
                long diff = d2.getTime() - d1.getTime();
                long diffSeconds = diff / 1000 % 60;
                long diffMinutes = diff / (60 * 1000) % 60;
                long diffHours = diff / (60 * 60 * 1000) % 24;
                long diffDays = diff / (24 * 60 * 60 * 1000);
                if (diffDays > 0) {
                    diffTime = diffDays + " days ago";
                } else {
                    if (diffHours > 0) {
                        diffTime = diffHours + " hrs ago";
                    } else {

                        if (diffMinutes > 0) {
                            diffTime = diffMinutes + " mins ago";
                        } else {
                            if (diffSeconds > 0) {
                                diffTime = diffSeconds + " secs ago";
                            } else {

                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return diffTime;
    }



    public JSONArray removePersnalMessage(JSONArray orignal) throws Exception
    {
        JSONArray mJwithaddarray=new JSONArray();
        JSONObject object=new JSONObject();
        int count=getPrsnalCount(orignal);





        if(count>0)
        {

            int count_unread=getUnreadMessageCountText(orignal);

            if(count_unread>0) {


                object.put("user_id", "");
                object.put("is_logged_in", "");
                object.put("os_type", "");
                object.put("is_active", "");
                object.put("msgid", "");
                object.put("is_read", "0");
                object.put("created_on", "");
                object.put("msg_title", "Please login");
                object.put("msg", "login to see your personal notifications");
                object.put("image", "");
                object.put("action", "LOGIN");
                object.put("customer_group_id", "");
                object.put("product_id", "");
                object.put("cta", "LOGIN");
                mJwithaddarray.put(object);
            }

        }

        for(int i=0;i<orignal.length();i++)
        {

            if(orignal.getJSONObject(i).getString("user_id").equalsIgnoreCase("0"))
            {
                mJwithaddarray.put(orignal.getJSONObject(i));
            }



        }





        return  mJwithaddarray;
    }






    public  int getPrsnalCount(JSONArray jsonArray)
    {
        int count = 0;
        try {

            for(int i=0;i<jsonArray.length();i++)
            {
                if(!(jsonArray.getJSONObject(i).getString("user_id").equalsIgnoreCase("0")))
                {
                    count++;
                }

            }

            return count;
        }catch (Exception e)
        {
            return count;
        }
    }




    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(mHandleMessageReceiver1);
            GCMRegistrar.onDestroy(this);
        } catch (Exception e) {
            Log.e("UnRegister Recei Error", "> " + e.getMessage());
        }
        super.onDestroy();
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(action_flag.equalsIgnoreCase("Y"))
        {
            Intent intent=new Intent(NotificationListActivity.this,NewDashboardActivity.class);
            startActivity(intent);
            finish();

        }else
        {
            super.onBackPressed();
        }


    }



    public  int getUnreadMessageCountText(JSONArray jsonArray)
    {
        int count = 0;
        try {

            for(int i=0;i<jsonArray.length();i++)
            {
                if((jsonArray.getJSONObject(i).getString("is_read").equalsIgnoreCase("0"))&&(!jsonArray.getJSONObject(i).getString("user_id").equalsIgnoreCase("0")))
                {
                    count++;
                }

            }

            return count;
        }catch (Exception e)
        {
            return count;
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {

            startActivity(getIntent());
            finish();
        }
    }





}
