package com.queppelin.universal;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;


import com.image.loader.ImageLoader;
import com.tool.HttpClass;
import com.tool.StaticMethod;

import org.json.JSONObject;


public class ThingsDetailsActivity extends AppCompatActivity {

    String blog_id;

    private SharedPreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_things_details);
        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        blog_id=getIntent().getExtras().getString("blog_id");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        String[] pro_key = new String[]{"blog_id"};
        String[] pro_value = new String[]{blog_id};
        new HttpClass(ThingsDetailsActivity.this,"getBlog", RestClient.RequestMethod.POST,pro_key,pro_value
        ).start();
        StaticMethod.returnProgressBar(
                ThingsDetailsActivity.this).setOnDismissListener(
                new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        if (HttpClass.ResponseVector != null && !HttpClass.ResponseVector.equalsIgnoreCase("")) {

                            // ((LinearLayout)findViewById(R.id.lalllayout)).setVisibility(View.VISIBLE);
                            // (findViewById(R.id.lspinner)).setVisibility(View.VISIBLE);
                            //(findViewById(R.id.line)).setVisibility(View.VISIBLE);
                            try {

                                System.out.println("getBlog"+HttpClass.ResponseVector);

                                JSONObject jsonObject=new JSONObject(HttpClass.ResponseVector);
                                setLayout(jsonObject);

                                // JSONObject jsonRecmandation=jsonObject.getJSONObject("recommended");
                             //   JSONArray jsonArray=jsonObject.getJSONArray("result");

                                //  imageLoader.DisplayImage(jsonRecmandation.getString("image"),((ImageView)findViewById(R.id.imgmorocco)));











                            } catch (Exception e) {
                                System.err.println(e);
                            }


                        }
                    }
                });












    }

    public void setLayout(JSONObject jsonObject)
    {

        ImageLoader imageLoader=new ImageLoader(ThingsDetailsActivity.this);

        try
        {

            ((TextView)findViewById(R.id.tv_thing_title)).setText(Html.fromHtml(jsonObject.getString("title")));

            ((TextView)findViewById(R.id.tv_dec)).setText(Html.fromHtml(jsonObject.getString("description")));

            imageLoader.DisplayImage(jsonObject.getString("image"),((ImageView)findViewById(R.id.img_banner)));


        }catch (Exception e)
        {

        }



    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //  getMenuInflater().inflate(R.menu.menu_expirence_details, menu);



        mPrefs = this.getSharedPreferences("mUniversalPrefs",
                Activity.MODE_PRIVATE);

        if(mPrefs.getString("isLogin","").equalsIgnoreCase("Y"))
        {
            getMenuInflater().inflate(R.menu.after_login, menu);
        }else
        {
            getMenuInflater().inflate(R.menu.before_login, menu);
            menu.findItem(R.id.menu_mytrip).setVisible(false);
        }



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id==android.R.id.home)
        {
            finish();
        }

        if(id==R.id.menu_logout)
        {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    ThingsDetailsActivity.this);
            alertDialogBuilder
                    .setMessage("Do you want to logout?")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {

                            SharedPreferences.Editor prefsEditor = mPrefs
                                    .edit();
                            prefsEditor
                                    .putString("userObject","");
                            prefsEditor.putString("isLogin", "N");
                            prefsEditor.commit();

                            for(int i=0;i< StaticMethod.state.size();i++)
                            {
                                StaticMethod.state.get(i).finish();
                            }
                            Intent intent=new Intent(getApplicationContext(),NewDashboardActivity.class);
                            startActivity(intent);
                            finish();

                        }
                    })
                    .setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito_Light.ttf");
            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTypeface(font);
        }
        if(id==R.id.menu_mytrip)
        {


            intent=new Intent(getApplicationContext(), MyTripsActivity.class);
            intent.putExtra("position",0);
            startActivity(intent);

        }
        if(id==R.id.menu_login)
        {
            intent=new Intent(getApplicationContext(), LoginActivity.class);
            intent.putExtra("position",0);
            intent.putExtra("action_flag","N");
            startActivity(intent);
        }




        return super.onOptionsItemSelected(item);
    }

}
